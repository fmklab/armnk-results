package com.fmklab.armnk_results.framework.data

import android.os.AsyncTask
import com.fmklab.armnk_result.data.NoneNumberDetailDataSource
import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectMoment
import com.fmklab.armnk_result.domain.Detector
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_results.BuildConfig
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.InsertNoneNumberDetailRequest
import com.fmklab.armnk_results.framework.NetworkBoundResult
import com.fmklab.armnk_results.framework.preferences.PreferencesDatabase
import com.fmklab.armnk_results.framework.retrofit.*
import com.fmklab.armnk_results.framework.room.ArmnkDatabase
import com.fmklab.armnk_results.framework.room.ControlZoneEntity
import com.fmklab.armnk_results.framework.room.DetectorEntity
import com.fmklab.armnk_results.framework.utils.SystemUtil
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class NoneNumberDetailDataSourceImpl(
    private val noneNumberDetailApi: NoneNumberDetailApi,
    private val armnkDatabase: ArmnkDatabase,
    private val preferencesDatabase: PreferencesDatabase,
    private val currentUser: CurrentUser
) :
    NoneNumberDetailDataSource {

    private val noneNumberDetailDao = armnkDatabase.noneNumberDetailDao()

    companion object {
        private const val CACHE_TTL = 3600 // seconds
        private const val NONE_NUMBER_DETAIL_KEY = "NoneNumberDetailKey"
    }

    init {

    }

    override fun getAllPaged(
        partId: Int,
        page: Int
    ): Observable<List<NoneNumberDetail>> {
        return noneNumberDetailApi.getAllPaged(
            currentUser.getUser().company.id,
            partId,
            page,
            currentUser.getAccessToken()
        )
            .map { list ->
                list.map {
                    NoneNumberDetail(it.id, it.partId)
                        .controlDate(it.controlDate)
                        .method(it.method)
                        .controlZone(
                            ControlZone(
                                it.controlZoneId,
                                it.controlZoneName,
                                it.controlZoneMethodStr
                            )
                        )
                        .detector(Detector(0, it.detectorName, "", it.method))
                        .allChecked(it.allChecked)
                        .defecationChecked(it.defecationChecked)
                        .visualChecked(it.visualChecked)
                        .ndtChecked(it.ndtChecked)
                        .spec(it.spec)
                        .editStatus(it.editStatus)
                        .createDate(it.createDate)
                        .isMobileApp(it.isMobileApp)
                }
            }
            .subscribeOn(Schedulers.io())
    }

    override fun getById(id: Int): Observable<NoneNumberDetail> {
        return object : NetworkBoundResult<NoneNumberDetailDto, NoneNumberDetailDto>() {

            override fun saveCallResult(item: NoneNumberDetailDto): Single<List<Long>> {
                return Single.just(item)
                    .flatMap {
                        preferencesDatabase.insert(it, NONE_NUMBER_DETAIL_KEY)
                        Single.just(List(1) { 1L })
                    }
            }

            override fun createCall(): Observable<NoneNumberDetailDto> {
                return noneNumberDetailApi.getById(id, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: NoneNumberDetailDto): Boolean {
                return item == NoneNumberDetailDto.DEFAULT
            }

            override fun loadFromDb(): Observable<NoneNumberDetailDto> {
                return Observable.just(
                    preferencesDatabase.get(
                        NONE_NUMBER_DETAIL_KEY,
                        NoneNumberDetailDto::class.java,
                        NoneNumberDetailDto.DEFAULT
                    )
                )
            }

        }.asObservable().map {
            NoneNumberDetail(it.id, it.partId)
                .controlDate(it.controlDate)
                .method(it.method)
                .controlZone(
                    ControlZone(
                        it.controlZoneId,
                        it.controlZoneName,
                        it.controlZoneMethodStr
                    )
                )
                .detector(Detector(0, it.detectorName, "", it.method))
                .allChecked(it.allChecked)
                .defecationChecked(it.defecationChecked)
                .visualChecked(it.visualChecked)
                .ndtChecked(it.ndtChecked)
                .spec(currentUser.getUser().toString())
                .editStatus(it.editStatus)
                .createDate(it.createDate)
                .isMobileApp(it.isMobileApp)
        }
    }

    override fun cacheDetail(noneNumberDetail: NoneNumberDetail) {
        preferencesDatabase.insert(
            NoneNumberDetailDto(
                noneNumberDetail.id,
                noneNumberDetail.partId,
                noneNumberDetail.controlDate,
                noneNumberDetail.method,
                noneNumberDetail.controlZone?.id ?: 0,
                noneNumberDetail.controlZone?.name ?: "",
                noneNumberDetail.controlZone?.methodStr,
                noneNumberDetail.detector?.toString() ?: "",
                noneNumberDetail.allChecked,
                noneNumberDetail.defecationChecked,
                noneNumberDetail.visualChecked,
                noneNumberDetail.ndtChecked,
                noneNumberDetail.spec,
                noneNumberDetail.editStatus,
                noneNumberDetail.createDate,
                noneNumberDetail.isMobileApp
            ), NONE_NUMBER_DETAIL_KEY
        )
    }

    override fun getControlZonesByMethod(partId: Int, method: String): Observable<List<ControlZone>> {
        return object : NetworkBoundResult<List<ControlZoneEntity>, List<ControlZoneDto>>() {
            override fun saveCallResult(item: List<ControlZoneDto>): Single<List<Long>> {
                noneNumberDetailDao.deleteControlZones(partId, method)
                return noneNumberDetailDao.insertControlZones(item.map {
                    ControlZoneEntity(
                        0, it.id, partId, DefectMoment.NONE.value, false, it.name, method,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<ControlZoneDto>> {
                return noneNumberDetailApi.getControlZones(
                    partId,
                    method,
                    currentUser.getAccessToken()
                )
            }

            override fun shouldFetch(item: List<ControlZoneEntity>): Boolean {
                val currentTime = (System.currentTimeMillis() / 1000).toInt()
                if (item.isEmpty()) return true
                return item.any { cz -> currentTime - cz.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<ControlZoneEntity>> {
                return noneNumberDetailDao.getControlZones(partId, method)
            }

        }.asObservable().map { list ->
            list.map {
                ControlZone(it.id, it.name, it.methodStr)
            }
        }
    }

    override fun insert(noneNumberDetail: NoneNumberDetail): Observable<Int> {
        val user = currentUser.getUser()
        return noneNumberDetailApi.insert(
            InsertNoneNumberDetailRequest(
                NoneNumberDetailDto(
                    noneNumberDetail.id,
                    noneNumberDetail.partId,
                    noneNumberDetail.controlDate,
                    noneNumberDetail.method,
                    noneNumberDetail.controlZone!!.id,
                    noneNumberDetail.controlZone!!.name,
                    noneNumberDetail.controlZone!!.methodStr,
                    noneNumberDetail.detector!!.toString(),
                    noneNumberDetail.allChecked,
                    noneNumberDetail.defecationChecked,
                    noneNumberDetail.visualChecked,
                    noneNumberDetail.ndtChecked,
                    noneNumberDetail.spec,
                    noneNumberDetail.editStatus,
                    noneNumberDetail.createDate,
                    noneNumberDetail.isMobileApp
                ),
                UserDto(
                    user.id,
                    user.name1,
                    user.name2,
                    user.name3,
                    user.company.id,
                    user.company.name
                ),
                BuildConfig.VERSION_CODE
            ), currentUser.getAccessToken()
        ).subscribeOn(Schedulers.io())
    }

    override fun clearPreferencesCache() {
        preferencesDatabase.remove(NONE_NUMBER_DETAIL_KEY)
    }

    override fun clearRequestCache() {
//        AsyncTask.execute {
//            noneNumberDetailDao.deleteAllControlZones()
//            noneNumberDetailDao.deleteAllDetectors()
//        }
    }
}







































