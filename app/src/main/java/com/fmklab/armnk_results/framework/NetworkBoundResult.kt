
package com.fmklab.armnk_results.framework

import com.fmklab.armnk_results.framework.room.ArmnkDatabase
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

abstract class NetworkBoundResult<ResultType, RequestType> {

    private var result: Observable<ResultType>

    init {

        result = loadFromDb()
            .flatMap { data ->
                if (shouldFetch(data)) {
                    createCall()
                        .flatMap { remoteData ->
                            saveCallResult(remoteData)
                                .toObservable()
                                .flatMap { loadFromDb() }
                        }
                } else {
                    Observable.just(data)
                }
            }
            .subscribeOn(Schedulers.io())
    }

    fun asObservable(): Observable<ResultType> = result

    protected abstract fun saveCallResult(item: RequestType): Single<List<Long>>

    protected abstract fun createCall(): Observable<RequestType>

    protected abstract fun shouldFetch(item: ResultType): Boolean

    protected abstract fun loadFromDb(): Observable<ResultType>
}
