package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_result.interactors.detail.*
import com.fmklab.armnk_result.interactors.numberdetail.*

data class BogieNumberDetailInteractors(
    val getAllBogiePaged: GetAllBogiePaged,
    val searchBogieNumberDetail: SearchBogieNumberDetail,
    val getBogieById: GetBogieById,
    val getBogieModels: GetBogieModels,
    val cacheBogieDetail: CacheBogieDetail,
    val clearBogiePreferenceCache: ClearBogiePreferenceCache,
    val getBogieModelFactories: GetBogieModelFactories,
    val getYears: GetYears,
    val getControlResults: GetControlResults,
    val getDefectTypes: GetDefectTypes,
    val getBogieControlZones: GetBogieControlZones,
    val getControlZones: GetControlZones,
    val getSteals: GetSteals,
    val getRepairDefectTypes: GetRepairDefectTypes,
    val getRepairControlZones: GetRepairControlZones,
    val getMethods: GetMethods,
    val getDetectors: GetDetectors,
    val checkAlreadyAddedDetail: CheckAlreadyAddedDetail,
    val addBogieNumberDetail: AddBogieNumberDetail
)
