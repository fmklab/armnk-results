package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


data class Token (
    @SerializedName("accessToken")
    @Expose
    val accessToken: String,
    @SerializedName("tokenExpire")
    @Expose
    val tokenExpire: Date,
    @SerializedName("lifeTime")
    @Expose
    val lifeTime: Long,
    @SerializedName("refreshToken")
    @Expose
    val refreshToken: String
)