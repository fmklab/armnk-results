package com.fmklab.armnk_results.framework.utils

import com.fmklab.armnk_result.domain.DefectMoment
import com.fmklab.armnk_result.domain.NumberDetailDefectMoment

class DetailUtil {

    companion object {
        fun defectMoment(value: Int): DefectMoment {
            return when (value) {
                DefectMoment.VISUAL.value -> DefectMoment.VISUAL
                DefectMoment.NDT.value -> DefectMoment.NDT
                DefectMoment.DEFECATION.value -> DefectMoment.DEFECATION
                else -> DefectMoment.NONE
            }
        }

        fun numberDetailDefectMoment(value: Int): NumberDetailDefectMoment {
            return when (value) {
                NumberDetailDefectMoment.VISUAL.value -> NumberDetailDefectMoment.VISUAL
                NumberDetailDefectMoment.NDT.value -> NumberDetailDefectMoment.NDT
                NumberDetailDefectMoment.DEFECATION.value -> NumberDetailDefectMoment.DEFECATION
                else -> NumberDetailDefectMoment.NONE
            }
        }
    }
}