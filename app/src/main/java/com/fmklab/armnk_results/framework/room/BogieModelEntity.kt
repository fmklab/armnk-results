package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bogie_models")
data class BogieModelEntity(
    @PrimaryKey
    val id: Int = 0,
    @ColumnInfo(name = "part_id")
    val partId: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "order_field")
    val orderField: Int,
    @ColumnInfo(name = "timestamp")
    val timestamp: Int
)