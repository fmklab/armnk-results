package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_results.framework.LoginParams
import com.fmklab.armnk_results.framework.retrofit.LoginApi
import com.fmklab.armnk_results.framework.retrofit.Session
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class Login(private val loginApi: LoginApi) {

    operator fun invoke(loginParams: LoginParams): Observable<Session> {
        return loginApi.getToken(loginParams).subscribeOn(Schedulers.io())
    }
}



























