package com.fmklab.armnk_results.framework

import com.fmklab.armnk_results.framework.retrofit.NoneNumberDetailDto
import com.fmklab.armnk_results.framework.retrofit.UserDto
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class InsertNoneNumberDetailRequest(
    @SerializedName("noneNumberDetailDto")
    @Expose
    val noneNumberDetailDto: NoneNumberDetailDto,
    @SerializedName("user")
    @Expose
    val userDto: UserDto,
    @SerializedName("appVersion")
    @Expose
    val appVersion: Int
)