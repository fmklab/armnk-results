package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fmklab.armnk_results.R

@Entity(tableName = "control_details")
data class ControlDetailEntity (
    @PrimaryKey
    val id: Int = 0,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "image")
    val image: String
)







































