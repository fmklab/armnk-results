package com.fmklab.armnk_results.framework

import android.content.Context
import com.fmklab.armnk_result.domain.Company
import com.fmklab.armnk_result.domain.User
import com.fmklab.armnk_results.framework.retrofit.Session
import com.fmklab.armnk_results.framework.retrofit.Token
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import io.reactivex.rxjava3.core.Observable

class CurrentUser(context: Context, private val gson: Gson) {

    private val preferences = context.getSharedPreferences("CurrentUser", Context.MODE_PRIVATE)
    private val loginParams = BehaviorRelay.create<LoginParams>()
    private val currentUser = BehaviorRelay.create<User>()

    companion object {
        private const val LOGIN_PARAMS_KEY = "LoginParamsKey"
        private const val USER_KEY = "UserKey"
        private const val TOKEN_KEY = "TokenKey"
    }

    init {
        loginParams
            .skip(1)
            .filter { it != null }
            .subscribe { p -> setLoginParams(p) }

        currentUser
            .skip(1)
            .filter { it != null }
            .subscribe { u -> setUser(u!!) }

        loginParams.accept(getLoginParams())
        currentUser.accept(getUser())
    }

    private fun setLoginParams(loginParams: LoginParams) {
        val json = gson.toJson(loginParams)
        val editor = preferences.edit()
        editor.putString(LOGIN_PARAMS_KEY, json)
        editor.apply()
    }

    fun rememberLoginParams(loginParams: LoginParams) {
        this.loginParams.accept(loginParams)
    }

    private fun getLoginParams(): LoginParams {
        val json = preferences.getString(LOGIN_PARAMS_KEY, null)
        return if (json == null) {
            LoginParams.DEFAULT
        } else {
            gson.fromJson(json, LoginParams::class.java)
        }
    }

    fun observeLoginParams(): Observable<LoginParams> = loginParams.hide()

    fun observeCurrentUser(): Observable<User> = currentUser.hide()

    fun login(session: Session) {
        val tokenJson = gson.toJson(session.token)
        val editor = preferences.edit()
        editor.putString(TOKEN_KEY, tokenJson)
        editor.apply()
        with(session.user) {
            currentUser.accept(User(id, name1, name2, name3, Company(companyId, companyName)))
        }
    }

    private fun setUser(user: User) {
        val json = gson.toJson(user)
        val editor = preferences.edit()
        editor.putString(USER_KEY, json)
        editor.apply()
    }

    fun getUser(): User {
        val json = preferences.getString(USER_KEY, null)
        return if (json == null) {
            User.DEFAULT
        } else {
            return gson.fromJson(json, User::class.java)
        }
    }

    fun setToken(token: Token) {
        val json = gson.toJson(token)
        val editor = preferences.edit()
        editor.putString(TOKEN_KEY, json)
        editor.apply()
    }

    fun getToken(): Token? {
        val json = preferences.getString(TOKEN_KEY, null)
        return if (json == null) {
            null
        } else {
            val token = gson.fromJson(json, Token::class.java)
            token
        }
    }

    fun getAccessToken(): String {
        return "Bearer ${getToken()?.accessToken}"
    }

    fun logout() {
        val editor = preferences.edit()
        editor.remove(LOGIN_PARAMS_KEY)
        editor.remove(USER_KEY)
        editor.remove(TOKEN_KEY)
        editor.apply()
        loginParams.accept(LoginParams.DEFAULT)
        currentUser.accept(User.DEFAULT)
    }
}









































