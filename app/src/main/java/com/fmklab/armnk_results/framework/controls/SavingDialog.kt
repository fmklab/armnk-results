package com.fmklab.armnk_results.framework.controls

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.fmklab.armnk_results.R

class SavingDialog(private val activity: Activity) {

    var dialog: AlertDialog? = null

    fun startSavingDialog() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        builder.setView(inflater.inflate(R.layout.dialog_save, null))
        builder.setCancelable(false)

        dialog = builder.create()
        dialog?.show()
    }

    fun dismissDialog() {
        dialog?.dismiss()
    }
}