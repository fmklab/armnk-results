package com.fmklab.armnk_results.framework.retrofit

import com.fmklab.armnk_result.domain.DefectMoment
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*


interface NumberDetailApi {

    companion object {
        private const val ROUTE = "nd"
        private const val ROUTE_BOGIE = "bnd"
    }

    @GET("$ROUTE/all")
    fun getAllPaged(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("page") page: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<NumberDetailListItemDto>>

    @GET("$ROUTE/{id}")
    fun getById(
        @Path("id") id: Int,
        @Header("Authorization") accessToken: String
    ): Observable<NumberDetailDto>

    @GET("$ROUTE/rdeftypes/{partId}")
    fun getRepairDefectTypes(
        @Path("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<DefectTypeDto>>

    @GET("$ROUTE/rcz/{partId}")
    fun getRepairControlZones(
        @Path("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<ControlZoneDto>>

    @GET("$ROUTE/steals")
    fun getSteals(@Header("Authorization") accessToken: String): Observable<List<StealDto>>

    @GET("$ROUTE/methods")
    fun getMethods(
        @Query("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<String>>

    @GET("$ROUTE/detectors/{companyId}/{method}")
    fun getDetectors(
        @Path("companyId") companyId: Int,
        @Path("method") method: String,
        @Header("Authorization") accessToken: String
    ): Observable<List<DetectorDto>>

    @GET("$ROUTE/added")
    fun checkAlreadyAddedDetail(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("number")
        number: String,
        @Header("Authorization") accessToken: String
    ): Observable<AddedDetailDto>

    @PUT(ROUTE)
    fun add(
        @Body request: AddNumberDetailRequest,
        @Header("Authorization") accessToken: String
    ): Observable<Int>

    @GET("$ROUTE/search")
    fun search(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("number") number: String,
        @Header("Authorization") accessToken: String
    ): Observable<List<NumberDetailListItemDto>>

    @GET("$ROUTE_BOGIE/all")
    fun getAllBogie(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("page") page: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<BogieNumberDetailListItemDto>>

    @GET("$ROUTE_BOGIE/{id}")
    fun getBogieById(
        @Path("id") id: Int,
        @Header("Authorization") accessToken: String
    ): Observable<BogieNumberDetailDto>

    @GET("$ROUTE_BOGIE/cz")
    fun getBogieControlZones(
        @Query("partId") partId: Int,
        @Query("defectMoment") defectMoment: DefectMoment,
        @Query("bogieModelId") bogieModelId: Int
    ): Observable<List<ControlZoneDto>>

    @PUT(ROUTE_BOGIE)
    fun addBogie(
        @Body request: AddBogieNumberDetailRequest,
        @Header("Authorization") accessToken: String
    ): Observable<Int>

    @GET("$ROUTE_BOGIE/search")
    fun searchBogie(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("number") number: String,
        @Header("Authorization") accessToken: String
    ): Observable<List<BogieNumberDetailListItemDto>>
}






































