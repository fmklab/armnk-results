package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class NumberDetailDto (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("factoryId")
    @Expose
    val factoryId: Int,
    @SerializedName("factoryYearValue")
    @Expose
    val factoryYearValue: Int,
    @SerializedName("number")
    @Expose
    val number: String,
    @SerializedName("isNewDetail")
    @Expose
    val isNewDetail: Boolean,
    @SerializedName("isFromServicePoint")
    @Expose
    val isFromServicePoint: Boolean,
    @SerializedName("servicePointCompany")
    @Expose
    val servicePointCompany: Int,
    @SerializedName("servicePointFault")
    @Expose
    val servicePointFault: Int,
    @SerializedName("noRejectionCriteria")
    @Expose
    val noRejectionCriteria: Boolean,
    @SerializedName("controlResultId")
    @Expose
    val controlResultId: Int,
    @SerializedName("defectTypeId")
    @Expose
    val defectTypeId: Int,
    @SerializedName("controlZoneId")
    @Expose
    val controlZoneId: Int,
    @SerializedName("repairDescription")
    @Expose
    val repairDescription: String?,
    @SerializedName("defectMoment")
    @Expose
    val defectMoment: Int,
    @SerializedName("steal")
    @Expose
    val steal: String?,
    @SerializedName("owner")
    @Expose
    val owner: String?,
    @SerializedName("defectDescription")
    @Expose
    val defectDescription: String?,
    @SerializedName("defectLength")
    @Expose
    val defectLength: String?,
    @SerializedName("defectDepth")
    @Expose
    val defectDepth: String?,
    @SerializedName("defectDiameter")
    @Expose
    val defectDiameter: String?,
    @SerializedName("method")
    @Expose
    val method: String?,
    @SerializedName("detector")
    @Expose
    val detector: String?,
    @SerializedName("defectSize")
    @Expose
    val defectSize: String?,
    @SerializedName("defectInfo")
    @Expose
    val defectInfo: String?,
    @SerializedName("resultName")
    @Expose
    val resultName: String?,
    @SerializedName("spec")
    @Expose
    val spec: String
) {

    companion object {
        val DEFAULT = NumberDetailDto(
            id = 0,
            partId = 0,
            controlDate = Date(),
            factoryId = 0,
            factoryYearValue = -1,
            number = "",
            isNewDetail = false,
            isFromServicePoint = false,
            servicePointCompany = 0,
            servicePointFault = 0,
            noRejectionCriteria = false,
            controlResultId = 0,
            defectTypeId = 0,
            controlZoneId = 0,
            repairDescription = "",
            defectMoment = 0,
            steal = "",
            owner = "",
            defectDescription = "",
            defectLength = "",
            defectDepth = "",
            defectDiameter = "",
            method = "",
            detector = "",
            defectSize = "",
            defectInfo = "",
            resultName = "",
            spec = ""
        )
    }
}




































