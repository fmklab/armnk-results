package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class NoneNumberDetailDto(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("method")
    @Expose
    val method: String,
    @SerializedName("controlZoneId")
    @Expose
    val controlZoneId: Int,
    @SerializedName("controlZoneName")
    @Expose
    val controlZoneName: String,
    @SerializedName("controlZoneMethodStr")
    @Expose
    val controlZoneMethodStr: String?,
    @SerializedName("detectorName")
    @Expose
    val detectorName: String,
    @SerializedName("allChecked")
    @Expose
    val allChecked: Int,
    @SerializedName("defecationChecked")
    @Expose
    val defecationChecked: Int,
    @SerializedName("visualChecked")
    @Expose
    val visualChecked: Int,
    @SerializedName("ndtChecked")
    @Expose
    val ndtChecked: Int,
    @SerializedName("spec")
    @Expose
    val spec: String,
    @SerializedName("editStatus")
    @Expose
    val editStatus: Boolean,
    @SerializedName("createDate")
    @Expose
    val createDate: Date,
    @SerializedName("isMobileApp")
    @Expose
    val isMobileApp: Boolean
) {

    companion object {
        val DEFAULT = NoneNumberDetailDto(
            0,
            0,
            Date(),
            "",
            0,
            "",
            "",
            "",
            0,
            0,
            0,
            0,
            "",
            false,
            Date(),
            true
        )
    }
}





































