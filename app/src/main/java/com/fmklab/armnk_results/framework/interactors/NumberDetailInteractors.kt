package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_result.interactors.detail.*
import com.fmklab.armnk_result.interactors.numberdetail.*

data class NumberDetailInteractors (
    val getAllPaged: GetAllPaged,
    val getById: GetById,
    val getFactories: GetFactories,
    val getFactoryYears: GetYears,
    val getControlResults: GetControlResults,
    val cacheDetail: CacheDetail,
    val clearPreferencesCache: ClearPreferencesCache,
    val getRepairDefectTypes: GetRepairDefectTypes,
    val getRepairControlZones: GetRepairControlZones,
    val getDefectTypes: GetDefectTypes,
    val getControlZones: GetControlZones,
    val getSteals: GetSteals,
    val getMethods: GetMethods,
    val getDetectors: GetDetectors,
    val checkAlreadyAddedDetail: CheckAlreadyAddedDetail,
    val addNumberDetail: AddNumberDetail,
    val searchNumberDetail: SearchNumberDetail,
    val getAllBogiePaged: GetAllBogiePaged
)