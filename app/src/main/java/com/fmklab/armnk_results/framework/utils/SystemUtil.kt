package com.fmklab.armnk_results.framework.utils

class SystemUtil {

    companion object {
        fun currentTime(): Int {
            return (System.currentTimeMillis() / 1000).toInt()
        }
    }
}