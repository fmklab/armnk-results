package com.fmklab.armnk_results.framework.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface NoneNumberDetailDao {

    @Query("SELECT * FROM control_zone WHERE part_id = :partId AND (method_str = :method OR method_str IS NULL)")
    fun getControlZones(partId: Int, method: String): Observable<List<ControlZoneEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertControlZones(controlZones: List<ControlZoneEntity>): Single<List<Long>>

    @Query("DELETE FROM control_zone")
    fun deleteAllControlZones()

    @Query("DELETE FROM control_zone WHERE part_id = :partId AND method_str = :method")
    fun deleteControlZones(partId: Int, method: String)
}