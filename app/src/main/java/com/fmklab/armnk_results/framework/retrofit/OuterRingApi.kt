package com.fmklab.armnk_results.framework.retrofit

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*

interface OuterRingApi {

    companion object {
        private const val ROUTE = "or"
    }

    @GET("$ROUTE/all")
    fun getAll(
        @Query("partId") partId: Int,
        @Query("companyId") companyId: Int,
        @Query("page") page: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<OuterRingListItemDto>>

    @GET("$ROUTE/{id}")
    fun getById(
        @Path("id") id: Int,
        @Header("Authorization") accessToken: String
    ): Observable<OuterRingDto>

    @PUT(ROUTE)
    fun add(
        @Body addOuterRingRequest: AddOuterRingRequest,
        @Header("Authorization") accessToken: String
    ): Observable<Int>

    @GET("$ROUTE/search")
    fun search(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("number") number: String,
        @Header("Authorization") accessToken: String
    ): Observable<List<OuterRingListItemDto>>
}











































