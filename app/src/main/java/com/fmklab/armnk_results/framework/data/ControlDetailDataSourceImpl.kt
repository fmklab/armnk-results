package com.fmklab.armnk_results.framework.data

import com.fmklab.armnk_result.data.ControlDetailDataSource
import com.fmklab.armnk_result.domain.ControlDetail
import com.fmklab.armnk_results.framework.room.ArmnkDatabase
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ControlDetailDataSourceImpl(database: ArmnkDatabase) : ControlDetailDataSource {

    private val controlDetailDao = database.controlDetailDao()

    override fun getAll(): Observable<List<ControlDetail>> {
        return controlDetailDao.getAll()
            .map { list ->
                list.map { controlDetail ->
                    ControlDetail(controlDetail.id, controlDetail.name, controlDetail.image)
                }
            }
            .toObservable()
            .subscribeOn(Schedulers.io())
    }
}