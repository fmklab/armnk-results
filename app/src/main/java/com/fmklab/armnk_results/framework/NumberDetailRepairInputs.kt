package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectType
import com.fmklab.armnk_result.domain.Error

data class NumberDetailRepairInputs (
    val defectTypes: Result<List<DefectType>>,
    val controlZones: Result<List<ControlZone>>
) {

    fun isSuccess()  = defectTypes.isSuccess && controlZones.isSuccess

    fun getError(): Error {
        return when {
            defectTypes.isError -> defectTypes.error!!
            controlZones.isError -> controlZones.error!!
            else -> Error.Unknown("No message")
        }
    }
}