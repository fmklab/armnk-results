package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_result.interactors.GetControlDetails

data class ControlDetailInteractors (
    val getControlDetails: GetControlDetails
)