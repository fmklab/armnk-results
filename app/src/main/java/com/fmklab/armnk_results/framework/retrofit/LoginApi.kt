package com.fmklab.armnk_results.framework.retrofit

import com.fmklab.armnk_results.framework.LoginParams
import io.reactivex.rxjava3.core.Observable
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginApi {

    companion object {
        private const val ROUTE = "auth"
    }

    @POST("$ROUTE/token")
    fun getToken(@Body request: LoginParams): Observable<Session>

    @GET("$ROUTE/refresh")
    fun refreshToken(
        @Query("accessToken") accessToken: String,
        @Query("refreshToken") refreshToken: String
    ): Call<Token>
}