package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetectorDto (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("methodStr")
    @Expose
    val methodStr: String
)