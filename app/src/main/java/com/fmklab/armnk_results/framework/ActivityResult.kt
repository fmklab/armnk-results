package com.fmklab.armnk_results.framework

import android.app.Activity
import android.content.Intent

class ActivityResult(private val requestCode: Int, private val resultCode: Int, val intent: Intent) {

    fun isCanceled() = resultCode == Activity.RESULT_CANCELED

    fun isOk() = resultCode == Activity.RESULT_OK

    fun isRequestCode(v: Int) = requestCode == v
}