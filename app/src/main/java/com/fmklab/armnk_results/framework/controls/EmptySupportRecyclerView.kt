package com.fmklab.armnk_results.framework.controls

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class EmptySupportRecyclerView: RecyclerView {

    private var emptyView: View? = null

    private val emptyObserver = object : AdapterDataObserver() {
        override fun onChanged() {
            if (adapter?.itemCount == 0) {
                emptyView?.visibility = View.VISIBLE
                this@EmptySupportRecyclerView.visibility = View.GONE
            } else {
                emptyView?.visibility = View.GONE
                this@EmptySupportRecyclerView.visibility = View.VISIBLE
            }
        }
    }

    constructor(context: Context): super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int): super(context, attrs, defStyle)

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)

        adapter?.registerAdapterDataObserver(emptyObserver)
        emptyObserver.onChanged()
    }

    fun setEmptyView(emptyView: View) {
        this.emptyView = emptyView
    }
}