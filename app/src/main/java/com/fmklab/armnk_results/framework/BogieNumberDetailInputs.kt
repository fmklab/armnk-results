package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.*

data class BogieNumberDetailInputs(
    val detail: Result<BogieNumberDetail>,
    val bogieModels: Result<List<BogieModel>>,
    val factories: Result<List<Factory>>,
    val factoryYears: Result<List<FactoryYear>>,
    val controlResults: Result<List<ControlResult>>
) {

    fun isSuccess(): Boolean {
        return detail.isSuccess && bogieModels.isSuccess && factories.isSuccess && factoryYears.isSuccess && controlResults.isSuccess
    }

    fun getError(): Error {
        return when {
            detail.isError -> detail.error!!
            bogieModels.isError -> bogieModels.error!!
            factories.isError -> factories.error!!
            factoryYears.isError -> factoryYears.error!!
            controlResults.isError -> controlResults.error!!
            else -> Error.Unknown("No message")
        }
    }
}