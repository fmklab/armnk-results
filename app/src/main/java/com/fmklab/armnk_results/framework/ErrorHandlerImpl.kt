package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.ErrorHandler
import retrofit2.HttpException
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.UnknownHostException

class ErrorHandlerImpl : ErrorHandler {

    override fun getError(throwable: Throwable): Error {
        return when(throwable) {
            is ConnectException -> Error.NoInternetConnection
            is UnknownHostException -> Error.NoInternetConnection
            is java.net.SocketTimeoutException -> Error.SocketTimeout
            is HttpException -> {
                when(throwable.code()) {
                    HttpURLConnection.HTTP_BAD_REQUEST -> Error.BadRequest
                    HttpURLConnection.HTTP_NOT_FOUND -> Error.NotFoundRemote
                    else -> Error.Unknown(throwable.message())
                }
            }
            is PreferenceValueIsNullException -> Error.NotFoundSharedPreferences
            else -> Error.Unknown(throwable.message ?: "No message")
        }
    }
}