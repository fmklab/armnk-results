package com.fmklab.armnk_results.framework

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginParams (
    @SerializedName("login")
    @Expose
    val login: String,
    @SerializedName("password")
    @Expose
    val password: String,
    val rememberMe: Boolean = false
) {

    companion object {
        val DEFAULT = LoginParams("", "", false)
    }

    fun isValid(): Boolean {
        return !(login.isBlank() || password.isBlank())
    }
}