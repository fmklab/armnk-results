package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Session (
    @SerializedName("user")
    @Expose
    val user: UserDto,
    @SerializedName("token")
    @Expose
    val token: Token
)