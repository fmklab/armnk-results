package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectType
import com.fmklab.armnk_result.domain.Detector
import com.fmklab.armnk_result.domain.Error

data class NumberDetailNdtInputs(
    val methods: Result<List<String>>,
    val defectTypes: Result<List<DefectType>>,
    val detectors: Result<List<Detector>>,
    val controlZones: Result<List<ControlZone>>,
    val steals: Result<List<String>>
) {

    fun isSuccess() =
        methods.isSuccess && defectTypes.isSuccess && detectors.isSuccess && controlZones.isSuccess && steals.isSuccess

    fun getError(): Error {
        return when {
            methods.isError -> methods.error!!
            defectTypes.isError -> defectTypes.error!!
            detectors.isError -> detectors.error!!
            controlZones.isError -> controlZones.error!!
            steals.isError -> steals.error!!
            else -> Error.Unknown("No message")
        }
    }
}