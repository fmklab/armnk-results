package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "control_result")
data class ControlResultEntity (
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "part_id")
    val partId: Int,
    @ColumnInfo(name = "timestamp")
    val timestamp: Int
)