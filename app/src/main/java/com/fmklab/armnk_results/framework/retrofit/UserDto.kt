package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserDto (
    @SerializedName("id")
    @Expose
    val id: Long,
    @SerializedName("name1")
    @Expose
    val name1: String,
    @SerializedName("name2")
    @Expose
    val name2: String,
    @SerializedName("name3")
    @Expose
    val name3: String,
    @SerializedName("companyId")
    @Expose
    val companyId: Int,
    @SerializedName("companyTitle")
    @Expose
    val companyName: String
)