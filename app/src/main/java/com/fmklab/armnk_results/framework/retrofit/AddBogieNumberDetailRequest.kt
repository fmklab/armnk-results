package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddBogieNumberDetailRequest(
    @SerializedName("bogieNumberDetailDto")
    @Expose
    val bogieNumberDetailDto: BogieNumberDetailDto,
    @SerializedName("user")
    @Expose
    val userDto: UserDto,
    @SerializedName("appVersion")
    @Expose
    val appVersion: Int
)