package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FactoryDto (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("orderField")
    @Expose
    val orderField: Int
)