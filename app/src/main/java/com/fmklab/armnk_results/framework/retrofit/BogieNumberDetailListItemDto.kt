package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class BogieNumberDetailListItemDto(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("number")
    @Expose
    val number: String?,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("factoryName")
    @Expose
    val factoryName: String,
    @SerializedName("factoryYear")
    @Expose
    val factoryYear: Int,
    @SerializedName("bogieModel")
    @Expose
    val bogieModel: String?,
    @SerializedName("controlResult")
    @Expose
    val controlResult: String,
    @SerializedName("defectDescription")
    @Expose
    val defectDescription: String?,
    @SerializedName("spec")
    @Expose
    val spec: String?,
    @SerializedName("isFromServicePoint")
    @Expose
    val isFromServicePoint: Boolean,
    @SerializedName("noRejectionCriteria")
    @Expose
    val noRejectionCriteria: Boolean,
    @SerializedName("editStatus")
    @Expose
    val editStatus: Boolean,
    @SerializedName("createDate")
    @Expose
    val createDate: Date,
    @SerializedName("isMobileApp")
    @Expose
    val isMobileApp: Boolean
)































