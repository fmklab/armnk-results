package com.fmklab.armnk_results.framework.data

import android.os.Build
import com.fmklab.armnk_result.data.NumberDetailDataSource
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.BuildConfig
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.NetworkBoundResult
import com.fmklab.armnk_results.framework.preferences.PreferencesDatabase
import com.fmklab.armnk_results.framework.retrofit.*
import com.fmklab.armnk_results.framework.room.*
import com.fmklab.armnk_results.framework.utils.DetailUtil
import com.fmklab.armnk_results.framework.utils.ServicePointUtil
import com.fmklab.armnk_results.framework.utils.SystemUtil
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class NumberDetailDataSourceImpl(
    private val numberDetailApi: NumberDetailApi,
    private val currentUser: CurrentUser,
    private val preferencesDatabase: PreferencesDatabase,
    armnkDatabase: ArmnkDatabase
) : NumberDetailDataSource {

    private val numberDetailDao = armnkDatabase.numberDetailDao()

    companion object {
        private const val CACHE_TTL = 3600
        private const val NUMBER_DETAIL_KEY = "NumberDetailKey"
        private const val BOGIE_NUMBER_DETAIL_KEY = "BogieNumberDetail"
    }

    override fun getAllPaged(partId: Int, page: Int): Observable<List<NumberDetailListItem>> {
        return numberDetailApi.getAllPaged(
            currentUser.getUser().company.id,
            partId,
            page,
            currentUser.getAccessToken()
        )
            .map { list ->
                list.map {
                    NumberDetailListItem(
                        it.id,
                        it.partId,
                        it.number ?: "",
                        it.controlDate,
                        it.factoryName,
                        FactoryYear(it.factoryYear),
                        it.controlResult,
                        it.defectDescription ?: "",
                        it.spec ?: "",
                        it.isFromServicePoint,
                        it.noRejectionCriteria,
                        it.editStatus,
                        it.createDate,
                        it.isMobileApp
                    )
                }
            }
            .subscribeOn(Schedulers.io())
    }

    override fun getById(id: Int): Observable<NumberDetail> {
        return object : NetworkBoundResult<NumberDetailDto, NumberDetailDto>() {
            override fun saveCallResult(item: NumberDetailDto): Single<List<Long>> {
                return Single.just(item)
                    .flatMap {
                        preferencesDatabase.insert(it, NUMBER_DETAIL_KEY)
                        Single.just(List(1) { 1L })
                    }
            }

            override fun createCall(): Observable<NumberDetailDto> {
                return numberDetailApi.getById(id, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: NumberDetailDto): Boolean {
                return item == NumberDetailDto.DEFAULT
            }

            override fun loadFromDb(): Observable<NumberDetailDto> {
                return Observable.just(
                    preferencesDatabase.get(
                        NUMBER_DETAIL_KEY,
                        NumberDetailDto::class.java,
                        NumberDetailDto.DEFAULT
                    )
                )
            }
        }.asObservable().map {
            NumberDetail(it.id, it.partId)
                .controlDate(it.controlDate)
                .factory(Factory.default(it.factoryId))
                .factoryYear(FactoryYear(it.factoryYearValue))
                .isNewDetail(it.isNewDetail)
                .number(it.number)
                .isFromServicePoint(it.isFromServicePoint)
                .servicePointCompany(ServicePointUtil.company(it.servicePointCompany))
                .servicePointFault(ServicePointUtil.fault(it.servicePointFault))
                .noRejectionCriteria(it.noRejectionCriteria)
                .controlResult(ControlResult.default(it.controlResultId))
                .spec(it.spec)
                .defectType(DefectType.default(it.defectTypeId))
                .controlZone(ControlZone.default(it.controlZoneId))
                .repairDescription(it.repairDescription ?: "")
                .defectMoment(DetailUtil.numberDetailDefectMoment(it.defectMoment))
                .steal(it.steal ?: "")
                .owner(it.owner ?: "")
                .defectDescription(it.defectDescription ?: "")
                .defectLength(it.defectLength ?: "")
                .defectDepth(it.defectDepth ?: "")
                .defectDiameter(it.defectDiameter ?: "")
                .method(it.method ?: "")
                .detector(Detector.default(it.detector ?: "", it.method ?: ""))
                .defectSize(it.defectSize ?: "")
                .defectInfo(it.defectInfo ?: "") as NumberDetail
        }
    }

    override fun cacheDetail(numberDetail: NumberDetail) {
        preferencesDatabase.insert(
            NumberDetailDto(
                numberDetail.id,
                numberDetail.partId,
                numberDetail.controlDate,
                numberDetail.factory?.id ?: 0,
                numberDetail.factoryYear?.value ?: -1,
                numberDetail.number,
                numberDetail.isNewDetail,
                numberDetail.isFromServicePoint,
                numberDetail.servicePoint.company(),
                numberDetail.servicePoint.fault(),
                numberDetail.noRejectionCriteria,
                numberDetail.controlResult?.id ?: 0,
                numberDetail.defectType?.id ?: 0,
                numberDetail.controlZone?.id ?: 0,
                numberDetail.repairDescription,
                numberDetail.defectMoment.value,
                numberDetail.steal,
                numberDetail.owner,
                numberDetail.defectDescription,
                numberDetail.defectLength,
                numberDetail.defectDepth,
                numberDetail.defectDiameter,
                numberDetail.method,
                numberDetail.detector?.toString() ?: "",
                numberDetail.defectSize,
                numberDetail.defectInfo,
                numberDetail.controlResult?.name ?: "",
                numberDetail.spec
            ), NUMBER_DETAIL_KEY
        )
    }

    override fun clearPreferencesCache() {
        preferencesDatabase.remove(NUMBER_DETAIL_KEY)
    }

    override fun getRepairDefectTypes(partId: Int): Observable<List<DefectType>> {
        return object : NetworkBoundResult<List<DefectTypeEntity>, List<DefectTypeDto>>() {
            override fun saveCallResult(item: List<DefectTypeDto>): Single<List<Long>> {
                numberDetailDao.deleteDefectTypes(partId, DefectMoment.NONE.value, true)
                return numberDetailDao.insertDefectTypes(item.map {
                    DefectTypeEntity(
                        0,
                        it.id,
                        it.name,
                        partId,
                        DefectMoment.NONE.value,
                        it.needControlZone,
                        true,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<DefectTypeDto>> {
                return numberDetailApi.getRepairDefectTypes(partId, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: List<DefectTypeEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { dt -> currentTime - dt.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<DefectTypeEntity>> {
                return numberDetailDao.getRepairDefectTypes(partId)
            }
        }.asObservable().map { list ->
            list.map {
                DefectType(it.id, it.name, it.needControlZone)
            }
        }
    }

    override fun getRepairControlZones(partId: Int): Observable<List<ControlZone>> {
        return object : NetworkBoundResult<List<ControlZoneEntity>, List<ControlZoneDto>>() {
            override fun saveCallResult(item: List<ControlZoneDto>): Single<List<Long>> {
                numberDetailDao.deleteControlZones(partId, DefectMoment.NONE.value, true)
                return numberDetailDao.insertControlZones(item.map {
                    ControlZoneEntity(
                        0,
                        it.id,
                        partId,
                        DefectMoment.NONE.value,
                        true,
                        it.name,
                        it.methodStr,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<ControlZoneDto>> {
                return numberDetailApi.getRepairControlZones(partId, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: List<ControlZoneEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { cz -> currentTime - cz.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<ControlZoneEntity>> {
                return numberDetailDao.getRepairControlZones(partId)
            }
        }.asObservable().map { list ->
            list.map {
                ControlZone(it.id, it.name, it.methodStr)
            }
        }
    }

    override fun getSteals(): Observable<List<String>> {
        return object : NetworkBoundResult<List<StealEntity>, List<StealDto>>() {
            override fun saveCallResult(item: List<StealDto>): Single<List<Long>> {
                numberDetailDao.deleteSteals()
                return numberDetailDao.insertSteals(item.map {
                    StealEntity(
                        it.id,
                        it.name,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<StealDto>> {
                return numberDetailApi.getSteals(currentUser.getAccessToken())
            }

            override fun shouldFetch(item: List<StealEntity>): Boolean {
                if (item.isEmpty()) return true
                val currentTime = SystemUtil.currentTime()
                return item.any { s -> currentTime - s.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<StealEntity>> {
                return numberDetailDao.getSteals()
            }
        }.asObservable().map { list ->
            list.map { it.name }
        }
    }

    override fun checkAlreadyAddedDetail(
        partId: Int,
        number: String
    ): Observable<AddedDetail> {
        return numberDetailApi.checkAlreadyAddedDetail(
            currentUser.getUser().company.id,
            partId,
            number,
            currentUser.getAccessToken()
        ).map { AddedDetail(it.number, it.date, it.status) }
            .subscribeOn(Schedulers.io())
    }

    override fun add(numberDetail: NumberDetail): Observable<Int> {
        val user = currentUser.getUser()
        return numberDetailApi.add(
            AddNumberDetailRequest(
                NumberDetailDto(
                    numberDetail.id,
                    numberDetail.partId,
                    numberDetail.controlDate,
                    numberDetail.factory?.id ?: 0,
                    numberDetail.factoryYear?.value ?: -1,
                    numberDetail.number,
                    numberDetail.isNewDetail,
                    numberDetail.isFromServicePoint,
                    numberDetail.servicePoint.company(),
                    numberDetail.servicePoint.fault(),
                    numberDetail.noRejectionCriteria,
                    numberDetail.controlResult?.id ?: 0,
                    numberDetail.defectType?.id ?: 0,
                    numberDetail.controlZone?.id ?: 0,
                    numberDetail.repairDescription,
                    numberDetail.defectMoment.value,
                    numberDetail.steal,
                    numberDetail.owner,
                    numberDetail.defectDescription,
                    numberDetail.defectLength,
                    numberDetail.defectDepth,
                    numberDetail.defectDiameter,
                    numberDetail.method,
                    numberDetail.detector?.toString() ?: "",
                    numberDetail.defectSize,
                    numberDetail.defectInfo,
                    numberDetail.controlResult?.name ?: "",
                    numberDetail.spec
                ),
                UserDto(
                    user.id,
                    user.name1,
                    user.name2,
                    user.name3,
                    user.company.id,
                    user.company.name
                ),
                BuildConfig.VERSION_CODE
            ), currentUser.getAccessToken()
        ).subscribeOn(Schedulers.io())
    }

    override fun search(partId: Int, number: String): Observable<List<NumberDetailListItem>> {
        return numberDetailApi.search(
            currentUser.getUser().company.id,
            partId,
            number,
            currentUser.getAccessToken()
        ).map { list ->
            list.map {
                NumberDetailListItem(
                    it.id,
                    it.partId,
                    it.number ?: "",
                    it.controlDate,
                    it.factoryName,
                    FactoryYear(it.factoryYear),
                    it.controlResult,
                    it.defectDescription ?: "",
                    it.spec ?: "",
                    it.isFromServicePoint,
                    it.noRejectionCriteria,
                    it.editStatus,
                    it.createDate,
                    it.isMobileApp
                )
            }
        }.subscribeOn(Schedulers.io())
    }

    override fun getAllBogiePaged(
        partId: Int,
        page: Int
    ): Observable<List<BogieNumberDetailListItem>> {
        return numberDetailApi.getAllBogie(
            currentUser.getUser().company.id,
            partId,
            page,
            currentUser.getAccessToken()
        ).map { list ->
            list.map {
                BogieNumberDetailListItem(
                    it.id,
                    it.partId,
                    it.number ?: "",
                    it.controlDate,
                    it.factoryName,
                    FactoryYear(it.factoryYear),
                    it.controlResult,
                    it.defectDescription ?: "",
                    it.spec ?: "",
                    it.isFromServicePoint,
                    it.noRejectionCriteria,
                    it.editStatus,
                    it.createDate,
                    it.isMobileApp,
                    it.bogieModel ?: "-"
                )
            }
        }
            .subscribeOn(Schedulers.io())
    }

    override fun searchBogie(
        partId: Int,
        number: String
    ): Observable<List<BogieNumberDetailListItem>> {
        return numberDetailApi.searchBogie(
            currentUser.getUser().company.id,
            partId,
            number,
            currentUser.getAccessToken()
        )
            .map { list ->
                list.map {
                    BogieNumberDetailListItem(
                        it.id,
                        it.partId,
                        it.number ?: "",
                        it.controlDate,
                        it.factoryName,
                        FactoryYear(it.factoryYear),
                        it.controlResult,
                        it.defectDescription ?: "",
                        it.spec ?: "",
                        it.isFromServicePoint,
                        it.noRejectionCriteria,
                        it.editStatus,
                        it.createDate,
                        it.isMobileApp,
                        it.bogieModel ?: "-"
                    )
                }
            }
    }

    override fun getBogieById(id: Int): Observable<BogieNumberDetail> {
        return numberDetailApi.getBogieById(id, currentUser.getAccessToken())
            .map {
                (BogieNumberDetail(it.id, it.partId)
                    .controlDate(it.controlDate)
                    .factory(Factory.default(it.factoryId))
                    .factoryYear(FactoryYear(it.factoryYearValue))
                    .isNewDetail(it.isNewDetail)
                    .number(it.number)
                    .isFromServicePoint(it.isFromServicePoint)
                    .servicePointCompany(ServicePointUtil.company(it.servicePointCompany))
                    .servicePointFault(ServicePointUtil.fault(it.servicePointFault))
                    .noRejectionCriteria(it.noRejectionCriteria)
                    .controlResult(ControlResult.default(it.controlResultId))
                    .spec(it.spec)
                    .defectType(DefectType.default(it.defectTypeId))
                    .controlZone(ControlZone.default(it.controlZoneId))
                    .repairDescription(it.repairDescription ?: "")
                    .defectMoment(DetailUtil.numberDetailDefectMoment(it.defectMoment))
                    .steal(it.steal ?: "")
                    .owner(it.owner ?: "")
                    .defectDescription(it.defectDescription ?: "")
                    .defectLength(it.defectLength ?: "")
                    .defectDepth(it.defectDepth ?: "")
                    .defectDiameter(it.defectDiameter ?: "")
                    .method(it.method ?: "")
                    .detector(Detector.default(it.detector ?: "", it.method ?: ""))
                    .defectSize(it.defectSize ?: "")
                    .defectInfo(it.defectInfo ?: "") as BogieNumberDetail)
                    .bogieModel(BogieModel.default(it.bogieModelId))
            }
    }

    override fun cacheBogieDetail(numberDetail: BogieNumberDetail) {
        preferencesDatabase.insert(
            BogieNumberDetailDto(
                numberDetail.id,
                numberDetail.partId,
                numberDetail.controlDate,
                numberDetail.factory?.id ?: 0,
                numberDetail.factoryYear?.value ?: -1,
                numberDetail.number,
                numberDetail.isNewDetail,
                numberDetail.isFromServicePoint,
                numberDetail.servicePoint.company(),
                numberDetail.servicePoint.fault(),
                numberDetail.noRejectionCriteria,
                numberDetail.controlResult?.id ?: 0,
                numberDetail.defectType?.id ?: 0,
                numberDetail.controlZone?.id ?: 0,
                numberDetail.repairDescription,
                numberDetail.defectMoment.value,
                numberDetail.steal,
                numberDetail.owner,
                numberDetail.defectDescription,
                numberDetail.defectLength,
                numberDetail.defectDepth,
                numberDetail.defectDiameter,
                numberDetail.method,
                numberDetail.detector?.toString() ?: "",
                numberDetail.defectSize,
                numberDetail.defectInfo,
                numberDetail.controlResult?.name ?: "",
                numberDetail.spec,
                numberDetail.bogieModel?.id ?: 0
            ), BOGIE_NUMBER_DETAIL_KEY
        )
    }

    override fun clearBogiePreferenceCache() {
        preferencesDatabase.remove(BOGIE_NUMBER_DETAIL_KEY)
    }

    override fun getBogieControlZones(
        partId: Int,
        defectMoment: DefectMoment,
        bogieModelId: Int
    ): Observable<List<ControlZone>> {
        return numberDetailApi.getBogieControlZones(partId, defectMoment, bogieModelId)
            .map { list ->
                list.map { ControlZone(it.id, it.name, it.methodStr) }
            }
    }

    override fun addBogie(bogieNumberDetail: BogieNumberDetail): Observable<Int> {
        val user = currentUser.getUser()
        return numberDetailApi.addBogie(
            AddBogieNumberDetailRequest(
                BogieNumberDetailDto(
                    bogieNumberDetail.id,
                    bogieNumberDetail.partId,
                    bogieNumberDetail.controlDate,
                    bogieNumberDetail.factory?.id ?: 0,
                    bogieNumberDetail.factoryYear?.value ?: -1,
                    bogieNumberDetail.number,
                    bogieNumberDetail.isNewDetail,
                    bogieNumberDetail.isFromServicePoint,
                    bogieNumberDetail.servicePoint.company(),
                    bogieNumberDetail.servicePoint.fault(),
                    bogieNumberDetail.noRejectionCriteria,
                    bogieNumberDetail.controlResult?.id ?: 0,
                    bogieNumberDetail.defectType?.id ?: 0,
                    bogieNumberDetail.controlZone?.id ?: 0,
                    bogieNumberDetail.repairDescription,
                    bogieNumberDetail.defectMoment.value,
                    bogieNumberDetail.steal,
                    bogieNumberDetail.owner,
                    bogieNumberDetail.defectDescription,
                    bogieNumberDetail.defectLength,
                    bogieNumberDetail.defectDepth,
                    bogieNumberDetail.defectDiameter,
                    bogieNumberDetail.method,
                    bogieNumberDetail.detector?.toString() ?: "",
                    bogieNumberDetail.defectSize,
                    bogieNumberDetail.defectInfo,
                    bogieNumberDetail.controlResult?.name ?: "",
                    bogieNumberDetail.spec,
                    bogieNumberDetail.bogieModel?.id ?: 0
                ),
                UserDto(
                    user.id,
                    user.name1,
                    user.name2,
                    user.name3,
                    user.company.id,
                    user.company.name
                ),
                BuildConfig.VERSION_CODE
            ), currentUser.getAccessToken()
        )
    }
}







































