package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "detector")
data class DetectorEntity (
    @PrimaryKey(autoGenerate = true)
    val autoId: Long,
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "code")
    val code: String,
    @ColumnInfo(name = "method_str")
    val methodStr: String,
    @ColumnInfo(name = "timestamp")
    val timestamp: Int
)