package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class AddedDetailDto(
    @SerializedName("number")
    @Expose
    val number: String?,
    @SerializedName("date")
    @Expose
    val date: Date?,
    @SerializedName("status")
    @Expose
    val status: Boolean
)