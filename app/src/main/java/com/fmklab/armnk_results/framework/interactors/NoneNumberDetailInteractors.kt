package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_result.interactors.detail.GetDetectors
import com.fmklab.armnk_result.interactors.detail.GetMethods
import com.fmklab.armnk_result.interactors.nonenumberdetail.*

data class NoneNumberDetailInteractors (
    val getAllPaged: GetAllPaged,
    val getById: GetById,
    val getMethods: GetMethods,
    val getControlZonesByMethod: GetControlZonesByMethod,
    val getDetectors: GetDetectors,
    val cacheDetail: CacheDetail,
    val clearPreferencesCache: ClearPreferencesCache,
    val clearRequestCache: ClearRequestCache,
    val insert: Insert
)