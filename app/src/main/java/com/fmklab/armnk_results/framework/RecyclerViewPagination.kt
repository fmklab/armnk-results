package com.fmklab.armnk_results.framework

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding4.recyclerview.scrollEvents
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Action
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.functions.Predicate
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.reflect


class RecyclerViewPagination(
    private val recyclerView: RecyclerView,
    private val nextPage: KFunction<Unit>,
    private val isLoading: Observable<Boolean>
) {

    private var disposable: Disposable? = null

    companion object {
        private const val DIRECTION_DOWN = 1
    }

    init {
        start()
    }

    fun start() {
        stop()

        val lastVisibleAndCount = recyclerView
            .scrollEvents()
            .filter {
                recyclerView.canScrollVertically(DIRECTION_DOWN)
            }
            .map { recyclerView.layoutManager }
            .ofType(LinearLayoutManager::class.java)
            .map(this::displayedItemFromLinearLayout)
            .filter { item ->
                item.second != 0
            }
            .distinctUntilChanged()

        val isNotLoading = isLoading
            .distinctUntilChanged()
            .filter { !it }

        val loadNextPage = lastVisibleAndCount
            .withLatestFrom(
                isNotLoading,
                BiFunction<Pair<Int, Int>, Boolean, Pair<Int, Int>> { lAVC: Pair<Int, Int>, _ ->
                    lAVC
                }
            )
            .distinctUntilChanged()
            .filter(this::visibleItemIsCloseToBottom)

        disposable = loadNextPage
            .subscribe { nextPage.call() }
    }

    fun stop() {
        disposable?.dispose()
    }

    private fun displayedItemFromLinearLayout(manager: LinearLayoutManager) =
        Pair(manager.findLastVisibleItemPosition(), manager.itemCount)

    private fun visibleItemIsCloseToBottom(visibleItemOfTotal: Pair<Int, Int>) =
        //visibleItemOfTotal.second - recyclerView.childCount <= visibleItemOfTotal.first + 1
        visibleItemOfTotal.first == visibleItemOfTotal.second - 1
}








































