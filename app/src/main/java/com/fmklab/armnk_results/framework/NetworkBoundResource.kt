package com.fmklab.armnk_results.framework

import androidx.room.rxjava3.EmptyResultSetException
import com.jakewharton.rxrelay3.BehaviorRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

abstract class NetworkBoundResource<ResultType, RequestType> {

    private lateinit var result: Observable<ResultType>

//    init {
//        val diskObservable = Observable.defer {
//            loadFromDb()
//                .subscribeOn(Schedulers.computation())
//        }
//
//        val networkObservable = Observable.defer {
//            createCall()
//                .subscribeOn(Schedulers.io())
//                .observeOn(Schedulers.computation())
//                .doOnNext { request ->
//                    saveCallResult(request)
//                }
//                .fla
//        }
//    }
//
//    protected abstract fun loadFromDb(): Observable<ResultType>
//
//    protected abstract fun createCall(): Observable<RequestType>
//
//    protected abstract fun saveCallResult(data: RequestType)
}