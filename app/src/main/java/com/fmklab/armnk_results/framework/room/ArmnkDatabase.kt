package com.fmklab.armnk_results.framework.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import java.util.concurrent.Executors

@Database(
    entities = [
        ControlDetailEntity::class,
        ControlZoneEntity::class,
        DetectorEntity::class,
        FactoryEntity::class,
        BogieModelFactoryEntity::class,
        ControlResultEntity::class,
        DefectTypeEntity::class,
        StealEntity::class,
        MethodEntity::class,
        BogieModelEntity::class
    ],
    version = 2
)
abstract class ArmnkDatabase : RoomDatabase() {

    abstract fun controlDetailDao(): ControlDetailDao

    abstract fun noneNumberDetailDao(): NoneNumberDetailDao

    abstract fun numberDetailDao(): NumberDetailDao

    abstract fun detailDao(): DetailDao

    companion object {
        private var INSTANCE: ArmnkDatabase? = null


        fun getInstance(context: Context): ArmnkDatabase {
            if (INSTANCE == null) {
                INSTANCE = synchronized(this) { buildDatabase(context) }
            }

            return INSTANCE!!
        }

        private fun buildDatabase(context: Context): ArmnkDatabase {
            return Room.databaseBuilder(context, ArmnkDatabase::class.java, "armnk")
                .createFromAsset("armnk.db")
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}

































