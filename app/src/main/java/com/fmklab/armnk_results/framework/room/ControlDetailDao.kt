package com.fmklab.armnk_results.framework.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Single

@Dao
interface ControlDetailDao {

    @Query("SELECT * FROM control_details")
    fun getAll(): Single<List<ControlDetailEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(controlDetails: List<ControlDetailEntity>)
}