package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.intellij.lang.annotations.Identifier

@Entity(tableName = "control_zone")
data class ControlZoneEntity (
    @PrimaryKey(autoGenerate = true)
    val idAuto: Int = 0,
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "part_id")
    val partId: Int,
    @ColumnInfo(name = "defect_moment")
    val defectMoment: Int,
    @ColumnInfo(name = "is_repair")
    val isRepair: Boolean,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "method_str")
    val methodStr: String?,
    @ColumnInfo(name = "timestamp")
    val timestamp: Int
)