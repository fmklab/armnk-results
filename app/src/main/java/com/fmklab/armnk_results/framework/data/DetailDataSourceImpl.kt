package com.fmklab.armnk_results.framework.data

import com.fmklab.armnk_result.data.DetailDataSource
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.NetworkBoundResource
import com.fmklab.armnk_results.framework.NetworkBoundResult
import com.fmklab.armnk_results.framework.retrofit.*
import com.fmklab.armnk_results.framework.room.*
import com.fmklab.armnk_results.framework.utils.SystemUtil
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class DetailDataSourceImpl(
    private val detailApi: DetailApi,
    armnkDatabase: ArmnkDatabase,
    private val currentUser: CurrentUser
) : DetailDataSource {

    private val detailDao = armnkDatabase.detailDao()

    companion object {
        private const val CACHE_TTL = 3600 // seconds
    }

    override fun getDetectors(method: String): Observable<List<Detector>> {
        return object : NetworkBoundResult<List<DetectorEntity>, List<DetectorDto>>() {
            override fun saveCallResult(item: List<DetectorDto>): Single<List<Long>> {
                detailDao.deleteDetectors(method)
                return detailDao.insertDetectors(item.map {
                    DetectorEntity(
                        0,
                        it.id,
                        it.name,
                        it.code,
                        it.methodStr,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<DetectorDto>> {
                return detailApi.getDetectors(
                    currentUser.getUser().company.id,
                    method,
                    currentUser.getAccessToken()
                ).toObservable()
            }

            override fun shouldFetch(item: List<DetectorEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { d -> currentTime - d.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<DetectorEntity>> {
                return detailDao.getDetectors(method).toObservable()
            }

        }.asObservable().map { list ->
            list.map {
                Detector(it.id, it.name, it.code, it.methodStr)
            }
        }.subscribeOn(Schedulers.io())
    }

    override fun getMethods(partId: Int): Observable<List<String>> {
        return object : NetworkBoundResult<List<MethodEntity>, List<String>>() {
            override fun saveCallResult(item: List<String>): Single<List<Long>> {
                detailDao.deleteMethods(partId)
                return detailDao.insertMethods(item.map {
                    MethodEntity(
                        0,
                        it,
                        partId,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<String>> {
                return detailApi.getMethods(partId, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: List<MethodEntity>): Boolean {
                if (item.isEmpty()) return true
                val currentTime = SystemUtil.currentTime()
                return item.any { m -> currentTime - m.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<MethodEntity>> {
                return detailDao.getMethods(partId)
            }
        }.asObservable().map { list ->
            list.map { it.name }
        }
    }

    override fun getFactories(partId: Int): Observable<List<Factory>> {
        return object : NetworkBoundResult<List<FactoryEntity>, List<FactoryDto>>() {
            override fun saveCallResult(item: List<FactoryDto>): Single<List<Long>> {
                detailDao.deleteFactories(partId)
                return detailDao.insertFactories(item.map {
                    FactoryEntity(it.id, it.code, partId, SystemUtil.currentTime(), it.orderField)
                })
            }

            override fun createCall(): Observable<List<FactoryDto>> {
                return detailApi.getFactories(partId, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: List<FactoryEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { f -> currentTime - f.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<FactoryEntity>> {
                return detailDao.getFactories(partId)
            }

        }.asObservable().map { list ->
            list.map {
                Factory(it.id, it.code)
            }
        }
    }

    override fun getBogieModelFactories(partId: Int, bogieModelId: Int): Observable<List<Factory>> {
        return detailApi.getBogieModelFactories(partId, bogieModelId, currentUser.getAccessToken())
            .map { list ->
                list.map {
                    Factory(it.id, it.code)
                }
            }
/*        return object : NetworkBoundResult<List<BogieModelFactoryEntity>, List<FactoryDto>>() {
            override fun saveCallResult(item: List<FactoryDto>): Single<List<Long>> {
                detailDao.deleteBogieModelFactories(partId, bogieModelId)
                return detailDao.insertBogieModelFactories(item.map {
                    BogieModelFactoryEntity(it.id, it.code, partId, bogieModelId, SystemUtil.currentTime(), it.orderField)
                })
            }

            override fun createCall(): Observable<List<FactoryDto>> {
                return detailApi.getBogieModelFactories(
                    partId,
                    bogieModelId,
                    currentUser.getAccessToken()
                )
            }

            override fun shouldFetch(item: List<BogieModelFactoryEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { f -> currentTime - f.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<BogieModelFactoryEntity>> {
                return detailDao.getBogieModelFactories(partId, bogieModelId)
            }
        }.asObservable().map { list ->
            list.map {
                Factory(it.id, it.code)
            }
        }*/
    }

    override fun getYears(factoryId: Int): Observable<List<FactoryYear>> {
        return detailApi.getFactoryYears(factoryId, currentUser.getAccessToken())
            .map { list ->
                list.map { FactoryYear(it) }
            }.subscribeOn(Schedulers.io())
    }

    override fun getControlResults(partId: Int): Observable<List<ControlResult>> {
        return object : NetworkBoundResult<List<ControlResultEntity>, List<ControlResultDto>>() {
            override fun saveCallResult(item: List<ControlResultDto>): Single<List<Long>> {
                detailDao.deleteControlResults(partId)
                return detailDao.insertControlResults(item.map {
                    ControlResultEntity(it.id, it.name, partId, SystemUtil.currentTime())
                })
            }

            override fun createCall(): Observable<List<ControlResultDto>> {
                return detailApi.getControlResults(partId, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: List<ControlResultEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { cr -> currentTime - cr.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<ControlResultEntity>> {
                return detailDao.getControlResults(partId)
            }

        }.asObservable().map { list ->
            list.map {
                ControlResult(it.id, it.name)
            }
        }
    }

    override fun getDefectTypes(
        partId: Int,
        defectMoment: DefectMoment
    ): Observable<List<DefectType>> {
        return object : NetworkBoundResult<List<DefectTypeEntity>, List<DefectTypeDto>>() {
            override fun saveCallResult(item: List<DefectTypeDto>): Single<List<Long>> {
                detailDao.deleteDefectTypes(partId, defectMoment.value, false)
                return detailDao.insertDefectTypes(item.map {
                    DefectTypeEntity(
                        0,
                        it.id,
                        it.name,
                        partId,
                        defectMoment.value,
                        it.needControlZone,
                        false,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<DefectTypeDto>> {
                return detailApi.getDefectTypes(
                    partId,
                    defectMoment,
                    currentUser.getAccessToken()
                )
            }

            override fun shouldFetch(item: List<DefectTypeEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { dt -> currentTime - dt.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<DefectTypeEntity>> {
                return detailDao.getDefectTypes(partId, defectMoment.value)
            }
        }.asObservable().map { list ->
            list.map {
                DefectType(it.id, it.name, it.needControlZone)
            }
        }
    }

    override fun getControlZones(
        partId: Int,
        defectMoment: DefectMoment
    ): Observable<List<ControlZone>> {
        return object : NetworkBoundResult<List<ControlZoneEntity>, List<ControlZoneDto>>() {
            override fun saveCallResult(item: List<ControlZoneDto>): Single<List<Long>> {
                detailDao.deleteControlZones(partId, defectMoment.value, false)
                return detailDao.insertControlZones(item.map {
                    ControlZoneEntity(
                        0,
                        it.id,
                        partId,
                        defectMoment.value,
                        false,
                        it.name,
                        it.methodStr,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<ControlZoneDto>> {
                return detailApi.getControlZones(
                    partId,
                    defectMoment,
                    currentUser.getAccessToken()
                )
            }

            override fun shouldFetch(item: List<ControlZoneEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { cz -> currentTime - cz.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<ControlZoneEntity>> {
                return detailDao.getControlZones(partId, defectMoment.value)
            }
        }.asObservable().map { list ->
            list.map {
                ControlZone(it.id, it.name, it.methodStr)
            }
        }
    }

    override fun getBogieModels(partId: Int): Observable<List<BogieModel>> {
        return object : NetworkBoundResult<List<BogieModelEntity>, List<BogieModelDto>>() {
            override fun saveCallResult(item: List<BogieModelDto>): Single<List<Long>> {
                detailDao.deleteBogieModels(partId)
                return detailDao.insertBogieModels(item.map {
                    BogieModelEntity(
                        it.id,
                        partId,
                        it.name,
                        it.orderField,
                        SystemUtil.currentTime()
                    )
                })
            }

            override fun createCall(): Observable<List<BogieModelDto>> {
                return detailApi.getBogieModels(
                    partId,
                    currentUser.getAccessToken()
                )
            }

            override fun shouldFetch(item: List<BogieModelEntity>): Boolean {
                val currentTime = SystemUtil.currentTime()
                if (item.isEmpty()) return true
                return item.any { bm -> currentTime - bm.timestamp >= CACHE_TTL }
            }

            override fun loadFromDb(): Observable<List<BogieModelEntity>> {
                return detailDao.getBogieModels(partId)
            }
        }.asObservable().map { list ->
            list.map {
                BogieModel(it.id, it.name)
            }
        }
    }
}






































