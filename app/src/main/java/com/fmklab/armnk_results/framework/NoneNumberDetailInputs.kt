package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.Detector
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.NoneNumberDetail

data class NoneNumberDetailInputs (
    val detail: Result<NoneNumberDetail>,
    val methods: Result<List<String>>,
    val controlZones: Result<List<ControlZone>>,
    val detectors: Result<List<Detector>>
) {

    fun isSuccess(): Boolean {
        return detail.isSuccess && methods.isSuccess && controlZones.isSuccess && detectors.isSuccess
    }

    fun getError(): Error {
        return when {
            detail.isError -> detail.error!!
            methods.isError -> methods.error!!
            controlZones.isError -> controlZones.error!!
            detectors.isError -> detectors.error!!
            else -> Error.Unknown("No message")
        }
    }
}