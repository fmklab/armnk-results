package com.fmklab.armnk_results.framework.utils

import com.fmklab.armnk_result.domain.CompanyEnum
import com.fmklab.armnk_result.domain.FaultEnum

class ServicePointUtil {

    companion object {
        fun company(value: Int): CompanyEnum {
            return when (value) {
                CompanyEnum.VRK1.value -> CompanyEnum.VRK1
                CompanyEnum.VRK2.value -> CompanyEnum.VRK2
                CompanyEnum.VRK3.value -> CompanyEnum.VRK3
                CompanyEnum.OTHER.value -> CompanyEnum.OTHER
                else -> CompanyEnum.NONE
            }
        }

        fun fault(value: Int): FaultEnum {
            return when (value) {
                FaultEnum.DEPOT.value -> FaultEnum.DEPOT
                FaultEnum.FACTORY.value -> FaultEnum.FACTORY
                FaultEnum.EXPLOITATION.value -> FaultEnum.EXPLOITATION
                else -> FaultEnum.NONE
            }
        }
    }
}