package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_result.domain.FactoryYear
import com.fmklab.armnk_result.interactors.detail.*
import com.fmklab.armnk_result.interactors.outerring.*

data class OuterRingInteractors (
    val getAllOuterRings: GetAllOuterRings,
    val getOuterRingById: GetOuterRingById,
    val getMethods: GetMethods,
    val getDetectors: GetDetectors,
    val getFactories: GetFactories,
    val getFactoryYear: GetYears,
    val getControlResults: GetControlResults,
    val cacheDetail: CacheDetail,
    val clearPreferencesCache: ClearPreferencesCache,
    val getDefectTypes: GetDefectTypes,
    val getControlZones: GetControlZones,
    val addOuterRing: AddOuterRing,
    val searchOuterRing: SearchOuterRing
)