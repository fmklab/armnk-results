package com.fmklab.armnk_results.framework.extensions

import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.retrofit.OuterRingDto
import com.fmklab.armnk_results.framework.retrofit.OuterRingListItemDto
import com.fmklab.armnk_results.framework.retrofit.UserDto
import com.fmklab.armnk_results.framework.utils.DetailUtil

fun View.setVisible(): View {
    return this.also {
        visibility = View.VISIBLE
    }
}

fun View.setInvisible(): View {
    return this.also {
        visibility = View.INVISIBLE
    }
}

fun View.setGone(): View {
    return this.also {
        visibility = View.GONE
    }
}

fun View.enable(): View {
    return this.also {
        isEnabled = true
    }
}

fun View.disable(): View {
    return this.also {
        isEnabled = false
    }
}

fun EditText.setTextDistinct(text: String) {
    if (text != this.text.toString()) this.setText(text)
}

fun CheckBox.setCheckedDistinct(isChecked: Boolean) {
    if (isChecked != this.isChecked) this.isChecked = isChecked
}

fun RadioButton.setCheckedDistinct(isChecked: Boolean) {
    if (isChecked != this.isChecked) this.isChecked = isChecked
}

fun OuterRingListItemDto.toListItem(): OuterRingListItem {
    return OuterRingListItem(
        id,
        partId,
        controlDate,
        method,
        factoryName,
        FactoryYear(factoryYear),
        number,
        controlResult,
        defectInfo,
        spec,
        editStatus,
        detector,
        createDate,
        isMobileApp
    )
}

fun OuterRingDto.toOuterRing(): OuterRing {
    return OuterRing(id, partId)
        .controlDate(controlDate)
        .method(method ?: "")
        .detector(Detector.default(detector ?: "", method ?: ""))
        .factory(Factory.default(factoryId))
        .factoryYear(FactoryYear(factoryYearValue))
        .number(number ?: "")
        .controlResult(ControlResult.default(controlResultId))
        .defectMoment(DetailUtil.defectMoment(defectMoment))
        .defectType(DefectType.default(defectTypeId))
        .controlZone(ControlZone.default(controlZoneId))
        .defectDescription(defectDescription ?: "")
        .spec(spec ?: "")
        .defectInfo(defectInfo ?: "")
}

fun OuterRing.toDto(): OuterRingDto {
    return OuterRingDto(
        id,
        partId,
        controlDate,
        method,
        detector?.toString() ?: "",
        factory?.id ?: 0,
        factoryYear?.value ?: -1,
        number,
        controlResult?.id ?: 0,
        defectMoment.value,
        defectType?.id ?: 0,
        controlZone?.id ?: 0,
        defectDescription,
        defectInfo,
        spec,
        controlResult?.name
    )
}

fun User.toDto(): UserDto {
    return UserDto(
        id,
        name1,
        name2,
        name3,
        company.id,
        company.name
    )
}





































