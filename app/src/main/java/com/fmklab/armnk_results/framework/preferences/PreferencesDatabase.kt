package com.fmklab.armnk_results.framework.preferences

import android.content.Context
import com.google.gson.Gson

class PreferencesDatabase(context: Context, private val gson: Gson) {

    private val preferences = context.getSharedPreferences("preferencesDatabase", Context.MODE_PRIVATE)

    fun <T> insert(data: T, key: String) {
        val json = gson.toJson(data)
        val editor = preferences.edit()
        editor.putString(key, json)
        editor.apply()
    }

    fun <T> get(key: String, ofClass: Class<T>, defValue: T): T {
        val json = preferences.getString(key, null)
        return if (json == null) {
            defValue
        } else {
            return gson.fromJson(json, ofClass)
        }
    }

    fun remove(key: String) {
        val editor = preferences.edit()
        editor.remove(key)
        editor.apply()
    }
}