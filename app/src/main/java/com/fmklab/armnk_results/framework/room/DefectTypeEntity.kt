package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fmklab.armnk_result.domain.DefectMoment

@Entity(tableName = "defect_type")
data class DefectTypeEntity (
    @PrimaryKey(autoGenerate = true)
    val autoId: Long = 0,
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "part_id")
    val partId: Int,
    @ColumnInfo(name = "defect_moment")
    val defectMoment: Int,
    @ColumnInfo(name = "need_control_zone")
    val needControlZone: Boolean,
    @ColumnInfo(name = "is_repair")
    val isRepair: Boolean,
    @ColumnInfo(name = "timestamp")
    val timestamp: Int
)