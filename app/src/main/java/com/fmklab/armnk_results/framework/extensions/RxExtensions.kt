package com.fmklab.armnk_results.framework.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.modules.common.ApiErrorHandler
import com.fmklab.armnk_results.modules.common.DataResponse
import com.jakewharton.rxrelay3.BehaviorRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.net.ConnectException
import java.net.UnknownHostException


inline fun <T, reified E : T> Observable<in T>.filterTo(classOf: Class<E>): Observable<out E> =
    this.filter {
        when(it) {
            is E -> true
            else -> false
        }
    }.map { it as E }

fun <T> Observable<T>.toResult(errorHandler: ErrorHandler): Observable<Result<T>> {
    return this
        .map { Result.success(it) }
        .onErrorReturn { Result.error(errorHandler.getError(it)) }
}

fun <T> Observable<T>.toResultApi(noInternetConnection: BehaviorRelay<Boolean>, errorHandler: ErrorHandler): Observable<Result<T>> where T: Any{
    return this
        .subscribeOn(Schedulers.io())
        .map { data -> Result.success(data) }
        .retryWhen { observable ->
            observable.catchConnectException().map { isConnectException ->
                noInternetConnection.accept(isConnectException)
            }
        }
        .onErrorResumeNext { t ->
            Observable.just(Result.error(errorHandler.getError(t)))
        }
        .doOnNext { noInternetConnection.accept(false) }
}

fun <T> Observable<T>.toResultApi(errorHandler: ErrorHandler): Observable<Result<T>> where T: Any {
    return this
        .subscribeOn(Schedulers.io())
        .map { data -> Result.success(data) }
        .onErrorResumeNext { t ->
            Observable.just(Result.error(errorHandler.getError(t)))
        }
}

fun Observable<Throwable>.catchConnectException(): Observable<Boolean> {
    return this.flatMap { throwable ->
        if (throwable is ConnectException || throwable is UnknownHostException) {
            Observable.just(true)
        } else {
            Observable.error<Boolean>(throwable)
        }
    }
}



































