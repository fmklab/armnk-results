package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddOuterRingRequest (
    @SerializedName("outerRingDto")
    @Expose
    val outerRingDto: OuterRingDto,
    @SerializedName("user")
    @Expose
    val user: UserDto,
    @SerializedName("appVersion")
    @Expose
    val appVersion: Int
)