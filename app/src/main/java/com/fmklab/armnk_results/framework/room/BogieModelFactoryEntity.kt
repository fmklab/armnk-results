package com.fmklab.armnk_results.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bogie_model_factory")
data class BogieModelFactoryEntity(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "code")
    val code: String,
    @ColumnInfo(name = "part_id")
    val partId: Int,
    @ColumnInfo(name = "bogie_model_id")
    val bogieModelId: Int,
    @ColumnInfo(name = "timestamp")
    val timestamp: Int,
    @ColumnInfo(name = "order_field")
    val orderField: Int
)