package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.ControlResult
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.Factory
import com.fmklab.armnk_result.domain.FactoryYear
import com.fmklab.armnk_result.domain.NumberDetail

data class NumberDetailInputs (
    val detail: Result<NumberDetail>,
    val factories: Result<List<Factory>>,
    val factoryYears: Result<List<FactoryYear>>,
    val controlResults: Result<List<ControlResult>>
) {

    fun isSuccess(): Boolean {
        return detail.isSuccess && factories.isSuccess && factoryYears.isSuccess && controlResults.isSuccess
    }

    fun getError(): Error {
        return when {
            detail.isError -> detail.error!!
            factories.isError -> factories.error!!
            factoryYears.isError -> factoryYears.error!!
            controlResults.isError -> controlResults.error!!
            else -> Error.Unknown("No message")
        }
    }
}