package com.fmklab.armnk_results.framework.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fmklab.armnk_result.domain.DefectMoment
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface NumberDetailDao {

    @Query("SELECT * FROM defect_type WHERE is_repair = 1 AND part_id = :partId")
    fun getRepairDefectTypes(partId: Int): Observable<List<DefectTypeEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertDefectTypes(defectTypes: List<DefectTypeEntity>): Single<List<Long>>

    @Query("SELECT * FROM control_zone WHERE is_repair = 1 AND part_id = :partId")
    fun getRepairControlZones(partId: Int): Observable<List<ControlZoneEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertControlZones(controlZones: List<ControlZoneEntity>): Single<List<Long>>

    @Query("DELETE FROM control_zone WHERE is_repair = :isRepair AND part_id = :partId AND defect_moment = :defectMoment")
    fun deleteControlZones(partId: Int, defectMoment: Int, isRepair: Boolean)

    @Query("DELETE FROM defect_type WHERE is_repair = :isRepair AND part_id = :partId AND defect_moment = :defectMoment")
    fun deleteDefectTypes(partId: Int, defectMoment: Int, isRepair: Boolean)

    @Query("SELECT * FROM defect_type WHERE is_repair = 0 AND part_id = :partId AND defect_moment = :defectMoment")
    fun getDefectTypes(partId: Int, defectMoment: Int): Observable<List<DefectTypeEntity>>

    @Query("SELECT * FROM control_zone WHERE is_repair = 0 AND part_id = :partId AND defect_moment = :defectMoment")
    fun getControlZones(partId: Int, defectMoment: Int): Observable<List<ControlZoneEntity>>

    @Query("SELECT * FROM steal")
    fun getSteals(): Observable<List<StealEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSteals(steals: List<StealEntity>): Single<List<Long>>

    @Query("DELETE FROM steal")
    fun deleteSteals()

    @Query("SELECT * FROM detector WHERE method_str = :method")
    fun getDetectors(method: String): Observable<List<DetectorEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertDetectors(detectors: List<DetectorEntity>): Single<List<Long>>

    @Query("DELETE FROM detector WHERE method_str = :method")
    fun deleteDetectors(method: String)
}





































