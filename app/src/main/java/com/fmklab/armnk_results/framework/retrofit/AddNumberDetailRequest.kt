package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddNumberDetailRequest (
    @SerializedName("numberDetailDto")
    @Expose
    val numberDetailDto: NumberDetailDto,
    @SerializedName("user")
    @Expose
    val userDto: UserDto,
    @SerializedName("appVersion")
    @Expose
    val appVersion: Int
)