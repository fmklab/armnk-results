package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.*

class Result<T> (val data: T?, val error: Error?, val isSuccess: Boolean, val isError: Boolean) {

    companion object {
        fun <T> success(data: T): Result<T> {
            return Result(
                data,
                null,
                isSuccess = true,
                isError = false
            )
        }

        fun <T> error(error: Error): Result<T> {
            return Result(
                null,
                error,
                isSuccess = false,
                isError = true
            )
        }
    }
}