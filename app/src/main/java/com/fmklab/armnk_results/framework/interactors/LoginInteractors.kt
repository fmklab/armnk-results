package com.fmklab.armnk_results.framework.interactors

import com.fmklab.armnk_results.framework.interactors.Login


data class LoginInteractors (
    val login: Login
)