package com.fmklab.armnk_results.framework.retrofit

import com.fmklab.armnk_result.domain.BogieNumberDetail
import com.fmklab.armnk_result.domain.DefectMoment
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface DetailApi {

    companion object {
        private const val ROUTE = "detail"
    }

    @GET("${ROUTE}/detectors")
    fun getDetectors(
        @Query("companyId") companyId: Int,
        @Query("method") method: String,
        @Header("Authorization") accessToken: String
    ): Single<List<DetectorDto>>

    @GET("${ROUTE}/methods")
    fun getMethods(
        @Query("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<String>>

    @GET("${ROUTE}/factories")
    fun getFactories(
        @Query("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<FactoryDto>>

    @GET("${ROUTE}/bmfactories")
    fun getBogieModelFactories(
        @Query("partId") partId: Int,
        @Query("bogieModelId") bogieModelId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<FactoryDto>>

    @GET("${ROUTE}/years")
    fun getFactoryYears(
        @Query("factoryId") factoryId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<Int>>

    @GET("${ROUTE}/results")
    fun getControlResults(
        @Query("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<ControlResultDto>>

    @GET("${ROUTE}/deftypes")
    fun getDefectTypes(
        @Query("partId") partId: Int,
        @Query("defectMoment") defectMoment: DefectMoment,
        @Header("Authorization") accessToken: String
    ): Observable<List<DefectTypeDto>>

    @GET("${ROUTE}/cz")
    fun getControlZones(
        @Query("partId") partId: Int,
        @Query("defectMoment") defectMoment: DefectMoment,
        @Header("Authorization") accessToken: String
    ): Observable<List<ControlZoneDto>>

    @GET("$ROUTE/bogiemodels")
    fun getBogieModels(
        @Query("partId") partId: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<BogieModelDto>>
}




























