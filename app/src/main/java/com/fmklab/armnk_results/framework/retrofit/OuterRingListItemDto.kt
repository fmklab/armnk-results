package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class OuterRingListItemDto (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("method")
    @Expose
    val method: String,
    @SerializedName("factory")
    @Expose
    val factoryName: String,
    @SerializedName("factoryYear")
    @Expose
    val factoryYear: Int,
    @SerializedName("number")
    @Expose
    val number: String,
    @SerializedName("result")
    @Expose
    val controlResult: String,
    @SerializedName("defectInfo")
    @Expose
    val defectInfo: String,
    @SerializedName("spec")
    @Expose
    val spec: String,
    @SerializedName("editStatus")
    @Expose
    val editStatus: Boolean,
    @SerializedName("detector")
    @Expose
    val detector: String,
    @SerializedName("createDate")
    @Expose
    val createDate: Date,
    @SerializedName("isMobileApp")
    @Expose
    val isMobileApp: Boolean
)





































