package com.fmklab.armnk_results.framework.data

import com.fmklab.armnk_result.data.OuterRingDataSource
import com.fmklab.armnk_result.domain.OuterRing
import com.fmklab.armnk_result.domain.OuterRingListItem
import com.fmklab.armnk_results.BuildConfig
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.NetworkBoundResult
import com.fmklab.armnk_results.framework.extensions.toDto
import com.fmklab.armnk_results.framework.extensions.toListItem
import com.fmklab.armnk_results.framework.extensions.toOuterRing
import com.fmklab.armnk_results.framework.preferences.PreferencesDatabase
import com.fmklab.armnk_results.framework.retrofit.AddOuterRingRequest
import com.fmklab.armnk_results.framework.retrofit.OuterRingApi
import com.fmklab.armnk_results.framework.retrofit.OuterRingDto
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class OuterRingDataSourceImpl(
    private val currentUser: CurrentUser,
    private val outerRingApi: OuterRingApi,
    private val preferencesDatabase: PreferencesDatabase
) : OuterRingDataSource {

    companion object {
        private const val CACHE_TTL = 3600
        private const val OUTER_RING_KEY = "OuterRingKey"
    }

    override fun getAll(partId: Int, page: Int): Observable<List<OuterRingListItem>> {
        return outerRingApi.getAll(
            partId,
            currentUser.getUser().getCompanyId(),
            page,
            currentUser.getAccessToken()
        )
            .map { list ->
                list.map { it.toListItem() }
            }
            .subscribeOn(Schedulers.io())
    }

    override fun getById(id: Int): Observable<OuterRing> {
        return object : NetworkBoundResult<OuterRingDto, OuterRingDto>() {
            override fun saveCallResult(item: OuterRingDto): Single<List<Long>> {
                return Single.just(item)
                    .flatMap {
                        preferencesDatabase.insert(it, OUTER_RING_KEY)
                        Single.just(List(1) { 1L })
                    }
            }

            override fun createCall(): Observable<OuterRingDto> {
                return outerRingApi.getById(id, currentUser.getAccessToken())
            }

            override fun shouldFetch(item: OuterRingDto): Boolean {
                return item == OuterRingDto.DEFAULT
            }

            override fun loadFromDb(): Observable<OuterRingDto> {
                return Observable.just(
                    preferencesDatabase.get(
                        OUTER_RING_KEY,
                        OuterRingDto::class.java,
                        OuterRingDto.DEFAULT
                    )
                )
            }
        }.asObservable().map {
            it.toOuterRing()
        }.subscribeOn(Schedulers.io())
    }

    override fun cacheDetail(outerRing: OuterRing) {
        preferencesDatabase.insert(
            outerRing.toDto(), OUTER_RING_KEY
        )
    }

    override fun clearPreferencesCache() {
        preferencesDatabase.remove(OUTER_RING_KEY)
    }

    override fun add(outerRing: OuterRing): Observable<Int> {
        val user = currentUser.getUser()
        return outerRingApi.add(
            AddOuterRingRequest(
                outerRing.toDto(),
                user.toDto(),
                BuildConfig.VERSION_CODE
            ), currentUser.getAccessToken()
        )
            .subscribeOn(Schedulers.io())
    }

    override fun search(partId: Int, number: String): Observable<List<OuterRingListItem>> {
        return outerRingApi.search(
            currentUser.getUser().getCompanyId(),
            partId,
            number,
            currentUser.getAccessToken()
        ).map { list -> list.map { it.toListItem() } }
            .subscribeOn(Schedulers.io())
    }
}





































