package com.fmklab.armnk_results.framework.retrofit

import com.fmklab.armnk_results.framework.InsertNoneNumberDetailRequest
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*

interface NoneNumberDetailApi {

    companion object {
        private const val ROUTE = "nnd"
    }

    @GET("$ROUTE/all")
    fun getAllPaged(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("page") page: Int,
        @Header("Authorization") accessToken: String
    ): Observable<List<NoneNumberDetailDto>>

    @GET("$ROUTE/{id}")
    fun getById(
        @Path("id") id: Int,
        @Header("Authorization") accessToken: String
    ): Observable<NoneNumberDetailDto>

    @GET("$ROUTE/cz/{partId}/{method}")
    fun getControlZones(
        @Path("partId") partId: Int,
        @Path("method") method: String,
        @Header("Authorization") accessToken: String
    ): Observable<List<ControlZoneDto>>

    @PUT(ROUTE)
    fun insert(
        @Body request: InsertNoneNumberDetailRequest,
        @Header("Authorization") accessToken: String
    ): Observable<Int>
}







































