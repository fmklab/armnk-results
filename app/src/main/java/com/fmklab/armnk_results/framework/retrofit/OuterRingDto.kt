package com.fmklab.armnk_results.framework.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class OuterRingDto (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("method")
    @Expose
    val method: String?,
    @SerializedName("detector")
    @Expose
    val detector: String?,
    @SerializedName("factoryId")
    @Expose
    val factoryId: Int,
    @SerializedName("factoryYearValue")
    @Expose
    val factoryYearValue: Int,
    @SerializedName("number")
    @Expose
    val number: String?,
    @SerializedName("controlResultId")
    @Expose
    val controlResultId: Int,
    @SerializedName("defectMoment")
    @Expose
    val defectMoment: Int,
    @SerializedName("defectTypeId")
    @Expose
    val defectTypeId: Int,
    @SerializedName("controlZoneId")
    @Expose
    val controlZoneId: Int,
    @SerializedName("defectDescription")
    @Expose
    val defectDescription: String?,
    @SerializedName("defectInfo")
    @Expose
    val defectInfo: String?,
    @SerializedName("spec")
    @Expose
    val spec: String?,
    @SerializedName("controlResultName")
    @Expose
    val controlResultName: String?
) {

    companion object {
        val DEFAULT = OuterRingDto(
            id = 0,
            partId = 0,
            controlDate = Date(),
            method = "",
            detector = "",
            number = "",
            factoryYearValue = -1,
            factoryId = 0,
            defectDescription = "",
            defectMoment = 0,
            controlZoneId = 0,
            defectTypeId = 0,
            controlResultId = 0,
            spec = "",
            defectInfo = "",
            controlResultName = ""
        )
    }
}






































