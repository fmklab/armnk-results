package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectType
import com.fmklab.armnk_result.domain.Error

data class NumberDetailVisualInputs(
    val defectTypes: Result<List<DefectType>>,
    val controlZones: Result<List<ControlZone>>,
    val steals: Result<List<String>>
) {

    fun isSuccess() = defectTypes.isSuccess && controlZones.isSuccess && steals.isSuccess

    fun getError(): Error {
        return when {
            defectTypes.isError -> defectTypes.error!!
            controlZones.isError -> controlZones.error!!
            steals.isError -> steals.error!!
            else -> Error.Unknown("No message")
        }
    }
}