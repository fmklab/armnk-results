package com.fmklab.armnk_results.framework.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface DetailDao {

    @Query("SELECT * FROM detector WHERE method_str = :method")
    fun getDetectors(method: String): Single<List<DetectorEntity>>

    @Query("DELETE FROM detector")
    fun deleteAllDetectors()

    @Query("DELETE FROM detector WHERE method_str = :method")
    fun deleteDetectors(method: String)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertDetectors(detectors: List<DetectorEntity>): Single<List<Long>>

    @Query("SELECT * FROM method WHERE part_id = :partId")
    fun getMethods(partId: Int): Observable<List<MethodEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMethods(methods: List<MethodEntity>): Single<List<Long>>

    @Query("DELETE FROM method WHERE part_id = :partId")
    fun deleteMethods(partId: Int)

    @Query("SELECT * FROM factory WHERE part_id = :partId ORDER BY order_field")
    fun getFactories(partId: Int): Observable<List<FactoryEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFactories(factories: List<FactoryEntity>): Single<List<Long>>

    @Query("DELETE FROM factory WHERE part_id = :partId")
    fun deleteFactories(partId: Int)

    @Query("SELECT * FROM bogie_model_factory WHERE part_id = :partId AND bogie_model_id = :bogieModelId ORDER BY order_field")
    fun getBogieModelFactories(partId: Int, bogieModelId: Int): Observable<List<BogieModelFactoryEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertBogieModelFactories(factories: List<BogieModelFactoryEntity>): Single<List<Long>>

    @Query("DELETE FROM bogie_model_factory WHERE part_id = :partId AND bogie_model_id = :bogieModelId")
    fun deleteBogieModelFactories(partId: Int, bogieModelId: Int)

    @Query("SELECT * FROM control_result WHERE part_id = :partId")
    fun getControlResults(partId: Int): Observable<List<ControlResultEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertControlResults(controlResults: List<ControlResultEntity>): Single<List<Long>>

    @Query("DELETE FROM control_result WHERE part_id = :partId")
    fun deleteControlResults(partId: Int)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertDefectTypes(defectTypes: List<DefectTypeEntity>): Single<List<Long>>

    @Query("DELETE FROM defect_type WHERE is_repair = :isRepair AND part_id = :partId AND defect_moment = :defectMoment")
    fun deleteDefectTypes(partId: Int, defectMoment: Int, isRepair: Boolean)

    @Query("SELECT * FROM defect_type WHERE is_repair = 0 AND part_id = :partId AND defect_moment = :defectMoment")
    fun getDefectTypes(partId: Int, defectMoment: Int): Observable<List<DefectTypeEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertControlZones(controlZones: List<ControlZoneEntity>): Single<List<Long>>

    @Query("DELETE FROM control_zone WHERE is_repair = :isRepair AND part_id = :partId AND defect_moment = :defectMoment")
    fun deleteControlZones(partId: Int, defectMoment: Int, isRepair: Boolean)

    @Query("SELECT * FROM control_zone WHERE is_repair = 0 AND part_id = :partId AND defect_moment = :defectMoment")
    fun getControlZones(partId: Int, defectMoment: Int): Observable<List<ControlZoneEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertBogieModels(bogieModels: List<BogieModelEntity>): Single<List<Long>>

    @Query("DELETE FROM bogie_models WHERE part_id = :partId")
    fun deleteBogieModels(partId: Int)

    @Query("SELECT * FROM bogie_models WHERE part_id = :partId ORDER BY order_field")
    fun getBogieModels(partId: Int): Observable<List<BogieModelEntity>>
}