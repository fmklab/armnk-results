package com.fmklab.armnk_results.framework

import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_result.interactors.detail.GetControlResults

data class OuterRingInputs(
    val outerRing: Result<OuterRing>,
    val methods: Result<List<String>>,
    val detectors: Result<List<Detector>>,
    val factories: Result<List<Factory>>,
    val factoryYears: Result<List<FactoryYear>>,
    val controlResults: Result<List<ControlResult>>
) {

    fun isSuccess(): Boolean {
        return outerRing.isSuccess && methods.isSuccess && detectors.isSuccess && factories.isSuccess && factoryYears.isSuccess && controlResults.isSuccess
    }

    fun getError(): Error {
        return when {
            outerRing.isError -> outerRing.error!!
            methods.isError -> methods.error!!
            detectors.isError -> detectors.error!!
            factories.isError -> factories.error!!
            factoryYears.isError -> factoryYears.error!!
            controlResults.isError -> controlResults.error!!
            else -> Error.Unknown("No message")
        }
    }
}