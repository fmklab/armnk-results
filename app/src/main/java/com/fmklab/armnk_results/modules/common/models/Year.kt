package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Year (
    @SerializedName("value")
    @Expose
    val value: Int = DEFAULT_VALUE
) {

    companion object {
        const val DEFAULT_VALUE = -1
    }

    override fun toString(): String {
        return if (value == -1) {
            "Нет"
        } else {
            value.toString()
        }
    }
}