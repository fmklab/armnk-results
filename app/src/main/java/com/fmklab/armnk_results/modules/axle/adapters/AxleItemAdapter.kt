package com.fmklab.armnk_results.modules.axle.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.models.Axle
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.common.models.RecyclerItem
import kotlinx.android.synthetic.main.axle_item.view.*
import kotlinx.android.synthetic.main.refresh_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AxleItemAdapter: DetailItemAdapter<Axle>() {

    override fun inflateView(parent: ViewGroup, viewType: Int): View {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_ITEM -> inflater.inflate(R.layout.axle_item, parent, false)
            VIEW_LOADING -> inflater.inflate(R.layout.progress_item, parent, false)
            else -> inflater.inflate(R.layout.refresh_item, parent, false)
        }
    }

    override fun onBindViewHolder(holder: BaseDetailViewHolder<Axle>, position: Int) {
        if (holder is ItemDetailViewHolder) {
            val axleItem = getItem(position)!!
            holder.itemView.axleNumberValueTextView.text = axleItem.axleNumber
            holder.itemView.factoryNameValueTextView.text = axleItem.factory.code
            holder.itemView.factoryYearValueTextView.text = axleItem.factoryYear
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            holder.itemView.controlDateTextView.text = simpleDateFormat.format(axleItem.controlDate)
            holder.itemView.axleStatusTextView.text = axleItem.result
            when (axleItem.getResult()) {
                true -> holder.itemView.axleStatusTextView.setBackgroundColor(Color.parseColor("#2b9432"))
                false -> holder.itemView.axleStatusTextView.setBackgroundColor(Color.parseColor("#b03636"))
            }
            when (axleItem.isMobileApp) {
                true -> Glide.with(holder.itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_smartphone_24)
                    .into(holder.itemView.sourceImageView)
                false -> Glide.with(holder.itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_computer_24)
                    .into(holder.itemView.sourceImageView)
            }
            val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
            holder.itemView.createDateTextView.text = createDateFormat.format(axleItem.createDate)
        }
    }
}





















