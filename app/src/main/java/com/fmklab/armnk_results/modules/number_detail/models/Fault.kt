package com.fmklab.armnk_results.modules.number_detail.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

enum class Fault(value: Int) {

    @SerializedName("0")
    @Expose
    NONE(0),
    @SerializedName("1")
    @Expose
    DEPOT(1),
    @SerializedName("2")
    @Expose
    FACTORY(2),
    @SerializedName("3")
    @Expose
    EXPLOITATION(3)
}