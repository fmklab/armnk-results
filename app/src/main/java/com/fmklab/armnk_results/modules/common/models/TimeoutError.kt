package com.fmklab.armnk_results.modules.common.models

class TimeoutError: Error("Время ожидания ответа от сервера истекло")