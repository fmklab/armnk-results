package com.fmklab.armnk_results.modules.number_detail.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit2.SkipCallbackExecutor

enum class DefectMoment(value: Int) {

    @SerializedName("0")
    @Expose
    NONE(0),
    @SerializedName("1")
    @Expose
    VISUAL(1),
    @SerializedName("2")
    @Expose
    NDT(2),
    @SerializedName("3")
    @Expose
    DEFECATION(3)
}