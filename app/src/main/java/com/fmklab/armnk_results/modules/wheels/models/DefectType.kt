package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DefectType (
    @SerializedName("id")
    @Expose
    val id: Int = DEFAULT_ID,
    @SerializedName("name")
    @Expose
    val name: String = ""
) {

    companion object {
        const val DEFAULT_ID = -1
    }

    override fun toString(): String {
        return name
    }
}