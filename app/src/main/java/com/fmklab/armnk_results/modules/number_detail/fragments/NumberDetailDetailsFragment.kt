package com.fmklab.armnk_results.modules.number_detail.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.number_detail.activities.EditNumberDetailActivity
import com.fmklab.armnk_results.modules.number_detail.activities.NumberDetailActivity
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailListItem
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_number_detail_details.*
import java.text.SimpleDateFormat
import java.util.*

class NumberDetailDetailsFragment : BottomSheetDialogFragment() {

    private lateinit var detail: NumberDetailListItem

    companion object {
        private const val NUMBER_DETAIL_KEY = "NumberDetailKey"

        fun newInstance(detail: NumberDetailListItem, name: String?): NumberDetailDetailsFragment {
            val fragment = NumberDetailDetailsFragment()
            val bundle = Bundle(2)
            val json = Gson().toJson(detail)
            bundle.putString(NUMBER_DETAIL_KEY, json)
            bundle.putString(NumberDetailActivity.PART_NAME_KEY, name)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupData()
        setupListeners()
    }

    private fun setupData() {
        val json = arguments?.getString(NUMBER_DETAIL_KEY)
        if (json != null) {
            detail = Gson().fromJson(json, NumberDetailListItem::class.java)
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            numberDetailDetailsToolbar.title =
                "${getString(R.string.detail_number)} ${detail.number}"
            controlDateValueTextView.text = simpleDateFormat.format(detail.controlDate)
            factoryNameValueTextView.text = detail.factory.code
            factoryYearValueTextView.text = detail.factoryYear.toString()
            resultValueTextView.text = detail.controlResult.name
            defectInfoValueTextView.text = detail.defectInfo
            specValueTextView.text = detail.spec

            if (detail.isFromServicePoint) {
                servicePointImageView.visibility = View.VISIBLE
            } else {
                servicePointImageView.visibility = View.GONE
            }

            if (detail.noRejectionCriteria) {
                noRejectionCriteriaImageView.visibility = View.VISIBLE
            } else {
                noRejectionCriteriaImageView.visibility = View.GONE
            }
        }
    }

    private fun setupListeners() {
        editNumberDetailFab.setOnClickListener {
            if (detail.editStatus) {
                val intent = Intent(requireActivity(), EditNumberDetailActivity::class.java).apply {
                    putExtra(EditNumberDetailActivity.NUMBER_DETAIL_ID, detail.id)
                    putExtra(
                        NumberDetailActivity.PART_NAME_KEY,
                        arguments?.getString(NumberDetailActivity.PART_NAME_KEY)
                    )
                }
                startActivity(intent)
            } else {
                Snackbar.make(
                    editMessageCoordinatorLayout,
                    getString(R.string.edit_none_number_detail_error),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }
}































