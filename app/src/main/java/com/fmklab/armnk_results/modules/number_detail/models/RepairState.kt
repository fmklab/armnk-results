package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.wheels.models.DefectType

class RepairState(defectType: DefectType?, zone: ControlZone?, repairDescription: String?) :
    NumberDetailState(defectType, zone) {

    constructor(state: NumberDetailState?) : this(
        state?.defectType,
        state?.zone,
        state?.repairDescription
    )

    override var repairDescription: String? = repairDescription
        get() = field

    override fun getDescription(): String {
        return "тип дефекта: \"${defectType?.name}\"; зона выявления: \"${zone?.name}\"; описание дефекта: \"$repairDescription\";"
    }
}