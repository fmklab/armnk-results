package com.fmklab.armnk_results.modules.axle.models

import com.fmklab.armnk_results.modules.common.models.Detector
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NtdAxleDefectResponse (
    @SerializedName("axleDefects")
    @Expose
    val axleDefects: List<AxleDefect>,
    @SerializedName("detectors")
    @Expose
    val detectors: List<Detector>
)