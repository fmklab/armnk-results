package com.fmklab.armnk_results.modules.axle.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.fragments.AxleDetailsFragment
import com.fmklab.armnk_results.modules.axle.adapters.AxleItemAdapter
import com.fmklab.armnk_results.modules.axle.models.Axle
import com.fmklab.armnk_results.modules.common.listeners.PageRecyclerViewOnScrollListener
import com.fmklab.armnk_results.modules.common.models.RecyclerItem
import com.fmklab.armnk_results.modules.axle.repositories.AxleRepository
import com.fmklab.armnk_results.modules.axle.viewmodels.AxleViewModel
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.activities.DetailItemsActivity
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_axle.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf

class AxleActivity : DetailItemsActivity<Axle>() {

    override val viewModel: DetailItemsViewModel<Axle> by lifecycleScope.viewModel<AxleViewModel>(this)
    override val detailsAdapter: DetailItemAdapter<Axle> by lifecycleScope.inject<AxleItemAdapter>()
    override val recyclerViewLayoutManager: LinearLayoutManager = LinearLayoutManager(this)
    override var detailsFragment: BottomSheetDialogFragment? = null

    companion object {
        private const val AXLE_DETAILS_BOTTOM_SHEET_FRAGMENT_TAG = "AxleDetails"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_axle)

        setupUI()
        setupListeners()
        setupData()
        subscribeObservers()
        viewModel.loadDetails(0)
    }

    override fun setupUI() {
        recyclerView = findViewById(R.id.axlesRecyclerView)
        swipeRefreshLayout = findViewById(R.id.axleSwipeRefreshLayout)
        addNewDetailFab = findViewById(R.id.addNewAxleFab)
        toolBar = findViewById(R.id.axleToolbar)
        emptyDataTextView = findViewById(R.id.emptyDataTextView)
        title = getString(R.string.axles)

        super.setupUI()
    }

    override fun showDetails(position: Int) {
        val axle = detailsAdapter.getItem(position)
        if (axle != null) {
            detailsFragment = AxleDetailsFragment.newInstance(axle)
            if (!supportFragmentManager.fragments.any { frg -> frg.tag == AXLE_DETAILS_BOTTOM_SHEET_FRAGMENT_TAG }) {
                detailsFragment?.show(
                    supportFragmentManager,
                    AXLE_DETAILS_BOTTOM_SHEET_FRAGMENT_TAG
                )
            }
        }
    }

    override fun addNewDetail() {
        val intent = Intent(this, EditAxleActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()

        val editedAxle = AxleRepository(this).getEditedAxle()
        if (editedAxle != null) {
            //viewModel.addOrUpdateDetail(editedAxle)
            detailsAdapter.addOrUpdateItem(editedAxle)
        }
    }

//    private val compositeDisposable = CompositeDisposable()
//    private val viewModel: AxleViewModel by lifecycleScope.viewModel(this)
//    private val axleItemAdapter: AxleItemAdapter by lifecycleScope.inject {
//        parametersOf(this, this)
//    }
//    private lateinit var axleDetails: BottomSheetBehavior<ConstraintLayout>
//    private var firstStart = true
//    private var axleDetailsFragment: AxleDetailsFragment? = null
//    private val linearLayoutManager = LinearLayoutManager(this)
//
//    private var isFirstLoading = true
//    private var isLoadingFirstPage = true
//    private var isLoadingAnotherPage = false
//
//
//    private val recyclerViewOnScrollListener = object : PageRecyclerViewOnScrollListener(linearLayoutManager) {
//        override fun onLoadMore(currentPage: Int) {
//            axleItemAdapter.addItemWithStatus(RecyclerItem.loading())
//            viewModel.load(currentPage)
//            isLoadingAnotherPage = true
//        }
//    }
//    private var onAxleAddListener = object : AxleItemAdapter.OnItemAddListener {
//        override fun onItemAdded() {
//            val firstItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition()
//            if (firstItemPosition == 0) {
//                axlesRecyclerView.smoothScrollToPosition(0)
//            }
//        }
//    }
//
//    companion object {
//        private const val AXLE_DETAILS_BOTTOM_SHEET_FRAGMENT_TAG = "AxleDetails"
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_axle)
//
//        setupUI()
//        setupData()
//        setupBindings()
//    }
//
//    private fun setupUI() {
//        setSupportActionBar(axleToolbar)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        axlesRecyclerView.layoutManager = linearLayoutManager
//        axlesRecyclerView.setHasFixedSize(true)
//    }
//
//    private fun setupData() {
//        axlesRecyclerView.adapter = axleItemAdapter
//    }
//
//    private fun setupBindings() {
//        viewModel.axleList
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeBy(onNext = {
//                when (it.status) {
//                    DataResponse.Status.LOADING -> {
//                        if (axleItemAdapter.itemCount == 0 && isFirstLoading) {
//                            showUpdate(false, null)
//                            showEmpty(false)
//                            showLoading(true)
//                        }
//                        if (isLoadingAnotherPage) {
//                            axleItemAdapter.addItemWithStatus(RecyclerItem.loading())
//                        }
//                    }
//                    DataResponse.Status.SUCCESS -> {
//                        showLoading(false)
//                        if (isFirstLoading) {
//                            isFirstLoading = false
//                        }
//                        if (it.data == null) {
//                            showEmpty(true)
//                            isFirstLoading = true
//                        } else {
//                            if (isLoadingFirstPage) {
//                                isLoadingFirstPage = false
//                                recyclerViewOnScrollListener.reset()
//                                axleItemAdapter.clearItemsWithoutNotify()
//                            }
//                            axleItemAdapter.appendAxleItems(it.data)
//                        }
//                    }
//                    DataResponse.Status.ERROR -> {
//                        showLoading(false)
//                        if (isFirstLoading) {
//                            showUpdate(true, it.error?.message)
//                        } else {
//                            if (isLoadingFirstPage) {
//                                toast(it.error?.message)
//                            }
//                            if (isLoadingAnotherPage) {
//                                axleItemAdapter.addItemWithStatus(RecyclerItem.error())
//                            }
//                        }
//                    }
//                }
//            }).addTo(compositeDisposable)
//        axleSwipeRefreshLayout.setOnRefreshListener {
//            isLoadingFirstPage = true
//            isLoadingAnotherPage = false
//            viewModel.update()
//        }
//        axleRefreshButton.setOnClickListener {
//            isLoadingFirstPage = true
//            isLoadingAnotherPage = false
//            viewModel.load(0)
//        }
//        addNewAxleFab.setOnClickListener {
//            val intent = Intent(this, EditAxleActivity::class.java)
//            startActivity(intent)
//        }
//        axlesRecyclerView.setOnScrollListener(recyclerViewOnScrollListener)
//        axleItemAdapter.onItemAddListener = onAxleAddListener
//    }
//
//    override fun onMoreInfoClick(itemIndex: Int) {
//        val axle = axleItemAdapter.getItem(itemIndex)!!
//        axleDetailsFragment =
//            AxleDetailsFragment.newInstance(axle)
//        axleDetailsFragment?.show(supportFragmentManager, AXLE_DETAILS_BOTTOM_SHEET_FRAGMENT_TAG)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item.itemId) {
//            android.R.id.home -> {
//                finish()
//                true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }
//    }
//
//    private fun showLoading(show: Boolean) {
//        if (show) {
//            loadingProgressBar.visibility = View.VISIBLE
//            addNewAxleFab.hide()
//        } else {
//            axleSwipeRefreshLayout.isRefreshing = false
//            loadingProgressBar.visibility = View.GONE
//            addNewAxleFab.show()
//        }
//    }
//
//    private fun showUpdate(show: Boolean, message: String?) {
//        if (show) {
//            axleSwipeRefreshLayout?.visibility = View.GONE
//            axleRefreshButton?.visibility = View.VISIBLE
//            errorMessageTextView.visibility = View.VISIBLE
//            errorMessageTextView.text = message
//            addNewAxleFab.hide()
//        } else {
//            axleSwipeRefreshLayout?.visibility = View.VISIBLE
//            axleRefreshButton?.visibility = View.GONE
//            errorMessageTextView.visibility = View.GONE
//            errorMessageTextView.text = ""
//            addNewAxleFab.show()
//        }
//    }
//
//    private fun showEmpty(show: Boolean) {
//        if (show) {
//            axleNotFoundTextView.visibility = View.VISIBLE
//        } else {
//            axleNotFoundTextView.visibility = View.GONE
//        }
//    }
//
//    private fun toast(message: String?) {
//        if (message != null) {
//            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//
//        if (firstStart) {
//            firstStart = false
//        } else {
//            axleDetailsFragment?.dismiss()
//            val editedAxle = AxleRepository(this).getEditedAxle()
//            if (editedAxle != null) {
//                axleItemAdapter.addOrUpdate(editedAxle)
//            }
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//
//        viewModel.dispose()
//        compositeDisposable.clear()
//    }
//
//    override fun onRefresh() {
//        viewModel.loadLastPage()
//    }
}


























