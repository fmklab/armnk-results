package com.fmklab.armnk_results.modules.common.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fmklab.armnk_results.modules.common.models.Factory

class FactoryNameSpinnerAdapter(context: Context, resource: Int, private val objects: Array<out Factory>) :
    ArrayAdapter<Factory>(context, resource, objects) {

    override fun getItem(position: Int): Factory? {
        return objects[position]
    }

    override fun getCount(): Int {
        return objects.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = super.getView(position, convertView, parent) as TextView
        textView.text = objects[position].code
        return textView
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = super.getDropDownView(position, convertView, parent) as TextView
        textView.text = objects[position].code
        return textView
    }
}