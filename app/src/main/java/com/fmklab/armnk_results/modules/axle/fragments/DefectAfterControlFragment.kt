package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Toast
import androidx.core.view.forEach
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.UserAxleModuleSettings
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.repositories.AxleRepository
import com.fmklab.armnk_results.modules.common.adapters.ManSpinnerAdapter
import com.fmklab.armnk_results.modules.axle.models.AxleControlZone
import com.fmklab.armnk_results.modules.axle.models.FullAxle
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.DataResponse
import com.google.android.material.checkbox.MaterialCheckBox
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_defect_after_control.*
import kotlinx.android.synthetic.main.fragment_defect_after_control.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DefectAfterControlFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel by sharedViewModel<EditAxleViewModel>()

    companion object {
        const val ULTRASONIC_CONTROL = 1
        const val MAGNETIC_CONTROL = 2
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_defect_after_control, container, false)
        view.defectAfterControlView.visibility = View.GONE
        return view
    }

    override fun onResume() {
        super.onResume()

        viewModel.loadDefectAfterInfo()
        setupUI()
        setupBindings()
    }

    override fun onPause() {
        super.onPause()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        doUltrasonicControlAutoCompleteTextView.keyListener = null
        doMagneticControlAutoCompleteTextView.keyListener = null
    }

    private fun setupBindings() {
        viewModel.axleDefectAfterInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false, null)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            ManSpinnerAdapter(
                                requireActivity(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.mans.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                doUltrasonicControlAutoCompleteTextView.setAdapter(adapter)
                                doMagneticControlAutoCompleteTextView.setAdapter(adapter)
                            }
                            ultrasonicControlLinearLayout.removeAllViews()
                            magneticControlLinearLayout.removeAllViews()
                            if (viewModel.editingAxle.controlZones.size == 0) {
                                viewModel.editingAxle.controlZones = ArrayList(it.data.controlZones)
                                //viewModel.editingAxle.selectControlZonesIfEditMode()
                            }
                            addControlZonesView(viewModel.editingAxle.controlZones)
                            createControlResultString()
                            setupData()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error?.message)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.defectAfterControlInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                checkDefectAfterControlInput()
            }).addTo(compositeDisposable)
        doUltrasonicControlAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text?.isNotEmpty() == true) {
                doUltrasonicControlTextInputLayout.error = null
            }
            viewModel.editingAxle.ultrasonicControlUser = text.toString()
            if (viewModel.editingAxle.id == FullAxle.DEFAULT_ID) {
                AxleRepository(
                    requireContext()
                ).setUltrasonicControlUser(text.toString())
            }
            createControlResultString()
        }
        doMagneticControlAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text?.isNotEmpty() == true) {
                doMagneticControlTextInputLayout.error = null
            }
            viewModel.editingAxle.magneticControlUser = text.toString()
            if (viewModel.editingAxle.id == FullAxle.DEFAULT_ID) {
                AxleRepository(
                    requireContext()
                ).setMagneticControlUser(text.toString())
            }
            createControlResultString()
        }
        removeUltrasonicManButton.setOnClickListener {
            doUltrasonicControlAutoCompleteTextView.setText("")
        }
        removeMagneticManButton.setOnClickListener {
            doMagneticControlAutoCompleteTextView.setText("")
        }
        refreshButton.setOnClickListener {
            viewModel.loadDefectAfterInfo()
        }
        checkAllZonesCheckBox.setOnCheckedChangeListener { _, isChecked ->
            AxleRepository(requireContext()).saveNewAxleControlZonesState(isChecked)
            ultrasonicControlLinearLayout.forEach {
                when (it) {
                    is CheckBox -> it.isChecked = isChecked
                }
            }
            magneticControlLinearLayout.forEach {
                when (it) {
                    is CheckBox -> it.isChecked = isChecked
                }
            }
        }
    }

    private fun setupData() {
        if (viewModel.editingAxle.id != FullAxle.DEFAULT_ID) {
            doUltrasonicControlAutoCompleteTextView.setText(viewModel.editingAxle.ultrasonicControlUser, false)
            doMagneticControlAutoCompleteTextView.setText(viewModel.editingAxle.magneticControlUser, false)
        } else {
            val repository =
                AxleRepository(
                    requireContext()
                )
            val ultrasonicControlUser = repository.getUltrasonicControlUser()
            val magneticControlUser = repository.getMagneticControlUser()
            doUltrasonicControlAutoCompleteTextView.setText(ultrasonicControlUser, false)
            doMagneticControlAutoCompleteTextView.setText(magneticControlUser, false)
        }
    }

    private fun addControlZonesView(controlZones: Collection<AxleControlZone>) {
        for (controlZone in controlZones) {
            val checkBox = MaterialCheckBox(requireContext())
            if (controlZone.selectable) {
                controlZone.checked = AxleRepository(requireContext()).getControlZoneState(controlZone.id)
                checkBox.isEnabled = true
            } else {
                controlZone.checked = true
                checkBox.isEnabled = false
            }
            if (viewModel.axleFullInfo.value?.data?.isNewDetail == true) {
                checkAllZonesCheckBox.visibility = View.VISIBLE
                checkAllZonesCheckBox.isChecked = AxleRepository(requireContext()).getNewAxleControlZonesState()
                controlZone.checked = AxleRepository(requireContext()).getNewAxleControlZonesState()
                checkBox.isEnabled = false
            }
            checkBox.text = controlZone.title
            checkBox.isChecked = controlZone.checked!!
            checkBox.tag = controlZone.id
//            viewModel.editingAxle.controlZones.find { c -> c.id == controlZone.id }.also { cc ->
//                cc?.checked = controlZone.checked!!
//            }
            checkBox.setOnCheckedChangeListener { _, isChecked ->
                viewModel.editingAxle.controlZones.find { controlZone -> controlZone.id == checkBox.tag }
                    .also { controlZone ->
                        controlZone?.checked = isChecked
                        if (controlZone?.vmInDb == ULTRASONIC_CONTROL) {
                            if (isChecked) {
                                viewModel.editingAxle.ultrasonicCount++
                            } else {
                                viewModel.editingAxle.ultrasonicCount--
                            }
                        }
                        if (controlZone?.vmInDb == MAGNETIC_CONTROL) {
                            if (isChecked) {
                                viewModel.editingAxle.magneticCount++
                            } else {
                                viewModel.editingAxle.magneticCount--
                            }
                        }
                        if (viewModel.editingAxle.defectVisualControlZone?.id == controlZone?.id) {
                            viewModel.editingAxle.defectVisualControlZone = null
                            viewModel.editingAxle.defectVisual = null
                            viewModel.editingAxle.detector = null
                            viewModel.resetAxleDefectsVisualInfo()
                        }
                        if (viewModel.editingAxle.defectNdtControlZone?.id == controlZone?.id) {
                            viewModel.editingAxle.defectNdtControlZone = null
                            viewModel.editingAxle.defectNdt = null
                            viewModel.resetDefectNdtInfo()
                        }
                        if (controlZone?.selectable == true) {
                            AxleRepository(requireContext()).saveControlZoneState(controlZone.id, isChecked)
                        }
                    }
                createControlResultString()
            }
            //checkBox.isChecked = true
            when (controlZone.vmInDb) {
                ULTRASONIC_CONTROL -> ultrasonicControlLinearLayout.addView(checkBox)
                MAGNETIC_CONTROL -> magneticControlLinearLayout.addView(checkBox)
            }
//            if (viewModel.editingAxle.selectedControlZonesId.size > 0) {
//                checkBox.isChecked =
//                    viewModel.editingAxle.selectedControlZonesId.any { z -> z == controlZone.fieldName }
//            }
        }
        //viewModel.editingAxle.selectedControlZonesId.clear()
    }

    private fun createControlResultString() {
        var controlResultString = ""
        when (viewModel.editingAxle.repairType) {
            RepairTypeFragment.CURRENT_REPAIR -> controlResultString += "Р[Т]"
            RepairTypeFragment.MIDDLE_REPAIR -> {
                controlResultString += if (viewModel.editingAxle.rings == 1) {
                    "Р[С] К[С]"
                } else {
                    "Р[С] К[Н]"
                }
            }
            RepairTypeFragment.CAPITAL_REPAIR -> controlResultString += "Р[К]"
        }
        var ultrasonicCount = 0
        var magneticCount = 0
        for (controlZone in viewModel.editingAxle.controlZones) {
            when (controlZone.vmInDb) {
                ULTRASONIC_CONTROL -> {
                    if (controlZone.checked == true) {
                        ultrasonicCount++
                    }
                }
                MAGNETIC_CONTROL -> {
                    if (controlZone.checked == true) {
                        magneticCount++
                    }
                }
            }
        }
        viewModel.editingAxle.ultrasonicCount = ultrasonicCount
        viewModel.editingAxle.magneticCount = magneticCount
        if (ultrasonicCount > 0) {
            controlResultString += " УЗК[$ultrasonicCount]"
            if (viewModel.editingAxle.ultrasonicControlUser.isNotEmpty()) {
                controlResultString += " УЗК: ${viewModel.editingAxle.ultrasonicControlUser}"
            }
        }
        if (magneticCount > 0) {
            controlResultString += " МПК[$magneticCount]"
            if (viewModel.editingAxle.magneticControlUser.isNotEmpty()) {
                controlResultString += " МПК: ${viewModel.editingAxle.magneticControlUser}"
            }
        }
        viewModel.editingAxle.controlInfo = controlResultString
        defectAfterControlResultTextView.text = controlResultString
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            defectAfterControlView.visibility = View.GONE
            loadingProgressBar.visibility = View.VISIBLE
        } else {
            loadingProgressBar.visibility = View.GONE
            defectAfterControlView.visibility = View.VISIBLE
        }
    }

    private fun showRefresh(show: Boolean, message: String?) {
        if (show) {
            defectAfterControlView.visibility = View.GONE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.visibility = View.VISIBLE
            errorMessageTextView.text = message
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            defectAfterControlView.visibility = View.VISIBLE
        }
    }

    fun checkDefectAfterControlInput(): Boolean {
        var result = true
        if (viewModel.editingAxle.ultrasonicCount > 0 && doUltrasonicControlAutoCompleteTextView.text.isNullOrEmpty()) {
            doUltrasonicControlTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (viewModel.editingAxle.magneticCount > 0 && doMagneticControlAutoCompleteTextView.text.isNullOrEmpty()) {
            doMagneticControlTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        return result
    }

    private fun showNoRepairTypeView(show: Boolean) {
        if (show) {
            errorMessageTextView.text = "Не указан тип ремонта"
            errorMessageTextView.visibility = View.VISIBLE
        } else {
            errorMessageTextView.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}



























