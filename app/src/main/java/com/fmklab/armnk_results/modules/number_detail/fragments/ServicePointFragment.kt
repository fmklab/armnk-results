package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.number_detail.models.Company
import com.fmklab.armnk_results.modules.number_detail.models.Fault
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jakewharton.rxbinding4.widget.checkedChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_service_point.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ServicePointFragment: BottomSheetDialogFragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditNumberDetailViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_service_point, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
        setupBindings()
    }

    private fun setupBindings() {
        viewModel.servicePoint
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it.company) {
                    Company.VRK1 -> vrk1Radio.isChecked = true
                    Company.VRK2 -> vrk2Radio.isChecked = true
                    Company.VRK3 -> vrk3Radio.isChecked = true
                    Company.OTHER -> otherRadio.isChecked = true
                    Company.NONE -> vrk1Radio.isChecked = true
                }

                when (it.fault) {
                    Fault.DEPOT -> depotCheckBox.isChecked = true
                    Fault.FACTORY -> factoryCheckBox.isChecked = true
                    Fault.EXPLOITATION -> exploitationCheckBox.isChecked = true
                    Fault.NONE -> {
                        depotCheckBox.isChecked = false
                        factoryCheckBox.isChecked = false
                        exploitationCheckBox.isChecked = false
                    }
                }
            }
            .addTo(compositeDisposable)

        vrk1Radio
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    viewModel.servicePointCompanyChangedConsumer.accept(Company.VRK1)
                }
            }
            .addTo(compositeDisposable)

        vrk2Radio
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                viewModel.servicePointCompanyChangedConsumer.accept(Company.VRK2)
                }
            }
            .addTo(compositeDisposable)

        vrk3Radio
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    viewModel.servicePointCompanyChangedConsumer.accept(Company.VRK3)
                }
            }

        otherRadio
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    viewModel.servicePointCompanyChangedConsumer.accept(Company.OTHER)
                }
            }

        depotCheckBox
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    factoryCheckBox.isChecked = false
                    exploitationCheckBox.isChecked = false
                    viewModel.servicePointFaultChangedConsumer.accept(Fault.DEPOT)
                }
            }
            .addTo(compositeDisposable)

        factoryCheckBox
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    depotCheckBox.isChecked = false
                    exploitationCheckBox.isChecked = false
                    viewModel.servicePointFaultChangedConsumer.accept(Fault.FACTORY)
                }
            }
            .addTo(compositeDisposable)

        exploitationCheckBox
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    depotCheckBox.isChecked = false
                    factoryCheckBox.isChecked = false
                    viewModel.servicePointFaultChangedConsumer.accept(Fault.EXPLOITATION)
                }
            }
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        acceptServicePointFab.setOnClickListener {
            dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}









































