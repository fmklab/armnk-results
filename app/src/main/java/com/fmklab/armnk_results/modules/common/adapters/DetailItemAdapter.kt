package com.fmklab.armnk_results.modules.common.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.fmklab.armnk_results.modules.common.models.RecyclerItem

abstract class DetailItemAdapter<T>:
    RecyclerView.Adapter<DetailItemAdapter.BaseDetailViewHolder<T>>() where T: AbstractDetail {

    abstract class BaseDetailViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView)

    class LoadingDetailViewHolder<T>(itemView: View): BaseDetailViewHolder<T>(itemView)

    class ErrorDetailViewHolder<T>(itemView: View, private val listener: OnRefreshClickListener?): BaseDetailViewHolder<T>(itemView) {

        private val refreshButton = itemView.findViewById<Button>(R.id.refreshItemButton)

        init {
            refreshButton.setOnClickListener {
                listener?.onRefreshClick()
            }
        }
    }

    class ItemDetailViewHolder<T>(itemView: View, private val listener: OnMoreInfoClickListener?): BaseDetailViewHolder<T>(itemView) {

        private val moreInfoButton = itemView.findViewById<Button>(R.id.moreInfoButton)

        init {
            moreInfoButton.setOnClickListener {
                listener?.onMoreInfoClick(adapterPosition)
            }
        }
    }

    interface OnMoreInfoClickListener {

        fun onMoreInfoClick(position: Int)
    }

    interface OnRefreshClickListener {

        fun onRefreshClick()
    }

    interface OnItemAddListener {

        fun onItemAdded()
    }

    companion object {
        const val VIEW_ITEM = 0
        const val VIEW_LOADING = 1
        const val VIEW_ERROR = 2
    }

    private val items: ArrayList<RecyclerItem<T>> = ArrayList()

    var onMoreInfoClickListener: OnMoreInfoClickListener? = null
    var onRefreshClickListener: OnRefreshClickListener? = null
    var onItemAddListener: OnItemAddListener? = null

    protected abstract fun inflateView(parent: ViewGroup, viewType: Int): View

    private fun removeStatusItem() {
        if (items.isNotEmpty() && items[items.size - 1].status != RecyclerItem.Status.ITEM) {
            items.removeAt(items.size - 1)
            notifyItemRemoved(items.size - 1)
        }
    }

    fun addItems(items: ArrayList<T>) {
        removeStatusItem()
        for (item in items) {
            this.items.add(RecyclerItem.item(item))
        }

        notifyDataSetChanged()
    }

    fun addItem(item: T) {
        items.add(0, RecyclerItem.item(item))
        notifyItemInserted(0)
    }

    fun updateItem(item: T, index: Int) {
        items[index] = RecyclerItem.item(item)
        notifyItemChanged(index)
    }

    fun addOrUpdateItem(newItem: T) {
        val oldItem = items.find { item -> item.item?.id == newItem.id }
        if (oldItem != null) {
            val index = items.indexOf(oldItem)
            items[index] = RecyclerItem.item(newItem)
            notifyItemChanged(index)
        } else {
            items.add(0, RecyclerItem.item(newItem))
            notifyItemInserted(0)
            onItemAddListener?.onItemAdded()
        }
    }

    fun showLoading() {
        removeStatusItem()
        items.add(RecyclerItem.loading())
        notifyItemInserted(items.size - 1)
    }

    fun hideLoading() {
        if (items.isNotEmpty() && items[items.size - 1].status == RecyclerItem.Status.LOADING) {
            items.removeAt(items.size - 1)
            notifyItemRemoved(items.size - 1)
        }
    }

    fun showError() {
        removeStatusItem()
        items.add(RecyclerItem.error())
        notifyItemInserted(items.size - 1)
    }

    fun getItem(position: Int): T? {
        return items[position].item
    }

    fun clearItemsWithoutNotify() {
        items.clear()
    }

    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].status) {
            RecyclerItem.Status.ITEM -> VIEW_ITEM
            RecyclerItem.Status.LOADING -> VIEW_LOADING
            RecyclerItem.Status.ERROR -> VIEW_ERROR
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseDetailViewHolder<T> {
        return when (viewType) {
            VIEW_ITEM -> {
                val view = inflateView(parent, viewType)
                ItemDetailViewHolder(view, onMoreInfoClickListener)
            }
            VIEW_LOADING -> {
                val view = inflateView(parent, viewType)
                LoadingDetailViewHolder(view)
            }
            else -> {
                val view = inflateView(parent, viewType)
                ErrorDetailViewHolder(view, onRefreshClickListener)
            }
        }
    }
}




































