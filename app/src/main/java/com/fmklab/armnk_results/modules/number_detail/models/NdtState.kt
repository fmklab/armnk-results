package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.wheels.models.DefectType

class NdtState(
    defectType: DefectType?,
    zone: ControlZone?,
    steal: String?,
    owner: String?,
    method: String?,
    defector: Detector?,
    defectSize: String?
) : DefectState(defectType, zone, steal, owner) {

    constructor(state: NumberDetailState?) : this(
        state?.defectType,
        state?.zone,
        state?.steal,
        state?.owner,
        state?.method,
        state?.detector,
        state?.defectSize
    )

    override var method: String? = method
        get() = field

    override var detector: Detector? = defector
        get() = field

    override var defectSize: String? = defectSize
        get() = field

    override fun getDescription(): String {
        return "обнаружен: \"средствами НК\"; вид (метод): \"$method\"; тип дефекта: \"${defectType?.name}\"; " +
                "размер (трещины), мм: \"$defectSize\"; тип дефектоскопа: \"${detector.toString()}\"; зона выявления: \"${zone?.name}\"; " +
                "сталь: \"$steal\"; собственник: \"$owner\";"
    }
}





































