package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.wheels.models.WheelDefectInfo
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_wrong_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WheelDefectInfoFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val visualAndNdtDefectFragment = WheelVisualDefectFragment()
    private val ndtDefectFragment = WheelNdtDefectFragment()
    private val viewModel: EditWheelViewModel by sharedViewModel()

    companion object {
        private const val VISUAL_AND_NDT_FRAGMENT_TAG = "VisualAndNdtFragmentTag"
        private const val NDT_FRAGMENT_TAG = "NdtFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wrong_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBindings()
    }

    private fun setupBindings() {
        viewModel.fullWheel
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.status == DataResponse.Status.SUCCESS) {
                    when (it.data?.defectInfo?.defectMoment) {
                        WheelDefectInfo.DEFECT_MOMENT_VISUAL -> visualAndNtdRadio.isChecked = true
                        WheelDefectInfo.DEFECT_MOMENT_NDT -> ntdRadio.isChecked = true
                    }
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectInfoRequestCheckFields::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                val result = checkFields()
                if (result) {
                    if (visualAndNtdRadio.isChecked) {
                        viewModel.getDefectInfo(WheelDefectInfo.DEFECT_MOMENT_VISUAL)
                    } else if (ntdRadio.isChecked) {
                        viewModel.getDefectInfo(WheelDefectInfo.DEFECT_MOMENT_NDT)
                    }
                }
                RxBus.publish(RxEvent.EventDefectInfoCheckFieldsResponse(result))
            }).addTo(compositeDisposable)

        visualAndNtdRadio.setOnCheckedChangeListener { _, isChecked ->
            val transaction = childFragmentManager.beginTransaction()
            val fragment = childFragmentManager.findFragmentByTag(VISUAL_AND_NDT_FRAGMENT_TAG)
            if (isChecked) {
                viewModel.setDefectMoment(WheelDefectInfo.DEFECT_MOMENT_VISUAL)
                if (fragment != null) {
                    transaction.remove(fragment)
                }
                transaction.add(R.id.detectTypeFrameLayout, visualAndNdtDefectFragment, VISUAL_AND_NDT_FRAGMENT_TAG).commit()
            } else {
                if (fragment != null) {
                    transaction.remove(fragment).commit()
                }
            }
        }

        ntdRadio.setOnCheckedChangeListener { _, isChecked ->
            val transaction = childFragmentManager.beginTransaction()
            val fragment = childFragmentManager.findFragmentByTag(NDT_FRAGMENT_TAG)
            if (isChecked) {
                viewModel.setDefectMoment(WheelDefectInfo.DEFECT_MOMENT_NDT)
                if (fragment != null) {
                    transaction.remove(fragment)
                }
                transaction.add(R.id.detectTypeFrameLayout, ndtDefectFragment, NDT_FRAGMENT_TAG).commit()
            } else {
                if (fragment != null) {
                    transaction.remove(fragment).commit()
                }
            }
        }
    }

    private fun checkFields(): Boolean {
        if (!visualAndNtdRadio.isChecked && !ntdRadio.isChecked) {
            toast(getString(R.string.zone_no_selected))
            return false
        }
        if (visualAndNtdRadio.isChecked && !visualAndNdtDefectFragment.checkFields()) {
            return false
        }
        if (ntdRadio.isChecked && !ndtDefectFragment.checkFields()) {
            return false
        }
        return true
    }

    private fun toast(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}



































