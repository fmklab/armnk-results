package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ControlZone (
    @SerializedName("id")
    @Expose
    val id: Int = 0,
    @SerializedName("partId")
    @Expose
    val partId: Int = 0,
    @SerializedName("name")
    @Expose
    val name: String? = null,
    @SerializedName("methodStr")
    @Expose
    val methodStr: String? = null
) {

    companion object {
        const val DEFAULT_ID = 0
    }

    override fun toString(): String {
        return name ?: ""
    }
}