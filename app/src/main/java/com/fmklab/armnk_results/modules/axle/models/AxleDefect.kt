package com.fmklab.armnk_results.modules.axle.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleDefect(
    @SerializedName("id")
    @Expose
    val id: Int = 0,
    @SerializedName("title")
    @Expose
    val title: String = "",
    @SerializedName("code")
    @Expose
    val code: String = ""
) {

    override fun toString(): String {
        return title
    }
}