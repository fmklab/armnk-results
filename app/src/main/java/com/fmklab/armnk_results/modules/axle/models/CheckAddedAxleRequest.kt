package com.fmklab.armnk_results.modules.axle.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CheckAddedAxleRequest (
    @SerializedName("companyId")
    @Expose
    val companyId: Int,
    @SerializedName("axleInfo")
    @Expose
    val axleInfo: FullAxle
)