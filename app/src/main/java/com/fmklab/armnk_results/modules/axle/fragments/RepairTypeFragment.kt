package com.fmklab.armnk_results.modules.axle.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_results.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_repair_type.*

class RepairTypeFragment :
    BottomSheetDialogFragment() {

    private val compositeDisposable = CompositeDisposable()
    private val middleRepairFragment = MiddleRepairFragment()
    val selectedRepairType: PublishSubject<Int> = PublishSubject.create()

    companion object {
        const val CURRENT_REPAIR = 1
        const val MIDDLE_REPAIR = 2
        const val MIDDLE_REPAIR_WITH_RINGS = 2
        const val MIDDLE_REPAIR_WITHOUT_RINGS = 5
        const val CAPITAL_REPAIR = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("REPAIR TYPE", "CRATED")
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_repair_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBindings()
    }

    private fun setupBindings() {
        middleRepairButton.setOnClickListener {
            middleRepairFragment.selectRingType.subscribeBy(onNext = {
                when (it) {
                    MiddleRepairFragment.WITH_RINGS -> selectedRepairType.onNext(
                        MIDDLE_REPAIR_WITH_RINGS)
                    MiddleRepairFragment.WITHOUT_RINGS -> selectedRepairType.onNext(
                        MIDDLE_REPAIR_WITHOUT_RINGS)
                }
            }).addTo(compositeDisposable)
            middleRepairFragment.show(parentFragmentManager, "MiddleRepairFragmentTag")
        }
        currentRepairButton.setOnClickListener {
            Log.d("REPAIR TYPE", "CLICK")
            selectedRepairType.onNext(CURRENT_REPAIR)
        }
        capitalRepairButton.setOnClickListener {
            selectedRepairType.onNext(CAPITAL_REPAIR)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        if (middleRepairFragment.isVisible) {
            middleRepairFragment.dismiss()
        }

        super.onDismiss(dialog)
    }

    override fun onDestroy() {
        compositeDisposable.clear()

        super.onDestroy()
    }
}