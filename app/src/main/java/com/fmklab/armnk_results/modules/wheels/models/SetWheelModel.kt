package com.fmklab.armnk_results.modules.wheels.models

import com.fmklab.armnk_results.modules.auth.models.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SetWheelModel (
    @SerializedName("user")
    @Expose
    val user: User,
    @SerializedName("fullWheel")
    @Expose
    val fullWheel: FullWheel,
    @SerializedName("appVersion")
    @Expose
    val appVersion: Int
)