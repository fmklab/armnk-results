package com.fmklab.armnk_results.modules.auth.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("name1")
    @Expose
    val name1: String,
    @SerializedName("name2")
    @Expose
    val name2: String,
    @SerializedName("name3")
    @Expose
    val name3: String,
    @SerializedName("companyId")
    @Expose
    val companyId: Int, //192
    @SerializedName("companyTitle")
    @Expose
    val companyTitle: String
) {

    override fun toString(): String {
        return if (name2.trim().isNotEmpty() && name3.trim().isNotEmpty()) {
            "$name1 ${name2[0]}.${name3[0]}."
        } else if (name2.trim().isNotEmpty() && name3.trim().isEmpty()) {
            "$name1 ${name2[0]}."
        } else if (name2.trim().isEmpty() && name3.trim().isNotEmpty()) {
            "$name1 ${name3[0]}."
        } else {
            name1
        }
    }
}