package com.fmklab.armnk_results.modules.wheels.models

import com.fmklab.armnk_results.modules.axle.models.AxleModel
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.common.models.AxleAndWheelType
import com.fmklab.armnk_results.modules.common.models.ControlResult
import com.fmklab.armnk_results.modules.common.models.Factory
import com.fmklab.armnk_results.modules.common.models.Year
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class FullWheel(
    @SerializedName("id")
    @Expose
    var id: Int = DEFAULT_ID,
    @SerializedName("controlDate")
    @Expose
    var date: Date = Date(),
    @SerializedName("factory")
    @Expose
    var factory: Factory = Factory(),
    @SerializedName("factoryYear")
    @Expose
    var factoryYear: Year = Year(),
    @SerializedName("type")
    @Expose
    var type: AxleAndWheelType = AxleAndWheelType(),
    @SerializedName("wheelNumber")
    @Expose
    var wheelNumber: String = "",
    @SerializedName("axle")
    @Expose
    var axle: AxleModel = AxleModel(),
    @SerializedName("result")
    @Expose
    var result: ControlResult? = null,
    @SerializedName("spec")
    @Expose
    var spec: String = "",
    @SerializedName("controlInfoStr")
    @Expose
    val controlInfoStr: String = "",
    @SerializedName("defectInfoStr")
    @Expose
    val defectInfoStr: String = "",
    @SerializedName("isNewDetail")
    @Expose
    var isNewDetail: Boolean = false,
    @SerializedName("isNewForm")
    @Expose
    var isNewForm: Boolean = false,
    @SerializedName("wheelControlInfo")
    @Expose
    val wheelControlInfo: WheelControlInfo = WheelControlInfo(),
    @SerializedName("defectInfo")
    @Expose
    val defectInfo: WheelDefectInfo = WheelDefectInfo(),
    @SerializedName("defectMomentInfo")
    @Expose
    val defectMomentInfo: WheelDefectMomentInfo = WheelDefectMomentInfo(),
    @SerializedName("defectVisualInfo")
    @Expose
    val defectVisualInfo: WheelDefectVisualInfo = WheelDefectVisualInfo(),
    @SerializedName("defectNdtInfo")
    @Expose
    val defectNdtInfo: WheelDefectNdtInfo = WheelDefectNdtInfo(),
    @SerializedName("isDefectBeforeNdt")
    @Expose
    var isDefectBeforeNdt: Boolean = false,
    @SerializedName("selectedControlZones")
    @Expose
    var selectedControlZones: ArrayList<Int>? = null,
    @SerializedName("replaceWheelId")
    @Expose
    var replaceWheelId: Int = DEFAULT_ID,
    @SerializedName("createDate")
    @Expose
    var createDate: Date = Date(),
    @SerializedName("isMobileApp")
    @Expose
    var isMobileApp: Boolean = true
) {

    companion object {
        const val DEFAULT_ID = -1
    }

    fun checkBaseInfo(): Boolean {
        if (factory.code == Factory.DEFAULT_CODE) {
            return false
        }
        if (type.id == AxleAndWheelType.DEFAULT_ID) {
            return false
        }
        if (wheelNumber.isBlank()) {
            return false
        }
        if (spec.isBlank()) {
            return false
        }

        return true
    }

    fun checkDefectMomentInfo(): Boolean {
        if (defectMomentInfo.defectType.id == DefectType.DEFAULT_ID) {
            return false
        }
        if (defectMomentInfo.wheelDefect.id == WheelDefect.DEFAULT_ID) {
            return false
        }
        if (defectMomentInfo.defectZone.id == WheelControlZone.DEFAULT_ID) {
            return false
        }

        return true
    }

    fun checkControlInfo(): Boolean {
        if (wheelControlInfo.ultrasonicCount > 0 && wheelControlInfo.ultrasonicSpec.isBlank()) {
            return false
        }
        if (wheelControlInfo.magneticCount > 0 && wheelControlInfo.magneticSpec.isBlank()) {
            return false
        }

        return true
    }

    fun checkDefectInfo(): Boolean {
        when (defectInfo.defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_VISUAL -> {
                if (defectVisualInfo.wheelDefect.id == WheelDefect.DEFAULT_ID) {
                    return false
                }
            }
            WheelDefectInfo.DEFECT_MOMENT_NDT -> {
                if (defectNdtInfo.wheelDefect.id == WheelDefect.DEFAULT_ID) {
                    return false
                }
                if (defectNdtInfo.detector.id == Detector.DEFAULT_ID) {
                    return false
                }
                if (defectNdtInfo.defectSize.isBlank()) {
                    return false
                }
            }
            WheelDefectInfo.DEFECT_MOMENT_NONE -> return false
        }

        return true
    }
}





































