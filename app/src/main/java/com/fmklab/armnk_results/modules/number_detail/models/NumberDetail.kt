package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlResult
import com.fmklab.armnk_results.modules.common.models.Factory
import com.fmklab.armnk_results.modules.common.models.Year
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import java.util.*

class NumberDetail(
    val id: Int,
    val partId: Int,
    var controlDate: Date = Date(),
    var factory: Factory = Factory(),
    var factoryYear: Year = Year(),
    var number: String = "",
    var isNewDetail: Boolean = false,
    var controlResult: ControlResult = ControlResult(),
    var isFromServicePoint: Boolean = false,
    var noRejectionCriteria: Boolean = false,
    var spec: String = "",
    var servicePoint: ServicePoint = ServicePoint(Company.NONE, Fault.NONE),
    var defectMoment: DefectMoment = DefectMoment.NONE
) {

    lateinit var state: NumberDetailState

    private var lastDefectState: DefectState? = null
    private var lastDefectMoment: DefectMoment = DefectMoment.NONE

    fun removeState() {
        if (state is DefectState) {
            lastDefectState = state as DefectState
            lastDefectMoment = defectMoment
        }
    }

    fun setDefecationState() {
        defectMoment = DefectMoment.DEFECATION
        state = DefecationState(state)
        lastDefectState = state as DefecationState
        lastDefectMoment = DefectMoment.DEFECATION
    }

    fun setVisualState() {
        defectMoment = DefectMoment.VISUAL
        state = VisualState(state)
        lastDefectState = state as VisualState
        lastDefectMoment = DefectMoment.VISUAL
    }

    fun setNdtState() {
        defectMoment = DefectMoment.NDT
        state = NdtState(state)
        lastDefectState = state as NdtState
        lastDefectMoment = DefectMoment.NDT
    }

    fun setRepairState() {
        defectMoment = DefectMoment.NONE
        state = RepairState(state)
    }

    fun restoreLastDefectState() {
        if (lastDefectState != null) {
            defectMoment = lastDefectMoment
        }
    }

    fun toDto(): NumberDetailDto {
        return NumberDetailDto(
            id,
            partId,
            controlDate,
            factory,
            factoryYear,
            number,
            isNewDetail,
            controlResult,
            isFromServicePoint,
            noRejectionCriteria,
            spec,
            "",
            servicePoint,
            defectMoment,
            state.defectType,
            state.zone,
            state.defectDescription,
            state.repairDescription,
            state.defectLength,
            state.defectDepth,
            state.defectDiameter,
            state.steal,
            state.owner,
            state.defectSize,
            state.detector.toString(),
            state.method
        )
    }
}








































