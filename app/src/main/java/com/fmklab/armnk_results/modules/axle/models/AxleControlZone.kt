package com.fmklab.armnk_results.modules.axle.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleControlZone(
    @SerializedName("id")
    @Expose
    val id: Int = 0,
    @SerializedName("repairType")
    @Expose
    val repairType: Int = 0,
    @SerializedName("vmName")
    @Expose
    val vmName: String = "",
    @SerializedName("vmInDb")
    @Expose
    val vmInDb: Int = 0,
    @SerializedName("rings")
    @Expose
    val rings: Int = 0,
    @SerializedName("title")
    @Expose
    val title: String = "",
    @SerializedName("fieldName")
    @Expose
    val fieldName: Int = 0,
    @SerializedName("checked")
    @Expose
    var checked: Boolean? = null,
    @SerializedName("zoneControlType")
    @Expose
    var zoneControlType: Int = 0,
    @SerializedName("selectable")
    @Expose
    val selectable: Boolean
) {

    override fun toString(): String {
        return title
    }
}