package com.fmklab.armnk_results.modules.axle.viewmodels

import androidx.lifecycle.ViewModel
import com.fmklab.armnk_results.BuildConfig
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.modules.auth.models.User
import com.fmklab.armnk_results.modules.axle.models.*
import com.fmklab.armnk_results.modules.axle.services.AxleApiService
import com.fmklab.armnk_results.modules.common.CheckAxleBaseInfoResponse
import com.fmklab.armnk_results.modules.common.Constants
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.models.*
import com.fmklab.armnk_results.modules.common.services.ArmApiService
import com.fmklab.armnk_results.modules.common.services.AxleAndWheelApiService
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.security.auth.callback.Callback

class EditAxleViewModel(
    private val axleApiService: AxleApiService,
    private val axleAndWheelApiService: AxleAndWheelApiService,
    private val armApiService: ArmApiService,
    private val currentUser: CurrentUser
) : ViewModel() {

    var viewModelState = EditViewModelState(null)

    private val compositeDisposable = CompositeDisposable()
    var editingAxle = FullAxle()
    val axleFullInfo: BehaviorSubject<DataResponse<FullAxle>> = BehaviorSubject.create()
    val axleBaseInfo: BehaviorSubject<DataResponse<AxleAndWheelBaseInfo>> = BehaviorSubject.create()
    val axleDefectAfterInfo: BehaviorSubject<DataResponse<AxleDefectAfterResponse>> =
        BehaviorSubject.create()
    val axleDefectVisualInfo: BehaviorSubject<DataResponse<List<AxleDefect>>> =
        BehaviorSubject.create()
    val axleDefectNdtInfo: BehaviorSubject<DataResponse<NtdAxleDefectResponse>> =
        BehaviorSubject.create()
    val axleDefectBeforeControlType: BehaviorSubject<DataResponse<List<AxleDefectBeforeControlType>>> =
        BehaviorSubject.create()
    val axleDefectBeforeZone: BehaviorSubject<DataResponse<List<AxleControlZone>>> =
        BehaviorSubject.create()
    val axleDefectBeforeCode: BehaviorSubject<DataResponse<List<AxleDefect>>> =
        BehaviorSubject.create()

    val baseInfoError: BehaviorSubject<CheckAxleBaseInfoResponse> = BehaviorSubject.create()
    val defectBeforeControlInfoError: BehaviorSubject<String> = BehaviorSubject.create()
    val defectAfterControlInfoError: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val wrongDetailInfoError: BehaviorSubject<String> = BehaviorSubject.create()
    val saveGoodDetailMessage: BehaviorSubject<String> = BehaviorSubject.create()
    val saveBadDetailMessage: BehaviorSubject<String> = BehaviorSubject.create()
    val axleSaved: BehaviorSubject<DataResponse<Int>> = BehaviorSubject.create()

    fun setSpec() {
        val user = currentUser.getUser()
        if (user != null) {
            editingAxle.specialistCredentials = user.toString()
        }
    }

    fun loadBaseInputInfo() {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        axleBaseInfo.onNext(DataResponse.loading())
        axleAndWheelApiService.getBaseInfo(Constants.DETAIL_AXLE, companyId, token)
            .enqueue(object : Callback, retrofit2.Callback<AxleAndWheelBaseInfo> {
                override fun onFailure(call: Call<AxleAndWheelBaseInfo>, t: Throwable) {
                    val error =
                        BaseInfoError(
                            "Ошибка соединения с сервером"
                        )
                    axleBaseInfo.onNext(DataResponse.error(error))
                }

                override fun onResponse(
                    call: Call<AxleAndWheelBaseInfo>,
                    response: Response<AxleAndWheelBaseInfo>
                ) {
                    if (response.body() != null) {
                        axleBaseInfo.onNext(DataResponse.success(response.body()!!))
                    } else {
                        val error =
                            BaseInfoError(
                                "Не удалось загрузить информацию по осям"
                            )
                        axleBaseInfo.onNext(DataResponse.error(error))
                    }
                }
            })
    }

    fun loadFullAxle(axleId: Int?) {
        if (axleId == null) {
            editingAxle = FullAxle()
            viewModelState = viewModelState.copy(isEditing = false)
            axleFullInfo.onNext(DataResponse.success(editingAxle))
            return
        }
        viewModelState = viewModelState.copy(isEditing = true)
        val token = currentUser.getAccessToken()
        axleApiService.getFullAxle(axleId, token)
            .enqueue(object : Callback, retrofit2.Callback<FullAxle> {
                override fun onFailure(call: Call<FullAxle>, t: Throwable) {
                    axleFullInfo.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                axleId
                            )
                        )
                    )
                }

                override fun onResponse(call: Call<FullAxle>, response: Response<FullAxle>) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            editingAxle = response.body()!!
                            axleFullInfo.onNext(DataResponse.success(editingAxle))
                        } else {
                            axleFullInfo.onNext(
                                DataResponse.error(
                                    AxleError(
                                        "Ось не найдена",
                                        axleId
                                    )
                                )
                            )
                        }
                    } else {
                        axleFullInfo.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Ошибка ${response.code()}",
                                    axleId
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadDefectAfterInfo() {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        val repairType = editingAxle.repairType
        val rings = editingAxle.rings
        axleDefectAfterInfo.onNext(DataResponse.loading())
        val axleType = editingAxle.type.getAxleType()
        axleApiService.getControlZones(axleType, repairType, companyId, rings, token)
            .enqueue(object : Callback, retrofit2.Callback<AxleDefectAfterResponse> {
                override fun onFailure(call: Call<AxleDefectAfterResponse>, t: Throwable) {
                    axleDefectAfterInfo.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                0
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<AxleDefectAfterResponse>,
                    response: Response<AxleDefectAfterResponse>
                ) {
                    if (response.body() != null) {
                        axleDefectAfterInfo.onNext(DataResponse.success(response.body()!!))
                    } else {
                        axleDefectAfterInfo.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Не удалось загрузить информацию",
                                    0
                                )
                            )
                        )
                    }
                }
            })
    }

    fun resetAxleDefectsVisualInfo() {
        axleDefectVisualInfo.onNext(DataResponse.empty())
    }

    fun loadAxleDefectsVisualInfo() {
        val token = currentUser.getAccessToken()
        val zoneId = editingAxle.defectVisualControlZone!!.id
        axleDefectVisualInfo.onNext(DataResponse.loading())
        axleApiService.getAxleDefectsVisual(zoneId, token)
            .enqueue(object : Callback, retrofit2.Callback<List<AxleDefect>> {
                override fun onFailure(call: Call<List<AxleDefect>>, t: Throwable) {
                    axleDefectVisualInfo.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                0
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<List<AxleDefect>>,
                    response: Response<List<AxleDefect>>
                ) {
                    axleDefectVisualInfo.onNext(DataResponse.success(response.body()))
                }
            })
    }

    fun resetDefectNdtInfo() {
        axleDefectNdtInfo.onNext(DataResponse.empty())
    }

    fun loadAxleDefectsNdtInfo(zoneId: Int, controlType: Int) {
        val token = currentUser.getAccessToken()
        val user = currentUser.getUser()
        axleDefectNdtInfo.onNext(DataResponse.loading())
        axleApiService.getAxleDefectsNdt(zoneId, controlType, user.company.id, token)
            .enqueue(object : Callback, retrofit2.Callback<NtdAxleDefectResponse> {
                override fun onFailure(call: Call<NtdAxleDefectResponse>, t: Throwable) {
                    axleDefectNdtInfo.onNext(
                        DataResponse.error(
                            NdtDefectError(
                                "Ошибка соединения с сервером",
                                controlType
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<NtdAxleDefectResponse>,
                    response: Response<NtdAxleDefectResponse>
                ) {
                    if (response.code() == 200) {
                        axleDefectNdtInfo.onNext(DataResponse.success(response.body()))
                    } else {
                        axleDefectNdtInfo.onNext(
                            DataResponse.error(
                                NdtDefectError(
                                    "Ошибка ${response.code()}",
                                    controlType
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadDefectBeforeControlTypes() {
        val token = currentUser.getAccessToken()
        axleDefectBeforeControlType.onNext(DataResponse.loading())
        axleApiService.getDefectBeforeControlTypes(token)
            .enqueue(object : Callback, retrofit2.Callback<List<AxleDefectBeforeControlType>> {
                override fun onFailure(
                    call: Call<List<AxleDefectBeforeControlType>>,
                    t: Throwable
                ) {
                    axleDefectBeforeControlType.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                0
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<List<AxleDefectBeforeControlType>>,
                    response: Response<List<AxleDefectBeforeControlType>>
                ) {
                    if (response.code() == 200) {
                        axleDefectBeforeControlType.onNext(DataResponse.success(response.body()))
                    } else {
                        axleDefectBeforeControlType.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Ошибка ${response.code()}",
                                    0
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadDefectBeforeZones() {
        val token = currentUser.getAccessToken()
//        val defectId = editingAxle.defectTypeId
        val defectId = editingAxle.defectBeforeControlType!!.id
        axleDefectBeforeZone.onNext(DataResponse.loading())
        axleApiService.getAxleBeforeZones(defectId, token)
            .enqueue(object : Callback, retrofit2.Callback<List<AxleControlZone>> {
                override fun onFailure(call: Call<List<AxleControlZone>>, t: Throwable) {
                    axleDefectBeforeZone.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                0
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<List<AxleControlZone>>,
                    response: Response<List<AxleControlZone>>
                ) {
                    if (response.code() == 200) {
                        axleDefectBeforeZone.onNext(DataResponse.success(response.body()))
                    } else {
                        axleDefectBeforeZone.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Ошибка ${response.code()}",
                                    0
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadDefectBeforeCodes() {
        val token = currentUser.getAccessToken()
//        val defectId = editingAxle.defectTypeId
        val defectId = editingAxle.defectBeforeControlType!!.id
//        val zoneId = editingAxle.defectZoneId
        val zoneId = editingAxle.defectControlZone!!.id
        axleDefectBeforeCode.onNext(DataResponse.loading())
        axleApiService.getAxleDefectBefore(defectId, zoneId, token)
            .enqueue(object : Callback, retrofit2.Callback<List<AxleDefect>> {
                override fun onFailure(call: Call<List<AxleDefect>>, t: Throwable) {
                    axleDefectBeforeCode.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                0
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<List<AxleDefect>>,
                    response: Response<List<AxleDefect>>
                ) {
                    if (response.code() == 200) {
                        axleDefectBeforeCode.onNext(DataResponse.success(response.body()))
                    } else {
                        axleDefectBeforeCode.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Ошибка ${response.code()}",
                                    0
                                )
                            )
                        )
                    }
                }
            })
    }

    fun attemptSave() {
        if (editingAxle.defectBeforeNdt == FullAxle.DEFECT_BEFORE_CONTROL) {
            val checkBaseInfoResult = editingAxle.checkBaseInfo()
            if (checkBaseInfoResult.status != CheckAxleBaseInfoResponse.Status.GOOD) {
                baseInfoError.onNext(checkBaseInfoResult)
                return
            }
            val defectBeforeControlInfoCheckResult = editingAxle.checkDefectBeforeControlInfo()
            if (!defectBeforeControlInfoCheckResult.first) {
                defectBeforeControlInfoError.onNext(defectBeforeControlInfoCheckResult.second ?: "")
                return
            }
        } else if (editingAxle.defectBeforeNdt == FullAxle.DEFECT_AFTER_CONTROL) {
            val checkBaseInfoResult = editingAxle.checkBaseInfo()
            if (checkBaseInfoResult.status != CheckAxleBaseInfoResponse.Status.GOOD) {
                baseInfoError.onNext(checkBaseInfoResult)
                return
            }
            if (!editingAxle.checkDefectAfterControlInfo()) {
                defectAfterControlInfoError.onNext(true)
                return
            }
            val wrongDetailInfoCheckResult = editingAxle.checkWrongDetailInfo()
            if (editingAxle.getResult() == false && !wrongDetailInfoCheckResult.first) {
                wrongDetailInfoError.onNext(wrongDetailInfoCheckResult.second ?: "")
                return
            }
        }

        if (editingAxle.getResult() == true) {
            var message = "Сохранить ось как ГОДНАЯ?"
            if (editingAxle.controlInfo.isNotEmpty()) {
                message += "\n\nИнформация о контроле\n\n${editingAxle.controlInfo}"
            }
            saveGoodDetailMessage.onNext(message)
        } else if (editingAxle.getResult() == false) {
            var message = "Сохранить ось как БРАК?"
            if (editingAxle.controlInfo.isNotEmpty()) {
                message += "\n\nИнформация о контроле\n\n${editingAxle.controlInfo}"
            }
            if (editingAxle.defectInfo.isNotEmpty()) {
                message += "\n\nОписание найденных дефектов\n\n${editingAxle.defectInfo}"
            }
            saveBadDetailMessage.onNext(message)
        } else {
            defectAfterControlInfoError.onNext(true)
        }
    }

    fun save() {
        val token = currentUser.getAccessToken()
        val user = currentUser.getUser()
        axleSaved.onNext(DataResponse.loading())
        axleApiService.checkAddedAxle(CheckAddedAxleRequest(user.company.id, editingAxle), token)
            .enqueue(object : Callback, retrofit2.Callback<CheckAddedDetailResponse> {
                override fun onFailure(call: Call<CheckAddedDetailResponse>, t: Throwable) {
                    axleSaved.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                editingAxle.id
                            )
                        )
                    )
                }

                override fun onResponse(
                    call: Call<CheckAddedDetailResponse>,
                    response: Response<CheckAddedDetailResponse>
                ) {
                    if (response.code() == 200) {
                        val result = response.body()!!
                        if (result.status || !result.status && viewModelState.isEditing == true) {
                            axleApiService.setAxleInfo(
                                SetAxleModel(
                                    User(
                                        user.id.toInt(),
                                        user.name1,
                                        user.name2,
                                        user.name3,
                                        user.company.id,
                                        user.company.name
                                    ), editingAxle,
                                    BuildConfig.VERSION_CODE
                                ), token
                            )
                                .enqueue(object : Callback, retrofit2.Callback<Int> {
                                    override fun onFailure(call: Call<Int>, t: Throwable) {
                                        axleSaved.onNext(
                                            DataResponse.error(
                                                AxleError(
                                                    "Ошибка соединения с сервером",
                                                    editingAxle.id
                                                )
                                            )
                                        )
                                    }

                                    override fun onResponse(
                                        call: Call<Int>,
                                        response: Response<Int>
                                    ) {
                                        if (response.code() == 200) {
                                            editingAxle.id = response.body()!!
                                            axleSaved.onNext(DataResponse.success(response.body()))
                                        } else {
                                            axleSaved.onNext(
                                                DataResponse.error(
                                                    AxleError(
                                                        "Ошибка код: ${response.code()}",
                                                        editingAxle.id
                                                    )
                                                )
                                            )
                                        }
                                    }
                                })
                        } else {
                            axleSaved.onNext(
                                DataResponse.error(
                                    SaveRepeatDetailError(
                                        result.message,
                                        result.date
                                    )
                                )
                            )
                        }
                    } else {
                        axleSaved.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Ошибка код: ${response.code()}",
                                    editingAxle.id
                                )
                            )
                        )
                    }
                }
            })
    }

    fun saveAnyway(date: Date) {
        val token = currentUser.getAccessToken()
        val user = currentUser.getUser()
        axleApiService.setAxleInfo(
            SetAxleModel(
                User(
                    user.id.toInt(),
                    user.name1,
                    user.name2,
                    user.name3,
                    user.company.id,
                    user.company.name
                ), editingAxle,
                BuildConfig.VERSION_CODE
            ), token
        )
            .enqueue(object : Callback, retrofit2.Callback<Int> {
                override fun onFailure(call: Call<Int>, t: Throwable) {
                    axleSaved.onNext(
                        DataResponse.error(
                            AxleError(
                                "Ошибка соединения с сервером",
                                editingAxle.id
                            )
                        )
                    )
                }

                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    if (response.code() == 200) {
                        editingAxle.id = response.body()!!
                        axleSaved.onNext(DataResponse.success(response.body()))
                        armApiService.setRepeatDetail(
                            RepeatDetail(
                                0,
                                user.company.id,
                                user.id.toInt(),
                                1,
                                editingAxle.controlDate,
                                date,
                                editingAxle.axleNumber
                            ), token
                        ).subscribeOn(Schedulers.io()).subscribe().addTo(compositeDisposable)
                    } else {
                        axleSaved.onNext(
                            DataResponse.error(
                                AxleError(
                                    "Ошибка код: ${response.code()}",
                                    editingAxle.id
                                )
                            )
                        )
                    }
                }
            })
    }

    fun dispose() {
        compositeDisposable.clear()
    }
}































