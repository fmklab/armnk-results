package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CheckAddedWheelRequest (
    @SerializedName("companyId")
    @Expose
    val companyId: Int,
    @SerializedName("wheel")
    @Expose
    val wheel: FullWheel
)