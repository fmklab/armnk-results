package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.fmklab.armnk_results.modules.common.models.ControlResult
import com.fmklab.armnk_results.modules.common.models.Factory
import com.fmklab.armnk_results.modules.common.models.Year
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class NumberDetailListItem(
    @SerializedName("id")
    @Expose
    override var id: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("factory")
    @Expose
    val factory: Factory,
    @SerializedName("factoryYear")
    @Expose
    val factoryYear: Year,
    @SerializedName("number")
    @Expose
    val number: String,
    @SerializedName("isNewDetail")
    @Expose
    val isNewDetail: Boolean,
    @SerializedName("result")
    @Expose
    val controlResult: ControlResult,
    @SerializedName("isFromServicePoint")
    @Expose
    val isFromServicePoint: Boolean,
    @SerializedName("noRejectionCriteria")
    @Expose
    val noRejectionCriteria: Boolean,
    @SerializedName("spec")
    @Expose
    val spec: String,
    @SerializedName("defectInfo")
    @Expose
    val defectInfo: String,
    @SerializedName("editStatus")
    @Expose
    val editStatus: Boolean
): AbstractDetail()





































