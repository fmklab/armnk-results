package com.fmklab.armnk_results.modules.common.extensions

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import com.fmklab.armnk_results.R
import java.util.*
import kotlin.collections.ArrayList

fun Long.toDate(): Date {
    val timeZone = TimeZone.getDefault()
    val offsetFromUTC = timeZone.getOffset(Date().time - 1)
    return Date(this + offsetFromUTC)
}

fun <T> AutoCompleteTextView.initArrayAdapter(context: Context, data: ArrayList<T>) {
    ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, data).also { adapter ->
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        this.setAdapter(adapter)
    }
}