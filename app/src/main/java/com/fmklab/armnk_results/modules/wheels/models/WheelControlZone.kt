package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelControlZone(
    @SerializedName("id")
    @Expose
    val id: Int = DEFAULT_ID,
    @SerializedName("vmName")
    @Expose
    val vmName: String? = null,
    @SerializedName("numRowInRep")
    @Expose
    val numRowInRep: Int = 0,
    @SerializedName("fieldName")
    @Expose
    val fieldName: Int = 0,
    @SerializedName("vmInDb")
    @Expose
    val vmInDb: Int = VM_DEFECTATION,
    @SerializedName("title")
    @Expose
    val title: String = ""
) {

    companion object {
        const val DEFAULT_ID = -1

        const val VM_DEFECTATION = 0
        const val VM_ULTRASONIC = 1
        const val VM_MAGNETIC = 2
        const val VM_NONE = -1
    }

    override fun toString(): String {
        return title
    }
}