package com.fmklab.armnk_results.modules.number_detail.viewmodels

import androidx.lifecycle.ViewModel
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.modules.auth.models.User
import com.fmklab.armnk_results.modules.common.Constants
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.extensions.apiQuery
import com.fmklab.armnk_results.modules.common.models.*
import com.fmklab.armnk_results.modules.common.repositories.LocalRepository
import com.fmklab.armnk_results.modules.common.services.ArmApiService
import com.fmklab.armnk_results.modules.number_detail.models.*
import com.fmklab.armnk_results.modules.number_detail.models.DefectMoment
import com.fmklab.armnk_results.modules.number_detail.services.NumberDetailApiService
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*

class EditNumberDetailViewModel(
    private val currentUser: CurrentUser,
    private val numberDetailApi: NumberDetailApiService,
    private val armApiService: ArmApiService,
    private val numberLocalRepository: LocalRepository<NumberDetailDto>
) : ViewModel() {

    private var editViewModelState: EditViewModelState

    private val compositeDisposable = CompositeDisposable()

    private val loadDetailById = PublishRelay.create<Int>()

    private val numberDetailLoading = BehaviorRelay.createDefault(false)
    val numberDetailLoadingState: Observable<Boolean> = numberDetailLoading.hide()

    private val numberDetailError = BehaviorRelay.create<Error>()
    val numberDetailErrorState: Observable<Error> = numberDetailError.hide()

    private val noInternetConnectionError = BehaviorRelay.createDefault(false)
    val noInternetConnectionErrorState: Observable<Boolean> = noInternetConnectionError.hide()

    private val numberDetail = BehaviorRelay.create<NumberDetailDto>()
    val numberDetailState: Observable<NumberDetailDto> = numberDetail.hide()

    private val loadFactories = BehaviorRelay.create<Unit>()

    private val factoriesLoading = BehaviorRelay.createDefault(false)
    val factoriesLoadingState: Observable<Boolean> = factoriesLoading.hide()

    private val baseInfoError = BehaviorRelay.create<Error>()
    val baseInfoErrorState: Observable<Error> = baseInfoError.hide()

    private val factories = BehaviorRelay.create<ArrayList<Factory>>()
    val factoriesState: Observable<ArrayList<Factory>> = factories.hide()

    private val loadFactoryYears = PublishRelay.create<Int>()

    private val yearsLoading = BehaviorRelay.createDefault(false)
    val yearsLoadingState: Observable<Boolean> = yearsLoading.hide()

    private val factoryYears = BehaviorRelay.create<ArrayList<Year>>()
    val factoryYearsState: Observable<ArrayList<Year>> = factoryYears.hide()

    private val loadControlResults = PublishRelay.create<Unit>()

    private val controlResultsLoading = BehaviorRelay.createDefault(false)

    private val controlResults = BehaviorRelay.create<ArrayList<ControlResult>>()
    val controlResultsState: Observable<ArrayList<ControlResult>> = controlResults.hide()

    private val repairDataLoading = BehaviorRelay.createDefault(false)
    val repairDataLoadingState: Observable<Boolean> = repairDataLoading.hide()

    private val repairDataError = PublishRelay.create<Error>()
    val repairDataErrorState: Observable<Error> = repairDataError.hide()

    private val repairDefectTypes = BehaviorRelay.create<ArrayList<DefectType>>()
    val repairDefectTypesState: Observable<ArrayList<DefectType>> = repairDefectTypes.hide()

    private val repairZones = BehaviorRelay.create<ArrayList<ControlZone>>()
    val repairZonesState: Observable<ArrayList<ControlZone>> = repairZones.hide()

    val loadDefectData: PublishRelay<Unit> = PublishRelay.create()

    private val defectDataLoading = BehaviorRelay.createDefault(false)
    val defectDataLoadingState: Observable<Boolean> = defectDataLoading.hide()

    private val defectDataError = BehaviorRelay.create<Error>()
    val defectDataErrorState: Observable<Error> = defectDataError.hide()

    private val defectTypes = BehaviorRelay.create<ArrayList<DefectType>>()
    val defectTypesState: Observable<ArrayList<DefectType>> = defectTypes.hide()

    private val zones = BehaviorRelay.create<ArrayList<ControlZone>>()
    val zonesState: Observable<ArrayList<ControlZone>> = zones.hide()

    private val steals = BehaviorRelay.create<ArrayList<String>>()
    val stealsState: Observable<ArrayList<String>> = steals.hide()

    private val baseInfoFieldError = PublishRelay.create<Unit>()
    val baseInfoFieldErrorState: Observable<Unit> = baseInfoFieldError.hide()

    private val repairInfoFieldError = PublishRelay.create<Unit>()
    val repairFieldErrorState: Observable<Unit> = repairInfoFieldError.hide()

    private val defectInfoFieldError = PublishRelay.create<Unit>()
    val defectInfoFieldErrorState: Observable<Unit> = defectInfoFieldError.hide()

    private val acceptDetailSaving = PublishRelay.create<String>()
    val acceptDetailSavingState: Observable<String> = acceptDetailSaving.hide()

    val loadMethods: PublishRelay<Unit> = PublishRelay.create()

    private val methodsLoading = BehaviorRelay.createDefault(false)
    val methodsLoadingState: Observable<Boolean> = methodsLoading.hide()

    private val methods = BehaviorRelay.create<ArrayList<String>>()
    val methodsState: Observable<ArrayList<String>> = methods.hide()

    val loadDetectors: PublishRelay<Unit> = PublishRelay.create()

    private val detectorsLoading = BehaviorRelay.createDefault(false)
    val detectorsLoadingState: Observable<Boolean> = detectorsLoading.hide()

    private val detectors = BehaviorRelay.create<ArrayList<Detector>>()
    val detectorsState: Observable<ArrayList<Detector>> = detectors.hide()

    private val saveDetailLoading = BehaviorRelay.createDefault(false)
    val saveDetailLoadingState: Observable<Boolean> = saveDetailLoading.hide()

    private val saveDetailError = BehaviorRelay.create<Error>()
    val saveDetailErrorState: Observable<Error> = saveDetailError.hide()

    private val saveDetailResult = BehaviorRelay.create<NumberDetailListItem>()
    val saveDetailResultState: Observable<NumberDetailListItem> = saveDetailResult.hide()

    private val zoneEnabled = BehaviorRelay.createDefault(true)
    val zoneEnabledState: Observable<Boolean> = zoneEnabled.hide()

    //region BASE FIELDS

    val controlDate: BehaviorRelay<Date> = BehaviorRelay.createDefault(Date())

    val factory: BehaviorRelay<Factory> = BehaviorRelay.createDefault(Factory())
    private val emptyFactoryError = PublishRelay.create<Unit>()
    val emptyFactoryErrorState: Observable<Unit> = emptyFactoryError.hide()

    val factoryYear: BehaviorRelay<Year> = BehaviorRelay.createDefault(Year())

    val isNewDetail: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(false)

    val detailNumber: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    private val emptyDetailNumberError = PublishRelay.create<Unit>()
    val emptyDetailNumberErrorState: Observable<Unit> = emptyDetailNumberError.hide()

    val isFromServicePoint: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(false)

    val servicePoint: BehaviorRelay<ServicePoint> =
        BehaviorRelay.createDefault(ServicePoint(Company.NONE, Fault.NONE))

    val noRejectionCriteria: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(false)

    val controlResult: BehaviorRelay<ControlResult> = BehaviorRelay.create()
    private val noControlResultError = PublishRelay.create<Unit>()
    val noControlResultErrorState: Observable<Unit> = noControlResultError.hide()
    private val goodResultFactoryError = PublishRelay.create<String>()
    val goodResultFactoryErrorState: Observable<String> = goodResultFactoryError.hide()

    //endregion

    //region DEFECT FIELDS

    val defectType: BehaviorRelay<DefectType> = BehaviorRelay.createDefault(DefectType())
    private val emptyDefectTypeError = PublishRelay.create<Unit>()
    val emptyDefectTypeErrorState: Observable<Unit> = emptyDefectTypeError.hide()

    val zone: BehaviorRelay<ControlZone> = BehaviorRelay.createDefault(ControlZone())
    private val emptyZoneError = PublishRelay.create<Unit>()
    val emptyZoneErrorState: Observable<Unit> = emptyZoneError.hide()

    val repairDescription: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    private val repairDescriptionError = PublishRelay.create<Unit>()
    val repairDescriptionErrorState: Observable<Unit> = repairDescriptionError.hide()

    val defectMoment: BehaviorRelay<DefectMoment> = BehaviorRelay.createDefault(DefectMoment.NONE)

    val steal: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    private val emptyStealError = PublishRelay.create<Unit>()
    val emptyStealErrorState: Observable<Unit> = emptyStealError.hide()

    val owner: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    private val emptyOwnerError = PublishRelay.create<Unit>()
    val emptyOwnerErrorState: Observable<Unit> = emptyOwnerError.hide()

    val defectDescription: BehaviorRelay<String> = BehaviorRelay.createDefault("")

    val defectLength: BehaviorRelay<String> = BehaviorRelay.createDefault("")

    val defectDepth: BehaviorRelay<String> = BehaviorRelay.createDefault("")

    val defectDiameter: BehaviorRelay<String> = BehaviorRelay.createDefault("")

    val method: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    private val emptyMethodError = PublishRelay.create<Unit>()
    val emptyMethodErrorState: Observable<Unit> = emptyMethodError.hide()

    val detector: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    private val emptyDetectorError = PublishRelay.create<Unit>()
    val emptyDetectorErrorState: Observable<Unit> = emptyDetectorError.hide()

    val defectSize: BehaviorRelay<String> = BehaviorRelay.createDefault("")

    private var defectInfo: String? = null
    //endregion

    //region EVENTS

    private val servicePointCompanyChanged = PublishRelay.create<Company>()
    val servicePointCompanyChangedConsumer: Consumer<Company> = servicePointCompanyChanged

    private val servicePointFaultChanged = PublishRelay.create<Fault>()
    val servicePointFaultChangedConsumer: Consumer<Fault> = servicePointFaultChanged

    private val saveClicked = PublishRelay.create<Unit>()
    val saveClickedConsumer: Consumer<Unit> = saveClicked

    private val acceptSaveClicked = PublishRelay.create<Unit>()
    val acceptSaveClickedConsumer: Consumer<Unit> = acceptSaveClicked

    private val loadRepairDataClick = PublishRelay.create<Unit>()
    val loadRepairDataClickConsumer: Consumer<Unit> = loadRepairDataClick

    private val saveDetailAnywayClick = PublishRelay.create<Date>()
    val saveDetailAnywayClickConsumer: Consumer<Date> = saveDetailAnywayClick

    //endregion

    companion object {
        private const val DUMP_DETAIL_TAG = "NumberDumpDetailTag"
        private const val DUMP_VM_STATE_TAG = "DumpVMStateTag"
    }

    init {
        editViewModelState =
            EditViewModelState(
                null
            )
        setupLoaders()
        setupFields()
        setupEvents()
    }

    private fun setupLoaders() {
        loadDetailById
            .filter { !isNumberDetailLoading() }
            .doOnNext { showNumberDetailLoading() }
            .flatMap { id ->
                val token = currentUser.getAccessToken()
                numberDetailApi.getNumberDetailById(id, token)
                    .apiQuery(noInternetConnectionError)
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideNumberDetailLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    val detail = it.data!!
                    acceptDetail(detail)
                }
            }
            .addTo(compositeDisposable)

        loadFactories
            .filter { !isFactoriesLoading() }
            .doOnNext { showFactoriesLoading() }
            .flatMap {
                val partId = numberDetail.value.partId
                val token = currentUser.getAccessToken()
                armApiService.getFactories(partId, token)
                    .apiQuery(noInternetConnectionError)
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideFactoriesLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    factories.accept(it.data)
                } else if (it.status == DataResponse.Status.ERROR) {
                    baseInfoError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)

        loadFactoryYears
            .filter { !isFactoryYearsLoading() }
            .doOnNext { showFactoryYearsLoading() }
            .flatMap { id ->
                factoryYears.accept(ArrayList())
                val token = currentUser.getAccessToken()
                numberDetailApi.getFactoryYears(id, token)
                    .apiQuery(noInternetConnectionError)
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideFactoryYearsLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    factoryYears.accept(it.data)
                    val years = it.data
                    val selectedYear = years?.find { year -> year == factoryYear.value } ?: Year()
                    factoryYear.accept(selectedYear)
                } else if (it.status == DataResponse.Status.ERROR) {
                    baseInfoError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)

        loadControlResults
            .filter { !isControlResultsLoading() }
            .doOnNext { showControlResultsLoading() }
            .flatMap {
                val partId = numberDetail.value.partId
                val token = currentUser.getAccessToken()
                armApiService.getResults(partId, token)
                    .apiQuery(noInternetConnectionError)
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideControlResultsLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    controlResults.accept(it.data)
                } else if (it.status == DataResponse.Status.ERROR) {
                    baseInfoError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)

        loadRepairDataClick
            .filter { !isRepairDataLoading() }
            .doOnNext { showRepairDataLoading() }
            .flatMap {
                defectInfo = ""
                repairDefectTypes.accept(ArrayList())
                repairZones.accept(ArrayList())
                val partId = numberDetail.value.partId
                val token = currentUser.getAccessToken()
                Observable.zip(
                    numberDetailApi.getRepairDefectTypes(partId, token)
                        .apiQuery(noInternetConnectionError),
                    numberDetailApi.getRepairZones(partId, token)
                        .apiQuery(noInternetConnectionError),
                    BiFunction { defectTypes: DataResponse<ArrayList<DefectType>>, zones: DataResponse<ArrayList<ControlZone>> ->
                        val list = ArrayList<Any>()
                        list.add(defectTypes)
                        list.add(zones)
                        list
                    }
                )
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideRepairDataLoading() }
            .subscribe {
                val defectTypes = it[0] as DataResponse<*>
                val zones = it[1] as DataResponse<*>
                if (defectTypes.status == DataResponse.Status.SUCCESS && zones.status == DataResponse.Status.SUCCESS) {
                    val defectTypesData = defectTypes.data as ArrayList<DefectType>
                    val zonesData = zones.data as ArrayList<ControlZone>
                    this.repairDefectTypes.accept(defectTypesData)
                    this.repairZones.accept(zonesData)
                    val selectedDefectType =
                        defectTypesData.find { d -> d.id == defectType.value.id }
                    if (selectedDefectType == null && defectTypesData.size == 1) {
                        defectType.accept(defectTypesData[0])
                    } else {
                        defectType.accept(selectedDefectType ?: DefectType())
                    }
                    val selectedZone = zonesData.find { z -> z.id == zone.value.id }
                    if (selectedZone == null && zonesData.size == 1) {
                        zone.accept(zonesData[0])
                    } else {
                        zone.accept(selectedZone ?: ControlZone())
                    }
                } else if (defectTypes.status == DataResponse.Status.ERROR) {
                    repairDataError.accept(defectTypes.error)
                } else if (zones.status == DataResponse.Status.ERROR) {
                    repairDataError.accept(zones.error)
                }
            }
            .addTo(compositeDisposable)

        loadDefectData
            .filter { !isDefectDataLoading() }
            .doOnNext { showDefectDataLoading() }
            .flatMap {
                defectInfo = ""
                defectTypes.accept(ArrayList())
                zones.accept(ArrayList())
                val partId = numberDetail.value.partId
                val defectMoment = this.defectMoment.value
                val token = currentUser.getAccessToken()
                Observable.zip(
                    numberDetailApi.getDefectTypes(partId, defectMoment, token)
                        .apiQuery(noInternetConnectionError),
                    numberDetailApi.getZones(partId, defectMoment, token)
                        .apiQuery(noInternetConnectionError),
                    numberDetailApi.getSteal(token).apiQuery(noInternetConnectionError),
                    Function3 { defectTypes: DataResponse<ArrayList<DefectType>>, zones: DataResponse<ArrayList<ControlZone>>, steals: DataResponse<ArrayList<String>> ->
                        val list = ArrayList<Any>()
                        list.add(defectTypes)
                        list.add(zones)
                        list.add(steals)
                        list
                    }
                )
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideDefectDataLoading() }
            .subscribe {
                val defectTypes = it[0] as DataResponse<*>
                val zones = it[1] as DataResponse<*>
                val steals = it[2] as DataResponse<*>
                if (defectTypes.status == DataResponse.Status.SUCCESS && zones.status == DataResponse.Status.SUCCESS && steals.status == DataResponse.Status.SUCCESS) {
                    val defectTypesData = defectTypes.data as ArrayList<DefectType>
                    val zonesData = zones.data as ArrayList<ControlZone>
                    val stealsData = steals.data as ArrayList<String>
                    this.defectTypes.accept(defectTypesData)
                    this.zones.accept(zonesData)
                    this.steals.accept(stealsData)
                    val selectedDefectType =
                        defectTypesData.find { dt -> dt.id == defectType.value.id }
                    if (selectedDefectType == null && defectTypesData.size == 1) {
                        defectType.accept(defectTypesData[0])
                    } else {
                        defectType.accept(selectedDefectType ?: DefectType())
                    }
                    if (defectMoment.value == DefectMoment.NDT) {
                        defectType.accept(defectTypesData.find { dt -> dt.name == "трещина" }
                            ?: DefectType())
                    }
//                    val selectedZone = zonesData.find { z -> z.id == zone.value.id }
//                    if (selectedZone == null && zonesData.size == 1) {
//                        zone.accept(zonesData[0])
//                    } else {
//                        zone.accept(selectedZone ?: ControlZone())
//                    }
                    if (stealsData.size == 1) {
                        steal.accept(stealsData[0])
                    }
                } else if (defectTypes.status == DataResponse.Status.ERROR) {
                    defectDataError.accept(defectTypes.error)
                } else if (zones.status == DataResponse.Status.ERROR) {
                    defectDataError.accept(zones.error)
                } else if (steals.status == DataResponse.Status.ERROR) {
                    defectDataError.accept(steals.error)
                }
            }
            .addTo(compositeDisposable)

        loadMethods
            .filter { !isMethodsLoading() }
            .doOnNext { showMethodsLoading() }
            .flatMap {
                val partId = numberDetail.value.partId
                val token = currentUser.getAccessToken()
                armApiService.getMethods(partId, token)
                    .apiQuery(noInternetConnectionError)
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideMethodsLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    methods.accept(it.data)
                    val methods = it.data ?: ArrayList()
                    val selectedMethod = methods.find { m -> m == method.value }
                    if (selectedMethod == null && methods.size == 1) {
                        method.accept(methods[0])
                    } else {
                        method.accept(selectedMethod ?: "")
                    }
                } else if (it.status == DataResponse.Status.ERROR) {
                    defectDataError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)

        loadDetectors
            .filter { !isDetectorsLoading() }
            .doOnNext { showDetectorsLoading() }
            .flatMap {
                detectors.accept(ArrayList())
                val companyId = currentUser.getUser().company.id
                val method = this.method.value
                val token = currentUser.getAccessToken()
                armApiService.getDetectors(companyId, method!!, token)
                    .apiQuery(noInternetConnectionError)
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideDetectorsLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    detectors.accept(it.data)
                    val detectors = it.data ?: ArrayList()
                    if (detectors.size == 1) {
                        detector.accept(detectors[0].toString())
                    }
                } else if (it.status == DataResponse.Status.ERROR) {
                    defectDataError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)
    }

    private fun setupFields() {
        factory
            .subscribeOn(Schedulers.io())
            .subscribe { loadFactoryYears(it.id) }
            .addTo(compositeDisposable)

        isFromServicePoint
            .subscribeOn(Schedulers.io())
            .subscribe {
                if (!it) {
                    noRejectionCriteria.accept(false)
                    servicePoint.accept(ServicePoint(Company.NONE, Fault.NONE))
                }
            }
            .addTo(compositeDisposable)

        controlResult
            .subscribeOn(Schedulers.io())
            .subscribe {
                if (it.name == "ремонт") {
                    defectMoment.accept(DefectMoment.NONE)
                }
            }
            .addTo(compositeDisposable)

        method
            .subscribeOn(Schedulers.io())
            .subscribe { loadDetectors.accept(Unit) }
            .addTo(compositeDisposable)

        defectType
            .subscribeOn(Schedulers.io())
            .subscribe {
                if ((numberDetail.value?.partId == Constants.SIDE_FRAME || numberDetail.value?.partId == Constants.SUPERSTRUCTURE_BEAM) &&
                    (it.name == "исправление/отсутствие знаков маркировки" || it.name == "исключение по базе АСУ" || it.name == "по износу" || it.name == "истек срок службы")
                ) {
                    zoneEnabled.accept(false)
                    zone.accept(ControlZone())
                } else {
                    zoneEnabled.accept(true)
                }
                if (it.id == 24) {
                    zoneEnabled.accept(false)
                    zone.accept(ControlZone())
                }
            }
            .addTo(compositeDisposable)
    }

    private fun setupEvents() {
        servicePointCompanyChanged
            .subscribeOn(Schedulers.io())
            .subscribe {
                val prevServicePoint = servicePoint.value
                servicePoint.accept(ServicePoint(it, prevServicePoint.fault))
            }
            .addTo(compositeDisposable)

        servicePointFaultChanged
            .subscribeOn(Schedulers.io())
            .subscribe {
                val prevServicePoint = servicePoint.value
                servicePoint.accept(ServicePoint(prevServicePoint.company, it))
            }
            .addTo(compositeDisposable)

        saveClicked
            .subscribeOn(Schedulers.io())
            .subscribe {
                val baseInfoResult = checkBaseInfo()
                if (!baseInfoResult) {
                    return@subscribe
                }
                when (controlResult.value.name) {
                    "годен" -> {
                        defectInfo
                        acceptDetailSaving.accept("Сохранить деталь как ГОДНАЯ?")
                    }
                    "ремонт" -> {
                        val repairInfoResult = checkRepairInfo()
                        if (!repairInfoResult) {
                            return@subscribe
                        }
                        if (defectInfo != null) {
                            createRepairInfo()
                        }
                        acceptDetailSaving.accept(
                            "Сохранить деталь как РЕМОНТ?\n\n" +
                                    defectInfo
                        )
                    }

                    "брак" -> {
                        val defectInfoResult = checkDefectInfo()
                        if (!defectInfoResult) {
                            return@subscribe
                        }
                        if (defectInfo != null) {
                            createDefectInfo()
                        }
                        acceptDetailSaving.accept("Сохранить деталь как БРАК?\n\n${defectInfo ?: numberDetail.value.defectInfo}")
                    }
                }
            }
            .addTo(compositeDisposable)

        acceptSaveClicked
            .filter { !isSaveDetailLoading() }
            .doOnNext { showSaveDetailLoading() }
            .flatMap {
                val detail = createDetail()
                val token = currentUser.getAccessToken()!!
                val user = currentUser.getUser()
                numberDetailApi.checkAddedDetail(
                    user.company.id,
                    detail.partId,
                    detail.number,
                    token
                )
                    .apiQuery()
                    .flatMap {
                        if (it.status == DataResponse.Status.ERROR) {
                            Observable.just(it)
                        } else {
                            if (it.data!!.status || !it.data.status && editViewModelState.isEditing == true) {
                                detail.spec = user.toString()
                                numberDetail.accept(detail)
                                val request = SetDetailRequest(
                                    User(
                                        user.id.toInt(),
                                        user.name1,
                                        user.name2,
                                        user.name3,
                                        user.company.id,
                                        user.company.name
                                    ), detail
                                )
                                numberDetailApi.setDetail(request, token).apiQuery()
                            } else {
                                Observable.just(
                                    DataResponse.error(
                                        SaveRepeatDetailError(
                                            it.data.message,
                                            it.data.date
                                        )
                                    )
                                )
                            }
                        }
                    }
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideSaveDetailLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    val detail = numberDetail.value
                    val newDetail = NumberDetailListItem(
                        it.data!! as Int,
                        detail.partId,
                        detail.controlDate,
                        detail.factory,
                        detail.factoryYear,
                        detail.number,
                        detail.isNewDetail,
                        detail.controlResult,
                        detail.isFromServicePoint,
                        detail.noRejectionCriteria,
                        detail.spec,
                        detail.defectInfo,
                        true
                    )
                    saveDetailResult.accept(newDetail)
                } else if (it.status == DataResponse.Status.ERROR) {
                    saveDetailError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)

        saveDetailAnywayClick
            .filter { !isSaveDetailLoading() }
            .doOnNext { showSaveDetailLoading() }
            .flatMap {
                val detail = createDetail()
                val token = currentUser.getAccessToken()
                val user = currentUser.getUser()
                detail.spec = user.toString()
                numberDetail.accept(detail)
                val request = SetDetailRequest(
                    User(
                        user.id.toInt(),
                        user.name1,
                        user.name2,
                        user.name3,
                        user.company.id,
                        user.company.name
                    ), detail
                )
                val repeatDetail =
                    RepeatDetail(
                        0,
                        user.company.id,
                        user.id.toInt(),
                        detail.partId,
                        it,
                        detail.controlDate,
                        detail.number
                    )
                numberDetailApi.setDetail(request, token).apiQuery()
                    .flatMap { response ->
                        if (response.status == DataResponse.Status.ERROR) {
                            Observable.just(response)
                        } else {
                            armApiService.setRepeatDetail(repeatDetail, token)
                                .subscribeOn(Schedulers.io()).subscribe()
                            Observable.just(response)
                        }
                    }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnEach { hideSaveDetailLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    val detail = numberDetail.value
                    val newDetail = NumberDetailListItem(
                        it.data!! as Int,
                        detail.partId,
                        detail.controlDate,
                        detail.factory,
                        detail.factoryYear,
                        detail.number,
                        detail.isNewDetail,
                        detail.controlResult,
                        detail.isFromServicePoint,
                        detail.noRejectionCriteria,
                        detail.spec,
                        detail.defectInfo,
                        true
                    )
                    saveDetailResult.accept(newDetail)
                } else if (it.status == DataResponse.Status.ERROR) {
                    saveDetailError.accept(it.error)
                }
            }
            .addTo(compositeDisposable)
    }

    private fun isNumberDetailLoading(): Boolean = numberDetailLoading.value

    private fun showNumberDetailLoading() {
        numberDetailLoading.accept(true)
    }

    private fun hideNumberDetailLoading() {
        numberDetailLoading.accept(false)
    }

    private fun isFactoriesLoading(): Boolean = factoriesLoading.value

    private fun showFactoriesLoading() {
        factoriesLoading.accept(true)
    }

    private fun hideFactoriesLoading() {
        factoriesLoading.accept(false)
    }

    private fun isFactoryYearsLoading(): Boolean = yearsLoading.value

    private fun showFactoryYearsLoading() {
        yearsLoading.accept(true)
    }

    private fun hideFactoryYearsLoading() {
        yearsLoading.accept(false)
    }

    private fun isControlResultsLoading(): Boolean = controlResultsLoading.value

    private fun showControlResultsLoading() {
        controlResultsLoading.accept(true)
    }

    private fun hideControlResultsLoading() {
        controlResultsLoading.accept(false)
    }

    private fun acceptDetail(detail: NumberDetailDto) {
        numberDetail.accept(detail)
        controlDate.accept(detail.controlDate)
        factoryYear.accept(detail.factoryYear)
        factory.accept(detail.factory)
        isNewDetail.accept(detail.isNewDetail)
        detailNumber.accept(detail.number)
        isFromServicePoint.accept(detail.isFromServicePoint)
        servicePoint.accept(detail.servicePoint)
        controlResult.accept(detail.controlResult)
        //defectInfo = detail.defectInfo
        if (detail.defectType != null) {
            defectType.accept(detail.defectType)
        }
        if (detail.zone != null) {
            zone.accept(detail.zone)
        }
        repairDescription.accept(detail.repairDescription ?: "")
        defectMoment.accept(detail.defectMoment)
        steal.accept(detail.steal ?: "")
        owner.accept(detail.owner ?: "")
        defectDescription.accept(detail.defectDescription ?: "")
        defectLength.accept(detail.defectLength ?: "")
        defectDepth.accept(detail.defectDepth ?: "")
        defectDiameter.accept(detail.defectDiameter ?: "")
        method.accept(detail.method ?: "")
        detector.accept(detail.detector ?: "")
        defectSize.accept(detail.defectSize ?: "")
        loadBaseInfo()
    }

    private fun loadFactories() {
        loadFactories.accept(Unit)
    }

    private fun loadFactoryYears(factoryId: Int) {
        if (factoryId > 0) {
            loadFactoryYears.accept(factoryId)
        }
    }

    private fun loadControlResults() {
        loadControlResults.accept(Unit)
    }

    private fun isRepairDataLoading(): Boolean = repairDataLoading.value

    private fun showRepairDataLoading() {
        repairDataLoading.accept(true)
    }

    private fun hideRepairDataLoading() {
        repairDataLoading.accept(false)
    }

    private fun isDefectDataLoading(): Boolean = defectDataLoading.value

    private fun showDefectDataLoading() {
        defectDataLoading.accept(true)
    }

    private fun hideDefectDataLoading() {
        defectDataLoading.accept(false)
    }

    private fun isMethodsLoading(): Boolean = methodsLoading.value

    private fun showMethodsLoading() {
        methodsLoading.accept(true)
    }

    private fun hideMethodsLoading() {
        methodsLoading.accept(false)
    }

    private fun isDetectorsLoading(): Boolean = detectorsLoading.value

    private fun showDetectorsLoading() {
        detectorsLoading.accept(true)
    }

    private fun hideDetectorsLoading() {
        detectorsLoading.accept(false)
    }

    private fun isSaveDetailLoading(): Boolean = saveDetailLoading.value

    private fun showSaveDetailLoading() {
        saveDetailLoading.accept(true)
    }

    private fun hideSaveDetailLoading() {
        saveDetailLoading.accept(false)
    }

    private fun checkBaseInfo(): Boolean {
        var result = true
        if (!factory.hasValue() || factory.value.id == Factory.DEFAULT_ID) {
            result = false
            emptyFactoryError.accept(Unit)
        }

        if (!detailNumber.hasValue() || detailNumber.value.isNullOrBlank()) {
            result = false
            emptyDetailNumberError.accept(Unit)
        }

        if (!controlResult.hasValue() || controlResult.value.id == ControlResult.DEFAULT_ID) {
            result = false
            noControlResultError.accept(Unit)
        }

        if (controlResult.hasValue() && controlResult.value.name == "годен" && (numberDetail.value.partId == Constants.SIDE_FRAME || numberDetail.value.partId == Constants.SUPERSTRUCTURE_BEAM)) {

            if (factory.hasValue() && factory.value.code.toLowerCase(Locale.ROOT) == "нет") {
                result = false
                goodResultFactoryError.accept("Если отсутствует клеймо завода-изготовителя, то деталь необходимо забраковать!")
            } else if (factoryYear.hasValue() && factoryYear.value.value == -1) {
                result = false
                goodResultFactoryError.accept("Если отсутствует год изготовления, то деталь необходимо забраковать!")
            }
        }

        if (!result) {
            baseInfoFieldError.accept(Unit)
        }

        return result
    }

    private fun checkRepairInfo(): Boolean {
        var result = true
        if (!defectType.hasValue() || defectType.value.id == DefectType.DEFAULT_ID) {
            result = false
            emptyDefectTypeError.accept(Unit)
        }

        if (!zone.hasValue() || zone.value.id == ControlZone.DEFAULT_ID) {
            result = false
            emptyZoneError.accept(Unit)
        }

        if (!repairDescription.hasValue() || repairDescription.value.length < 10) {
            result = false
            repairDescriptionError.accept(Unit)
        }

        if (!result) {
            repairInfoFieldError.accept(Unit)
        }
        return result
    }

    private fun checkDefectInfo(): Boolean {
        var result = true
        if (!defectType.hasValue() || defectType.value.id == DefectType.DEFAULT_ID) {
            result = false
            emptyDefectTypeError.accept(Unit)
        }

        if (!((defectType.value.name == "исправление/отсутствие знаков маркировки" || defectType.value.name == "исключение по базе АСУ" || defectType.value.name == "по износу" || defectType.value.name == "истек срок службы")
                    && (numberDetail.value?.partId == Constants.SIDE_FRAME || numberDetail.value?.partId == Constants.SUPERSTRUCTURE_BEAM) || defectType.value.id == 24)
        ) {
            if (!zone.hasValue() || zone.value.id == ControlZone.DEFAULT_ID) {
                result = false
                emptyZoneError.accept(Unit)
            }
        }

        if (!steal.hasValue() || steal.value.isNullOrBlank()) {
            result = false
            emptyStealError.accept(Unit)
        }

        if (!owner.hasValue() || owner.value.isNullOrBlank()) {
            result = false
            emptyOwnerError.accept(Unit)
        }

        if (defectMoment.value == DefectMoment.NDT) {
            if (!method.hasValue() || method.value.isBlank()) {
                result = false
                emptyMethodError.accept(Unit)
            }

            if (!detector.hasValue() || detector.value.isBlank()) {
                result = false
                emptyDetectorError.accept(Unit)
            }
        }

        if (!result) {
            defectInfoFieldError.accept(Unit)
        }

        return result
    }

    private fun createRepairInfo() {
        defectInfo = "тип дефекта: \"${defectType.value}\"; зона выявления: \"${zone.value}\"; " +
                "описание дефекта: \"${repairDescription.value}\";"
    }

    private fun createDefectInfo() {
        defectInfo = "обнаружен: "
        when (defectMoment.value) {
            DefectMoment.DEFECATION -> {
                defectInfo += "\"при дефектации\"; тип дефекта: \"${defectType.value}\"; "
                if (zone.value.id != ControlZone.DEFAULT_ID) {
                    defectInfo += "зона выявления: \"${zone.value}\"; "
                }
                if (defectDescription.value.isNotBlank()) {
                    defectInfo += "описание: \"${defectDescription.value}\"; "
                }
                defectInfo += "сталь: \"${steal.value}\"; собственник: \"${owner.value}\";"
            }
            DefectMoment.VISUAL -> {
                defectInfo += "\"визуально\"; тип дефекта: \"${defectType.value}\"; зона выявления: " +
                        "\"${zone.value}\"; "
                if (defectLength.value.isNotBlank()) {
                    defectInfo += "L=${defectLength.value} мм; "
                }
                if (defectDepth.value.isNotBlank()) {
                    defectInfo += "H=${defectDepth.value} мм; "
                }
                if (defectDiameter.value.isNotBlank()) {
                    defectInfo += "D=${defectDiameter.value} мм; "
                }
                defectInfo += "сталь: \"${steal.value}\"; собственник: \"${owner.value}\";"
            }

            DefectMoment.NDT -> {
                defectInfo += "\"средствами НК\"; вид (метод): \"${method.value}\"; тип дефекта: \"${defectType.value}\"; "
                if (defectSize.value.isNotBlank()) {
                    defectInfo += "размер (трещины), мм: \"${defectSize.value}\"; "
                }
                defectInfo += "тип дефектоскопа: \"${detector.value}\"; зона выявления: \"${zone.value}\"; " +
                        "сталь: \"${steal.value}\"; собственник: \"${owner.value}\""
            }
        }
    }

    private fun createDetail(): NumberDetailDto {
        val detail = numberDetail.value
        detail.controlDate = controlDate.value
        detail.factory = factory.value
        detail.factoryYear = factoryYear.value
        detail.isNewDetail = isNewDetail.value
        detail.number = detailNumber.value
        detail.isFromServicePoint = isFromServicePoint.value
        detail.servicePoint = servicePoint.value
        detail.noRejectionCriteria = noRejectionCriteria.value
        detail.controlResult = controlResult.value
        detail.defectType = defectType.value
        detail.zone = zone.value
        detail.repairDescription = repairDescription.value
        detail.defectMoment = defectMoment.value
        detail.steal = steal.value
        detail.owner = owner.value
        detail.defectDescription = defectDescription.value
        detail.defectLength = defectLength.value
        detail.defectDepth = defectDepth.value
        detail.defectDiameter = defectDiameter.value
        detail.method = method.value
        detail.detector = detector.value
        detail.defectSize = defectSize.value
        if (defectInfo != null) {
            detail.defectInfo = defectInfo!!
        }
        return detail
    }

    fun loadNumberDetailById(id: Int, partId: Int? = null) {
        if (id == 0) {
            val newDetail = NumberDetailDto(id, partId!!)
            acceptDetail(newDetail)
            editViewModelState = editViewModelState.copy(isEditing = false)
        } else {
            loadDetailById.accept(id)
            editViewModelState = editViewModelState.copy(isEditing = true)
        }
    }

    fun dumpNumberDetail() {
        val detail = createDetail()
        numberLocalRepository.dumpDetail(
            detail,
            DUMP_DETAIL_TAG
        )
        numberLocalRepository.dumpObject(editViewModelState, DUMP_VM_STATE_TAG)
    }

    fun restoreNumberDetail() {
        val detail = numberLocalRepository.loadDetail(DUMP_DETAIL_TAG, NumberDetailDto::class.java)
        if (detail != null) {
            acceptDetail(detail)
        }
        editViewModelState =
            (numberLocalRepository.loadObject(
                DUMP_VM_STATE_TAG,
                EditViewModelState::class.java
            ) as? EditViewModelState)
                ?: EditViewModelState(
                    null
                )
    }

    fun loadBaseInfo() {
        loadFactories()
        loadControlResults()
    }

    override fun onCleared() {
        super.onCleared()

        compositeDisposable.clear()
    }
}







































