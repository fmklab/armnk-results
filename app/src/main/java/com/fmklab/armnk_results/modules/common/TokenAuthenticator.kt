package com.fmklab.armnk_results.modules.common

import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.retrofit.LoginApi
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator(
    private val currentUser: CurrentUser,
    private val loginApi: LoginApi
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val newToken = getNewToken()
        return response.request().newBuilder().header("Authorization", "Bearer $newToken").build()
    }

    private fun getNewToken(): String {
        val oldToken = currentUser.getToken()
        if (oldToken != null) {
            val accessToken = oldToken.accessToken
            val refreshToken = oldToken.refreshToken
            val newToken = loginApi.refreshToken(accessToken, refreshToken).execute().body()!!
            currentUser.setToken(newToken)
            return newToken.accessToken
        }
        return ""
    }
}