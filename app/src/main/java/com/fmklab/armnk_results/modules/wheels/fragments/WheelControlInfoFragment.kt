package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.ManSpinnerAdapter
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.FatalError
import com.fmklab.armnk_results.modules.common.models.InternetConnectionError
import com.fmklab.armnk_results.modules.wheels.models.*
import com.fmklab.armnk_results.modules.wheels.repositories.WheelRepository
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.checkbox.MaterialCheckBox
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_wheel_control_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WheelControlInfoFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wheel_control_info, container, false)
    }

    override fun onResume() {
        super.onResume()

        loadControlZones()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    private fun setupUI() {
        doUltrasonicControlAutoCompleteTextView.keyListener = null
        doMagneticVortexAutoCompleteTextView.keyListener = null
    }

    private fun setupBindings() {
        viewModel.wheelControlInfoControlZonesInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                        showRefresh(false)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventControlInfoSuccessLoading())
                        if (it.data != null) {
                            ultrasonicControlLinearLayout.removeAllViews()
                            magneticVortexLinearLayout.removeAllViews()
                            if (viewModel.getControlZones().isNullOrEmpty()) {
                                viewModel.setControlZones(it.data)
                            }
                           // val isNewData = viewModel.getSelectedControlZones().isNullOrEmpty()
                            for (controlZone in viewModel.getControlZones()!!) {
                                viewModel.setSelectedControlZone(controlZone.fieldName)
                                val checkBox = MaterialCheckBox(requireContext())
                                checkBox.text = controlZone.title
                                checkBox.tag = controlZone
                                checkBox.isChecked = true
                                checkBox.setOnCheckedChangeListener { _, isChecked ->
                                    val cz = checkBox.tag as WheelControlZone
                                    if (isChecked) {
                                        viewModel.setSelectedControlZone(cz.fieldName)
                                    } else {
                                        viewModel.deselectControlZone(cz.fieldName)
                                    }
                                    val controlInfo = viewModel.getControlInfo()
                                    controlInfoTextView.text = controlInfo
                                }
                                when (controlZone.vmInDb) {
                                    WheelControlZone.VM_ULTRASONIC -> ultrasonicControlLinearLayout.addView(checkBox)
                                    WheelControlZone.VM_MAGNETIC -> magneticVortexLinearLayout.addView(checkBox)
                                }
                                checkBox.isChecked = viewModel.getFullWheel()?.isNewDetail != true
                                checkBox.isEnabled = false
//                                if (isNewData) {
//                                    checkBox.isChecked = true
//                                } else {
//                                    checkBox.isChecked =
//                                        viewModel.getSelectedControlZones()
//                                            ?.any { sz -> sz == controlZone.fieldName } ?: true
//                                }
                            }
                        }
                        viewModel.loadSpecialists()
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventControlInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.specialistsInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status){
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventControlInfoSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            ManSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                doUltrasonicControlAutoCompleteTextView.setAdapter(adapter)
                                doMagneticVortexAutoCompleteTextView.setAdapter(adapter)
                            }
                            val wheel = viewModel.getFullWheel()
                            val ultrasonicUser: String
                            val magneticUser: String
                            if (wheel?.id == FullWheel.DEFAULT_ID) {
                                val repository = WheelRepository(requireContext())
                                ultrasonicUser = repository.getUltrasonicControlUser() ?: ""
                                magneticUser = repository.getMagneticControlUser() ?: ""
                            } else {
                                ultrasonicUser = wheel?.wheelControlInfo?.ultrasonicSpec ?: ""
                                magneticUser = wheel?.wheelControlInfo?.magneticSpec ?: ""
                            }
                            doUltrasonicControlAutoCompleteTextView.setText(ultrasonicUser, false)
                            doMagneticVortexAutoCompleteTextView.setText(magneticUser, false)
                        }
                        val controlInfo = viewModel.getControlInfo()
                        controlInfoTextView.text = controlInfo
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventControlInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventRepairInfoSet::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                viewModel.removeControlInfoZones()
                loadControlZones()
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventControlInfoRequestCheckFields::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                val result = checkFields()
                RxBus.publish(RxEvent.EventControlInfoCheckFieldsResponse(result))
            }).addTo(compositeDisposable)

        refreshButton.setOnClickListener {
            loadControlZones()
        }

        doUltrasonicControlAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setUltrasonicControlUser(text.toString())
                WheelRepository(requireContext()).setUltrasonicControlUser(text.toString())
                val controlInfo = viewModel.getControlInfo()
                controlInfoTextView.text = controlInfo
            }
            if (text!!.isNotBlank()) {
                doUltrasonicControlTextInputLayout.error = null
            }
        }

        doMagneticVortexAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setMagneticControlUser(text.toString())
                WheelRepository(requireContext()).setMagneticControlUser(text.toString())
                val controlInfo = viewModel.getControlInfo()
                controlInfoTextView.text = controlInfo
            }
            if (text!!.isNotBlank()) {
                doMagneticVortexTextInputLayout.error = null
            }
        }

        removeUltrasonicManButton.setOnClickListener {
            doUltrasonicControlAutoCompleteTextView.setText("", false)
        }

        removeMagneticVortexManButton.setOnClickListener {
            doMagneticVortexAutoCompleteTextView.setText("", false)
        }
    }

    private fun loadControlZones() {
        val fullWheel = viewModel.getFullWheel()
        val controlInfo = fullWheel?.wheelControlInfo
        if (controlInfo != null) {
            val wheelControlZonesRequest = WheelControlZonesRequest(
                WheelControlZone.VM_NONE,
                controlInfo.wheelThickness,
                controlInfo.ringsState,
                WheelDefectInfo.DEFECT_MOMENT_NONE,
                controlInfo.repairType,
                fullWheel.type.getWheelType(),
                true,
                false,
                controlInfo.turning == WheelControlInfo.TURNING_YES)
            viewModel.loadControlZones(wheelControlZonesRequest, WheelDefectInfo.DEFECT_MOMENT_NONE)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
        } else {
            loadingProgressBar.visibility = View.GONE
        }
    }

    private fun showRefresh(show: Boolean, message: String? = null) {
        if (show) {
            controlInfoConstraintLayout.visibility = View.GONE
            refreshConstraintLayout.visibility = View.VISIBLE
            refreshMessageTextView.text = message
        } else {
            refreshConstraintLayout.visibility = View.GONE
            controlInfoConstraintLayout.visibility = View.VISIBLE
        }
    }

    private fun checkFields(): Boolean {
        var result = true
        val ultrasonicCount = viewModel.getUltrasonicCount()
        var magneticCount = viewModel.getMagneticCount()
        if (ultrasonicCount > 0 && doUltrasonicControlAutoCompleteTextView.text.isNullOrEmpty()) {
            doUltrasonicControlTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (magneticCount > 0 && doMagneticVortexAutoCompleteTextView.text.isNullOrEmpty()) {
            doMagneticVortexTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }

        return result
    }

    private fun toast(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
        viewModel.resetControlInfoSubjects()
    }
}



































