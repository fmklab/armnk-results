package com.fmklab.armnk_results.modules.common

class DataResponse<T> (val data: T?, val status: Status, val error: Error?) {

    enum class Status {
        SUCCESS,
        LOADING,
        ERROR,
        EMPTY
    }

    companion object {
        fun <T> success(data: T?): DataResponse<T> {
            return DataResponse(
                data,
                Status.SUCCESS,
                null
            )
        }
        fun <T> error(error: Error): DataResponse<T> {
            return DataResponse(
                null,
                Status.ERROR,
                error
            )
        }
        fun <T> loading(): DataResponse<T> {
            return DataResponse(null, Status.LOADING, null)
        }
        fun <T> empty(): DataResponse<T> {
            return DataResponse(null, Status.EMPTY, null)
        }
    }
}