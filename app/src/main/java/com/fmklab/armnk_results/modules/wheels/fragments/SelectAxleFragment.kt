package com.fmklab.armnk_results.modules.wheels.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.models.AxleModel
import com.fmklab.armnk_results.modules.axle.models.AxleModelSelectable
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.wheels.adapters.AxleSearchItemAdapter
import com.fmklab.armnk_results.modules.wheels.adapters.AxleSearchItemViewHolder
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_select_axle.*
import kotlinx.android.synthetic.main.fragment_select_axle.loadingProgressBar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class SelectAxleFragment: BottomSheetDialogFragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by sharedViewModel()
    private lateinit var axleSearchItemAdapter: AxleSearchItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_select_axle, container, false)
    }

    override fun onStart() {
        super.onStart()

        if (dialog != null) {
            val bottomSheet = (dialog as Dialog).findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        requireView().post {
            val parent = requireView().parent as? View
            val params = parent?.layoutParams as? CoordinatorLayout.LayoutParams
            val behavior = params?.behavior
            val bottomSheetBehaviour = behavior as? BottomSheetBehavior
            bottomSheetBehaviour?.peekHeight = requireView().measuredHeight
        }
        searchTextEditText.requestFocus()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupData()
        setupBindings()
    }

    private fun setupUI() {
        axleListRecyclerView.setHasFixedSize(true)
        axleListRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        newFormCheckBox.isChecked = viewModel.isNewForm() ?: false
    }

    private fun setupData() {
        axleSearchItemAdapter = AxleSearchItemAdapter()
        axleListRecyclerView.adapter = axleSearchItemAdapter
    }

    private fun setupBindings() {
        viewModel.foundAxle
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            axleSearchItemAdapter.setItems(ArrayList(it.data), viewModel.getWheelAxle())
                        } else {
                            axleSearchItemAdapter.clearItems()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.baseInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    if (it.data!!.isNewDetailEnabled) {
                        newFormCheckBox.visibility = View.GONE
                    } else {
                        newFormCheckBox.visibility = View.VISIBLE
                    }
                }
            }

        searchTextEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.searchAxle(text.toString())
            }
        }

        searchTextEditText.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                val inputMethodManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }
        }

        axleSearchItemAdapter.onAxleSelectedListener = object : AxleSearchItemViewHolder.OnAxleSelectedListener {
            override fun onAxleSelected(axle: AxleModelSelectable, position: Int) {
                val axleModel = axleSearchItemAdapter.getItem(position)
                if (axleModel != null) {
                    when {
                        axleModel.wheel1Id == 0 -> {
                            newFormCheckBox.isChecked = axleModel.isNewForm
                            axleModel.isNewWheel1 = true
                            axleModel.oldWheelId = axleModel.wheel1Id
                            axleModel.wheel1Id = viewModel.getFullWheel()?.id ?: 0
                            axleModel.oldWheelNumber = axleModel.wheel1Number
                            axleModel.wheel1Number = viewModel.getFullWheel()?.wheelNumber ?: "-"
                            viewModel.setWheelAxle(axleModel)
                            axleSearchItemAdapter.notifyItemChanged(position)
                        }
                        axleModel.wheel2Id == 0 -> {
                            if (axleModel.isNewForm) {
                                newFormCheckBox.isChecked = true
                            }
                            axleModel.isNewWheel1 = false
                            axleModel.oldWheelId = axleModel.wheel2Id
                            axleModel.wheel2Id = viewModel.getFullWheel()?.id ?: 0
                            axleModel.oldWheelNumber = axleModel.wheel2Number
                            axleModel.wheel2Number = viewModel.getFullWheel()?.wheelNumber ?: "-"
                            viewModel.setWheelAxle(axleModel)
                            axleSearchItemAdapter.notifyItemChanged(position)
                        }
                        else -> {
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle(getString(R.string.attention))
                                .setMessage(getString(R.string.axle_have_two_wheels))
                                .setPositiveButton(getString(R.string.replace_first_wheel)) { _, _ ->
                                    if (axleModel.isNewForm) {
                                        newFormCheckBox.isChecked = true
                                    }
                                    axleModel.isNewWheel1 = true
                                    axleModel.oldWheelId = axleModel.wheel1Id
                                    axleModel.wheel1Id = viewModel.getFullWheel()?.id ?: axleModel.oldWheelId
                                    axleModel.oldWheelNumber = axleModel.wheel1Number
                                    axleModel.wheel1Number = viewModel.getFullWheel()?.wheelNumber ?: "-"
                                    viewModel.setWheelAxle(axleModel)
                                    axleSearchItemAdapter.notifyItemChanged(position)
                                }
                                .setNegativeButton(getString(R.string.replace_second_wheel)) { _, _ ->
                                    if (axleModel.isNewForm) {
                                        newFormCheckBox.isChecked = true
                                    }
                                    axleModel.isNewWheel1 = false
                                    axleModel.oldWheelId = axleModel.wheel2Id
                                    axleModel.wheel2Id = viewModel.getFullWheel()?.id ?: axleModel.oldWheelId
                                    axleModel.oldWheelNumber = axleModel.wheel2Number
                                    axleModel.wheel2Number = viewModel.getFullWheel()?.wheelNumber ?: "-"
                                    viewModel.setWheelAxle(axleModel)
                                    axleSearchItemAdapter.notifyItemChanged(position)
                                }
                                .setNeutralButton(getText(R.string.cancel)) { _, _ ->
                                    axleSearchItemAdapter.onAxleDeselected(axle, position)
                                }
                                .setCancelable(false)
                                .show()
                        }
                    }
                }
            }

            override fun onAxleDeselected(axle: AxleModelSelectable, position: Int) {
                viewModel.setWheelAxle(AxleModel())
            }
        }

        newFormCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setIsNewForm(isChecked)
        }

        completeSelectFab.setOnClickListener {
            dismiss()
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
        } else {
            loadingProgressBar.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}




































