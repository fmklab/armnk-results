package com.fmklab.armnk_results.modules.number_detail.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.extensions.dateChanged
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.models.ControlResult
import com.fmklab.armnk_results.modules.common.models.Factory
import com.fmklab.armnk_results.modules.common.models.Year
import com.fmklab.armnk_results.modules.number_detail.models.Company
import com.fmklab.armnk_results.modules.number_detail.models.Fault
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.radiobutton.MaterialRadioButton
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.*
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.controlDateEditText
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryNameAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryNameTextInputLayout
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryYearAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.isNewDetailCheckBox
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NumberBaseInputFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel by sharedViewModel<EditNumberDetailViewModel>()
    private val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
    private lateinit var materialDatePicker: MaterialDatePicker<Long>

    companion object {
        private const val DATE_PICKER_TAG = "DatePickerTag"
        private const val SERVICE_POINT_FRAGMENT_TAG = "ServicePointFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_base_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupListeners()
        setupBindings()
    }

    private fun setupUI() {
        controlDateEditText.keyListener = null
        factoryNameAutoCompleteTextView.keyListener = null
        factoryYearAutoCompleteTextView.keyListener = null

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText(R.string.select_date)
        materialDatePicker = builder.build()
    }

    private fun setupBindings() {
        viewModel.baseInfoErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Snackbar.make(
                    baseInputsNestedScrollView,
                    it.message as CharSequence,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.refresh) {
                        viewModel.loadBaseInfo()
                    }
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.factoriesLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showFactoriesLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.factoriesState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { factoryNameAutoCompleteTextView.initArrayAdapter(requireContext(), it) }
            .addTo(compositeDisposable)

        viewModel.yearsLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showFactoryYearsLoading(it) }
            .addTo(compositeDisposable)

        viewModel.factoryYearsState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { factoryYearAutoCompleteTextView.initArrayAdapter(requireContext(), it) }
            .addTo(compositeDisposable)

        viewModel.controlDate
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlDateEditText.setText(simpleDateFormat.format(it))
            }
            .addTo(compositeDisposable)

        viewModel.factory
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { factoryNameAutoCompleteTextView.setText(it.toString(), false) }
            .addTo(compositeDisposable)

        viewModel.factoryYear
            .filter { factoryYearAutoCompleteTextView.text.toString() != it.toString() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { factoryYearAutoCompleteTextView.setText(it.toString(), false) }
            .addTo(compositeDisposable)

        viewModel.isNewDetail
            .filter { isNewDetailCheckBox.isChecked != it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isNewDetailCheckBox.isChecked = it }
            .addTo(compositeDisposable)

        viewModel.detailNumber
            .filter { numberDetailEditText.text.toString() != it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { numberDetailEditText.setText(it.toString()) }
            .addTo(compositeDisposable)

        viewModel.isFromServicePoint
            .filter { servicePointCheckBox.isChecked != it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { servicePointCheckBox.isChecked = it }
            .addTo(compositeDisposable)

        viewModel.noRejectionCriteria
            .filter { rejectionCriteriaCheckBox.isChecked != it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { rejectionCriteriaCheckBox.isChecked = it }
            .addTo(compositeDisposable)

        viewModel.servicePoint
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                var servicePointDescription = ""
                servicePointDescription += when (it.company) {
                    Company.VRK1 -> "Компания: ВРК-1"
                    Company.VRK2 -> "Компания: ВРК-2"
                    Company.VRK3 -> "Компания: ВРК-3"
                    Company.OTHER -> "Компания: Прочие"
                    else -> ""
                }
                servicePointDescription += when (it.fault) {
                    Fault.DEPOT -> " Вина: Депо"
                    Fault.FACTORY -> " Вина: Завод"
                    Fault.EXPLOITATION -> " Вина: Эксплуатация"
                    else -> ""
                }
                servicePointDescriptionTextView.text = servicePointDescription
            }
            .addTo(compositeDisposable)

        Observable.zip(
            viewModel.controlResultsState.subscribeOn(Schedulers.io()),
            viewModel.controlResult.subscribeOn(Schedulers.io()),
            BiFunction { controlResults: ArrayList<ControlResult>, controlResult: ControlResult ->
                val list = ArrayList<Any>()
                list.add(controlResults)
                list.add(controlResult)
                list
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                val controlResults = it[0] as ArrayList<ControlResult>
                val controlResult = it[1] as ControlResult
                initControlResults(controlResults, controlResult)
            }
            .addTo(compositeDisposable)


        viewModel.emptyFactoryErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                factoryNameTextInputLayout.error = getString(R.string.field_empty_error)
            }
            .addTo(compositeDisposable)

        viewModel.emptyDetailNumberErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                numberDetailTextInputLayout.error = getString(R.string.field_empty_error)
            }
            .addTo(compositeDisposable)

        viewModel.noControlResultErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.control_result_error),
                    Toast.LENGTH_SHORT
                ).show()
            }
            .addTo(compositeDisposable)

        viewModel.goodResultFactoryErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
            .addTo(compositeDisposable)

        factoryNameAutoCompleteTextView
            .itemSelected<Factory>()
            .subscribe(viewModel.factory)
            .addTo(compositeDisposable)

        factoryYearAutoCompleteTextView
            .itemSelected<Year>()
            .subscribe(viewModel.factoryYear)
            .addTo(compositeDisposable)

        materialDatePicker
            .dateChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.controlDate.accept(it)
                controlDateEditText.setText(simpleDateFormat.format(it))
            }
            .addTo(compositeDisposable)

        isNewDetailCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe(viewModel.isNewDetail)
            .addTo(compositeDisposable)

        numberDetailEditText
            .textChanges()
            .skipInitialValue()
            .subscribe { viewModel.detailNumber.accept(it.toString()) }
            .addTo(compositeDisposable)

        servicePointCheckBox
            .checkedChanges()
            .skipInitialValue()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.isFromServicePoint.accept(it)
                rejectionCriteriaCheckBox.isEnabled = it
            }
            .addTo(compositeDisposable)

        rejectionCriteriaCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe(viewModel.noRejectionCriteria)
            .addTo(compositeDisposable)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupListeners() {
        controlDateEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                materialDatePicker.show(parentFragmentManager, DATE_PICKER_TAG)
                return@setOnTouchListener true
            }
            false
        }

        servicePointCheckBox.setOnClickListener {
            if (servicePointCheckBox.isChecked) {
                if (!parentFragmentManager.fragments.any { frg -> frg.tag == SERVICE_POINT_FRAGMENT_TAG }) {
                    ServicePointFragment().show(parentFragmentManager, SERVICE_POINT_FRAGMENT_TAG)
                }
            }
        }

        factoryNameAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrBlank()) {
                factoryNameTextInputLayout.error = null
            }
        }

        numberDetailEditText.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrBlank()) {
                numberDetailTextInputLayout.error = null
            }
        }
    }

    private fun showFactoriesLoading(show: Boolean) {
        if (show) {
            factoryNameAutoCompleteTextView.isEnabled = false
            factoriesLoadingProgress.visibility = View.VISIBLE
        } else {
            factoryNameAutoCompleteTextView.isEnabled = true
            factoriesLoadingProgress.visibility = View.GONE
        }
    }

    private fun showFactoryYearsLoading(show: Boolean) {
        if (show) {
            factoryYearAutoCompleteTextView.setAdapter(null)
            factoryYearsLoadingProgress.visibility = View.VISIBLE
        } else {
            factoryYearsLoadingProgress.visibility = View.GONE
        }
    }

    private fun initControlResults(controlResults: ArrayList<ControlResult>, selectedControlResult: ControlResult) {
        resultRadioGroup.removeAllViews()
        for (result in controlResults) {
            val radioButton = MaterialRadioButton(requireContext())
            resultRadioGroup.addView(radioButton)
            radioButton.tag = result
            radioButton.text = result.name
            if (result == selectedControlResult) {
                radioButton.isChecked = true
            }
            radioButton
                .clicks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (radioButton.isChecked) {
                        val controlResult = radioButton.tag as ControlResult
                        viewModel.controlResult.accept(controlResult)
                    }
                }
                .addTo(compositeDisposable)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}



































