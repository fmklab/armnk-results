package com.fmklab.armnk_results.modules.wheels.models

class WheelLoadError(message: String): Error(message)