package com.fmklab.armnk_results.modules.common

import com.fmklab.armnk_results.modules.common.models.InternetConnectionError
import com.fmklab.armnk_results.modules.common.models.RequestError
import com.fmklab.armnk_results.modules.common.models.TimeoutError
import io.reactivex.rxjava3.subjects.BehaviorSubject
import retrofit2.HttpException
import java.net.ConnectException
import java.util.concurrent.TimeoutException

object ApiErrorHandler {

    fun <T> dataResponseError(t: Throwable, subject: BehaviorSubject<DataResponse<T>>) {
        when (t) {
            is ConnectException -> subject.onNext(DataResponse.error(InternetConnectionError()))
            is TimeoutException -> subject.onNext(DataResponse.error(TimeoutError()))
            else -> subject.onNext(DataResponse.error(RequestError()))
        }
    }

    fun getError(t: Throwable): Error {
        return when (t) {
            is ConnectException -> InternetConnectionError()
            is TimeoutException -> TimeoutError()
            is HttpException -> Error(t.code().toString())
            else -> RequestError()
        }
    }
}