package com.fmklab.armnk_results.modules.common.models

data class EditViewModelState (
    val isEditing: Boolean?
)