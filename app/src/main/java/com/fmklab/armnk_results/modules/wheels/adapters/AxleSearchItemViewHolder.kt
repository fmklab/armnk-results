package com.fmklab.armnk_results.modules.wheels.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.fmklab.armnk_results.modules.axle.models.AxleModelSelectable
import com.google.android.material.card.MaterialCardView
import kotlin.properties.Delegates

class AxleSearchItemViewHolder(itemView: View, private val listener: OnAxleSelectedListener): RecyclerView.ViewHolder(itemView), View.OnClickListener {

    var item: AxleModelSelectable by Delegates.notNull()

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val cardView = itemView as? MaterialCardView ?: return
        item.isSelected = !cardView.isChecked
        cardView.toggle()
        if (cardView.isChecked) {
            listener.onAxleSelected(item, adapterPosition)
        } else {
            listener.onAxleDeselected(item, adapterPosition)
        }
    }

    interface OnAxleSelectedListener {

        fun onAxleSelected(axle: AxleModelSelectable, position: Int)

        fun onAxleDeselected(axle: AxleModelSelectable, position: Int)
    }
}