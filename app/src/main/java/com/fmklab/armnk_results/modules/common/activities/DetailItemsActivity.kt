package com.fmklab.armnk_results.modules.common.activities

import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.common.listeners.PageRecyclerViewOnScrollListener
import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.appcompat.queryTextChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo

abstract class DetailItemsActivity<T>: AppCompatActivity() where T: AbstractDetail{

    //UI
    lateinit var recyclerView: RecyclerView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var addNewDetailFab: FloatingActionButton
    lateinit var toolBar: Toolbar
    lateinit var emptyDataTextView: TextView
    var title: String? = null

    protected val compositeDisposable = CompositeDisposable()
    abstract val viewModel: DetailItemsViewModel<T>
    abstract val detailsAdapter: DetailItemAdapter<T>
    abstract val recyclerViewLayoutManager: LinearLayoutManager
    abstract val detailsFragment: BottomSheetDialogFragment?
    private var recyclerViewOnScrollListener: PageRecyclerViewOnScrollListener? = null

    protected abstract fun showDetails(position: Int)

    protected abstract fun addNewDetail()

    protected open fun setupUI() {
        setSupportActionBar(toolBar)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerView.layoutManager = recyclerViewLayoutManager
        recyclerView.setHasFixedSize(true)
    }

    protected open fun setupData() {
        recyclerView.adapter = detailsAdapter
    }

    protected fun setupListeners() {
        recyclerViewOnScrollListener = object : PageRecyclerViewOnScrollListener(recyclerViewLayoutManager) {
            override fun onLoadMore(currentPage: Int) {
                viewModel.loadDetails(currentPage)
            }
        }

        swipeRefreshLayout.setOnRefreshListener {
            viewModel.loadDetails(0)
        }

        //recyclerView.setOnScrollListener(recyclerViewOnScrollListener)
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener as PageRecyclerViewOnScrollListener)

        detailsAdapter.onItemAddListener = object : DetailItemAdapter.OnItemAddListener {
            override fun onItemAdded() {
                showEmptyData(false)
                val firstItemPosition = recyclerViewLayoutManager.findFirstCompletelyVisibleItemPosition()
                if (firstItemPosition == 0) {
                    recyclerView.smoothScrollToPosition(0)
                }
            }
        }

        detailsAdapter.onMoreInfoClickListener = object : DetailItemAdapter.OnMoreInfoClickListener {
            override fun onMoreInfoClick(position: Int) {
                showDetails(position)
            }
        }

        detailsAdapter.onRefreshClickListener = object : DetailItemAdapter.OnRefreshClickListener {
            override fun onRefreshClick() {
                viewModel.refreshDetails()
            }
        }

        addNewDetailFab.setOnClickListener {
            addNewDetail()
        }
    }

    protected open fun subscribeObservers() {
        viewModel.loadingDetailsStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.detailsStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (viewModel.getLastPage() == 0) {
                    recyclerViewOnScrollListener?.reset()
                    detailsAdapter.clearItemsWithoutNotify()
                }
                detailsAdapter.addItems(it)
            }
            .addTo(compositeDisposable)

        viewModel.errorDetailsStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (viewModel.getLastPage() == 0) {
                    addNewDetailFab.hide()
                    Snackbar.make(recyclerView, it.message as CharSequence, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.refresh) {
                            if (detailsAdapter.itemCount == 0) {
                                detailsAdapter.showLoading()
                            } else {
                                swipeRefreshLayout.isRefreshing = true
                            }
                            viewModel.refreshDetails()
                        }
                        .show()
                } else {
                    detailsAdapter.showError()
                }
            }
            .addTo(compositeDisposable)

        viewModel.noInternetConnectionStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showNoInternetConnection(it)
            }
            .addTo(compositeDisposable)

        viewModel.detailAddedStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detailsAdapter.addItem(it)
                scrollToFirstElement()
            }
            .addTo(compositeDisposable)

        viewModel.detailUpdatedStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detailsAdapter.updateItem(it.first, it.second)
            }
            .addTo(compositeDisposable)

        viewModel.toastState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
            .addTo(compositeDisposable)

        viewModel.searchLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                swipeRefreshLayout.isRefreshing = it
            }

        detailsAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                if (detailsAdapter.itemCount == 0) {
                    showEmptyData(true)
                } else {
                    showEmptyData(false)
                }
            }
        })
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            addNewDetailFab.hide()
            if (!swipeRefreshLayout.isRefreshing) {
                detailsAdapter.showLoading()
            }
        } else {
            addNewDetailFab.show()
            swipeRefreshLayout.isRefreshing = false
            detailsAdapter.hideLoading()
        }
    }

    private fun showNoInternetConnection(show: Boolean) {
        if (show) {
            supportActionBar?.title = getString(R.string.wait_for_connection)
            swipeRefreshLayout.isEnabled = false
        } else {
            supportActionBar?.title = title
            swipeRefreshLayout.isEnabled = true
        }
    }

    private fun scrollToFirstElement() {
        val firstItemPosition = recyclerViewLayoutManager.findFirstCompletelyVisibleItemPosition()
        if (firstItemPosition == 0) {
            recyclerView.smoothScrollToPosition(0)
        }
    }

    private fun showEmptyData(show: Boolean) {
        if (show) {
            emptyDataTextView.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            emptyDataTextView.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()

        detailsFragment?.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchMenuItem = menu?.findItem(R.id.search_button)
        val searchView = searchMenuItem?.actionView as SearchView
        searchView.queryHint = getString(R.string.search_by_number)
        searchView.queryTextChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .filter(String::isNotBlank)
            .subscribe(viewModel.searchTextChangeConsumer)
            .addTo(compositeDisposable)

        searchMenuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                recyclerView.setOnScrollListener(null)
                swipeRefreshLayout.isEnabled = false
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                swipeRefreshLayout.isEnabled = true
                viewModel.loadDetails(0)
                recyclerViewOnScrollListener?.reset()
                recyclerView.setOnScrollListener(recyclerViewOnScrollListener)
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}































