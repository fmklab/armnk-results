package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetail
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_number_detail_ndt.*

class NumberDetailNdtFragment: NumberDetailAbstractDefectInfoFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_ndt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //viewModel.loadMethods()
    }

    override fun setupUI() {
        defectTypeAutoCompleteTextView = requireView().findViewById(R.id.defectTypeAutoCompleteTextView)
        defectTypeTextInputLayout = requireView().findViewById(R.id.defectTypeTextInputLayout)
        defectZoneAutoCompleteTextView = requireView().findViewById(R.id.defectZoneAutoCompleteTextView)
        defectZoneTextInputLayout = requireView().findViewById(R.id.defectZoneTextInputLayout)
        stealAutoCompleteTextView = requireView().findViewById(R.id.stealAutoCompleteTextView)
        stealTextInputLayout = requireView().findViewById(R.id.stealTextInputLayout)
        ownerEditText = requireView().findViewById(R.id.ownerEditText)
        ownerTextInputLayout = requireView().findViewById(R.id.ownerTextInputLayout)
        defectTypeLoadingProgress = requireView().findViewById(R.id.defectTypeLoadingProgress)
//        zoneLoadingProgress = requireView().findViewById(R.id.zoneLoadingProgress)
//        stealLoadingProgress = requireView().findViewById(R.id.stealLoadingProgress)
        viewConstraintLayout = requireView().findViewById(R.id.viewConstraintLayout)

        methodAutoCompleteTextView.keyListener = null
        detectorAutoCompleteTextView.keyListener = null

        defectTypeTextInputLayout.isEnabled = false

        super.setupUI()
    }

    override fun setupBindings() {
        super.setupBindings()

        viewModel.methodsLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showMethodsLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.methodsState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.detectorsLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showDetectorsLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.detectorsState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detectorAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.method
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodAutoCompleteTextView.setText(it, false)
            }
            .addTo(compositeDisposable)

        viewModel.detector
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detectorAutoCompleteTextView.setText(it, false)
            }
            .addTo(compositeDisposable)

        viewModel.defectSize
            .filter { defectSizeEditText.text.toString() != it }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectSizeEditText.setText(it)
            }
            .addTo(compositeDisposable)

        viewModel.emptyMethodErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodTextInputLayout.error = getString(R.string.field_empty_error)
            }
            .addTo(compositeDisposable)

        viewModel.emptyDetectorErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detectorTextInputLayout.error = getString(R.string.field_empty_error)
            }
            .addTo(compositeDisposable)

        methodAutoCompleteTextView
            .itemSelected<String>()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.detector.accept("")
                viewModel.method.accept(it)
            }
            .addTo(compositeDisposable)

        detectorAutoCompleteTextView
            .itemSelected<Detector>()
            .map(Detector::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.detector)
            .addTo(compositeDisposable)

        defectSizeEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.defectSize)
            .addTo(compositeDisposable)
    }

    override fun setupListeners() {
        super.setupListeners()

        methodAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                methodTextInputLayout.error = null
            }
        }

        detectorAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                detectorTextInputLayout.error = null
            }
        }
    }

    private fun showMethodsLoading(show: Boolean) {
//        if (show) {
//            methodsLoadingProgress.visibility = View.VISIBLE
//            methodsLoadingProgress.isIndeterminate = true
//        } else {
//            methodsLoadingProgress.visibility = View.GONE
//        }
    }

    private fun showDetectorsLoading(show: Boolean) {
//        if (show) {
//            detectorLoadingProgress.visibility = View.VISIBLE
//            detectorLoadingProgress.isIndeterminate = true
//        } else {
//            detectorLoadingProgress.visibility = View.GONE
//        }
    }
}











































