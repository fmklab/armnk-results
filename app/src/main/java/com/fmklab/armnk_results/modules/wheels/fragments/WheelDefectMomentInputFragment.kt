package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.AxleAndWheelType
import com.fmklab.armnk_results.modules.common.models.FatalError
import com.fmklab.armnk_results.modules.common.models.InternetConnectionError
import com.fmklab.armnk_results.modules.wheels.adapters.DefectTypeSpinnerAdapter
import com.fmklab.armnk_results.modules.wheels.adapters.WheelControlZoneSpinnerAdapter
import com.fmklab.armnk_results.modules.wheels.adapters.WheelDefectSpinnerAdapter
import com.fmklab.armnk_results.modules.wheels.models.*
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.*
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectCodeAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectCodeTextInputLayout
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectDescriptionEditText
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectTypeAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectTypeLoadingProgress
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectTypeTextInputLayout
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectZoneAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.defectZoneTextInputLayout
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.errorMessageTextView
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.refreshButton
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.removeDefectCodeButton
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.removeDefectTypeButton
import kotlinx.android.synthetic.main.fragment_wheel_defect_moment_input.removeDefectZoneButton
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WheelDefectMomentInputFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by sharedViewModel()
    private var isErrorShowed = false

    companion object {
        private const val DEFECT_TYPE = 1
        private const val DEFECT_ZONE = 2
        private const val DEFECT_CODE = 3
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wheel_defect_moment_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    override fun onResume() {
        super.onResume()

        isErrorShowed = false
        viewModel.loadDefectBeforeControlTypes()
    }

    private fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        defectZoneAutoCompleteTextView.keyListener = null
        defectCodeAutoCompleteTextView.keyListener = null
    }

    private fun setupBindings() {
        viewModel.fullWheel
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.status == DataResponse.Status.SUCCESS) {
                    defectDescriptionEditText.setText(it.data?.defectMomentInfo?.description)
                }
            }).addTo(compositeDisposable)
        viewModel.defectBeforeControlTypesInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true, DEFECT_TYPE)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectMomentSuccessLoading())
                        if (it.data != null) {
                            DefectTypeSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectTypeAutoCompleteTextView.setAdapter(adapter)
                                if (it.data.size == 1) {
                                    defectTypeAutoCompleteTextView.setText(it.data[0].name, false)
                                    viewModel.setWheelDefectBeforeNdtType(it.data[0])
                                    loadControlZones()

                                } else if (viewModel.getWheelDefectBeforeNdtType()?.id != DefectType.DEFAULT_ID) {
                                    val defectType = it.data.find { dt -> dt.id == viewModel.getWheelDefectBeforeNdtType()?.id }
                                    if (defectType != null) {
                                        defectTypeAutoCompleteTextView.setText(defectType.name, false)
                                        loadControlZones()
                                    } else {
                                        defectTypeAutoCompleteTextView.setText("", false)
                                        showLoading(false)
                                    }
                                } else {
                                    defectTypeAutoCompleteTextView.setText("", false)
                                    showLoading(false)
                                }
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectMomentInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                viewModel.loadDefectBeforeControlTypes()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, DEFECT_TYPE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.wheelDefectMomentControlZonesInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true, DEFECT_ZONE)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectMomentSuccessLoading())
                        if (it.data != null) {
                            WheelControlZoneSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item, it.data.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectZoneAutoCompleteTextView.setAdapter(adapter)
                            }
                            if (it.data.size == 1) {
                                defectZoneAutoCompleteTextView.setText(it.data[0].title, false)
                                viewModel.setWheelDefectMomentControlZone(it.data[0])
                                loadDefects()
                            } else if (viewModel.getWheelDefectMomentControlZone()?.id != WheelControlZone.DEFAULT_ID) {
                                val controlZone = it.data.find { cz -> cz.id == viewModel.getWheelDefectMomentControlZone()?.id }
                                if (controlZone != null) {
                                    defectZoneAutoCompleteTextView.setText(controlZone.title, false)
                                    loadDefects()
                                } else {
                                    defectZoneAutoCompleteTextView.setText("", false)
                                    showLoading(false)
                                }
                            } else {
                                defectZoneAutoCompleteTextView.setText("", false)
                                showLoading(false)
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectMomentInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, DEFECT_ZONE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.wheelDefectsInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true, DEFECT_CODE)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectMomentSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            WheelDefectSpinnerAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, it.data.toTypedArray()).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectCodeAutoCompleteTextView.setAdapter(adapter)
                            }
                            if (it.data.size == 1) {
                                defectCodeAutoCompleteTextView.setText(it.data[0].title, false)
                                viewModel.setWheelDefectMomentDefect(it.data[0])
                            } else if (viewModel.getWheelDefectMomentDefect()?.id != WheelDefect.DEFAULT_ID) {
                                val defect = it.data.find { d -> d.id == viewModel.getWheelDefectMomentDefect()?.id }
                                if (defect != null) {
                                    defectCodeAutoCompleteTextView.setText(defect.title, false)
                                } else {
                                    defectCodeAutoCompleteTextView.setText("", false)
                                }
                            } else {
                                defectCodeAutoCompleteTextView.setText("", false)
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectMomentInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadDefects()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, DEFECT_CODE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectMomentRequestCheckFields::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                val result = checkFields()
                if (result) {
                    viewModel.getDefectInfo(WheelDefectInfo.DEFECT_MOMENT_DEFECT)
                }
                RxBus.publish(RxEvent.EventDefectMomentCheckFieldsResponse(result))
            }).addTo(compositeDisposable)

        defectTypeAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val defectType = parent.getItemAtPosition(position) as? DefectType
            if (defectType != null) {
                viewModel.setWheelDefectBeforeNdtType(defectType)
                loadControlZones()
            }
        }

        defectTypeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectTypeTextInputLayout.error = null
            }
        }

        defectZoneAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val defectZone = parent.getItemAtPosition(position) as? WheelControlZone
            if (defectZone != null) {
                viewModel.setWheelDefectMomentControlZone(defectZone)
                loadDefects()
            }
        }

        defectZoneAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectZoneTextInputLayout.error = null
            }
        }

        defectCodeAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val defect = parent.getItemAtPosition(position) as? WheelDefect
            if (defect != null) {
                viewModel.setWheelDefectMomentDefect(defect)
            }
        }

        defectCodeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectCodeTextInputLayout.error = null
            }
        }

        defectDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setWheelDefectMomentDescription(text.toString())
            }
        }

        removeDefectCodeButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("", false)
            viewModel.setWheelDefectMomentDefect(WheelDefect())
        }

        removeDefectZoneButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("", false)
            defectZoneAutoCompleteTextView.setText("", false)
            viewModel.setWheelDefectMomentDefect(WheelDefect())
            viewModel.setWheelDefectMomentControlZone(WheelControlZone())
        }

        removeDefectTypeButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("", false)
            defectZoneAutoCompleteTextView.setText("", false)
            defectTypeAutoCompleteTextView.setText("", false)
            viewModel.setWheelDefectMomentDefect(WheelDefect())
            viewModel.setWheelDefectMomentControlZone(WheelControlZone())
            viewModel.setWheelDefectBeforeNdtType(DefectType())
        }
    }

    private fun loadControlZones() {
        val wheelControlZonesRequest = WheelControlZonesRequest(
            WheelControlZone.VM_DEFECTATION,
            WheelControlInfo.WHEEL_THICKNESS_NONE,
            WheelControlInfo.RING_STATE_NONE,
            WheelDefectInfo.DEFECT_MOMENT_DEFECT,
            WheelControlInfo.REPAIR_TYPE_NONE,
            AxleAndWheelType.WHEEL_TYPE_NONE,
            false,
            false,
            null
        )
        viewModel.loadControlZones(wheelControlZonesRequest, WheelDefectInfo.DEFECT_MOMENT_DEFECT)
    }

    private fun loadDefects() {
        viewModel.loadDefects(WheelDefectInfo.DEFECT_MOMENT_DEFECT)
    }

    private fun showLoading(show: Boolean, field: Int = 0) {
        if (show) {
            when (field) {
                DEFECT_TYPE -> {
                    defectTypeLoadingProgress.visibility = View.VISIBLE
                    defectZoneLoadingProgress.visibility = View.GONE
                    defectCodeLoadingProgress.visibility = View.GONE
                }
                DEFECT_ZONE -> {
                    defectTypeLoadingProgress.visibility = View.GONE
                    defectZoneLoadingProgress.visibility = View.VISIBLE
                    defectCodeLoadingProgress.visibility = View.GONE
                }
                DEFECT_CODE -> {
                    defectTypeLoadingProgress.visibility = View.GONE
                    defectZoneLoadingProgress.visibility = View.GONE
                    defectCodeLoadingProgress.visibility = View.VISIBLE
                }
            }
            removeDefectTypeButton.isEnabled = false
            removeDefectZoneButton.isEnabled = false
            removeDefectCodeButton.isEnabled = false
            defectTypeTextInputLayout.isEnabled = false
            defectZoneTextInputLayout.isEnabled = false
            defectCodeTextInputLayout.isEnabled = false
        } else {
            defectTypeLoadingProgress.visibility = View.GONE
            defectZoneLoadingProgress.visibility = View.GONE
            defectCodeLoadingProgress.visibility = View.GONE
            removeDefectTypeButton.isEnabled = true
            removeDefectZoneButton.isEnabled = true
            removeDefectCodeButton.isEnabled = true
            defectTypeTextInputLayout.isEnabled = true
            defectZoneTextInputLayout.isEnabled = true
            defectCodeTextInputLayout.isEnabled = true
        }
    }

    private fun showRefresh(show: Boolean, errorType: Int = -1, message: String? = null) {
        if (show) {
            defectMomentInputNestedScrollView.visibility = View.GONE
            refreshConstraintLayout.visibility = View.VISIBLE
            errorMessageTextView.text = message
            when (errorType) {
                DEFECT_TYPE -> {
                    refreshButton.setOnClickListener {
                        viewModel.loadDefectBeforeControlTypes()
                    }
                }
                DEFECT_ZONE -> {
                    refreshButton.setOnClickListener {
                        loadControlZones()
                    }
                }
                DEFECT_CODE -> {
                    refreshButton.setOnClickListener {
                        loadDefects()
                    }
                }
            }
        } else {
            refreshConstraintLayout.visibility = View.GONE
            defectMomentInputNestedScrollView.visibility = View.VISIBLE
        }
    }

    private fun checkFields(): Boolean {
        var result = true
        if (defectTypeAutoCompleteTextView.text.isNullOrEmpty()) {
            result = false
            defectTypeTextInputLayout.error = getString(R.string.field_empty_error)
        }
        if (defectZoneAutoCompleteTextView.text.isNullOrEmpty()) {
            result = false
            defectZoneTextInputLayout.error = getString(R.string.field_empty_error)
        }
        if (defectCodeAutoCompleteTextView.text.isNullOrEmpty()) {
            result = false
            defectCodeTextInputLayout.error = getString(R.string.field_empty_error)
        }

        return result
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
        viewModel.resetDefectMomentSubjects()
    }
}














































































