package com.fmklab.armnk_results.modules.number_detail.services

import com.fmklab.armnk_results.modules.common.models.CheckAddedDetailResponse
import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.common.models.Year
import com.fmklab.armnk_results.modules.number_detail.models.DefectMoment
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailDto
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailListItem
import com.fmklab.armnk_results.modules.number_detail.models.SetDetailRequest
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*

interface NumberDetailApiService {

    companion object {
        private const val ROUTE = "numberdetail"
    }

    @GET("$ROUTE/all")
    fun getNumberDetails(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("page") page: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<NumberDetailListItem>>

    @GET(ROUTE)
    fun getNumberDetailById(
        @Query("id") id: Int,
        @Header("Authorization") token: String
    ): Observable<NumberDetailDto>

    @GET("$ROUTE/years")
    fun getFactoryYears(
        @Query("factoryId") factoryId: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Year>>

    @GET("$ROUTE/repair/defecttypes")
    fun getRepairDefectTypes(
        @Query("partId") partId: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<DefectType>>

    @GET("$ROUTE/repair/zones")
    fun getRepairZones(
        @Query("partId") partId: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<ControlZone>>

    @GET("$ROUTE/defecttypes")
    fun getDefectTypes(
        @Query("partId") partId: Int,
        @Query("defectMoment") defectMoment: DefectMoment,
        @Header("Authorization") token: String
    ): Observable<ArrayList<DefectType>>

    @GET("$ROUTE/zones")
    fun getZones(
        @Query("partId")
        partId: Int,
        @Query("defectMoment")
        defectMoment: DefectMoment,
        @Header("Authorization") token: String
    ): Observable<ArrayList<ControlZone>>

    @GET("$ROUTE/steal")
    fun getSteal(@Header("Authorization") token: String): Observable<ArrayList<String>>

    @POST(ROUTE)
    fun setDetail(
        @Body request: SetDetailRequest,
        @Header("Authorization") token: String
    ): Observable<Int>

    @GET("$ROUTE/search")
    fun searchDetails(
        @Query("companyId") companyId: Int,
        @Query("partId") partId: Int,
        @Query("number") number: String,
        @Header("Authorization") token: String
    ): Observable<ArrayList<NumberDetailListItem>>

    @GET("$ROUTE/check")
    fun checkAddedDetail(
        @Query("companyId")
        companyId: Int,
        @Query("partId")
        partId: Int,
        @Query("number") number: String,
        @Header("Authorization") token: String
    ): Observable<CheckAddedDetailResponse>
}


































