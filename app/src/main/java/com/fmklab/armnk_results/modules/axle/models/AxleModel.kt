package com.fmklab.armnk_results.modules.axle.models

import com.fmklab.armnk_results.modules.common.models.Factory
import com.fmklab.armnk_results.modules.common.models.Year
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class AxleModel (
    @SerializedName("id")
    @Expose
    val id: Int = DEFAULT_ID,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date = Date(),
    @SerializedName("axleNumber")
    @Expose
    val axleNumber: String = DEFAULT_AXLE_NUMBER,
    @SerializedName("factory")
    @Expose
    val factory: Factory? = null,
    @SerializedName("factoryYear")
    @Expose
    val factoryYear: Year? = null,
    @SerializedName("isNewDetail")
    @Expose
    val isNewDetail: Boolean = DEFAULT_IS_NEW_DETAIL,
    @SerializedName("isNewForm")
    @Expose
    val isNewForm: Boolean = false,
    @SerializedName("wheel1Id")
    @Expose
    var wheel1Id: Int = DEFAULT_WHEEL_1,
    @SerializedName("wheel2Id")
    @Expose
    var wheel2Id: Int = DEFAULT_WHEEL_2,
    @SerializedName("wheel1Number")
    @Expose
    var wheel1Number: String = "-",
    @SerializedName("wheel2Number")
    @Expose
    var wheel2Number: String = "-",
    @SerializedName("isNewWheel1")
    @Expose
    var isNewWheel1: Boolean? = null,
    @SerializedName("oldWheelId")
    @Expose
    var oldWheelId: Int = -1,
    @SerializedName("oldWheelNumber")
    @Expose
    var oldWheelNumber: String? = "-"
) {

    companion object {
        const val DEFAULT_ID = -1
        const val DEFAULT_AXLE_NUMBER = "-"
        const val DEFAULT_IS_NEW_DETAIL = false
        const val DEFAULT_WHEEL_1 = 0
        const val DEFAULT_WHEEL_2 = 0
    }
}