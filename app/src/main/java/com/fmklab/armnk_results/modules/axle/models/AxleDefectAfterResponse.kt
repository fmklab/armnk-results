package com.fmklab.armnk_results.modules.axle.models

import com.fmklab.armnk_results.modules.auth.models.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleDefectAfterResponse (
    @SerializedName("controlZones")
    @Expose
    val controlZones: List<AxleControlZone>,
    @SerializedName("mans")
    @Expose
    val mans: List<User>
)