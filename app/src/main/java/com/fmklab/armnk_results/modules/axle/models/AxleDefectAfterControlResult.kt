package com.fmklab.armnk_results.modules.axle.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleDefectAfterControlResult(
    @SerializedName("ultrasonicControlUser")
    @Expose
    var ultrasonicControlUser: String = "",
    @SerializedName("magneticControlUser")
    @Expose
    var magneticControlUser: String = "",
    @SerializedName("repairType")
    @Expose
    var repairType: Int = 0,
    @SerializedName("rings")
    @Expose
    var rings: Int = -1
) {

    var controlZones: ArrayList<AxleControlZone> = ArrayList()
    var infoControlDescription = ""
    var ultrasonicCount: Int = 0
    var magneticCount: Int = 0
    var isLoaded: Boolean = false
}