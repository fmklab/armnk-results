package com.fmklab.armnk_results.modules.wheels.activities

import android.graphics.Color
import android.graphics.Rect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.controls.MenuFloatingActionButton
import com.fmklab.armnk_results.framework.controls.SavingDialog
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.*
import com.fmklab.armnk_results.modules.wheels.adapters.WheelEditPagerAdapter
import com.fmklab.armnk_results.modules.wheels.fragments.WheelRepairTypeFragment
import com.fmklab.armnk_results.modules.wheels.models.FullWheel
import com.fmklab.armnk_results.modules.wheels.repositories.WheelRepository
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.chip.Chip
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.gordonwong.materialsheetfab.MaterialSheetFab
import com.gordonwong.materialsheetfab.MaterialSheetFabEventListener
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_wheel.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf

class EditWheelActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by lifecycleScope.viewModel(this)
    private val wheelEditPagerAdapter: WheelEditPagerAdapter by lifecycleScope.inject {
        parametersOf(supportFragmentManager)
    }
    private lateinit var goodBadDetailMenuFloatingActionButton: MaterialSheetFab<MenuFloatingActionButton>
    private var baseInfoFirstLoad = true
    private val wheelRepairTypeFragment = WheelRepairTypeFragment()
    private val savingDialog = SavingDialog(this)

    companion object {
        private const val SAVED_WHEEL_KEY = "SavedWheelKey"
        private const val SAVED_VM_STATE_KEY = "SavedVMStateKey"
        private const val WHEEL_REPAIR_TYPE_FRAGMENT_TAG = "WheelRepairTypeFragmentTag"

        private const val BASE_INFO = 0
        private const val DEFECT_MOMENT_INFO = 1
        private const val CONTROL_INFO = 2
        private const val DEFECT_INFO = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_wheel)

        setupUI()
        setupData(savedInstanceState)
        setupBindings()
    }

    private fun setupData(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            val json = savedInstanceState.getString(SAVED_WHEEL_KEY)
            if (json != null) {
                val wheel = Gson().fromJson(json, FullWheel::class.java)
                val vmStateJson = savedInstanceState.getString(SAVED_VM_STATE_KEY)
                if (vmStateJson != null) {
                    val vmState = Gson().fromJson(vmStateJson, EditViewModelState::class.java)
                    viewModel.viewModelState = vmState
                }
                viewModel.fullWheel.onNext(DataResponse.success(wheel))
                return
            }
        }
        val wheelId = intent.extras?.getInt(WheelActivity.WHEEL_ID_TAG)
        viewModel.loadFullWheel(wheelId!!)
    }

    private fun setupUI() {
        setSupportActionBar(editWheelToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.wheel_base_input_title)
        editWheelViewPager.adapter = wheelEditPagerAdapter
        goodBadDetailMenuFloatingActionButton = MaterialSheetFab(
            goodBadDetailFab,
            fabSheet,
            overlay,
            Color.TRANSPARENT,
            R.color.colorPrimary
        )
        editWheelViewPager.visibility = View.GONE
    }

    private fun setupBindings() {
        viewModel.fullWheel
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        supportActionBar?.title = getString(R.string.wheel_base_input_title)
                        showLoading(false)
                        if (it.data != null) {
                            viewModel.loadBaseInfo()
                        } else {
                            showRefresh(true, getString(R.string.full_wheel_load_error))
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        when (it.error) {
                            is InternetConnectionError -> {
                                supportActionBar?.title = getString(R.string.wait_for_connection)
                                val wheelId = intent.extras?.getInt(WheelActivity.WHEEL_ID_TAG)
                                viewModel.loadFullWheel(wheelId!!)
                            }
                            is FatalError -> {
                                finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, it?.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.baseInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        nextWheelInputFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (baseInfoFirstLoad) {
                            nextWheelInputFab.show()
                            defectAfterControlChip.isEnabled = viewModel.isRepairInfoSet() == true
                            wrongDetailChip.isEnabled = viewModel.getControlResult()?.id != ControlResult.WHEEL_GOOD && viewModel.isRepairInfoSet() == true
                            baseInfoFirstLoad = false
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        baseInfoFirstLoad = true
                        nextWheelInputFab.hide()
                    }
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventBaseInfoCheckFieldsResponse::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (!it.result) {
                    return@subscribeBy
                }
                val fullWheel = viewModel.getFullWheel() ?: return@subscribeBy
                if (!fullWheel.isDefectBeforeNdt) {
                    if (fullWheel.wheelControlInfo.isRepairInfoSet) {
                        editWheelViewPager.setCurrentItem(CONTROL_INFO, true)
                    } else if (!supportFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_TYPE_FRAGMENT_TAG }) {
                        wheelRepairTypeFragment.show(
                            supportFragmentManager,
                            WHEEL_REPAIR_TYPE_FRAGMENT_TAG
                        )
                    }
                } else {
                    editWheelViewPager.setCurrentItem(DEFECT_MOMENT_INFO, true)
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventRepairInfoSet::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                defectAfterControlChip.isEnabled = true
                editWheelViewPager.setCurrentItem(CONTROL_INFO, true)
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectBeforeNdtChanged::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.value) {
                    defectBeforeControlChip.isEnabled = true
                    defectAfterControlChip.isEnabled = false
                    wrongDetailChip.isEnabled = false
                } else {
                    defectBeforeControlChip.isEnabled = false
                    defectAfterControlChip.isEnabled = viewModel.isRepairInfoSet() == true
                    wrongDetailChip.isEnabled = viewModel.getControlResult()?.id != ControlResult.WHEEL_GOOD
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventControlInfoCheckFieldsResponse::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.result) {
                    goodBadDetailMenuFloatingActionButton.hideSheetThenFab()
                    wrongDetailChip.isEnabled = true
                    viewModel.setControlResult(ControlResult.WHEEL_BAD)
                    editWheelViewPager.setCurrentItem(DEFECT_INFO, true)
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectInfoCheckFieldsResponse::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (!it.value) {
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectMomentCheckFieldsResponse::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (!it.value) {
                }
            }).addTo(compositeDisposable)

        viewModel.baseInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                editWheelViewPager.setCurrentItem(BASE_INFO, true)
                RxBus.publish(RxEvent.EventBaseInfoRequestCheckFields())
            }).addTo(compositeDisposable)

        viewModel.defectMomentInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                editWheelViewPager.setCurrentItem(DEFECT_MOMENT_INFO, true)
                RxBus.publish(RxEvent.EventDefectMomentRequestCheckFields())
            }).addTo(compositeDisposable)

        viewModel.controlInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (viewModel.isRepairInfoSet() == false) {
                    if (!supportFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_TYPE_FRAGMENT_TAG }) {
                        wheelRepairTypeFragment.show(
                            supportFragmentManager,
                            WHEEL_REPAIR_TYPE_FRAGMENT_TAG
                        )
                    }
                } else {
                    editWheelViewPager.setCurrentItem(CONTROL_INFO, true)
                    RxBus.publish(RxEvent.EventControlInfoRequestCheckFields())
                }
            }).addTo(compositeDisposable)

        viewModel.defectInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (viewModel.isRepairInfoSet() == true) {
                    editWheelViewPager.setCurrentItem(DEFECT_INFO, true)
                    RxBus.publish(RxEvent.EventDefectInfoRequestCheckFields())
                } else {
                    if (!supportFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_TYPE_FRAGMENT_TAG }) {
                        wheelRepairTypeFragment.show(
                            supportFragmentManager,
                            WHEEL_REPAIR_TYPE_FRAGMENT_TAG
                        )
                    }
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectMomentInfoLoadingOrError::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (editWheelViewPager.currentItem == DEFECT_MOMENT_INFO) {
                    saveWheelFab.hide()
                    saveWheelButton.isEnabled = false
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventDefectMomentSuccessLoading::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (editWheelViewPager.currentItem == DEFECT_MOMENT_INFO) {
                    saveWheelFab.show()
                    saveWheelButton.isEnabled = true
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventControlInfoLoadingOrError::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (editWheelViewPager.currentItem == CONTROL_INFO) {
                    editControlInfoFab.hide()
                    goodBadDetailMenuFloatingActionButton.hideSheetThenFab()
                    saveWheelButton.isEnabled = false
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventControlInfoSuccessLoading::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (editWheelViewPager.currentItem == CONTROL_INFO) {
                    editControlInfoFab.show()
                    goodBadDetailMenuFloatingActionButton.showFab()
                    saveWheelButton.isEnabled = true
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventWheelCheckParametersSuccess::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                val result = viewModel.getControlResult()
                if (result?.id == ControlResult.WHEEL_GOOD) {
                    saveGoodWheel()
                } else if (result?.id == ControlResult.WHEEL_BAD) {
                    saveBadWheel()
                }
            }).addTo(compositeDisposable)

        viewModel.wheelSaveResult
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        savingDialog.startSavingDialog()
                    }
                    DataResponse.Status.SUCCESS -> {
                        savingDialog.dismissDialog()
                        toast(getString(R.string.success))
                        WheelRepository(this).saveEditedWheel(viewModel.getFullWheel()!!)
                        finish()
                    }
                    DataResponse.Status.ERROR -> {
                        when (it.error) {
                            is SaveRepeatDetailError -> MaterialAlertDialogBuilder(this)
                                .setTitle(getString(R.string.attention))
                                .setMessage(it.error.message)
                                .setPositiveButton("Продолжить") { _, _ ->
                                    viewModel.saveAnyway(it.error.date)
                                }
                                .setNegativeButton("Отмена", null)
                                .show()
                            else -> toast(it.error?.message)
                        }
                        savingDialog.dismissDialog()
                    }
                }
            }).addTo(compositeDisposable)

        goodDetailButton.setOnClickListener {
            viewModel.setControlResult(ControlResult.WHEEL_GOOD)
            viewModel.checkAllParameters()
        }

        refreshButton.setOnClickListener {
            viewModel.refreshFullAxle()
        }

        editWheelViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                if (state != 0) {
                    goodBadDetailMenuFloatingActionButton.hideSheetThenFab()
                    nextWheelInputFab.hide()
                    prevWheelInputsFab.hide()
                    saveWheelFab.hide()
                    editControlInfoFab.hide()
                }
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (positionOffset == 0f) {
                    when (position) {
                        BASE_INFO -> {
                            supportActionBar?.title = getString(R.string.wheel_base_input_title)
                            if (!baseInfoFirstLoad) {
                                nextWheelInputFab.show()
                            }
                        }
                        DEFECT_MOMENT_INFO -> {
                            supportActionBar?.title = getString(R.string.wheel_defect_moment_input_title)
                            prevWheelInputsFab.show()
                        }
                        CONTROL_INFO -> {
                            supportActionBar?.title = getString(R.string.defect_after_control_title)
                            prevWheelInputsFab.show()
                        }
                        DEFECT_INFO -> {
                            supportActionBar?.title = getString(R.string.wrong_detail_title)
                            goodBadDetailMenuFloatingActionButton.hideSheetThenFab()
                            editControlInfoFab.hide()
                            prevWheelInputsFab.show()
                            saveWheelFab.show()
                        }
                    }
                }
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    BASE_INFO -> baseChip.isChecked = true
                    DEFECT_MOMENT_INFO -> defectBeforeControlChip.isChecked = true
                    CONTROL_INFO -> defectAfterControlChip.isChecked = true
                    DEFECT_INFO -> wrongDetailChip.isChecked = true
                }
            }
        })

        nextWheelInputFab.setOnClickListener {
            when (editWheelViewPager.currentItem) {
                BASE_INFO -> RxBus.publish(RxEvent.EventBaseInfoRequestCheckFields())
            }
        }

        prevWheelInputsFab.setOnClickListener {
            when (editWheelViewPager.currentItem) {
                DEFECT_MOMENT_INFO -> editWheelViewPager.setCurrentItem(BASE_INFO, true)
                CONTROL_INFO -> editWheelViewPager.setCurrentItem(BASE_INFO, true)
                DEFECT_INFO -> editWheelViewPager.setCurrentItem(CONTROL_INFO, true)
            }
        }

        goodBadDetailMenuFloatingActionButton.setEventListener(object : MaterialSheetFabEventListener() {
            override fun onShowSheet() {
                editControlInfoFab.hide()
            }

            override fun onHideSheet() {
                editControlInfoFab.show()
            }
        })

        badDetailButton.setOnClickListener {
            RxBus.publish(RxEvent.EventControlInfoRequestCheckFields())
        }

        editControlInfoFab.setOnClickListener {
            if (!supportFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_TYPE_FRAGMENT_TAG }) {
                wheelRepairTypeFragment.show(
                    supportFragmentManager,
                    WHEEL_REPAIR_TYPE_FRAGMENT_TAG
                )
            }
        }

        saveWheelFab.setOnClickListener {
            viewModel.checkAllParameters()
        }

        saveWheelButton.setOnClickListener {
            //viewModel.checkAllParameters()
            val wheel = viewModel.getFullWheel()
            if (wheel?.wheelControlInfo?.controlZones.isNullOrEmpty()) {
                RxBus.publish(RxEvent.EventBaseInfoRequestCheckFields())
            } else {
                viewModel.checkAllParameters()
            }
//            when (editWheelViewPager.currentItem) {
//                BASE_INFO -> RxBus.publish(RxEvent.EventBaseInfoRequestCheckFields())
//                else -> viewModel.checkAllParameters()
//            }
        }

        baseChip.setOnClickListener {
            if (baseChip.isChecked) {
                editWheelViewPager.setCurrentItem(BASE_INFO, true)
            } else {
                baseChip.isChecked = true
            }
        }

        defectBeforeControlChip.setOnClickListener {
            if (defectBeforeControlChip.isChecked) {
                editWheelViewPager.setCurrentItem(DEFECT_MOMENT_INFO, true)
            } else {
                defectBeforeControlChip.isChecked = true
            }
        }

        defectAfterControlChip.setOnClickListener {
            if (defectAfterControlChip.isChecked) {
                editWheelViewPager.setCurrentItem(CONTROL_INFO, true)
            } else {
                defectAfterControlChip.isChecked = true
            }
        }

        wrongDetailChip.setOnClickListener {
            if (wrongDetailChip.isChecked) {
                editWheelViewPager.setCurrentItem(DEFECT_INFO, true)
            } else {
                wrongDetailChip.isChecked = true
            }
        }

        inputsChipGroup.setOnCheckedChangeListener { _, checkedId ->
            val chip = inputsChipGroup.findViewById<Chip>(checkedId) ?: return@setOnCheckedChangeListener
            val scrollBounds = Rect()
            inputsChipsHorizontalScrollView.getHitRect(scrollBounds)
            if (!chip.getLocalVisibleRect(scrollBounds) || scrollBounds.width() < chip.width) {
                inputsChipsHorizontalScrollView.smoothScrollTo(chip.left - chip.paddingLeft, chip.top)
            }
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
            editWheelViewPager.visibility = View.GONE
        } else {
            editWheelViewPager.visibility = View.VISIBLE
            loadingProgressBar.visibility = View.GONE
        }
    }

    private fun showRefresh(show: Boolean, message: String? = null) {
        if (show) {
            editWheelViewPager.visibility = View.GONE
            refreshConstraintLayout.visibility = View.VISIBLE
            refreshMessageTextView.text = message
        } else {
            refreshConstraintLayout.visibility = View.GONE
            editWheelViewPager.visibility = View.VISIBLE
        }
    }

    private fun saveGoodWheel() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.save)
            .setMessage(viewModel.getGoodWheelMessage())
            .setPositiveButton(R.string.ok) { _, _ ->
                viewModel.save()
            }
            .setNegativeButton(R.string.cancel, null)
            .setCancelable(false)
            .show()
    }

    private fun saveBadWheel() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.save)
            .setMessage(viewModel.getBadWheelMessage())
            .setPositiveButton(R.string.ok) { _, _ ->
                viewModel.save()
            }
            .setNegativeButton(R.string.cancel, null)
            .setCancelable(false)
            .show()
    }

    private fun closeWithoutSaving() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.exit)
            .setMessage(R.string.exit_without_saving_dialog)
            .setPositiveButton(R.string.yes) { _, _ ->
                finish()
            }
            .setNegativeButton(R.string.no, null)
            .show()
    }

    private fun toast(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val fullWheel = viewModel.getFullWheel()
        if (fullWheel != null) {
            val json = Gson().toJson(fullWheel)
            val vMSateJson = Gson().toJson(viewModel.viewModelState)
            outState.putString(SAVED_WHEEL_KEY, json)
            outState.putString(SAVED_VM_STATE_KEY, vMSateJson)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                closeWithoutSaving()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        closeWithoutSaving()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}










































