package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.adapters.AxleDefectSpinnerAdapter
import com.fmklab.armnk_results.modules.axle.models.AxleDefect
import com.fmklab.armnk_results.modules.axle.models.AxleControlZone
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.DataResponse
import com.google.android.material.radiobutton.MaterialRadioButton
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_visual_and_ntd_defect.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class VisualAndNtdDefectFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel by sharedViewModel<EditAxleViewModel>()
    private lateinit var magneticRadioGroup: RadioGroup

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_visual_and_ntd_defect, container, false)
    }

    override fun onResume() {
        super.onResume()

        setupUI()
        setupData()
        setupBindings()
        setupControlZones()
    }

    override fun onPause() {
        super.onPause()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        defectCodeAutoCompleteTextView.keyListener = null
        defectCodeAutoCompleteTextView.setText("", false)
        magneticControlLinearLayout.removeAllViews()
        magneticRadioGroup = RadioGroup(requireContext())
        magneticControlLinearLayout.addView(magneticRadioGroup)
    }

    private fun setupData() {
        if (viewModel.editingAxle.controlZones.size == 0) {
            Log.d("TAG!!!!!!!!", "SETUP")
            viewModel.loadDefectAfterInfo()
        }
    }

    private fun setupBindings() {
        viewModel.axleDefectVisualInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false, null)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        defectCodeAutoCompleteTextView.setText("", false)
                        if (it.data != null) {
                            AxleDefectSpinnerAdapter(
                                requireActivity(),
                                R.layout.support_simple_spinner_dropdown_item,
                                ArrayList(it.data)
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectCodeAutoCompleteTextView.setAdapter(adapter)
                            }
//                            val defect =
//                                it.data.find { defect -> defect.id == viewModel.editingAxle.visualCodeId }
                            val defect =
                                it.data.find { defect -> defect.id == viewModel.editingAxle.defectVisual?.id }
                            if (defect != null) {
                                defectCodeAutoCompleteTextView.setText(defect.title, false)
                                //viewModel.editingAxle.visualCodeId = defect.id
                                viewModel.editingAxle.defectVisual = defect
                            } else if (it.data.size == 1) {
                                defectCodeAutoCompleteTextView.setText(it.data[0].title)
                                //viewModel.editingAxle.visualCodeId = it.data[0].id
                                viewModel.editingAxle.defectVisual = it.data[0]
                            }
                            defectDescriptionEditText.setText(viewModel.editingAxle.visualDescription)
                            generateDefectString()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error?.message)
                    }
                    DataResponse.Status.EMPTY -> {
                        viewModel.editingAxle.defectVisualControlZone = null
                        viewModel.editingAxle.defectVisual = null
                        defectCodeAutoCompleteTextView.setText("")
                        defectCodeAutoCompleteTextView.setAdapter(null)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectAfterInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                        showRefresh(false, null)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            if (viewModel.editingAxle.controlZones.size == 0) {
                                viewModel.editingAxle.controlZones = ArrayList(it.data.controlZones)
                            }
                        }
                        setupControlZones()
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error?.message)
                    }
                }
            }).addTo(compositeDisposable)
        defectCodeAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val defect = defectCodeAutoCompleteTextView.adapter.getItem(position) as AxleDefect
            viewModel.editingAxle.defectVisual = defect
            //viewModel.editingAxle.visualCodeId = defect.id
            generateDefectString()
        }
        defectCodeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectCodeTextInputLayout.error = null
            }
        }
        defectDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.editingAxle.visualDescription = text.toString()
            generateDefectString()
        }
        removeDefectCodeButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("")
            //viewModel.editingAxle.visualCodeId = FullAxle.DEFAULT_VISUAL_CODE_ID
            viewModel.editingAxle.defectVisual = null
            generateDefectString()
        }
        removeDefectDescriptionButton.setOnClickListener {
            defectDescriptionEditText.setText("")
        }
        refreshButton.setOnClickListener {
            viewModel.loadAxleDefectsVisualInfo()
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
            magneticRadioGroup.isEnabled = false
            defectCodeAutoCompleteTextView.isEnabled = false
            removeDefectCodeButton.isEnabled = false
        } else {
            loadingProgressBar.visibility = View.GONE
            magneticRadioGroup.isEnabled = true
            defectCodeAutoCompleteTextView.isEnabled = true
            removeDefectCodeButton.isEnabled = true
        }
    }

    private fun showRefresh(show: Boolean, message: String?) {
        if (show) {
            visualAndNdtNestedScrollView.visibility = View.INVISIBLE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.visibility = View.VISIBLE
            errorMessageTextView.text = message
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            visualAndNdtNestedScrollView.visibility = View.VISIBLE
        }
    }

    fun checkFields(): Boolean {
        if (defectCodeAutoCompleteTextView?.text.isNullOrEmpty()) {
            defectCodeTextInputLayout?.error = getString(R.string.field_empty_error)
            return false
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun generateDefectString() {
        var str =
            "обнаружен: \"при ВО при НК\", метод (зона): \"${viewModel.editingAxle.defectVisualControlZone?.title}\", код дефекта: \"${viewModel.editingAxle.defectVisual.toString()}\""
        if (viewModel.editingAxle.visualDescription.isNotEmpty()) {
            str += ", описание: \"${viewModel.editingAxle.visualDescription}\""
        }
        viewModel.editingAxle.defectInfo = str
    }

    private fun setupControlZones() {
        magneticRadioGroup.removeAllViews()
        for (controlZone in viewModel.editingAxle.controlZones) {
//            if (controlZone.checked == false) {
//                if (viewModel.editingAxle.defectVisualControlZone?.id == controlZone.id) {
//
//                    viewModel.resetAxleDefectsVisualInfo()
//                }
//                continue
//            }
            if (controlZone.vmInDb == DefectAfterControlFragment.MAGNETIC_CONTROL) {
                val radioButton = MaterialRadioButton(requireContext())
                radioButton.text = controlZone.title
                radioButton.tag = controlZone
                radioButton.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) {
                        val zone = radioButton.tag as AxleControlZone
                        //viewModel.editingAxle.visualZoneId = zone.id
                        viewModel.editingAxle.defectVisualControlZone = zone
                        viewModel.editingAxle.selectControlZone(zone.id)
                        viewModel.loadAxleDefectsVisualInfo()
                    }
                }
                magneticRadioGroup.addView(radioButton)
                if (viewModel.editingAxle.defectVisualControlZone?.id == controlZone.id) {
                    radioButton.isChecked = true
                }
            }
        }
    }
}





























