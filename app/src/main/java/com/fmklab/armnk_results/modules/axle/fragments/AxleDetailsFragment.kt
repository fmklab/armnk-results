package com.fmklab.armnk_results.modules.axle.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.activities.EditAxleActivity
import com.fmklab.armnk_results.modules.axle.models.Axle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_axle_details.*
import java.text.SimpleDateFormat
import java.util.*

class AxleDetailsFragment(): BottomSheetDialogFragment() {

    private lateinit var axle: Axle

    companion object {
        fun newInstance(axle: Axle): AxleDetailsFragment {
            val fragment = AxleDetailsFragment()
            val bundle = Bundle(1)
            val json = Gson().toJson(axle)
            bundle.putString("Axle", json)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_axle_details, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val json = arguments?.getString("Axle")
        axle = if (json != null) {
            Gson().fromJson(json, Axle::class.java)
        } else {
            Axle()
        }

        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI() {
        axleNestedScrollView.setOnScrollChangeListener { _: NestedScrollView?, _: Int, scrollY: Int, _: Int, oldScrollY: Int ->
            run {
                if (oldScrollY > scrollY) {
                    editAxleFAB.show()
                } else if (oldScrollY < scrollY) {
                    editAxleFAB.hide()
                }
            }
        }
        axleDetailsToolbar.title = "Ось №${axle.axleNumber}"
        val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
        createDateValueTextView.text = createDateFormat.format(axle.createDate)
        val simpleDateFormatter = SimpleDateFormat("dd.MM.yyyy", Locale.US)
        controlDateValueTextView.text = simpleDateFormatter.format(axle.controlDate)
        factoryNameValueTextView.text = axle.factory.code
        factoryYearValueTextView.text = axle.factoryYear.toString()
        axleTypeValueTextView.text = axle.type.name
        wheelNumberValueTextView.text = "${axle.wheel1} / ${axle.wheel2}"
        controlInfoValueTextView.text = axle.controlInfo
        defectDescriptionValueTextView.text = axle.defectInfo
        specialistCredentialsValueTextView.text = axle.specialistCredentials
        axleStatusValueTextView.text = axle.result
    }

    private fun setupBindings() {
        editAxleFAB.setOnClickListener {
            val intent = Intent(activity, EditAxleActivity::class.java).apply {
                //val json = Gson().toJson(axle)
                putExtra("axleId", axle.id)
            }
            startActivity(intent)
        }
    }
}



















