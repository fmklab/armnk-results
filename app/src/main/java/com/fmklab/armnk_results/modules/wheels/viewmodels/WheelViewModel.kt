package com.fmklab.armnk_results.modules.wheels.viewmodels

import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import com.fmklab.armnk_results.modules.wheels.models.Wheel
import com.fmklab.armnk_results.modules.wheels.services.WheelApiService
import io.reactivex.rxjava3.core.Observable

class WheelViewModel(private val currentUser: CurrentUser, private val wheelApiService: WheelApiService):
    DetailItemsViewModel<Wheel>() {

    override fun getLoadDetailsObservable(page: Int): Observable<ArrayList<Wheel>> {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        return wheelApiService.getWheelList(companyId, page, token)
    }

    override fun getSearchDetailObservable(searchString: String): Observable<ArrayList<Wheel>> {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        return wheelApiService.searchWheel(companyId, searchString, token)
    }
}
































