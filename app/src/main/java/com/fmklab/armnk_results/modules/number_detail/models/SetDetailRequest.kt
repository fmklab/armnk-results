package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.auth.models.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SetDetailRequest (
    @SerializedName("user")
    @Expose
    val user: User,
    @SerializedName("detail")
    @Expose
    val detail: NumberDetailDto
)