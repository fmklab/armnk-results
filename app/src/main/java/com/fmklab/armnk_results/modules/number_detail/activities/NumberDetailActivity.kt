package com.fmklab.armnk_results.modules.number_detail.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.activities.DetailItemsActivity
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import com.fmklab.armnk_results.modules.number_detail.adapters.NumberDetailAdapter
import com.fmklab.armnk_results.modules.number_detail.fragments.NumberDetailDetailsFragment
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailListItem
import com.fmklab.armnk_results.modules.number_detail.viewmodels.NumberDetailViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf

class NumberDetailActivity: DetailItemsActivity<NumberDetailListItem>() {

    override val viewModel: DetailItemsViewModel<NumberDetailListItem> by lifecycleScope.viewModel<NumberDetailViewModel>(this) {
        parametersOf(intent.extras?.getInt(PART_ID_KEY))
    }
    override val detailsAdapter: DetailItemAdapter<NumberDetailListItem> by lifecycleScope.inject<NumberDetailAdapter>()
    override val recyclerViewLayoutManager: LinearLayoutManager = LinearLayoutManager(this)
    override var detailsFragment: BottomSheetDialogFragment? = null

    companion object {
        const val PART_ID_KEY = "PartIdKey"
        const val PART_NAME_KEY = "PartNameKey"
        const val DETAILS_FRAGMENT_TAG = "DetailsFragmentTag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_small_detail)

        setupUI()
        setupListeners()
        setupData()
        subscribeObservers()
    }

    override fun onResume() {
        super.onResume()

        viewModel.loadDetails(0)
    }

    override fun setupUI() {
        recyclerView = findViewById(R.id.smallDetailRecyclerView)
        swipeRefreshLayout = findViewById(R.id.smallDetailSwipeRefreshLayout)
        addNewDetailFab = findViewById(R.id.addNewSmallDetailFab)
        toolBar = findViewById(R.id.smallDetailToolBar)
        emptyDataTextView = findViewById(R.id.emptyDataTextView)
        title = intent.extras?.getString(PART_NAME_KEY)

        super.setupUI()
    }

    override fun subscribeObservers() {
        super.subscribeObservers()

        RxBus.listen(RxEvent.EventNumberDetailEditFinish::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detailsAdapter.addOrUpdateItem(it.detail)
                Toast.makeText(this, getString(R.string.success), Toast.LENGTH_SHORT).show()
            }
            .addTo(compositeDisposable)
    }

    override fun showDetails(position: Int) {
        val detail = detailsAdapter.getItem(position)
        if (detail != null) {
            detailsFragment = NumberDetailDetailsFragment.newInstance(detail, intent.extras?.getString(
                PART_NAME_KEY
            ))
            if (!supportFragmentManager.fragments.any { frg -> frg.tag == DETAILS_FRAGMENT_TAG }) {
                detailsFragment?.show(supportFragmentManager,
                    DETAILS_FRAGMENT_TAG
                )
            }
        }
    }

    override fun addNewDetail() {
        val intent = Intent(this, EditNumberDetailActivity::class.java).apply {
            putExtra(EditNumberDetailActivity.NUMBER_DETAIL_ID, 0)
            putExtra(PART_ID_KEY, intent.extras?.getInt(PART_ID_KEY))
            putExtra(PART_NAME_KEY, intent.extras?.getString(PART_NAME_KEY))
        }
        startActivity(intent)
    }
}

































