package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetail
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

abstract class NumberDetailAbstractDefectInfoFragment: Fragment() {

    //UI
    protected lateinit var defectTypeAutoCompleteTextView: AutoCompleteTextView
    protected lateinit var defectTypeTextInputLayout: TextInputLayout
    protected lateinit var defectZoneAutoCompleteTextView: AutoCompleteTextView
    protected lateinit var defectZoneTextInputLayout: TextInputLayout
    protected lateinit var stealAutoCompleteTextView: AutoCompleteTextView
    protected lateinit var stealTextInputLayout: TextInputLayout
    protected lateinit var ownerEditText: EditText
    protected lateinit var ownerTextInputLayout: TextInputLayout
    protected lateinit var defectTypeLoadingProgress: MaterialProgressBar
    protected lateinit var zoneLoadingProgress: MaterialProgressBar
    protected lateinit var stealLoadingProgress: MaterialProgressBar
    protected lateinit var viewConstraintLayout: ConstraintLayout

    protected val compositeDisposable = CompositeDisposable()
    protected val viewModel: EditNumberDetailViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupListeners()
        setupBindings()
    }

    protected open fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        defectZoneAutoCompleteTextView.keyListener = null
        stealAutoCompleteTextView.keyListener = null
    }

    protected open fun setupBindings() {
        viewModel.defectDataLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showDefectDataLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.defectDataErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Snackbar.make(viewConstraintLayout, it.message as CharSequence, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.refresh) {
                        refreshDefectData()
                    }
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.defectTypesState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.zonesState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectZoneAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.stealsState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                stealAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.defectType
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.setText(it.toString(), false)
            }
            .addTo(compositeDisposable)

        viewModel.zone
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectZoneAutoCompleteTextView.setText(it.toString(), false)
            }
            .addTo(compositeDisposable)

        viewModel.steal
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                stealAutoCompleteTextView.setText(it, false)
            }
            .addTo(compositeDisposable)

        viewModel.owner
            .filter { ownerEditText.text.toString() != it }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                ownerEditText.setText(it)
            }
            .addTo(compositeDisposable)

        viewModel.emptyDefectTypeErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { defectTypeTextInputLayout.error = getString(R.string.field_empty_error) }
            .addTo(compositeDisposable)

        viewModel.emptyZoneErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { defectZoneTextInputLayout.error = getString(R.string.field_empty_error) }
            .addTo(compositeDisposable)

        viewModel.emptyStealErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { stealTextInputLayout.error = getString(R.string.field_empty_error) }
            .addTo(compositeDisposable)

        viewModel.emptyOwnerErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { ownerTextInputLayout.error = getString(R.string.field_empty_error) }
            .addTo(compositeDisposable)

        defectTypeAutoCompleteTextView
            .itemSelected<DefectType>()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.defectType)
            .addTo(compositeDisposable)

        defectZoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.zone)
            .addTo(compositeDisposable)

        stealAutoCompleteTextView
            .itemSelected<String>()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.steal)
            .addTo(compositeDisposable)

        ownerEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.owner.accept(it.toString())
            }
            .addTo(compositeDisposable)
    }

    protected open fun setupListeners() {
        defectTypeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                defectTypeTextInputLayout.error = null
            }
        }

        defectZoneAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                defectZoneTextInputLayout.error = null
            }
        }

        stealAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                stealTextInputLayout.error = null
            }
        }

        ownerEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                ownerTextInputLayout.error = null
            }
        }
    }

    protected open fun showDefectDataLoading(show: Boolean) {
        if (show) {
            defectTypeLoadingProgress.visibility = View.VISIBLE
            zoneLoadingProgress.visibility = View.VISIBLE
            stealLoadingProgress.visibility = View.VISIBLE
            defectTypeLoadingProgress.isIndeterminate = true
            zoneLoadingProgress.isIndeterminate = true
            stealLoadingProgress.isIndeterminate = true
        } else {
            defectTypeLoadingProgress.visibility = View.GONE
            zoneLoadingProgress.visibility = View.GONE
            stealLoadingProgress.visibility = View.GONE
            defectTypeLoadingProgress.isIndeterminate = false
            zoneLoadingProgress.isIndeterminate = false
            stealLoadingProgress.isIndeterminate = false
        }
    }

    protected open fun refreshDefectData() {
        viewModel.loadDefectData.accept(Unit)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}