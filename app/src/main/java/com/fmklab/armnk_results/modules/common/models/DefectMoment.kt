package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

enum class DefectMoment(value: Int) {

    @SerializedName("0")
    @Expose
    NONE(0),
    @SerializedName("1")
    @Expose
    DEFECT(1),
    @SerializedName("2")
    @Expose
    VISUAL(2),
    @SerializedName("3")
    @Expose
    NDT(3)
}