package com.fmklab.armnk_results.modules.wheels.services

import com.fmklab.armnk_results.modules.common.models.CheckAddedDetailResponse
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.wheels.models.*
import io.reactivex.rxjava3.core.Observable
import retrofit2.Call
import retrofit2.http.*

interface WheelApiService {

    @GET("wheel/wheellist")
    fun getWheelList(
        @Query("companyId") companyId: Int,
        @Query("page") page: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Wheel>>

    @GET("wheel/full")
    fun getFullWheel(
        @Query("id") id: Int,
        @Header("Authorization") token: String
    ): Call<FullWheel>

    @GET("wheel/before/dtypes")
    fun getDefectBeforeControlTypes(@Header("Authorization") token: String): Call<ArrayList<DefectType>>

    @POST("wheel/controlzones")
    fun getControlZones(
        @Body controlZonesRequest: WheelControlZonesRequest,
        @Header("Authorization") token: String
    ): Call<ArrayList<WheelControlZone>>

    @GET("wheel/defects")
    fun getWheelDefects(
        @Query("defectTypeId") defectTypeId: Int,
        @Query("defectZoneId") defectZoneId: Int,
        @Query("defectMoment") defectMoment: Int,
        @Header("Authorization") token: String
    ): Call<ArrayList<WheelDefect>>

    @GET("wheel/detectors")
    fun getDetectors(
        @Query("companyId") companyId: Int,
        @Query("vm") vm: Int,
        @Header("Authorization") token: String
    ): Call<ArrayList<Detector>>

    @POST("wheel/set")
    fun setWheel(
        @Body setWheelModel: SetWheelModel,
        @Header("Authorization") token: String
    ): Call<Int>

    @GET("wheel/search")
    fun searchWheel(
        @Query("companyId") companyId: Int,
        @Query("number") number: String,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Wheel>>

    @POST("wheel/check")
    fun checkAddedWheel(
        @Body request: CheckAddedWheelRequest,
        @Header("Authorization") token: String
    ): Call<CheckAddedDetailResponse>
}





































