package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


data class RepeatDetail (
    @SerializedName("id")
    @Expose
    val id: Int = 0,
    @SerializedName("companyId")
    @Expose
    val companyId: Int,
    @SerializedName("manId")
    @Expose
    val manId: Int,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("controlDate")
    @Expose
    val controlDate: Date,
    @SerializedName("repeatControlDate")
    @Expose
    val repeatControlDate: Date,
    @SerializedName("number")
    @Expose
    val number: String
)