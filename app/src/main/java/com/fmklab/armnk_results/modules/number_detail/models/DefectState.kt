package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.wheels.models.DefectType

open class DefectState(defectType: DefectType?, zone: ControlZone?, steal: String?, owner: String?) :
    NumberDetailState(defectType, zone) {

    constructor(state: NumberDetailState?): this (
        state?.defectType,
        state?.zone,
        state?.steal,
        state?.owner
    )

    constructor(): this (
        null,
        null,
        null,
        null
    )

    override var steal: String? = steal
        get() = field

    override var owner: String? = owner
        get() = field

    override fun getDescription(): String {
        return ""
    }
}