package com.fmklab.armnk_results.modules.common.models

import java.util.*

class AxleError(message: String?, val axleId: Int): Error(message)

class BaseInfoError(message: String?) : Error(message)

class DefectBeforeControlTypesInfoError(message: String?): Error(message)

class NdtDefectError(message: String?, val controlType: Int) : Error(message)

class SearchAxleError(message: String?): Error(message)

class ControlZonesError(message: String?): Error(message)

class DefectsError(message: String?): Error(message)

class SaveRepeatDetailError(message: String?, val date: Date) : Error(message)