package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Method (
    @SerializedName("customId")
    @Expose
    val id: Int = 0,
    @SerializedName("fullName")
    @Expose
    val fullName: String? = null,
    @SerializedName("shortName")
    @Expose
    val shortName: String? = null
)