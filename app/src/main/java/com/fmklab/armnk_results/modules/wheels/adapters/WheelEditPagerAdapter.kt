package com.fmklab.armnk_results.modules.wheels.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.fmklab.armnk_results.modules.wheels.fragments.WheelBaseInputFragment
import com.fmklab.armnk_results.modules.wheels.fragments.WheelControlInfoFragment
import com.fmklab.armnk_results.modules.wheels.fragments.WheelDefectInfoFragment
import com.fmklab.armnk_results.modules.wheels.fragments.WheelDefectMomentInputFragment

class WheelEditPagerAdapter(fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {

    private var fragments = arrayOf(
        WheelBaseInputFragment(),
        WheelDefectMomentInputFragment(),
        WheelControlInfoFragment(),
        WheelDefectInfoFragment()
    )

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragments[position] = fragment
        return fragment
    }
}