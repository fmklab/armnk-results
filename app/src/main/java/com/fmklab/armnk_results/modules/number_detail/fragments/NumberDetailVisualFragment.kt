package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetail
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_number_detail_visual.*

class NumberDetailVisualFragment: NumberDetailAbstractDefectInfoFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_visual, container, false)
    }

    override fun setupUI() {
        defectTypeAutoCompleteTextView = requireView().findViewById(R.id.defectTypeAutoCompleteTextView)
        defectTypeTextInputLayout = requireView().findViewById(R.id.defectTypeTextInputLayout)
        defectZoneAutoCompleteTextView = requireView().findViewById(R.id.defectZoneAutoCompleteTextView)
        defectZoneTextInputLayout = requireView().findViewById(R.id.defectZoneTextInputLayout)
        stealAutoCompleteTextView = requireView().findViewById(R.id.stealAutoCompleteTextView)
        stealTextInputLayout = requireView().findViewById(R.id.stealTextInputLayout)
        ownerEditText = requireView().findViewById(R.id.ownerEditText)
        ownerTextInputLayout = requireView().findViewById(R.id.ownerTextInputLayout)
        defectTypeLoadingProgress = requireView().findViewById(R.id.defectTypeLoadingProgress)
//        zoneLoadingProgress = requireView().findViewById(R.id.zoneLoadingProgress)
//        stealLoadingProgress = requireView().findViewById(R.id.stealLoadingProgress)
        viewConstraintLayout = requireView().findViewById(R.id.viewConstraintLayout)

        super.setupUI()
    }

    override fun setupBindings() {
        super.setupBindings()

        viewModel.defectLength
            .filter { defectLengthEditText.text.toString() != it }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectLengthEditText.setText(it)
            }
            .addTo(compositeDisposable)

        viewModel.defectDepth
            .filter { defectDepthEditText.text.toString() != it }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectDepthEditText.setText(it)
            }
            .addTo(compositeDisposable)

        viewModel.defectDiameter
            .filter { defectDiameterEditText.text.toString() != it }
            .observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectDiameterEditText.setText(it)
            }
            .addTo(compositeDisposable)

        defectLengthEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.defectLength)
            .addTo(compositeDisposable)

        defectDepthEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.defectDepth)
            .addTo(compositeDisposable)

        defectDiameterEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.defectDiameter)
            .addTo(compositeDisposable)
    }
}










































