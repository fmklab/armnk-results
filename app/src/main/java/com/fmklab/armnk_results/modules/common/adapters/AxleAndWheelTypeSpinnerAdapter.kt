package com.fmklab.armnk_results.modules.common.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fmklab.armnk_results.modules.common.models.AxleAndWheelType

class AxleAndWheelTypeSpinnerAdapter(
    context: Context,
    resource: Int,
    private val objects: Array<out AxleAndWheelType>
) : ArrayAdapter<AxleAndWheelType>(context, resource, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = super.getView(position, convertView, parent) as TextView
        textView.text = objects[position].shortName
        return textView
    }
}