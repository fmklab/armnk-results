package com.fmklab.armnk_results.modules.axle.models

import com.fmklab.armnk_results.modules.auth.models.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SetAxleModel (
    @SerializedName("user")
    @Expose
    val user: User,
    @SerializedName("fullAxleInfo")
    @Expose
    val fullAxle: FullAxle,
    @SerializedName("appVersion")
    @Expose
    val appVersion: Int
)