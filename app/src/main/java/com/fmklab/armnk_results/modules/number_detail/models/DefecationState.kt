package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.wheels.models.DefectType

class DefecationState(
    defectType: DefectType?, zone: ControlZone?, steal: String?, owner: String?,
    defectDescription: String?
) : DefectState(defectType, zone, steal, owner) {

    constructor(state: NumberDetailState?): this(state?.defectType, state?.zone, state?.steal, state?.owner, state?.defectDescription)

    override var defectDescription: String? = defectDescription
        get() = field

    override fun getDescription(): String {
        return "обнаружен: \"при дефектации\"; тип дефекта: \"${defectType?.name}\"; зона выявления: \"${zone?.name}\"; описание: \"$defectDescription\"; " +
                "сталь: \"$steal\"; собственник: \"$owner\";"
    }
}