package com.fmklab.armnk_results.modules.axle.models

import com.fmklab.armnk_results.modules.axle.fragments.WrongDetailFragment
import com.fmklab.armnk_results.modules.common.CheckAxleBaseInfoResponse
import com.fmklab.armnk_results.modules.common.models.AxleAndWheelType
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.common.models.Factory
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class FullAxle (
    @SerializedName("id")
    @Expose
    var id: Int = DEFAULT_ID,
    @SerializedName("controlDate")
    @Expose
    var controlDate: Date = Date(),
    @SerializedName("factory")
    @Expose
    var factory: Factory = Factory(),
    @SerializedName("factoryYear")
    @Expose
    var factoryYear: String = DEFAULT_FACTORY_YEAR,
    @SerializedName("axleNumber")
    @Expose
    var axleNumber: String = DEFAULT_AXLE_NUMBER,
    @SerializedName("type")
    @Expose
    var type: AxleAndWheelType = AxleAndWheelType(),
    @SerializedName("wheel1")
    @Expose
    val wheel1: String = "",
    @SerializedName("wheel2")
    @Expose
    val wheel2: String = "",
    @SerializedName("wheel1Id")
    @Expose
    val wheel1Id: Int = 0,
    @SerializedName("wheel2Id")
    @Expose
    val wheel2Id: Int = 0,
    @SerializedName("controlInfo")
    @Expose
    var controlInfo: String = "",
    @SerializedName("defectInfo")
    @Expose
    var defectInfo: String = "",
    @SerializedName("specialistCredentials")
    @Expose
    var specialistCredentials: String = DEFAULT_SPEC_CREDENTIALS,
    @SerializedName("result")
    @Expose
    var result: String = "",
    @SerializedName("ultrasonicControlUser")
    @Expose
    var ultrasonicControlUser: String = DEFAULT_ULTRASONIC_CONTROL_USER,
    @SerializedName("magneticControlUser")
    @Expose
    var magneticControlUser: String = DEFAULT_MAGNETIC_CONTROL_USER,
    @SerializedName("repairType")
    @Expose
    var repairType: Int = DEFAULT_REPAIR_TYPE,
    @SerializedName("rings")
    @Expose
    var rings: Int = 0,
    @SerializedName("defectBeforeControlZone")
    @Expose
    var defectControlZone: AxleControlZone? = null,
    @SerializedName("defectBeforeControl")
    @Expose
    var defect: AxleDefect? = null,
    @SerializedName("defectBeforeControlType")
    @Expose
    var defectBeforeControlType: AxleDefectBeforeControlType? = null,
    @SerializedName("controlType")
    @Expose
    var controlType: Int = DEFAULT_CONTROL_TYPE,
    @SerializedName("defectDescription")
    @Expose
    var defectDescription: String = DEFAULT_DEFECT_DESCRIPTION,
    @SerializedName("defectVisualControlZone")
    @Expose
    var defectVisualControlZone: AxleControlZone? = null,
    @SerializedName("defectVisualControl")
    @Expose
    var defectVisual: AxleDefect? = null,
    @SerializedName("visualDescription")
    @Expose
    var visualDescription: String = DEFAULT_VISUAL_DESCRIPTION,
    @SerializedName("defectNdtControlZone")
    @Expose
    var defectNdtControlZone: AxleControlZone? = null,
    @SerializedName("defectNdtControl")
    @Expose
    var defectNdt: AxleDefect? = null,
    @SerializedName("ntdDescription")
    @Expose
    var ntdDescription: String = DEFAULT_NDT_DESCRIPTION,
    @SerializedName("detector")
    @Expose
    var detector: Detector? = Detector(),
    @SerializedName("defectSize")
    @Expose
    var defectSize: String = DEFAULT_DEFECT_SIZE,
    @SerializedName("sizeType")
    @Expose
    var sizeType: Int = 0,
    @SerializedName("ultrasonicCount")
    @Expose
    var ultrasonicCount: Int = 0,
    @SerializedName("magneticCount")
    @Expose
    var magneticCount: Int = 0,
    @SerializedName("defectBeforeNdt")
    @Expose
    var defectBeforeNdt: Int = 0,
    @SerializedName("isNewDetail")
    @Expose
    var isNewDetail: Boolean = false,
    @SerializedName("isMagnetic")
    @Expose
    var isMagnetic: Boolean = false,
    @SerializedName("selectedControlZonesId")
    @Expose
    var selectedControlZonesId: ArrayList<Int> = ArrayList(),
    @SerializedName("createDate")
    @Expose
    val createDate: Date = Date(),
    @SerializedName("isMobileApp")
    @Expose
    val isMobileApp: Boolean = true
) {

    companion object {
        const val DEFAULT_ID = 0
        const val DEFAULT_FACTORY_YEAR = ""
        const val DEFAULT_AXLE_NUMBER = ""
        const val DEFAULT_ULTRASONIC_CONTROL_USER = ""
        const val DEFAULT_MAGNETIC_CONTROL_USER = ""
        const val DEFAULT_CONTROL_TYPE = 0
        const val DEFAULT_REPAIR_TYPE = 0
        const val DEFAULT_RINGS = 0
        const val DEFAULT_RESULT = ""
        const val DEFAULT_SPEC_CREDENTIALS = ""

        const val DEFAULT_DEFECT_DESCRIPTION = ""

        const val DEFAULT_VISUAL_DESCRIPTION = ""

        const val DEFAULT_NDT_DESCRIPTION = ""

        const val DEFECT_AFTER_CONTROL = 0
        const val DEFECT_BEFORE_CONTROL = 1

        const val DEFAULT_DEFECT_SIZE = ""
    }

    @SerializedName("controlZones")
    @Expose
    var controlZones: ArrayList<AxleControlZone> = ArrayList()

    fun getResult(): Boolean? {
        if (result.toLowerCase(Locale.ROOT) == "годен") {
            return true
        }
        if (result.toLowerCase(Locale.ROOT) == "брак") {
            return false
        }
        return null
    }

    fun checkBaseInfo(): CheckAxleBaseInfoResponse {
        if (factory.code == Factory.DEFAULT_CODE) {
            return CheckAxleBaseInfoResponse.factoryCode()
        }
        if (factoryYear == DEFAULT_FACTORY_YEAR) {
            return CheckAxleBaseInfoResponse.factoryYear()
        }
        if (type.name == AxleAndWheelType.DEFAULT_NAME) {
            return CheckAxleBaseInfoResponse.name()
        }
        if (axleNumber == DEFAULT_AXLE_NUMBER) {
            return CheckAxleBaseInfoResponse.axleNumber()
        }
        if (specialistCredentials == DEFAULT_SPEC_CREDENTIALS) {
            return CheckAxleBaseInfoResponse.specName()
        }
        if (repairType == DEFAULT_REPAIR_TYPE) {
            return CheckAxleBaseInfoResponse.repairType()
        }
        return CheckAxleBaseInfoResponse.good()
    }

    fun checkDefectBeforeControlInfo(): Pair<Boolean, String?> {
        if (defect == null) {
            return Pair(false, null)
        }
        if (defectControlZone == null) {
            return Pair(false, null)
        }
        if (defectBeforeControlType == null) {
            return Pair(false, null)
        }
        if (repairType == DEFAULT_REPAIR_TYPE) {
            return Pair(false, "Не выбран тип ремонта")
        }
        return Pair(true, null)
    }

    fun checkDefectAfterControlInfo(): Boolean {
        if (ultrasonicCount > 0) {
            if (ultrasonicControlUser == DEFAULT_ULTRASONIC_CONTROL_USER) {
                return false
            }
        }
        if (magneticCount > 0) {
            if (magneticControlUser == DEFAULT_MAGNETIC_CONTROL_USER) {
                return false
            }
        }
        return true
    }

    fun checkWrongDetailInfo(): Pair<Boolean, String?> {
        return when (controlType) {
            WrongDetailFragment.VISUAL_CONTROL -> {
                if (defectVisual == null) {
                   return Pair(false, null)
                }
               Pair(true, null)
            }
            WrongDetailFragment.NDT_CONTROL -> {
                if (defectNdt == null) {
                    return Pair(false, null)
                }
                if (detector?.id == Detector.DEFAULT_ID) {
                    return Pair(false, null)
                }
                if (defectSize == DEFAULT_DEFECT_SIZE) {
                    return Pair(false, null)
                }
                Pair(true, null)
            }
            else -> Pair(false, "Не выбран момент браковки оси")
        }
    }

    fun selectControlZone(id: Int) {
        for (controlZone in controlZones) {
            controlZone.zoneControlType = 0
        }
        controlZones.find { z -> z.id == id }?.zoneControlType = controlType
    }

    fun selectControlZonesIfEditMode() {
        for (controlZone in controlZones) {
            if (selectedControlZonesId.contains(controlZone.fieldName)) {
                controlZone.checked = true
            }
        }
    }
}



























