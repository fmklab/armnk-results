package com.fmklab.armnk_results.modules.common.services

import com.fmklab.armnk_results.modules.common.models.ControlResult
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.common.models.Factory
import com.fmklab.armnk_results.modules.common.models.RepeatDetail
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*

interface ArmApiService {

    companion object {
        private const val ROUTE = "arm"
    }

    @GET("$ROUTE/factories")
    fun getFactories(
        @Query("partId") partId: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Factory>>

    @GET("$ROUTE/results")
    fun getResults(
        @Query("partId") partId: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<ControlResult>>

    @GET("$ROUTE/methods")
    fun getMethods(
        @Query("partId") partId: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<String>>

    @GET("$ROUTE/detectors")
    fun getDetectors(
        @Query("companyId") companyId: Int,
        @Query("method") method: String,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Detector>>

    @POST("$ROUTE/repeat")
    fun setRepeatDetail(
        @Body repeatDetail: RepeatDetail,
        @Header("Authorization") token: String
    ): Observable<Int>
}


































