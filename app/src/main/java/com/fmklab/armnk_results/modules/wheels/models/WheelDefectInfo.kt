package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelDefectInfo (
    @SerializedName("defectMoment")
    @Expose
    var defectMoment: Int = DEFECT_MOMENT_NONE,
    @SerializedName("info")
    @Expose
    var info: String = ""
) {

    companion object {
        const val DEFECT_MOMENT_NONE = 0
        const val DEFECT_MOMENT_DEFECT = 1
        const val DEFECT_MOMENT_VISUAL = 2
        const val DEFECT_MOMENT_NDT = 3

        const val DEFECT_TYPE_NONE = -1
    }

    fun getDefectInfo(): String {
        var result = "обнаружен: "
        when (defectMoment) {
            DEFECT_MOMENT_DEFECT -> result += "\"при дефектации\""
            DEFECT_MOMENT_VISUAL -> result += "\"при ВО при НК\""
        }

        return result
    }
}