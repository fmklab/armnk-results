package com.fmklab.armnk_results.modules.common.viewmodels

import androidx.lifecycle.ViewModel
import com.fmklab.armnk_results.modules.common.ApiErrorHandler
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.extensions.apiQuery
import com.fmklab.armnk_results.modules.common.extensions.catchConnectException
import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers

abstract class DetailItemsViewModel<T>: ViewModel() where T: AbstractDetail {

    private var lastPage = 0

    protected val compositeDisposable = CompositeDisposable()

    private val loadDetails = PublishRelay.create<Int>()

    private val loadingDetails = BehaviorRelay.createDefault(false)
    val loadingDetailsStatus: Observable<Boolean> = loadingDetails.hide()

    private val errorDetails: BehaviorRelay<Error> = BehaviorRelay.create()
    val errorDetailsStatus: Observable<Error> = errorDetails.hide()

    private val noInternetConnection: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(false)
    val noInternetConnectionStatus: Observable<Boolean> = noInternetConnection.hide()

    private val details: BehaviorRelay<ArrayList<T>> = BehaviorRelay.create()
    val detailsStatus: Observable<ArrayList<T>> = details.hide()

    private val detailAdded: BehaviorRelay<T> = BehaviorRelay.create()
    val detailAddedStatus: Observable<T> = detailAdded.hide()

    private val detailUpdated: BehaviorRelay<Pair<T, Int>> = BehaviorRelay.create()
    val detailUpdatedStatus: Observable<Pair<T, Int>> = detailUpdated.hide()

    protected val toast: PublishRelay<String> = PublishRelay.create<String>()
    val toastState: Observable<String> = toast.hide()

    private val searchTextChanged: PublishRelay<String> = PublishRelay.create<String>()
    val searchTextChangeConsumer: Consumer<String> = searchTextChanged

    private val searchLoading: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(false)
    val searchLoadingState: Observable<Boolean> = searchLoading.hide()

    init {
        loadDetails
            .filter { !isDetailLoading() }
            .doOnNext { showDetailsLoading() }
            .flatMap { page ->
                getLoadDetailsObservable(page)
                    .subscribeOn(Schedulers.io())
                    .map { data -> DataResponse.success(data) }
                    .retryWhen { observable ->
                        observable.catchConnectException().map { isConnectException ->
                            noInternetConnection.accept(isConnectException)
                        }
                    }
                    .onErrorResumeNext { t ->
                        Observable.just(DataResponse.error(ApiErrorHandler.getError(t)))
                    }
                    .doOnNext { noInternetConnection.accept(false) }
            }
            .subscribeOn(Schedulers.io())
            .doOnEach { hideDetailsLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.SUCCESS) {
                    details.accept(it.data)
                } else if (it.status == DataResponse.Status.ERROR) {
                    errorDetails.accept(it.error)
                }
            }
            .addTo(compositeDisposable)

        searchTextChanged
            .filter { !isSearchLoading() }
            .doOnNext { showSearchLoading() }
            .flatMap { searchString ->
                getSearchDetailObservable(searchString).apiQuery()
                    .subscribeOn(Schedulers.io())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnEach { hideSearchLoading() }
            .subscribe {
                if (it.status == DataResponse.Status.ERROR) {
                    toast.accept("Ошибка: ${it.error?.message}")
                } else if (it.status == DataResponse.Status.SUCCESS) {
                    details.accept(it.data)
                }
            }
            .addTo(compositeDisposable)
    }

    abstract fun getLoadDetailsObservable(page: Int): Observable<ArrayList<T>>

    abstract fun getSearchDetailObservable(searchString: String): Observable<ArrayList<T>>

    private fun showDetailsLoading() {
        loadingDetails.accept(true)
    }

    private fun hideDetailsLoading() {
        loadingDetails.accept(false)
    }

    private fun isDetailLoading(): Boolean = loadingDetails.value

    protected fun showDetailsError(error: Error) {
        errorDetails.accept(error)
    }

    private fun isSearchLoading() = searchLoading.value

    private fun showSearchLoading() {
        searchLoading.accept(true)
    }

    private fun hideSearchLoading() {
        searchLoading.accept(false)
    }

    fun loadDetails(page: Int) {
        lastPage = page
        loadDetails.accept(page)
    }

    fun getLastPage(): Int {
        return lastPage
    }

    fun refreshDetails() {
        loadDetails.accept(lastPage)
    }

    override fun onCleared() {
        super.onCleared()

        compositeDisposable.clear()
    }
}




























