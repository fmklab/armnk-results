package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Factory (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("code")
    @Expose
    val code: String
) {

    companion object {
        const val DEFAULT_ID = 0
        const val DEFAULT_CODE = ""
    }

    constructor() : this(DEFAULT_ID, DEFAULT_CODE)

    override fun toString(): String {
        return code
    }
}