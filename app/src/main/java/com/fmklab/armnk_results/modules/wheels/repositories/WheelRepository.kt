package com.fmklab.armnk_results.modules.wheels.repositories

import android.content.Context
import android.content.SharedPreferences
import com.fmklab.armnk_results.modules.wheels.models.FullWheel
import com.fmklab.armnk_results.modules.wheels.models.Wheel
import com.fmklab.armnk_results.modules.wheels.models.WheelDefectInfo
import com.google.gson.Gson

class WheelRepository(context: Context) {

    private val preferences: SharedPreferences = context.getSharedPreferences("Wheel", Context.MODE_PRIVATE)

    companion object {
        private const val ULTRASONIC_CONTROL_USER = "UltrasonicControlUser"
        private const val MAGNETIC_CONTROL_USER = "MagneticControlUser"
        private const val EDITING_WHEEL = "EditingWheel"
    }

    fun setUltrasonicControlUser(user: String) {
        val editor = preferences.edit()
        editor.putString(ULTRASONIC_CONTROL_USER, user)
        editor.apply()
    }

    fun setMagneticControlUser(user: String) {
        val editor = preferences.edit()
        editor.putString(MAGNETIC_CONTROL_USER, user)
        editor.apply()
    }

    fun getUltrasonicControlUser(): String? {
        return preferences.getString(ULTRASONIC_CONTROL_USER, null)
    }

    fun getMagneticControlUser(): String? {
        return preferences.getString(MAGNETIC_CONTROL_USER, null)
    }

    fun saveEditedWheel(wheel: FullWheel) {
        val json = Gson().toJson(wheel)
        val editor = preferences.edit()
        editor.putString(EDITING_WHEEL, json)
        editor.apply()
    }

    fun getEditedWheel(): Wheel? {
        val json = preferences.getString(EDITING_WHEEL, null) ?: return null
        if (json == "null") return null
        val fullWheel = Gson().fromJson(json, FullWheel::class.java)
        var defectInfo = ""
        when (fullWheel.defectInfo.defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_DEFECT -> defectInfo = fullWheel.defectMomentInfo.getDefectInfo()
            WheelDefectInfo.DEFECT_MOMENT_VISUAL -> defectInfo = fullWheel.defectVisualInfo.getDefectInfo()
            WheelDefectInfo.DEFECT_MOMENT_NDT -> defectInfo = fullWheel.defectNdtInfo.getDefectInfo()
        }
        val editor = preferences.edit()
        editor.remove(EDITING_WHEEL)
        editor.apply()
        return Wheel(
            fullWheel.id,
            fullWheel.date,
            fullWheel.factory,
            fullWheel.factoryYear,
            fullWheel.type,
            fullWheel.wheelNumber,
            fullWheel.axle,
            fullWheel.result!!,
            fullWheel.spec,
            fullWheel.wheelControlInfo.info,
            fullWheel.defectInfo.getDefectInfo() + defectInfo,
            fullWheel.createDate,
            fullWheel.isMobileApp
        )
    }
}





































