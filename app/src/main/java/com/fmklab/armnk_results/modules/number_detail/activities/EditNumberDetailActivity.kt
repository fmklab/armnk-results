package com.fmklab.armnk_results.modules.number_detail.activities

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.controls.SavingDialog
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.SaveRepeatDetailError
import com.fmklab.armnk_results.modules.number_detail.adapters.NumberEditPagerAdapter
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.view.clicks
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_number_detail.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import java.util.concurrent.TimeUnit

class EditNumberDetailActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()
    private val viewPagerAdapter: NumberEditPagerAdapter by lifecycleScope.inject {
        parametersOf(supportFragmentManager)
    }
    private val viewModel: EditNumberDetailViewModel by lifecycleScope.viewModel(this)
    private var title: String? = null
    private var defectInfoPage: Int =
        DEFECT_INFO_PAGE
    private var firstLoad: Boolean = true
    private val savingDialog = SavingDialog(this)

    companion object {
        const val NUMBER_DETAIL_ID = "NumberDetailId"

        private const val BASE_INFO_PAGE = 0
        private const val DEFECT_INFO_PAGE = 1
        private const val REPAIR_INFO_PAGE = 2
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_number_detail)

        setupUI()
        setupListeners()
        setupBindings()

        if (savedInstanceState != null) {
            viewModel.restoreNumberDetail()
        } else {
            val detailId = intent.extras?.getInt(NUMBER_DETAIL_ID)
            val partId = intent.extras?.getInt(NumberDetailActivity.PART_ID_KEY)
            if (detailId != null) {
                viewModel.loadNumberDetailById(detailId, partId)
            }
        }
    }

    private fun setupUI() {
        setSupportActionBar(editDetailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        editDetailViewPager.adapter = viewPagerAdapter
        title = intent.extras?.getString(NumberDetailActivity.PART_NAME_KEY, "")
        supportActionBar?.title = title
    }

    private fun setupBindings() {
        viewModel.noInternetConnectionErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showNoInternetConnection(it)
            }
            .addTo(compositeDisposable)

        viewModel.numberDetailLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showDetailLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.numberDetailErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Snackbar.make(
                    viewConstraintLayout,
                    it.message as CharSequence,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.refresh) {
                        val detailId = intent.extras?.getInt(NUMBER_DETAIL_ID)
                        if (detailId != null) {
                            viewModel.loadNumberDetailById(detailId)
                        }
                    }
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.controlResult
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it.name) {
                    "годен" -> {
                        nextPageFab.hide()
                        defectInfoChip.isEnabled = false
                    }
                    "брак" -> {
                        nextPageFab.show()
                        defectInfoChip.isEnabled = true
                        defectInfoPage =
                            DEFECT_INFO_PAGE
                        if (!firstLoad) {
                            editDetailViewPager.setCurrentItem(defectInfoPage, true)
                        }
                    }
                    "ремонт" -> {
                        nextPageFab.show()
                        defectInfoChip.isEnabled = true
                        defectInfoPage =
                            REPAIR_INFO_PAGE
                        if (!firstLoad) {
                            editDetailViewPager.setCurrentItem(defectInfoPage, true)
                        }
                    }
                    else -> nextPageFab.hide()
                }
                firstLoad = false
            }
            .addTo(compositeDisposable)

        viewModel.baseInfoFieldErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                editDetailViewPager.setCurrentItem(BASE_INFO_PAGE, true)
            }
            .addTo(compositeDisposable)

        viewModel.repairFieldErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                editDetailViewPager.setCurrentItem(REPAIR_INFO_PAGE, true)
            }
            .addTo(compositeDisposable)

        viewModel.defectInfoFieldErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                editDetailViewPager.setCurrentItem(DEFECT_INFO_PAGE, true)
            }
            .addTo(compositeDisposable)

        viewModel.acceptDetailSavingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                MaterialAlertDialogBuilder(this)
                    .setCancelable(false)
                    .setTitle(getString(R.string.save))
                    .setMessage(it)
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        viewModel.acceptSaveClickedConsumer.accept(Unit)
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.saveDetailLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    savingDialog.startSavingDialog()
                } else {
                    savingDialog.dismissDialog()
                }
            }

        viewModel.saveDetailErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    is SaveRepeatDetailError -> MaterialAlertDialogBuilder(this)
                        .setTitle(getString(R.string.attention))
                        .setMessage(it.message)
                        .setPositiveButton("Продолжить") { _, _ ->
                            viewModel.saveDetailAnywayClickConsumer.accept(it.date)
                        }
                        .setNegativeButton("Отмена", null)
                        .show()
                    else -> Toast.makeText(
                        this,
                        "${getString(R.string.saving_error)}. Ошибка: ${it.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            .addTo(compositeDisposable)

        viewModel.saveDetailResultState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                RxBus.publish(RxEvent.EventNumberDetailEditFinish(it))
                finish()
            }
            .addTo(compositeDisposable)

        nextPageFab
            .clicks()
            .observeOn(AndroidSchedulers.mainThread())
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe {
                editDetailViewPager.setCurrentItem(defectInfoPage, true)
            }
            .addTo(compositeDisposable)

        defectInfoChip
            .clicks()
            .observeOn(AndroidSchedulers.mainThread())
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe {
                if (!defectInfoChip.isChecked) {
                    defectInfoChip.isChecked = true
                }
                editDetailViewPager.setCurrentItem(defectInfoPage, true)
            }
            .addTo(compositeDisposable)

        saveDetailButton
            .clicks()
            .subscribe(viewModel.saveClickedConsumer)
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        editDetailViewPager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                if (state != 0) {
                    nextPageFab.hide()
                    prevPageFab.hide()
                }
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (positionOffset == 0f) {
                    when (position) {
                        BASE_INFO_PAGE -> {
                            nextPageFab.show()
                        }
                        REPAIR_INFO_PAGE -> {
                            prevPageFab.show()
                            viewModel.loadRepairDataClickConsumer.accept(Unit)
                        }
                        DEFECT_INFO_PAGE -> {
                            prevPageFab.show()
                            viewModel.loadDefectData.accept(Unit)
                        }
                    }
                }
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    BASE_INFO_PAGE -> {
                        baseInputsChip.isChecked = true
                    }
                    REPAIR_INFO_PAGE, DEFECT_INFO_PAGE -> {
                        defectInfoChip.isChecked = true
                    }
                }
            }
        })

        prevPageFab.setOnClickListener {
            editDetailViewPager.setCurrentItem(BASE_INFO_PAGE, true)
        }

        baseInputsChip.setOnClickListener {
            if (!baseInputsChip.isChecked) {
                baseInputsChip.isChecked = true
            }
            editDetailViewPager.setCurrentItem(BASE_INFO_PAGE, true)
        }

        defectInfoChip.setOnClickListener {
            if (!defectInfoChip.isChecked) {
                defectInfoChip.isChecked = true
            }
            editDetailViewPager.setCurrentItem(defectInfoPage, true)
        }
    }

    private fun showNoInternetConnection(show: Boolean) {
        if (show) {
            supportActionBar?.title = getString(R.string.wait_for_connection)
        } else {
            supportActionBar?.title = title
        }
    }

    private fun showDetailLoading(show: Boolean) {
//        if (show) {
//            loadingProgressBar.visibility = View.VISIBLE
//            loadingProgressBar.isIndeterminate = true
//        } else {
//            loadingProgressBar.visibility = View.GONE
//            loadingProgressBar.isIndeterminate = false
//        }
    }

    private fun showExitWithoutSavingDialog() {
        MaterialAlertDialogBuilder(this)
            .setCancelable(false)
            .setTitle(R.string.exit)
            .setMessage(R.string.exit_without_saving_dialog)
            .setPositiveButton(R.string.yes) { _, _ ->
                finish()
            }
            .setNegativeButton(R.string.no, null)
            .show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        viewModel.dumpNumberDetail()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                showExitWithoutSavingDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}







































