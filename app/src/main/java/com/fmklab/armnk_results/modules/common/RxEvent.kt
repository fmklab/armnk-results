package com.fmklab.armnk_results.modules.common

import com.fmklab.armnk_results.modules.axle.models.AxleModel
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailListItem

class RxEvent {

    data class EventWheelAxleSelected(val axleModel: AxleModel)

    data class EventBaseInfoCheckFieldsResponse(val result: Boolean)

    data class EventControlInfoCheckFieldsResponse(val result: Boolean)

    data class EventDefectBeforeNdtChanged(val value: Boolean)

    data class EventDefectInfoCheckFieldsResponse(val value: Boolean)

    data class EventDefectMomentCheckFieldsResponse(val value: Boolean)


    data class EventNumberDetailEditFinish(val detail: NumberDetailListItem)

    class EventBaseInfoRequestCheckFields

    class EventControlInfoRequestCheckFields

    class EventDefectInfoRequestCheckFields

    class EventDefectMomentRequestCheckFields

    class EventRepairInfoSet

    class EventDefectMomentInfoLoadingOrError

    class EventDefectMomentSuccessLoading

    class EventControlInfoLoadingOrError

    class EventControlInfoSuccessLoading

    class EventDefectInfoLoadingOrError

    class EventDefectInfoSuccessLoading

    class EventWheelCheckParametersSuccess
}


































