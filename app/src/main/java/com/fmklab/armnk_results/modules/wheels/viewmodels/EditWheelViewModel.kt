package com.fmklab.armnk_results.modules.wheels.viewmodels

import androidx.lifecycle.ViewModel
import com.fmklab.armnk_results.BuildConfig
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.modules.auth.models.User
import com.fmklab.armnk_results.modules.axle.models.*
import com.fmklab.armnk_results.modules.axle.services.AxleApiService
import com.fmklab.armnk_results.modules.common.ApiErrorHandler
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.*
import com.fmklab.armnk_results.modules.common.services.ArmApiService
import com.fmklab.armnk_results.modules.common.services.AxleAndWheelApiService
import com.fmklab.armnk_results.modules.common.services.UserApiService
import com.fmklab.armnk_results.modules.wheels.models.*
import com.fmklab.armnk_results.modules.wheels.services.WheelApiService
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.security.auth.callback.Callback
import kotlin.collections.ArrayList

class EditWheelViewModel(
    private val currentUser: CurrentUser,
    private val axleAndWheelApiService: AxleAndWheelApiService,
    private val wheelApiService: WheelApiService,
    private val axleApiService: AxleApiService,
    private val userApiService: UserApiService,
    private val armApiService: ArmApiService
) : ViewModel() {

    var viewModelState = EditViewModelState(null)

    val fullWheel: BehaviorSubject<DataResponse<FullWheel>> = BehaviorSubject.create()
    private var lastLoadedId: Int = 0
    val baseInfo: BehaviorSubject<DataResponse<AxleAndWheelBaseInfo>> = BehaviorSubject.create()
    val foundAxle: BehaviorSubject<DataResponse<List<AxleModel>>> = BehaviorSubject.create()
    val defectBeforeControlTypesInfo: BehaviorSubject<DataResponse<ArrayList<DefectType>>> =
        BehaviorSubject.create()
    val wheelDefectMomentControlZonesInfo: BehaviorSubject<DataResponse<ArrayList<WheelControlZone>>> =
        BehaviorSubject.create()
    val wheelDefectsInfo: BehaviorSubject<DataResponse<ArrayList<WheelDefect>>> =
        BehaviorSubject.create()
    val wheelControlInfoControlZonesInfo: BehaviorSubject<DataResponse<ArrayList<WheelControlZone>>> =
        BehaviorSubject.create()
    val specialistsInfo: BehaviorSubject<DataResponse<ArrayList<User>>> = BehaviorSubject.create()
    val wheelVisualDefectInfoControlZonesInfo: BehaviorSubject<DataResponse<ArrayList<WheelControlZone>>> =
        BehaviorSubject.create()
    val visualDefectInfo: BehaviorSubject<DataResponse<ArrayList<WheelDefect>>> =
        BehaviorSubject.create()
    val wheelNdtDefectInfoControlZoneInfo: BehaviorSubject<DataResponse<ArrayList<WheelControlZone>>> =
        BehaviorSubject.create()
    val ndtDefectInfo: BehaviorSubject<DataResponse<ArrayList<WheelDefect>>> =
        BehaviorSubject.create()
    val detectors: BehaviorSubject<DataResponse<ArrayList<Detector>>> = BehaviorSubject.create()
    val baseInfoError: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val defectMomentInfoError: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val controlInfoError: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val defectInfoError: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val wheelSaveResult: BehaviorSubject<DataResponse<Int>> = BehaviorSubject.create()

    fun getFullWheel(): FullWheel? {
        return fullWheel.value.data
    }

    fun setWheelFactory(factory: Factory) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.factory = factory
        }
    }

    fun setWheelFactoryYear(year: Year) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.factoryYear = year
        }
    }

    fun setWheelType(type: AxleAndWheelType) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.type = type
        }
    }

    fun setWheelSpec() {
        val wheel = getFullWheel()
        val user = currentUser.getUser()
        if (wheel != null && user != null) {
            wheel.spec = user.toString()
        }
    }

    fun setWheelControlDate(date: Date) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.date = date
        }
    }

    fun setNewDetail(isNewDetail: Boolean) {
        val wheel = getFullWheel()
        if (wheel != null) {
            if (isNewDetail) {
                wheel.selectedControlZones?.clear()
            } else {
                if (wheel.wheelControlInfo.controlZones != null) {
                    for (zone in wheel.wheelControlInfo.controlZones!!) {
                        setSelectedControlZone(zone.fieldName)
                    }
                }
            }
            wheel.isNewDetail = isNewDetail
        }
    }

    fun setWheelNumber(wheelNumber: String) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.wheelNumber = wheelNumber
        }
    }

    fun setIsNewForm(isNewForm: Boolean) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.isNewForm = isNewForm
        }
    }

    fun isNewForm(): Boolean? {
        return getFullWheel()?.isNewForm
    }

    fun setWheelAxle(axleModel: AxleModel) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.axle = axleModel
            RxBus.publish(RxEvent.EventWheelAxleSelected(axleModel))
        }
    }

    fun getWheelAxle(): AxleModel? {
        return getFullWheel()?.axle
    }

    fun setWheelDefectBeforeNdt(isDefectBeforeNdt: Boolean) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.isDefectBeforeNdt = isDefectBeforeNdt
            if (!isDefectBeforeNdt) {
                setControlResult(ControlResult.WHEEL_GOOD)
                setDefectMoment(WheelDefectInfo.DEFECT_MOMENT_NONE)
            } else {
                setControlResult(ControlResult.WHEEL_BAD)
                setDefectMoment(WheelDefectInfo.DEFECT_MOMENT_DEFECT)
            }
            RxBus.publish(RxEvent.EventDefectBeforeNdtChanged(isDefectBeforeNdt))
        }
    }

    fun setWheelDefectBeforeNdtType(defectType: DefectType) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.defectMomentInfo.defectType = defectType
        }
    }

    fun getWheelDefectBeforeNdtType(): DefectType? {
        val wheel = getFullWheel()
        return wheel?.defectMomentInfo?.defectType
    }

    fun getWheelDefectMomentControlZone(): WheelControlZone? {
        val wheel = getFullWheel()
        return wheel?.defectMomentInfo?.defectZone
    }

    fun setWheelDefectMomentControlZone(controlZone: WheelControlZone) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.defectMomentInfo.defectZone = controlZone
        }
    }

    fun setRepairInfo(repairType: Int, wheelThickness: Int, ringsState: Int, isTurning: Boolean, stealT: Boolean) {
        val wheel = getFullWheel()
        wheel?.wheelControlInfo?.repairType = repairType
        wheel?.wheelControlInfo?.wheelThickness = wheelThickness
        wheel?.wheelControlInfo?.ringsState = ringsState
        wheel?.wheelControlInfo?.isRepairInfoSet = true
        if (isTurning) {
            wheel?.wheelControlInfo?.turning = WheelControlInfo.TURNING_YES
        } else {
            wheel?.wheelControlInfo?.turning = WheelControlInfo.TURNING_NO
        }
        if (stealT) {
            wheel?.wheelControlInfo?.stealT = WheelControlInfo.STEAL_T_YES
        } else {
            wheel?.wheelControlInfo?.stealT = WheelControlInfo.STEAL_T_NO
        }
        RxBus.publish(RxEvent.EventRepairInfoSet())
    }

    fun isRepairInfoSet(): Boolean? {
        val wheel = getFullWheel()
        return wheel?.wheelControlInfo?.isRepairInfoSet
    }

    fun getWheelDefectMomentDefect(): WheelDefect? {
        val wheel = getFullWheel()
        return wheel?.defectMomentInfo?.wheelDefect
    }

    fun setWheelDefectMomentDefect(wheelDefect: WheelDefect) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.defectMomentInfo.wheelDefect = wheelDefect
        }
    }

    fun setWheelDefectMomentDescription(description: String) {
        val wheel = getFullWheel()
        if (wheel != null) {
            wheel.defectMomentInfo.description = description
        }
    }

    fun setControlZones(controlZones: ArrayList<WheelControlZone>) {
        val wheel = getFullWheel()
        wheel?.wheelControlInfo?.controlZones = controlZones
    }

    fun getControlZones(): ArrayList<WheelControlZone>? {
        val wheel = getFullWheel()
        return wheel?.wheelControlInfo?.controlZones
    }

    fun getSelectedControlZones(): ArrayList<Int>? {
        val wheel = getFullWheel()
        return wheel?.selectedControlZones
    }

    fun setSelectedControlZone(fieldName: Int) {
        val wheel = getFullWheel()
        if (wheel?.selectedControlZones.isNullOrEmpty()) {
            wheel?.selectedControlZones = ArrayList()
        }
        val removed = wheel?.selectedControlZones?.remove(fieldName)
        if (removed == true) {
            //wheel.wheelControlInfo.removeZone(fieldName)
        }
        wheel?.selectedControlZones?.add(fieldName)
        //wheel?.wheelControlInfo?.addZone(fieldName)
    }

    fun deselectControlZone(fieldName: Int) {
        val wheel = getFullWheel()
        wheel?.selectedControlZones?.remove(fieldName)
        //wheel?.wheelControlInfo?.removeZone(fieldName)
    }

    fun setUltrasonicControlUser(user: String) {
        val wheel = getFullWheel()
        wheel?.wheelControlInfo?.ultrasonicSpec = user
    }

    fun setMagneticControlUser(user: String) {
        val wheel = getFullWheel()
        wheel?.wheelControlInfo?.magneticSpec = user
    }

    fun getControlInfo(): String? {
        val wheel = getFullWheel()
        val selectedZones = wheel?.selectedControlZones
        return wheel?.wheelControlInfo?.getControlInfo(wheel.defectInfo.defectMoment, selectedZones)
    }

    fun removeControlInfoZones() {
        val wheel = getFullWheel()
        wheel?.wheelControlInfo?.controlZones?.clear()
        wheel?.selectedControlZones = null
    }

    fun resetControlInfoSubjects() {
        wheelControlInfoControlZonesInfo.onNext(DataResponse.empty())
        specialistsInfo.onNext(DataResponse.empty())
    }

    fun resetDefectMomentSubjects() {
        defectBeforeControlTypesInfo.onNext(DataResponse.empty())
        wheelDefectMomentControlZonesInfo.onNext(DataResponse.empty())
        wheelDefectsInfo.onNext(DataResponse.empty())
    }

    fun getControlResult(): ControlResult? {
        val wheel = getFullWheel()
        return wheel?.result
    }

    fun setControlResult(resultId: Int) {
        val wheel = getFullWheel()
        when (resultId) {
            ControlResult.WHEEL_GOOD -> wheel?.result = ControlResult(resultId, "годен")
            ControlResult.WHEEL_BAD -> wheel?.result = ControlResult(resultId, "брак")
        }
    }

    fun getUltrasonicCount(): Int {
        val wheel = getFullWheel()
        return wheel?.wheelControlInfo?.ultrasonicCount ?: 0
    }

    fun getMagneticCount(): Int {
        val wheel = getFullWheel()
        return wheel?.wheelControlInfo?.magneticCount ?: 0
    }

    fun setDefectMoment(defectMoment: Int) {
        val wheel = getFullWheel()
        wheel?.defectInfo?.defectMoment = defectMoment
    }

    fun setVisualControlZone(controlZone: WheelControlZone) {
        val wheel = getFullWheel()
        wheel?.defectVisualInfo?.defectZone = controlZone
    }

    fun getVisualControlZone(): WheelControlZone? {
        val wheel = getFullWheel()
        return wheel?.defectVisualInfo?.defectZone
    }

    fun setVisualDefectCode(wheelDefect: WheelDefect) {
        val wheel = getFullWheel()
        wheel?.defectVisualInfo?.wheelDefect = wheelDefect
    }

    fun setVisualDefectDescription(description: String) {
        val wheel = getFullWheel()
        wheel?.defectVisualInfo?.description = description
    }

    fun getVisualDefectCode(): WheelDefect? {
        val wheel = getFullWheel()
        return wheel?.defectVisualInfo?.wheelDefect
    }

    fun setSizeType(sizeType: Int) {
        val wheel = getFullWheel()
        wheel?.defectNdtInfo?.sizeType = sizeType
    }

    fun setNdtDefectZone(controlZone: WheelControlZone) {
        val wheel = getFullWheel()
        wheel?.defectNdtInfo?.defectZone = controlZone
    }

    fun getNtdControlZone(): WheelControlZone? {
        val wheel = getFullWheel()
        return wheel?.defectNdtInfo?.defectZone
    }

    fun setNdtDefectCode(defect: WheelDefect) {
        val wheel = getFullWheel()
        wheel?.defectNdtInfo?.wheelDefect = defect
    }

    fun getNdtDefectCode(): WheelDefect? {
        val wheel = getFullWheel()
        return wheel?.defectNdtInfo?.wheelDefect
    }

    fun setDetector(detector: Detector) {
        val wheel = getFullWheel()
        wheel?.defectNdtInfo?.detector = detector
    }

    fun getDetector(): Detector? {
        val wheel = getFullWheel()
        return wheel?.defectNdtInfo?.detector
    }

    fun setDefectSize(size: String) {
        val wheel = getFullWheel()
        wheel?.defectNdtInfo?.defectSize = size
    }

    fun setNdtDefectDescription(description: String) {
        val wheel = getFullWheel()
        wheel?.defectNdtInfo?.description = description
    }

    fun getDefectInfo(defectMoment: Int): String {
        val wheel = getFullWheel()
        var result = wheel?.defectInfo?.getDefectInfo() ?: ""
        when (defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_DEFECT -> {
                result += wheel?.defectMomentInfo?.getDefectInfo()
            }
            WheelDefectInfo.DEFECT_MOMENT_VISUAL -> {
                result += wheel?.defectVisualInfo?.getDefectInfo()
            }
            WheelDefectInfo.DEFECT_MOMENT_NDT -> {
                result += wheel?.defectNdtInfo?.getDefectInfo()
            }
        }
        wheel?.defectInfo?.info = result
        return result
    }

    fun getGoodWheelMessage(): String {
        val wheel = getFullWheel()
        return "Сохранить колесо как ГОДНОЕ?\n\nИнформация о контроле\n\n${getControlInfo()}"
    }

    fun getBadWheelMessage(): String {
        val wheel = getFullWheel()
        return "Сохранить колесо как БРАК\n\nИнформация о контроле\n\n${getControlInfo() ?: ""}\n\nОписание найденных дефектов\n\n${getDefectInfo(
            wheel?.defectInfo!!.defectMoment
        )}"
    }

    fun loadFullWheel(id: Int) {
        fullWheel.onNext(DataResponse.loading())
        lastLoadedId = id
        if (id == FullWheel.DEFAULT_ID) {
            fullWheel.onNext(DataResponse.success(FullWheel()))
            viewModelState = viewModelState.copy(isEditing = false)
            return
        }
        viewModelState = viewModelState.copy(isEditing = true)
        val token = currentUser.getAccessToken()
        if (token.isEmpty()) {
            fullWheel.onNext(DataResponse.error(FatalError()))
        } else {
            wheelApiService.getFullWheel(id, token)
                .enqueue(object : Callback, retrofit2.Callback<FullWheel> {
                    override fun onFailure(call: Call<FullWheel>, t: Throwable) {
                        ApiErrorHandler.dataResponseError(t, fullWheel)
                    }

                    override fun onResponse(call: Call<FullWheel>, response: Response<FullWheel>) {
                        if (response.code() == 200) {
                            fullWheel.onNext(DataResponse.success(response.body()))
                        } else {
                            fullWheel.onNext(DataResponse.error(WheelLoadError("Ошибка с кодом ${response.code()}")))
                        }
                    }
                })
        }
    }

    fun refreshFullAxle() {
        loadFullWheel(lastLoadedId)
    }

    fun loadBaseInfo() {
        baseInfo.onNext(DataResponse.loading())
        val companyId = currentUser.getUser().company.id
        val token = currentUser.getAccessToken()
        if (token.isEmpty()) {
            baseInfo.onNext(DataResponse.error(FatalError()))
        } else {
            axleAndWheelApiService.getBaseInfo(2, companyId, token)
                .enqueue(object : Callback, retrofit2.Callback<AxleAndWheelBaseInfo> {
                    override fun onFailure(call: Call<AxleAndWheelBaseInfo>, t: Throwable) {
                        ApiErrorHandler.dataResponseError(t, baseInfo)
                    }

                    override fun onResponse(
                        call: Call<AxleAndWheelBaseInfo>,
                        response: Response<AxleAndWheelBaseInfo>
                    ) {
                        if (response.code() == 200) {
                            baseInfo.onNext(DataResponse.success(response.body()))
                        } else {
                            baseInfo.onNext(
                                DataResponse.error(
                                    BaseInfoError(
                                        "Ошибка с кодом ${response.code()}"
                                    )
                                )
                            )
                        }
                    }
                })
        }
    }

    fun searchAxle(searchString: String) {
        foundAxle.onNext(DataResponse.loading())
        val companyId = currentUser.getUser().company.id
        val token = currentUser.getAccessToken()
        axleApiService.searchAxle(companyId, searchString, token)
            .enqueue(object : Callback, retrofit2.Callback<List<AxleModel>> {
                override fun onFailure(call: Call<List<AxleModel>>, t: Throwable) {
                    ApiErrorHandler.dataResponseError(t, foundAxle)
                }

                override fun onResponse(
                    call: Call<List<AxleModel>>,
                    response: Response<List<AxleModel>>
                ) {
                    if (response.code() == 200) {
                        foundAxle.onNext(DataResponse.success(response.body()))
                    } else {
                        foundAxle.onNext(
                            DataResponse.error(
                                SearchAxleError(
                                    "Ошибка с кодом ${response.code()}"
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadDefectBeforeControlTypes() {
        defectBeforeControlTypesInfo.onNext(DataResponse.loading())
        val token = currentUser.getAccessToken()
        wheelApiService.getDefectBeforeControlTypes(token)
            .enqueue(object : Callback, retrofit2.Callback<ArrayList<DefectType>> {
                override fun onFailure(
                    call: Call<ArrayList<DefectType>>,
                    t: Throwable
                ) {
                    ApiErrorHandler.dataResponseError(t, defectBeforeControlTypesInfo)
                }

                override fun onResponse(
                    call: Call<ArrayList<DefectType>>,
                    response: Response<ArrayList<DefectType>>
                ) {
                    if (response.code() == 200) {
                        defectBeforeControlTypesInfo.onNext(DataResponse.success(response.body()))
                    } else {
                        defectBeforeControlTypesInfo.onNext(
                            DataResponse.error(
                                DefectBeforeControlTypesInfoError(
                                    "Ошибка с кодом ${response.code()}"
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadControlZones(wheelControlZonesRequest: WheelControlZonesRequest, defectMoment: Int) {
        val subject = when (defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_DEFECT -> wheelDefectMomentControlZonesInfo
            WheelDefectInfo.DEFECT_MOMENT_VISUAL -> wheelVisualDefectInfoControlZonesInfo
            WheelDefectInfo.DEFECT_MOMENT_NDT -> wheelNdtDefectInfoControlZoneInfo
            else -> wheelControlInfoControlZonesInfo
        }
        subject.onNext(DataResponse.loading())
        val token = currentUser.getAccessToken()
        wheelApiService.getControlZones(wheelControlZonesRequest, token)
            .enqueue(object : Callback, retrofit2.Callback<ArrayList<WheelControlZone>> {
                override fun onFailure(call: Call<ArrayList<WheelControlZone>>, t: Throwable) {
                    ApiErrorHandler.dataResponseError(t, subject)
                }

                override fun onResponse(
                    call: Call<ArrayList<WheelControlZone>>,
                    response: Response<ArrayList<WheelControlZone>>
                ) {
                    if (response.code() == 200) {
                        subject.onNext(DataResponse.success(response.body()))
                    } else {
                        subject.onNext(
                            DataResponse.error(
                                ControlZonesError(
                                    "Ошибка с кодом ${response.code()}"
                                )
                            )
                        )
                    }
                }
            })
    }

    fun loadDefects(defectMoment: Int) {
        val subject = when (defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_DEFECT -> wheelDefectsInfo
            WheelDefectInfo.DEFECT_MOMENT_VISUAL -> visualDefectInfo
            WheelDefectInfo.DEFECT_MOMENT_NDT -> ndtDefectInfo
            else -> wheelDefectsInfo
        }
        subject.onNext(DataResponse.loading())
        val token = currentUser.getAccessToken()
        val defectTypeId = when (defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_DEFECT -> getWheelDefectBeforeNdtType()?.id
            else -> WheelDefectInfo.DEFECT_TYPE_NONE
        }
        val defectZoneId = when (defectMoment) {
            WheelDefectInfo.DEFECT_MOMENT_DEFECT -> getWheelDefectMomentControlZone()?.id
            WheelDefectInfo.DEFECT_MOMENT_VISUAL -> getVisualControlZone()?.id
            WheelDefectInfo.DEFECT_MOMENT_NDT -> getNtdControlZone()?.id
            else -> getVisualControlZone()?.id
        }
        if (defectTypeId != null && defectZoneId != null) {
            wheelApiService.getWheelDefects(defectTypeId, defectZoneId, defectMoment, token)
                .enqueue(object : Callback, retrofit2.Callback<ArrayList<WheelDefect>> {
                    override fun onFailure(call: Call<ArrayList<WheelDefect>>, t: Throwable) {
                        ApiErrorHandler.dataResponseError(t, subject)
                    }

                    override fun onResponse(
                        call: Call<ArrayList<WheelDefect>>,
                        response: Response<ArrayList<WheelDefect>>
                    ) {
                        if (response.code() == 200) {
                            subject.onNext(DataResponse.success(response.body()))
                        } else {
                            subject.onNext(
                                DataResponse.error(
                                    DefectsError(
                                        "Ошибка с кодом ${response.code()}"
                                    )
                                )
                            )
                        }
                    }
                })
        }
    }

    fun loadSpecialists() {
        specialistsInfo.onNext(DataResponse.loading())
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        userApiService.getUsers(companyId, token)
            .enqueue(object : Callback, retrofit2.Callback<ArrayList<User>> {
                override fun onFailure(call: Call<ArrayList<User>>, t: Throwable) {
                    ApiErrorHandler.dataResponseError(t, specialistsInfo)
                }

                override fun onResponse(
                    call: Call<ArrayList<User>>,
                    response: Response<ArrayList<User>>
                ) {
                    if (response.code() == 200) {
                        specialistsInfo.onNext(DataResponse.success(response.body()))
                    } else {
                        specialistsInfo.onNext(DataResponse.error(Error("Ошибка с кодом ${response.code()}")))
                    }
                }
            })
    }

    fun loadDetectors() {
        detectors.onNext(DataResponse.loading())
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        val vm = getNtdControlZone()?.vmInDb ?: WheelControlZone.VM_NONE
        wheelApiService.getDetectors(companyId, vm, token)
            .enqueue(object : Callback, retrofit2.Callback<ArrayList<Detector>> {
                override fun onFailure(call: Call<ArrayList<Detector>>, t: Throwable) {
                    ApiErrorHandler.dataResponseError(t, detectors)
                }

                override fun onResponse(
                    call: Call<ArrayList<Detector>>,
                    response: Response<ArrayList<Detector>>
                ) {
                    if (response.code() == 200) {
                        detectors.onNext(DataResponse.success(response.body()))
                    } else {
                        detectors.onNext(DataResponse.error(Error("Ошибка с кодом ${response.code()}")))
                    }
                }
            })
    }

    fun checkAllParameters() {
        val wheel = getFullWheel() ?: return
        if (wheel.isDefectBeforeNdt) {
            val checkBaseInfoResult = wheel.checkBaseInfo()
            if (!checkBaseInfoResult) {
                baseInfoError.onNext(true)
                return
            }
            val checkDefectMomentInfoResult = wheel.checkDefectMomentInfo()
            if (!checkDefectMomentInfoResult) {
                defectMomentInfoError.onNext(true)
                return
            }
        } else {
            val checkBaseInfoResult = wheel.checkBaseInfo()
            if (!checkBaseInfoResult) {
                baseInfoError.onNext(true)
                return
            }
            if (!wheel.checkControlInfo()) {
                controlInfoError.onNext(true)
                return
            }
            if (wheel.result?.id == ControlResult.WHEEL_BAD && !wheel.checkDefectInfo()) {
                defectInfoError.onNext(true)
                return
            }
        }
        RxBus.publish(RxEvent.EventWheelCheckParametersSuccess())
    }

    fun save() {
        val fullWheel = getFullWheel()
        val user = currentUser.getUser()
        val token = currentUser.getAccessToken()
        if (fullWheel == null) {
            wheelSaveResult.onNext(DataResponse.error(FatalError()))
        } else {
            wheelApiService.checkAddedWheel(
                CheckAddedWheelRequest(user.company.id, fullWheel),
                token
            )
                .enqueue(object : Callback, retrofit2.Callback<CheckAddedDetailResponse> {
                    override fun onFailure(call: Call<CheckAddedDetailResponse>, t: Throwable) {
                        ApiErrorHandler.dataResponseError(t, wheelSaveResult)
                    }

                    override fun onResponse(
                        call: Call<CheckAddedDetailResponse>,
                        response: Response<CheckAddedDetailResponse>
                    ) {
                        if (response.code() == 200) {
                            val result = response.body()!!
                            if (result.status || !result.status && viewModelState.isEditing == true) {
                                wheelApiService.setWheel(
                                    SetWheelModel(
                                        User(
                                            user.id.toInt(),
                                            user.name1,
                                            user.name2,
                                            user.name3,
                                            user.company.id,
                                            user.company.name
                                        ), fullWheel,
                                        BuildConfig.VERSION_CODE
                                    ), token
                                )
                                    .enqueue(object : Callback, retrofit2.Callback<Int> {
                                        override fun onFailure(call: Call<Int>, t: Throwable) {
                                            ApiErrorHandler.dataResponseError(t, wheelSaveResult)
                                        }

                                        override fun onResponse(
                                            call: Call<Int>,
                                            response: Response<Int>
                                        ) {
                                            if (response.code() == 200) {
                                                fullWheel.id = response.body()!!
                                                wheelSaveResult.onNext(DataResponse.success(response.body()))
                                            } else {
                                                wheelSaveResult.onNext(DataResponse.error(Error("Не удалось сохранить колесо, повторите попытку. Ошибка с кодом ${response.code()}")))
                                            }
                                        }
                                    })
                            } else {
                                wheelSaveResult.onNext(
                                    DataResponse.error(
                                        SaveRepeatDetailError(
                                            result.message,
                                            result.date
                                        )
                                    )
                                )
                            }
                        } else {
                            wheelSaveResult.onNext(DataResponse.error(Error("Не удалось сохранить колесо, повторите попытку. Ошибка с кодом ${response.code()}")))
                        }
                    }
                })
//            wheelApiService.setWheel(SetWheelModel(user, fullWheel), token)
//                .enqueue(object : Callback, retrofit2.Callback<Int> {
//                    override fun onFailure(call: Call<Int>, t: Throwable) {
//                        ApiErrorHandler.dataResponseError(t, wheelSaveResult)
//                    }
//
//                    override fun onResponse(call: Call<Int>, response: Response<Int>) {
//                        if (response.code() == 200) {
//                            fullWheel.id = response.body()!!
//                            wheelSaveResult.onNext(DataResponse.success(response.body()))
//                        } else {
//                            wheelSaveResult.onNext(DataResponse.error(Error("Не удалось сохранить колесо, повторите попытку. Ошибка с кодом ${response.code()}")))
//                        }
//                    }
//                })
        }
    }

    fun saveAnyway(date: Date) {
        val token = currentUser.getAccessToken()
        val user = currentUser.getUser()
        val fullWheel = getFullWheel()!!
        wheelApiService.setWheel(
            SetWheelModel(
                User(
                    user.id.toInt(),
                    user.name1,
                    user.name2,
                    user.name3,
                    user.company.id,
                    user.company.name
                ), fullWheel,
                BuildConfig.VERSION_CODE
            ), token
        )
            .enqueue(object : Callback, retrofit2.Callback<Int> {
                override fun onFailure(call: Call<Int>, t: Throwable) {
                    ApiErrorHandler.dataResponseError(t, wheelSaveResult)
                }

                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    if (response.code() == 200) {
                        fullWheel.id = response.body()!!
                        wheelSaveResult.onNext(DataResponse.success(response.body()))
                        armApiService.setRepeatDetail(
                            RepeatDetail(
                                0,
                                user.company.id,
                                user.id.toInt(),
                                2,
                                fullWheel.date,
                                date,
                                fullWheel.wheelNumber
                            ), token
                        )
                            .subscribeOn(Schedulers.io()).subscribe()
                    } else {
                        wheelSaveResult.onNext(DataResponse.error(Error("Не удалось сохранить колесо, повторите попытку. Ошибка с кодом ${response.code()}")))
                    }
                }
            })
    }
}



























