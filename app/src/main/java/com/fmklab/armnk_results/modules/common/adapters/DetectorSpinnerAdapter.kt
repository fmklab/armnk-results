package com.fmklab.armnk_results.modules.common.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fmklab.armnk_results.modules.common.models.Detector

class DetectorSpinnerAdapter(context: Context, resource: Int, private val objects: Array<out Detector>) :
    ArrayAdapter<Detector>(context, resource, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = super.getView(position, convertView, parent) as TextView
        var result = objects[position].name
        if (objects[position].number.isNotEmpty()) {
            result += " (зав. № ${objects[position].number})"
        }
        textView.text = result
        return textView
    }
}