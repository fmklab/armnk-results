package com.fmklab.armnk_results.modules.axle.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleDefectResult(
    @SerializedName("controlType")
    @Expose
    var controlType: Int = 0,
    @SerializedName("defectTypeId")
    @Expose
    var defectTypeId: Int = 0,
    @SerializedName("defectCodeId")
    @Expose
    var defectCodeId: Int = 0,
    @SerializedName("defectZoneId")
    @Expose
    var defectZoneId: Int = 0,
    @SerializedName("defectDescription")
    @Expose
    val defectDescription: String = "",
    @SerializedName("visualZoneId")
    @Expose
    var visualZoneId: Int = 0,
    @SerializedName("visualCodeId")
    @Expose
    var visualCodeId: Int = 0,
    @SerializedName("visualDescription")
    @Expose
    var visualDescription: String = "",

    @SerializedName("ntdZoneId")
    @Expose
    var ntdZoneId: Int = 0,
    @SerializedName("ntdCodeId")
    @Expose
    var ntdCodeId: Int = 0,
    @SerializedName("ntdDescription")
    @Expose
    var ntdDescription: String = ""
)


























