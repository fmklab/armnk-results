package com.fmklab.armnk_results.modules.wheels.models

import com.fmklab.armnk_results.modules.axle.models.AxleModel
import com.fmklab.armnk_results.modules.common.models.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class Wheel() : AbstractDetail() {
    @SerializedName("id")
    @Expose
    override var id: Int = 0

    @SerializedName("controlDate")
    @Expose
    var date: Date = Date()
        private set

    @SerializedName("factory")
    @Expose
    var factory: Factory = Factory()
        private set

    @SerializedName("factoryYear")
    @Expose
    var factoryYear: Year? = Year()
        private set

    @SerializedName("type")
    @Expose
    var type: AxleAndWheelType = AxleAndWheelType()
        private set

    @SerializedName("wheelNumber")
    @Expose
    var wheelNumber: String = ""
        private set

    @SerializedName("axle")
    @Expose
    var axle: AxleModel? = AxleModel()
        private set

    @SerializedName("result")
    @Expose
    var result: ControlResult = ControlResult()
        private set

    @SerializedName("spec")
    @Expose
    var spec: String = ""
        private set

    @SerializedName("controlInfoStr")
    @Expose
    var controlInfoStr: String = ""
        private set

    @SerializedName("defectInfoStr")
    @Expose
    var defectInfoStr: String = ""
        private set

    @SerializedName("createDate")
    @Expose
    var createDate: Date = Date()
        private set

    @SerializedName("isMobileApp")
    @Expose
    var isMobileApp: Boolean = true
        private set

    constructor(
        id: Int,
        date: Date,
        factory: Factory,
        factoryYear: Year,
        type: AxleAndWheelType,
        wheelNumber: String,
        axle: AxleModel,
        result: ControlResult,
        spec: String,
        controlInfoStr: String,
        defectInfoStr: String,
        createDate: Date,
        isMobileApp: Boolean
    ): this() {
        this.id = id
        this.date = date
        this.factory = factory
        this.factoryYear = factoryYear
        this.type = type
        this.wheelNumber = wheelNumber
        this.axle = axle
        this.result = result
        this.spec = spec
        this.controlInfoStr = controlInfoStr
        this.defectInfoStr = defectInfoStr
        this.createDate = createDate
        this.isMobileApp = isMobileApp
    }

    fun getResult(): Boolean {
        return result.name.toLowerCase(Locale.ROOT) == "годен"
    }
}






























