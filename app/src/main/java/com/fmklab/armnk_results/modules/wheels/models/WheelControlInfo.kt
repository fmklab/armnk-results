package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelControlInfo(
    @SerializedName("repairType")
    @Expose
    var repairType: Int = REPAIR_TYPE_NONE,
    @SerializedName("ultrasonicSpec")
    @Expose
    var ultrasonicSpec: String = "",
    @SerializedName("magneticSpec")
    @Expose
    var magneticSpec: String = "",
    @SerializedName("ultrasonicCount")
    @Expose
    var ultrasonicCount: Int = 0,
    @SerializedName("magneticCount")
    @Expose
    var magneticCount: Int = 0,
    @SerializedName("wheelThickness")
    @Expose
    var wheelThickness: Int = WHEEL_THICKNESS_LESS_40,
    @SerializedName("ringsState")
    @Expose
    var ringsState: Int = RING_STATE_NONE,
    @SerializedName("stealT")
    @Expose
    var stealT: Int = STEAL_T_NO,
    @SerializedName("turning")
    @Expose
    var turning: Int = TURNING_YES,
    @SerializedName("info")
    @Expose
    var info: String = ""
) {

    var controlZones: ArrayList<WheelControlZone>? = ArrayList()
    var isRepairInfoSet = false

    companion object {
        const val REPAIR_TYPE_NONE = 0
        const val REPAIR_TYPE_CURRENT = 1
        const val REPAIR_TYPE_MIDDLE = 2
        const val REPAIR_TYPE_CAPITAL = 3

        const val RING_STATE_NONE = 0
        const val RING_STATE_ON = 2
        const val RING_STATE_OFF = 1

        const val WHEEL_THICKNESS_NONE = 0
        const val WHEEL_THICKNESS_LESS_40 = 1
        const val WHEEL_THICKNESS_MORE_40 = 2
        const val WHEEL_THICKNESS_MORE_50 = 3

        const val STEAL_T_NO = 0
        const val STEAL_T_YES = 1

        const val TURNING_NO = 0
        const val TURNING_YES = 1
    }

//    fun removeZone(fieldName: Int) {
//        val zone = controlZones.find { z -> z.fieldName == fieldName }
//        if (zone?.vmInDb == WheelControlZone.VM_ULTRASONIC) {
//            if (ultrasonicCount > 0) {
//                ultrasonicCount--
//            }
//        }
//        if (zone?.vmInDb == WheelControlZone.VM_MAGNETIC) {
//            if (magneticCount > 0) {
//                magneticCount--
//            }
//        }
//    }
//
//    fun addZone(fieldName: Int) {
//        val zone = controlZones.find { z -> z.fieldName == fieldName }
//        if (zone?.vmInDb == WheelControlZone.VM_ULTRASONIC) {
//            ultrasonicCount++
//        }
//        if (zone?.vmInDb == WheelControlZone.VM_MAGNETIC) {
//            magneticCount++
//        }
//    }

    fun getControlInfo(defectMoment: Int, selectedZones: ArrayList<Int>? = null): String {
        if (defectMoment == WheelDefectInfo.DEFECT_MOMENT_DEFECT) {
            info = "колесо забраковано до НК"
            return info
        }
        val zones  = controlZones?.filter { z -> selectedZones?.contains(z.fieldName) == true } ?: return info
        var result = ""
        ultrasonicCount = 0
        magneticCount = 0
        when (repairType) {
            REPAIR_TYPE_CURRENT -> result += "Р[Т]"
            REPAIR_TYPE_MIDDLE -> result += "Р[С]"
            REPAIR_TYPE_CAPITAL -> result += "Р[К]"
        }
        when (ringsState) {
            RING_STATE_ON -> result += " К[Н]"
            RING_STATE_OFF -> result += " К[С]"
        }
        for (zone in zones) {
            when(zone.vmInDb) {
                WheelControlZone.VM_ULTRASONIC -> ultrasonicCount++
                WheelControlZone.VM_MAGNETIC -> magneticCount++
            }
        }
        if (ultrasonicCount > 0) {
            result += " УЗК[$ultrasonicCount] УЗК: $ultrasonicSpec"
        }
        if (magneticCount > 0) {
            result += " ВТК/МПК[$magneticCount] ВТК/МПК: $magneticSpec"
        }
        info = result
        return result
    }
}































