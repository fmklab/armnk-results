package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelDefectMomentInfo (
    @SerializedName("wheelDefect")
    @Expose
    var wheelDefect: WheelDefect = WheelDefect(),
    @SerializedName("description")
    @Expose
    var description: String = "",
    @SerializedName("defectType")
    @Expose
    var defectType: DefectType = DefectType(),
    @SerializedName("defectZone")
    @Expose
    var defectZone: WheelControlZone = WheelControlZone()
) {

    fun getDefectInfo(): String {
        var result = ", тип дефекта: \"${defectType.name}\", код дефекта: \"${wheelDefect.title}\", зона обнаружение: \"${defectZone.title}\""
        if (description.isNotBlank()) {
            result += ", опиание: \"$description\""
        }
        return result
    }
}