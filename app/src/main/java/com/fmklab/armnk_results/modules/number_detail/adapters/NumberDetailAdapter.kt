package com.fmklab.armnk_results.modules.number_detail.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailListItem
import kotlinx.android.synthetic.main.number_detail_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class NumberDetailAdapter: DetailItemAdapter<NumberDetailListItem>() {

    override fun inflateView(parent: ViewGroup, viewType: Int): View {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_ITEM -> inflater.inflate(R.layout.number_detail_item, parent, false)
            VIEW_LOADING -> inflater.inflate(R.layout.progress_item, parent, false)
            else -> inflater.inflate(R.layout.refresh_item, parent, false)
        }
    }

    override fun onBindViewHolder(
        holder: BaseDetailViewHolder<NumberDetailListItem>,
        position: Int
    ) {
        if (holder is ItemDetailViewHolder) {
            val detail = getItem(position)!!
            if (detail.editStatus) {
                Glide.with(holder.itemView)
                    .asDrawable()
                    .load(R.drawable.status_unlock)
                    .into(holder.itemView.statusImageView)
            } else {
                Glide.with(holder.itemView)
                    .asDrawable()
                    .load(R.drawable.status_lock)
                    .into(holder.itemView.statusImageView)
            }
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            holder.itemView.controlDateTextView.text = simpleDateFormat.format(detail.controlDate)
            holder.itemView.detailNumberValueTextView.text = detail.number
            holder.itemView.factoryNameValueTextView.text = detail.factory.code
            holder.itemView.factoryYearValueTextView.text = detail.factoryYear.toString()
            when (detail.controlResult.name) {
                "годен" -> {
                    holder.itemView.resultTextView.setBackgroundColor(Color.parseColor("#2b9432"))
                }
                "брак" -> {
                    holder.itemView.resultTextView.setBackgroundColor(Color.parseColor("#b03636"))
                }
                "ремонт" -> {
                    holder.itemView.resultTextView.setBackgroundColor(Color.parseColor("#B6AF0B"))
                }
            }
            holder.itemView.resultTextView.text = detail.controlResult.name
        }
    }
}





































