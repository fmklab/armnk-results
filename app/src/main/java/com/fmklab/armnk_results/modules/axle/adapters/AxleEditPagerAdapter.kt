package com.fmklab.armnk_results.modules.axle.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.fmklab.armnk_results.modules.axle.fragments.BaseAxleInputFragment
import com.fmklab.armnk_results.modules.axle.fragments.DefectAfterControlFragment
import com.fmklab.armnk_results.modules.axle.fragments.DefectBeforeAxleInputFragment
import com.fmklab.armnk_results.modules.axle.fragments.WrongDetailFragment

class AxleEditPagerAdapter(fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {

    private var fragments = arrayOf(
        BaseAxleInputFragment(),
        DefectBeforeAxleInputFragment(),
        DefectAfterControlFragment(),
        WrongDetailFragment()
    )

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun checkBaseInputFields(): Boolean {
        return (fragments[0] as BaseAxleInputFragment).checkBaseAxleInputFields()
    }

    fun checkDefectAfterControlInputFields(): Boolean {
        return (fragments[2] as DefectAfterControlFragment).checkDefectAfterControlInput()
    }

    fun checkWrongDetailInputFields(): Boolean {
        return (fragments[3] as WrongDetailFragment).checkFields()
    }

    fun checkDefectBeforeControlFields(): Boolean {
        return (fragments[1] as DefectBeforeAxleInputFragment).checkFields()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragments[position] = fragment
        return fragment
    }
}

































