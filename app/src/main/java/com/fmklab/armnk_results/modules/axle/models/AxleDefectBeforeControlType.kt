package com.fmklab.armnk_results.modules.axle.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleDefectBeforeControlType (
    @SerializedName("id")
    @Expose
    var id: Int,
    @SerializedName("name")
    @Expose
    var name: String
) {

    override fun toString(): String {
        return name
    }
}