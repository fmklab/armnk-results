package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AxleAndWheelBaseInfo (
    @SerializedName("factories")
    @Expose
    val factories: List<Factory>,
    @SerializedName("types")
    @Expose
    val types: List<AxleAndWheelType>,
    @SerializedName("years")
    @Expose
    val years: List<String>,
    @SerializedName("years2")
    @Expose
    val years2: List<Year>,
    @SerializedName("specialists")
    @Expose
    val specialists: List<String>,
    @SerializedName("isNewDetailEnabled")
    @Expose
    val isNewDetailEnabled: Boolean
)