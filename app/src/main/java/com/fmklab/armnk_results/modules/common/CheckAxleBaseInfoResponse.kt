package com.fmklab.armnk_results.modules.common

class CheckAxleBaseInfoResponse (val status: Status) {

    enum class Status {
        GOOD,
        FACTORY_CODE,
        FACTORY_YEAR,
        NAME,
        AXLE_NUMBER,
        REPAIR_TYPE,
        SPEC_NAME
    }

    companion object {
        fun good(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.GOOD)
        }
        fun factoryCode(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.FACTORY_CODE)
        }
        fun factoryYear(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.FACTORY_YEAR)
        }
        fun name(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.NAME)
        }
        fun axleNumber(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.AXLE_NUMBER)
        }
        fun repairType(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.REPAIR_TYPE)
        }
        fun specName(): CheckAxleBaseInfoResponse {
            return CheckAxleBaseInfoResponse(Status.SPEC_NAME)
        }
    }
}