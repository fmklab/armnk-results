package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.adapters.AxleDefectSpinnerAdapter
import com.fmklab.armnk_results.modules.common.adapters.DetectorSpinnerAdapter
import com.fmklab.armnk_results.modules.axle.models.*
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.common.models.NdtDefectError
import com.google.android.material.radiobutton.MaterialRadioButton
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_ntd_defect.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.lang.Error

class NtdDefectFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var ultrasonicRadioGroup: RadioGroup
    private lateinit var magneticRadioGroup: RadioGroup
    private val viewModel by sharedViewModel<EditAxleViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ntd_defect, container, false)
    }

    override fun onResume() {
        super.onResume()

        setupUI()
        setupData()
        setupBindings()
        setupControlZones()
    }

    override fun onPause() {
        super.onPause()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        defectCodeAutoCompleteTextView.keyListener = null
        detectorAutoCompleteTextView.keyListener = null
        ultrasonicControlLinearLayout.removeAllViews()
        magneticControlLinearLayout.removeAllViews()
        ultrasonicRadioGroup = RadioGroup(requireContext())
        magneticRadioGroup = RadioGroup(requireContext())
        ultrasonicControlLinearLayout.addView(ultrasonicRadioGroup)
        magneticControlLinearLayout.addView(magneticRadioGroup)
    }

    private fun setupData() {
        if (viewModel.editingAxle.controlZones.size == 0) {
            viewModel.loadDefectAfterInfo()
        }
    }

    private fun setupBindings() {
        viewModel.axleDefectNdtInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false, null)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        defectCodeAutoCompleteTextView.setText("", false)
                        detectorAutoCompleteTextView.setText("", false)
                        if (it.data != null) {
                            AxleDefectSpinnerAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, ArrayList(it.data.axleDefects)).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectCodeAutoCompleteTextView.setAdapter(adapter)
                            }
                            DetectorSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.detectors.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                detectorAutoCompleteTextView.setAdapter(adapter)
                            }
//                            val defect = it.data.axleDefects.find { defect -> defect.id == viewModel.editingAxle.ntdCodeId }
                            val defect = it.data.axleDefects.find { defect -> defect.id == viewModel.editingAxle.defectNdt?.id }
                            if (defect != null) {
                                defectCodeAutoCompleteTextView.setText(defect.title, false)
                                //viewModel.editingAxle.ntdCodeId = defect.id
                                viewModel.editingAxle.defectNdt = defect
                            } else if (it.data.axleDefects.size == 1) {
                                defectCodeAutoCompleteTextView.setText(it.data.axleDefects[0].title, false)
                               // viewModel.editingAxle.ntdCodeId = it.data.axleDefects[0].id
                                viewModel.editingAxle.defectNdt = it.data.axleDefects[0]
                            } else {
                                viewModel.editingAxle.defectNdt = null
                            }
                            val detector = it.data.detectors.find { detector -> detector.id == viewModel.editingAxle.detector?.id ?: 0 }
                            if (detector != null) {
                                viewModel.editingAxle.detector = detector
                                detectorAutoCompleteTextView.setText(detector.toString(), false)
                            } else {
                                viewModel.editingAxle.detector =
                                    Detector()
                            }
                            defectSizeEditText.setText(viewModel.editingAxle.defectSize)
                            defectDescriptionEditText.setText(viewModel.editingAxle.ntdDescription)
                            generateDefectString()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error)
                    }
                    DataResponse.Status.EMPTY -> {
                        viewModel.editingAxle.defectNdtControlZone = null
                        viewModel.editingAxle.defectNdt = null
                        viewModel.editingAxle.detector= null
                        viewModel.editingAxle.defectSize = ""
                        defectCodeAutoCompleteTextView.setText("")
                        defectCodeAutoCompleteTextView.setAdapter(null)
                        detectorAutoCompleteTextView.setText("")
                        detectorAutoCompleteTextView.setAdapter(null)
                        defectSizeEditText.setText("")
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectAfterInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                        showRefresh(false, null)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            if (viewModel.editingAxle.controlZones.size == 0) {
                                viewModel.editingAxle.controlZones = ArrayList(it.data.controlZones)
                            }
                        }
                        setupControlZones()
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error)
                    }
                }
            }).addTo(compositeDisposable)
        defectCodeAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val defect = defectCodeAutoCompleteTextView.adapter.getItem(position) as AxleDefect
            //viewModel.editingAxle.ntdCodeId = defect.id
            viewModel.editingAxle.defectNdt = defect
            generateDefectString()
        }
        defectCodeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectCodeTextInputLayout.error = null
            }
        }
        detectorAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val detector = detectorAutoCompleteTextView.adapter.getItem(position) as Detector
            viewModel.editingAxle.detector = detector
            generateDefectString()
        }
        detectorAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                detectorTextInputLayout.error = null
            }
        }
        defectSizeEditText.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty() && !text.isNullOrBlank()) {
                defectSizeTextInputLayout.error = null
            }
            viewModel.editingAxle.defectSize = text.toString()
            generateDefectString()
        }
        defectDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.editingAxle.ntdDescription = text.toString()
            generateDefectString()
        }
        removeDefectCodeButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("", false)
            viewModel.editingAxle.defectNdt = null
            generateDefectString()
        }
        removeDetectorButton.setOnClickListener {
            detectorAutoCompleteTextView.setText("", false)
            viewModel.editingAxle.detector =
                Detector()
            generateDefectString()
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            ultrasonicLoadingProgressBar.visibility = View.VISIBLE
            magneticLoadingProgress.visibility = View.VISIBLE
            ultrasonicRadioGroup.isEnabled = false
            magneticRadioGroup.isEnabled = false
            defectCodeAutoCompleteTextView.isEnabled = false
            removeDefectCodeButton.isEnabled = false
            detectorAutoCompleteTextView.isEnabled = false
            removeDetectorButton.isEnabled = false
        } else {
            ultrasonicLoadingProgressBar.visibility = View.GONE
            magneticLoadingProgress.visibility = View.GONE
            ultrasonicRadioGroup.isEnabled = true
            magneticRadioGroup.isEnabled = true
            defectCodeAutoCompleteTextView.isEnabled = true
            removeDefectCodeButton.isEnabled = true
            detectorAutoCompleteTextView.isEnabled = true
            removeDetectorButton.isEnabled = true
        }
    }

    private fun showRefresh(show: Boolean, error: Error?) {
        if (show) {
            if (error is NdtDefectError) {
                refreshButton.setOnClickListener {
                    viewModel.loadAxleDefectsNdtInfo(viewModel.editingAxle.defectNdtControlZone!!.id, error.controlType)
                }
            }
            ndtDefectNestedScrollView.visibility = View.INVISIBLE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.visibility = View.VISIBLE
            errorMessageTextView.text = error?.message
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            ndtDefectNestedScrollView.visibility = View.VISIBLE
        }
    }

    fun checkFields(): Boolean {
        var result = true
        if (defectCodeAutoCompleteTextView.text.isNullOrEmpty()) {
            defectCodeTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (detectorAutoCompleteTextView.text.isNullOrEmpty()) {
            detectorTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (defectSizeEditText.text.isNullOrEmpty()) {
            defectSizeTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        return result
    }

    private fun generateDefectString() {
        var str = "обнаружен: "
        when (viewModel.editingAxle.defectNdtControlZone?.vmInDb) {
            DefectAfterControlFragment.ULTRASONIC_CONTROL -> str += "\"средставами УЗК\""
            DefectAfterControlFragment.MAGNETIC_CONTROL -> str += "\"средставами МПК\""
        }
        str += ", метод (зона): \"${viewModel.editingAxle.defectNdtControlZone.toString()}\", код дефекта: \"${viewModel.editingAxle.defectNdt?.toString()}\", дефектоскоп: \"${viewModel.editingAxle.detector.toString()}\", размер: \"${viewModel.editingAxle.defectSize}\""
        if (viewModel.editingAxle.ntdDescription.isNotEmpty()) {
            str += ", описание: \"${viewModel.editingAxle.ntdDescription}\""
        }
        viewModel.editingAxle.defectInfo = str
    }

    private fun setupControlZones() {
        magneticRadioGroup.removeAllViews()
        ultrasonicRadioGroup.removeAllViews()
        for (controlZone in viewModel.editingAxle.controlZones) {
//            if (controlZone.checked == false) {
//                if (viewModel.editingAxle.defectNdtControlZone?.id == controlZone.id) {
//
//                    viewModel.resetDefectNdtInfo()
//                }
//                continue
//            }
            val radioButton = MaterialRadioButton(requireContext())
            radioButton.text = controlZone.title
            radioButton.tag = controlZone
            radioButton.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    val controlZone = radioButton.tag as AxleControlZone
                    when (controlZone.vmInDb) {
                        DefectAfterControlFragment.ULTRASONIC_CONTROL -> {
                            magneticRadioGroup.clearCheck()
                            defectSizeTextInputLayout.hint = getString(R.string.detector_ultrasonic_size_hint)
                            viewModel.editingAxle.sizeType = 1
                            viewModel.editingAxle.isMagnetic = false
                        }
                        DefectAfterControlFragment.MAGNETIC_CONTROL -> {
                            ultrasonicRadioGroup.clearCheck()
                            defectSizeTextInputLayout.hint = getString(R.string.detector_magnetic_size_hint)
                            viewModel.editingAxle.sizeType = 2
                            viewModel.editingAxle.isMagnetic = true
                        }
                    }
                    viewModel.editingAxle.selectControlZone(controlZone.id)
                    viewModel.loadAxleDefectsNdtInfo(controlZone.id, controlZone.vmInDb)
                    //viewModel.editingAxle.ntdZoneId = controlZone.id
                    viewModel.editingAxle.defectNdtControlZone = controlZone
                }
            }
            when (controlZone.vmInDb) {
                DefectAfterControlFragment.ULTRASONIC_CONTROL -> {
                    ultrasonicRadioGroup.addView(radioButton)
                }
                DefectAfterControlFragment.MAGNETIC_CONTROL -> {
                    magneticRadioGroup.addView(radioButton)
                }
            }
            if (viewModel.editingAxle.defectNdtControlZone?.id == controlZone.id) {
                radioButton.isChecked = true
            }
        }
    }
}




























