package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.FactoryNameSpinnerAdapter
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.adapters.AxleAndWheelTypeSpinnerAdapter
import com.fmklab.armnk_results.modules.common.adapters.FactoryYearSpinnerAdapter
import com.fmklab.armnk_results.modules.common.models.*
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputLayout
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_wheel_base_input.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class WheelBaseInputFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by sharedViewModel()
    private lateinit var materialDatePicker: MaterialDatePicker<Long>
    private var selectAxleFragment: SelectAxleFragment = SelectAxleFragment()
    private val editTextFields: MutableMap<EditText, TextInputLayout> = HashMap()

    companion object {
        private const val MATERIAL_DATE_PICKER_TAG = "MaterialDatePickerTag"
        private const val SELECT_AXLE_FRAGMENT_TAG = "SelectAxleFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wheel_base_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    fun setupUI() {
        wheelBaseInputNestedScrollView.visibility = View.GONE
        factoryNameAutoCompleteTextView.keyListener = null
        factoryYearAutoCompleteTextView.keyListener = null
        wheelTypeAutocompleteTextView.keyListener = null
        controlDateEditText.keyListener = null
        axleNumberEditText.keyListener = null

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText(getString(R.string.select_date))
        materialDatePicker = builder.build()

        editTextFields[controlDateEditText] = controlDateTextInputLayout
        editTextFields[factoryNameAutoCompleteTextView] = factoryNameTextInputLayout
        editTextFields[factoryYearAutoCompleteTextView] = factoryYearTextInputLayout
        editTextFields[wheelTypeAutocompleteTextView] = wheelTypeTextInputLayout
        editTextFields[wheelNumberTextInputEditText] = wheelNumberTextInputLayout
        editTextFields[axleNumberEditText] = axleNumberTextInputLayout
    }

    fun setupBindings() {
        for (editTextField in editTextFields) {
            editTextField.key.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrBlank()) {
                    editTextField.value.error = null
                }
            }
        }

        viewModel.fullWheel
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.status == DataResponse.Status.SUCCESS) {
                    if (it.data != null) {
                        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
                        controlDateEditText.setText(simpleDateFormat.format(it.data.date))
                        factoryNameAutoCompleteTextView.setText(it.data.factory.code, false)
                        factoryYearAutoCompleteTextView.setText(it.data.factoryYear.toString(), false)
                        isNewDetailCheckBox.isChecked = it.data.isNewDetail
                        wheelTypeAutocompleteTextView.setText(it.data.type.shortName, false)
                        wheelNumberTextInputEditText.setText(it.data.wheelNumber)
                        axleNumberEditText.setText(it.data.axle.axleNumber)
                        defectBeforeCheckBox.isChecked = it.data.isDefectBeforeNdt
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.baseInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            FactoryNameSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.factories.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                factoryNameAutoCompleteTextView.setAdapter(adapter)
                            }
                            FactoryYearSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.years2.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                factoryYearAutoCompleteTextView.setAdapter(adapter)
                            }
                            AxleAndWheelTypeSpinnerAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, it.data.types.toTypedArray()).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                wheelTypeAutocompleteTextView.setAdapter(adapter)
                            }
                            if (it.data.isNewDetailEnabled) {
                                isNewDetailCheckBox.visibility = View.VISIBLE
                            } else {
                                isNewDetailCheckBox.visibility = View.GONE
                            }
                            viewModel.setWheelSpec()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        when (it.error) {
                            is InternetConnectionError -> {
                                viewModel.loadBaseInfo()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventWheelAxleSelected::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                axleNumberEditText.setText(it.axleModel.axleNumber)
            }).addTo(compositeDisposable)

        RxBus.listen(RxEvent.EventBaseInfoRequestCheckFields::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                val result = checkFields()
                RxBus.publish(RxEvent.EventBaseInfoCheckFieldsResponse(result))
            }).addTo(compositeDisposable)

        refreshButton.setOnClickListener {
            viewModel.loadBaseInfo()
        }

        factoryNameAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val factory = parent.getItemAtPosition(position) as? Factory
            if (factory != null) {
                viewModel.setWheelFactory(factory)
            }
        }

        factoryYearAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val year = parent.getItemAtPosition(position) as? Year
            if (year != null) {
                viewModel.setWheelFactoryYear(year)
            }
        }

        wheelTypeAutocompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val type = parent.getItemAtPosition(position) as? AxleAndWheelType
            if (type != null) {
                viewModel.setWheelType(type)
            }
        }

        controlDateEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (!parentFragmentManager.fragments.any { frg -> frg.tag == MATERIAL_DATE_PICKER_TAG }) {
                    materialDatePicker.show(parentFragmentManager, MATERIAL_DATE_PICKER_TAG)
                }
                return@setOnTouchListener true
            }
            false
        }

        materialDatePicker.addOnPositiveButtonClickListener {
            val timeZone = TimeZone.getDefault()
            val offsetFromUTC = timeZone.getOffset(Date().time) * -1
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            val date = Date(it + offsetFromUTC)
            controlDateEditText.setText(simpleDateFormat.format(date))
            viewModel.setWheelControlDate(date)
        }

        isNewDetailCheckBox.setOnCheckedChangeListener { _ , isChecked ->
            viewModel.setNewDetail(isChecked)
        }

        wheelNumberTextInputEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setWheelNumber(text.toString())
            }
        }

        axleNumberEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                selectAxleFragment = SelectAxleFragment()
                selectAxleFragment.show(parentFragmentManager, SELECT_AXLE_FRAGMENT_TAG)
                return@setOnTouchListener true
            }
            false
        }

        defectBeforeCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setWheelDefectBeforeNdt(isChecked)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
            wheelBaseInputNestedScrollView.visibility = View.GONE
        } else {
            wheelBaseInputNestedScrollView.visibility = View.VISIBLE
            loadingProgressBar.visibility = View.GONE
        }
    }

    private fun showRefresh(show: Boolean, message: String? = null) {
        if (show) {
            wheelBaseInputNestedScrollView.visibility = View.GONE
            refreshConstraintLayout.visibility = View.VISIBLE
            refreshMessageTextView.text = message
        } else {
            refreshConstraintLayout.visibility = View.GONE
            wheelBaseInputNestedScrollView.visibility = View.VISIBLE
        }
    }

    private fun checkFields(): Boolean {
        var result = true
        for (editTextField in editTextFields) {
            if (editTextField.key.text.isNullOrBlank()) {
                editTextField.value.error = getString(R.string.field_empty_error)
                result = false
            }
        }
        return result
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}





































