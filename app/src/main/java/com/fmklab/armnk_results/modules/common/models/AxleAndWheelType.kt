package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class AxleAndWheelType (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("partId")
    @Expose
    val partId: Int,
    @SerializedName("shortName")
    @Expose
    val shortName: String
) {

    companion object {
        const val DEFAULT_ID = 0
        const val DEFAULT_NAME = ""
        const val DEFAULT_PART_ID = 0
        const val DEFAULT_SHORT_NAME = ""

        const val WHEEL_TYPE_P = 1
        const val WHEEL_TYPE_K = 2
        const val WHEEL_TYPE_NONE = 0
    }

    constructor() : this(DEFAULT_ID, DEFAULT_NAME, DEFAULT_PART_ID, DEFAULT_SHORT_NAME)

    fun getAxleType(): Int {
        return when (name.toLowerCase(Locale.US)) {
            "ру1" -> 1
            "ру1ш" -> 2
            "рв2ш" -> 3
            else -> 0
        }
    }

    fun getWheelType(): Int {
        return when (shortName.toLowerCase(Locale.ROOT)) {
            "п" -> WHEEL_TYPE_P
            "к" -> WHEEL_TYPE_K
            else -> WHEEL_TYPE_NONE
        }
    }

    override fun toString(): String {
        return shortName
    }
}