package com.fmklab.armnk_results.modules.axle.services

import com.fmklab.armnk_results.modules.axle.models.*
import com.fmklab.armnk_results.modules.common.models.CheckAddedDetailResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.Call
import retrofit2.http.*

interface AxleApiService {

    @GET("axle/axlelist")
    fun getAxleList(
        @Query("companyId") companyId: Int,
        @Query("page") page: Int,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Axle>>

    @GET("axle/controlzones")
    fun getControlZones(
        @Query("axleType") axleType: Int,
        @Query("repairType") repairType: Int,
        @Query("companyId") companyId: Int,
        @Query("rings") rings: Int,
        @Header("Authorization") token: String
    ): Call<AxleDefectAfterResponse>

    @GET("axle/defects/visual")
    fun getAxleDefectsVisual(
        @Query("zoneId") zoneId: Int,
        @Header("Authorization") token: String
    ): Call<List<AxleDefect>>

    @GET("axle/defects/ndt")
    fun getAxleDefectsNdt(
        @Query("zoneId") zoneId: Int,
        @Query("controlType") controlType: Int,
        @Query("companyId") companyId: Int,
        @Header("Authorization") token: String
    ): Call<NtdAxleDefectResponse>

    @GET("axle/full")
    fun getFullAxle(
        @Query("axleId") axleId: Int,
        @Header("Authorization") token: String
    ): Call<FullAxle>

    @GET("axle/before/defect")
    fun getDefectBeforeControlTypes(@Header("Authorization") token: String): Call<List<AxleDefectBeforeControlType>>

    @GET("axle/before/zones")
    fun getAxleBeforeZones(
        @Query("defectId") defectId: Int,
        @Header("Authorization") token: String
    ): Call<List<AxleControlZone>>

    @GET("axle/before/codes")
    fun getAxleDefectBefore(
        @Query("defectId") defectId: Int,
        @Query("zoneId") zoneId: Int,
        @Header("Authorization") token: String
    ): Call<List<AxleDefect>>

    @POST("axle/add")
    fun setAxleInfo(
        @Body setAxleModel: SetAxleModel,
        @Header("Authorization") token: String
    ): Call<Int>

    @GET("axle/search")
    fun searchAxle(
        @Query("companyId") companyId: Int,
        @Query("searchString") searchString: String,
        @Header("Authorization") token: String
    ): Call<List<AxleModel>>

    @GET("axle/listsearch")
    fun searchAxleInList(
        @Query("companyId") companyId: Int,
        @Query("searchString") searchString: String,
        @Header("Authorization") token: String
    ): Observable<ArrayList<Axle>>

    @POST("axle/check")
    fun checkAddedAxle(
        @Body request: CheckAddedAxleRequest,
        @Header("Authorization") token: String
    ): Call<CheckAddedDetailResponse>
}







































