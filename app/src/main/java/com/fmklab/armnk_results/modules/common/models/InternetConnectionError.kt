package com.fmklab.armnk_results.modules.common.models

class InternetConnectionError: Error("Ошибка ответа от сервера. Проверьте подключение к сети.")