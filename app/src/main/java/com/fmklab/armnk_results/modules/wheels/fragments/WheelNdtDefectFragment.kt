package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.DetectorSpinnerAdapter
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.FatalError
import com.fmklab.armnk_results.modules.common.models.InternetConnectionError
import com.fmklab.armnk_results.modules.wheels.adapters.WheelDefectSpinnerAdapter
import com.fmklab.armnk_results.modules.wheels.models.*
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.radiobutton.MaterialRadioButton
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_ntd_defect.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WheelNdtDefectFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by sharedViewModel()
    private lateinit var ultrasonicRadioGroup: RadioGroup
    private lateinit var magneticRadioGroup: RadioGroup

    companion object {
        private const val CONTROL_ZONE = 1
        private const val DEFECT_CODE = 2
        private const val DETECTOR = 3
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ntd_defect, container, false)
    }

    override fun onResume() {
        super.onResume()

        loadControlZones()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    private fun setupUI() {
        magneticControlTextView.text = getString(R.string.magnetic_vortex_control)
        defectCodeAutoCompleteTextView.keyListener = null
        detectorAutoCompleteTextView.keyListener = null
        ultrasonicRadioGroup = RadioGroup(requireContext())
        magneticRadioGroup = RadioGroup(requireContext())
        ultrasonicControlLinearLayout.addView(ultrasonicRadioGroup)
        magneticControlLinearLayout.addView(magneticRadioGroup)
    }

    private fun setupBindings() {
        viewModel.fullWheel
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.status == DataResponse.Status.SUCCESS) {
                    defectSizeEditText.setText(it.data?.defectNdtInfo?.defectSize)
                    defectDescriptionEditText.setText(it.data?.defectNdtInfo?.description)
                }
            }).addTo(compositeDisposable)
        viewModel.wheelNdtDefectInfoControlZoneInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectInfoSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            ultrasonicRadioGroup.removeAllViews()
                            magneticRadioGroup.removeAllViews()
                            for (controlZone in it.data) {
                                val radioButton = MaterialRadioButton(requireContext())
                                radioButton.text = controlZone.title
                                radioButton.tag = controlZone
                                radioButton.setOnCheckedChangeListener { _, isChecked ->
                                    if (isChecked) {
                                        val cz = radioButton.tag as WheelControlZone
                                        when (cz.vmInDb) {
                                            WheelControlZone.VM_ULTRASONIC -> {
                                                magneticRadioGroup.clearCheck()
                                                defectSizeTextInputLayout.hint = getString(R.string.detector_ultrasonic_size_hint)
                                                viewModel.setSizeType(WheelDefectNdtInfo.SIZE_TYPE_AMPLITUDE)
                                            }
                                            WheelControlZone.VM_MAGNETIC -> {
                                                ultrasonicRadioGroup.clearCheck()
                                                defectSizeTextInputLayout.hint = getString(R.string.detector_magnetic_size_hint)
                                                viewModel.setSizeType(WheelDefectNdtInfo.SIZE_TYPE_LENGTH)
                                            }
                                        }
                                        viewModel.setNdtDefectZone(cz)
                                        viewModel.loadDefects(WheelDefectInfo.DEFECT_MOMENT_NDT)
                                    }
                                }
                                when (controlZone.vmInDb) {
                                    WheelControlZone.VM_ULTRASONIC -> ultrasonicRadioGroup.addView(radioButton)
                                    WheelControlZone.VM_MAGNETIC -> magneticRadioGroup.addView(radioButton)
                                }
                                if (controlZone.id == viewModel.getNtdControlZone()?.id) {
                                    radioButton.isChecked = true
                                }
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, CONTROL_ZONE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.ndtDefectInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectInfoSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            WheelDefectSpinnerAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, it.data.toTypedArray()).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectCodeAutoCompleteTextView.setAdapter(adapter)
                            }
                            if (it.data.size == 1) {
                                defectCodeAutoCompleteTextView.setText(it.data[0].title, false)
                                viewModel.setNdtDefectCode(it.data[0])
                            } else if (viewModel.getNdtDefectCode()?.id != WheelDefect.DEFAULT_ID) {
                                val defectCode = it.data.find { d -> d.id == viewModel.getNdtDefectCode()?.id }
                                if (defectCode != null) {
                                    defectCodeAutoCompleteTextView.setText(defectCode.title, false)
                                } else {
                                    defectCodeAutoCompleteTextView.setText("", false)
                                }
                            } else {
                                defectCodeAutoCompleteTextView.setText("", false)
                            }
                        }
                        viewModel.loadDetectors()
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, DEFECT_CODE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.detectors
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectInfoSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            DetectorSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                detectorAutoCompleteTextView.setAdapter(adapter)
                            }
                            if (it.data.size == 1) {
                                detectorAutoCompleteTextView.setText(it.data[0].toString(), false)
                                viewModel.setDetector(it.data[0])
                            } else if (viewModel.getDetector()?.id != Detector.DEFAULT_ID) {
                                val detector = it.data.find { d -> d.id == viewModel.getDetector()?.id }
                                if (detector != null) {
                                    detectorAutoCompleteTextView.setText(detector.toString(), false)
                                } else {
                                    detectorAutoCompleteTextView.setText("", false)
                                }
                            } else {
                                detectorAutoCompleteTextView.setText("", false)
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, DETECTOR, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        defectCodeAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val defectCode = parent.getItemAtPosition(position) as? WheelDefect
            if (defectCode != null) {
                viewModel.setNdtDefectCode(defectCode)
            }
        }

        defectCodeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectCodeTextInputLayout.error = null
            }
            if (text.isNullOrBlank()) {
                viewModel.setNdtDefectCode(WheelDefect())
            }
        }

        detectorAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val detector = parent.getItemAtPosition(position) as? Detector
            if (detector != null) {
                viewModel.setDetector(detector)
            }
        }

        detectorAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                detectorTextInputLayout.error = null
            }
            if (text.isNullOrBlank()) {
                viewModel.setDetector(Detector())
            }
        }

        defectSizeEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setDefectSize(text.toString())
                if (!text.isNullOrBlank()) {
                    defectSizeTextInputLayout.error = null
                }
            }
        }

        defectDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setNdtDefectDescription(text.toString())
            }
        }

        removeDetectorButton.setOnClickListener {
            detectorAutoCompleteTextView.setText("", false)
            viewModel.setDetector(Detector())
        }

        removeDefectCodeButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("", false)
            viewModel.setNdtDefectCode(WheelDefect())
        }
    }

    private fun loadControlZones() {
        val fullWheel = viewModel.getFullWheel()
        val controlInfo = fullWheel?.wheelControlInfo
        if (controlInfo != null) {
            val wheelControlZonesRequest = WheelControlZonesRequest(
                WheelControlZone.VM_NONE,
                controlInfo.wheelThickness,
                controlInfo.ringsState,
                WheelDefectInfo.DEFECT_MOMENT_NDT,
                controlInfo.repairType,
                fullWheel.type.getWheelType(),
                false,
                true,
                controlInfo.turning == WheelControlInfo.TURNING_YES
            )
            viewModel.loadControlZones(wheelControlZonesRequest, WheelDefectInfo.DEFECT_MOMENT_NDT)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            ultrasonicLoadingProgressBar.visibility = View.VISIBLE
            magneticLoadingProgress.visibility = View.VISIBLE
            ultrasonicRadioGroup.isEnabled = false
            magneticRadioGroup.isEnabled = false
            defectCodeAutoCompleteTextView.isEnabled = false
            removeDefectCodeButton.isEnabled = false
            detectorAutoCompleteTextView.isEnabled = false
            removeDetectorButton.isEnabled = false
        } else {
            ultrasonicLoadingProgressBar.visibility = View.GONE
            magneticLoadingProgress.visibility = View.GONE
            ultrasonicRadioGroup.isEnabled = true
            magneticRadioGroup.isEnabled = true
            defectCodeAutoCompleteTextView.isEnabled = true
            removeDefectCodeButton.isEnabled = true
            detectorAutoCompleteTextView.isEnabled = true
            removeDetectorButton.isEnabled = true
        }
    }

    private fun showRefresh(show: Boolean, errorType: Int = -1, message: String? = null) {
        if (show) {
            ndtDefectNestedScrollView.visibility = View.INVISIBLE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.visibility = View.VISIBLE
            errorMessageTextView.text = message
            when (errorType) {
                CONTROL_ZONE -> {
                    refreshButton.setOnClickListener {
                        loadControlZones()
                    }
                }
                DEFECT_CODE -> {
                    refreshButton.setOnClickListener {
                        viewModel.loadDefects(WheelDefectInfo.DEFECT_MOMENT_NDT)
                    }
                }
                DETECTOR -> {
                    refreshButton.setOnClickListener {
                        viewModel.loadDetectors()
                    }
                }
            }
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            ndtDefectNestedScrollView.visibility = View.VISIBLE
        }
    }

    fun checkFields(): Boolean {
        var result = true
        if (defectCodeAutoCompleteTextView.text.isNullOrEmpty()) {
            result = false
            defectCodeTextInputLayout.error = getString(R.string.field_empty_error)
        }
        if (detectorAutoCompleteTextView.text.isNullOrEmpty()) {
            result = false
            detectorTextInputLayout.error = getString(R.string.field_empty_error)
        }
        if (defectSizeEditText.text.isNullOrBlank()) {
            result = false
            defectSizeTextInputLayout.error = getString(R.string.field_empty_error)
        }

        return result
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}




































