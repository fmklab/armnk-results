package com.fmklab.armnk_results.modules.wheels.models

data class WheelControlZonesRequest (
    val vm: Int,
    val wheelThickness: Int,
    val ringsState: Int,
    val defectMoment: Int,
    val repairType: Int,
    val wheelType: Int,
    val forControl: Boolean,
    val forDefect: Boolean,
    val isTurning: Boolean?
)