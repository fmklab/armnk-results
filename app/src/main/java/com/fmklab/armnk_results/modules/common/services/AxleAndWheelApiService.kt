package com.fmklab.armnk_results.modules.common.services

import com.fmklab.armnk_results.modules.common.models.AxleAndWheelBaseInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface AxleAndWheelApiService {

    @GET("axlenwheel/baseinfo")
    fun getBaseInfo(
        @Query("detailType") detailType: Int,
        @Query("companyID") companyId: Int,
        @Header("Authorization") token: String
    ): Call<AxleAndWheelBaseInfo>
}