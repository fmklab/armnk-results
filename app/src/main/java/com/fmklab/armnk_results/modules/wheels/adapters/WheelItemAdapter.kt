package com.fmklab.armnk_results.modules.wheels.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.common.models.RecyclerItem
import com.fmklab.armnk_results.modules.wheels.models.Wheel
import kotlinx.android.synthetic.main.refresh_item.view.*
import kotlinx.android.synthetic.main.wheel_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.zip.Inflater
import kotlin.collections.ArrayList

class WheelItemAdapter: DetailItemAdapter<Wheel>() {

    override fun inflateView(parent: ViewGroup, viewType: Int): View {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_ITEM -> inflater.inflate(R.layout.wheel_item, parent, false)
            VIEW_LOADING -> inflater.inflate(R.layout.progress_item, parent, false)
            else -> inflater.inflate(R.layout.refresh_item, parent, false)
        }
    }

    override fun onBindViewHolder(holder: BaseDetailViewHolder<Wheel>, position: Int) {
        if (holder is ItemDetailViewHolder) {
            val item = getItem(position)!!
            holder.itemView.wheelNumberValueTextView.text = item.wheelNumber
            holder.itemView.axleNumberValueTextView.text = item.axle?.axleNumber ?: "-"
            holder.itemView.factoryNameValueTextView.text = item.factory.code
            holder.itemView.factoryYearValueTextView.text = item.factoryYear?.toString()
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            holder.itemView.controlDateTextView.text = simpleDateFormat.format(item.date ?: Date())
            holder.itemView.wheelStatusTextView.text = item.result.name
            if (item.getResult()) {
                holder.itemView.wheelStatusTextView.setBackgroundColor(Color.parseColor("#2b9432"))
            } else if (!item.getResult()) {
                holder.itemView.wheelStatusTextView.setBackgroundColor(Color.parseColor("#b03636"))
            }
            when (item.isMobileApp) {
                true -> Glide.with(holder.itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_smartphone_24)
                    .into(holder.itemView.sourceImageView)
                false -> Glide.with(holder.itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_computer_24)
                    .into(holder.itemView.sourceImageView)
            }
            val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
            holder.itemView.createDateTextView.text = createDateFormat.format(item.createDate)
        }
    }
}

//class WheelItemAdapter: RecyclerView.Adapter<WheelItemAdapter.BaseViewHolder>() {
//
//    open class BaseViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
//
//    class WheelItemViewHolder(itemView: View, private val listener: OnMoreInfoClickListener?): BaseViewHolder(itemView) {
//
//        init {
//            itemView.moreInfoButton.setOnClickListener {
//                listener?.onMoreInfoClick(adapterPosition)
//            }
//        }
//    }
//
//    class LoadingItemViewHolder(itemView: View): BaseViewHolder(itemView)
//
//    class RefreshItemViewHolder(itemView: View, private val listener: OnRefreshItemClickListener?): BaseViewHolder(itemView) {
//
//        init {
//            itemView.refreshItemButton.setOnClickListener {
//                listener?.onRefreshItemClick()
//            }
//        }
//    }
//
//    interface OnMoreInfoClickListener {
//
//        fun onMoreInfoClick(position: Int)
//    }
//
//    interface OnItemAddListener {
//
//        fun onItemAdded()
//    }
//
//    interface OnRefreshItemClickListener {
//
//        fun onRefreshItemClick()
//    }
//
//    private var items: ArrayList<RecyclerItem<Wheel>> = ArrayList()
//    var onMoreInfoClickListener: OnMoreInfoClickListener? = null
//    var onItemAddListener: OnItemAddListener? = null
//    var onRefreshItemClickListener: OnRefreshItemClickListener? = null
//
//    companion object {
//        private const val VIEW_ITEM = 0
//        private const val VIEW_LOADING = 1
//        private const val VIEW_ERROR = 2
//    }
//
//    fun addItems(items: ArrayList<Wheel>) {
//        if (this.items.isNotEmpty()) {
//            this.items.removeAll { item -> item.status != RecyclerItem.Status.ITEM }
//        }
//        for (item in items) {
//            this.items.add(RecyclerItem.item(item))
//        }
//        notifyDataSetChanged()
//    }
//
//    fun addOrUpdateItem(newItem: Wheel) {
//        val oldItem = items.find { item -> item.item?.id == newItem.id }
//        if (oldItem != null) {
//            val index = items.indexOf(oldItem)
//            items[index] = RecyclerItem.item(newItem)
//            notifyItemChanged(index)
//        } else {
//            items.add(0, RecyclerItem.item(newItem))
//            notifyItemInserted(0)
//            onItemAddListener?.onItemAdded()
//        }
//    }
//
//    fun addStatusItem(statusItem: RecyclerItem<Wheel>) {
//        if (items.isNotEmpty()) {
//            items.removeAll { item -> item.status != RecyclerItem.Status.ITEM }
//        }
//        items.add(statusItem)
//        notifyDataSetChanged()
//    }
//
//    fun getItem(position: Int): Wheel? {
//        return items[position].item
//    }
//
//    fun clearItemsWithoutNotify() {
//        items.clear()
//    }
//
//    fun clearItems() {
//        items.clear()
//        notifyDataSetChanged()
//    }
//
//    override fun getItemViewType(position: Int): Int {
//        return when (items[position].status) {
//            RecyclerItem.Status.ITEM -> VIEW_ITEM
//            RecyclerItem.Status.LOADING -> VIEW_LOADING
//            RecyclerItem.Status.ERROR -> VIEW_ERROR
//        }
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
//        var layoutInflater = LayoutInflater.from(parent.context)
//        return when (viewType) {
//            VIEW_ITEM -> {
//                val view = layoutInflater.inflate(R.layout.wheel_item, parent, false)
//                WheelItemViewHolder(view, onMoreInfoClickListener)
//            }
//            VIEW_LOADING -> {
//                val view = layoutInflater.inflate(R.layout.progress_item, parent, false)
//                LoadingItemViewHolder(view)
//            }
//            else -> {
//                val view = layoutInflater.inflate(R.layout.refresh_item, parent, false)
//                RefreshItemViewHolder(view, onRefreshItemClickListener)
//            }
//        }
//    }
//
//    override fun getItemCount(): Int {
//        return items.size
//    }
//
//    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
//        if (holder is WheelItemViewHolder) {
//            val item = items[position].item
//            holder.itemView.wheelNumberValueTextView.text = item?.wheelNumber
//            holder.itemView.axleNumberValueTextView.text = item?.axle?.axleNumber ?: "-"
//            holder.itemView.factoryNameValueTextView.text = item?.factory?.code
//            holder.itemView.factoryYearValueTextView.text = item?.factoryYear?.toString()
//            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
//            holder.itemView.controlDateTextView.text = simpleDateFormat.format(item?.date ?: Date())
//            holder.itemView.wheelStatusTextView.text = item?.result?.name
//            if (item?.getResult() == true) {
//                holder.itemView.wheelStatusTextView.setBackgroundColor(Color.parseColor("#2b9432"))
//            } else if (item?.getResult() == false) {
//                holder.itemView.wheelStatusTextView.setBackgroundColor(Color.parseColor("#b03636"))
//            }
//        }
//    }
//}
























