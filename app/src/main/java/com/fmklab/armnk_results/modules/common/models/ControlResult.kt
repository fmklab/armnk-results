package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ControlResult (
    @SerializedName("id")
    @Expose
    val id: Int = DEFAULT_ID,
    @SerializedName("name")
    @Expose
    val name: String = ""
) {

    companion object {
        const val DEFAULT_ID = 0

        const val WHEEL_GOOD = 25
        const val WHEEL_BAD = 27
    }
}