package com.fmklab.armnk_results.modules.wheels.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.wheels.activities.EditWheelActivity
import com.fmklab.armnk_results.modules.wheels.activities.WheelActivity
import com.fmklab.armnk_results.modules.wheels.models.Wheel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_wheel_details.*
import java.text.SimpleDateFormat
import java.util.*

class WheelDetailsFragment(): BottomSheetDialogFragment() {

    private lateinit var wheel: Wheel

    companion object {
        private const val WHEEL_KEY = "Wheel"

        fun newInstance(wheel: Wheel): WheelDetailsFragment {
            val fragment = WheelDetailsFragment()
            val bundle = Bundle(1)
            val json = Gson().toJson(wheel)
            bundle.putString(WHEEL_KEY, json)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wheel_details, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    private fun setupData() {
        val json = arguments?.getString(WHEEL_KEY)
        if (json != null) {
            wheel = Gson().fromJson(json, Wheel::class.java)
        }
    }

    private fun setupUI() {
        wheelDetailsNestedScrollView.setOnScrollChangeListener { _: NestedScrollView?, _: Int, scrollY: Int, _: Int, oldScrollY: Int ->
            run {
                if (oldScrollY > scrollY) {
                    editWheelFab.show()
                } else if (oldScrollY < scrollY) {
                    editWheelFab.hide()
                }
            }
        }
        wheelDetailsToolbar.title = "Колесо №${wheel.wheelNumber}"
        val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
        createDateValueTextView.text = createDateFormat.format(wheel.createDate)
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        controlDateValueTextView.text = simpleDateFormat.format(wheel.date)
        factoryNameValueTextView.text = wheel.factory.code
        factoryYearValueTextView.text = wheel.factoryYear.toString()
        wheelTypeValueTextView.text = wheel.type.shortName
        axleNumberValueTextView.text = wheel.axle?.axleNumber ?: "-"
        wheelResultValueTextView.text = wheel.result.name
        controlInfoValueTextView.text = wheel.controlInfoStr
        defectDescriptionValueTextView.text = wheel.defectInfoStr
        specialistCredentialsValueTextView.text = wheel.spec

    }

    private fun setupBindings() {
        editWheelFab.setOnClickListener {
            val intent = Intent(requireActivity(), EditWheelActivity::class.java).apply {
                putExtra(WheelActivity.WHEEL_ID_TAG, wheel.id)
            }
            startActivity(intent)
        }
    }
}




































