package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_results.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_middle_repair.*

class MiddleRepairFragment: BottomSheetDialogFragment() {

    val selectRingType: PublishSubject<Int> = PublishSubject.create()

    companion object {
        const val WITH_RINGS = 2
        const val WITHOUT_RINGS = 1
        const val NONE_RINGS = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_middle_repair, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBindings()
    }

    private fun setupBindings() {
        withRingsButton.setOnClickListener {
            selectRingType.onNext(WITH_RINGS)
        }
        withoutRingsButton.setOnClickListener {
            selectRingType.onNext(WITHOUT_RINGS)
        }
    }
}