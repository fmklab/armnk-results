package com.fmklab.armnk_results.modules.number_detail.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.fmklab.armnk_results.modules.number_detail.fragments.NumberBaseInputFragment
import com.fmklab.armnk_results.modules.number_detail.fragments.NumberDetailDefectInfoFragment
import com.fmklab.armnk_results.modules.number_detail.fragments.NumberDetailRepairInfoFragment

class NumberEditPagerAdapter(fm: FragmentManager, behavior: Int): FragmentStatePagerAdapter(fm, behavior) {

    private var fragments = arrayOf(
        NumberBaseInputFragment(),
        NumberDetailDefectInfoFragment(),
        NumberDetailRepairInfoFragment()
    )

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragments[position] = fragment
        return fragment
    }
}






























