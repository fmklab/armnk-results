package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.common.models.FatalError
import com.fmklab.armnk_results.modules.common.models.InternetConnectionError
import com.fmklab.armnk_results.modules.wheels.adapters.WheelDefectSpinnerAdapter
import com.fmklab.armnk_results.modules.wheels.models.*
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.radiobutton.MaterialRadioButton
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_visual_and_ntd_defect.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WheelVisualDefectFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditWheelViewModel by sharedViewModel()
    private lateinit var magneticRadioGroup: RadioGroup

    companion object {
        private const val CONTROL_ZONE = 1
        private const val DEFECT_CODE = 2
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_visual_and_ntd_defect, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    override fun onResume() {
        super.onResume()

        loadControlZones()
    }

    private fun setupUI() {
        magneticControlTextView.text = getString(R.string.magnetic_vortex_control)
        defectCodeAutoCompleteTextView.keyListener = null
        magneticRadioGroup = RadioGroup(requireContext())
        magneticControlLinearLayout.addView(magneticRadioGroup)
    }

    private fun setupBindings() {
        viewModel.fullWheel
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.status == DataResponse.Status.SUCCESS) {
                    defectDescriptionEditText.setText(it.data?.defectVisualInfo?.description)
                }
            }).addTo(compositeDisposable)
        viewModel.wheelVisualDefectInfoControlZonesInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectInfoSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            magneticRadioGroup.removeAllViews()
                            for (controlZone in it.data) {
                                val radioButton = MaterialRadioButton(requireContext())
                                radioButton.text = controlZone.title
                                radioButton.tag = controlZone
                                radioButton.setOnCheckedChangeListener { _, isChecked ->
                                    if (isChecked) {
                                        val zone = radioButton.tag as WheelControlZone
                                        viewModel.setVisualControlZone(zone)
                                        viewModel.loadDefects(WheelDefectInfo.DEFECT_MOMENT_VISUAL)
                                    }
                                }
                                magneticRadioGroup.addView(radioButton)
                                val currentVisualControlZone = viewModel.getVisualControlZone()
                                if (currentVisualControlZone?.id == controlZone.id) {
                                    radioButton.isChecked = true
                                }
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, CONTROL_ZONE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        viewModel.visualDefectInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        RxBus.publish(RxEvent.EventDefectInfoSuccessLoading())
                        showLoading(false)
                        if (it.data != null) {
                            WheelDefectSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                it.data.toTypedArray()
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectCodeAutoCompleteTextView.setAdapter(adapter)
                            }
                            if (it.data.size == 1) {
                                defectCodeAutoCompleteTextView.setText(it.data[0].title, false)
                                viewModel.setVisualDefectCode(it.data[0])
                            } else if (viewModel.getVisualDefectCode()?.id != WheelDefect.DEFAULT_ID) {
                                val defectCode = it.data.find { d -> d.id == viewModel.getVisualDefectCode()?.id }
                                if (defectCode != null) {
                                    defectCodeAutoCompleteTextView.setText(defectCode.title, false)
                                } else {
                                    defectCodeAutoCompleteTextView.setText("", false)
                                }
                            } else {
                                defectCodeAutoCompleteTextView.setText("", false)
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        RxBus.publish(RxEvent.EventDefectInfoLoadingOrError())
                        when (it.error) {
                            is InternetConnectionError -> {
                                loadControlZones()
                            }
                            is FatalError -> {
                                requireActivity().finish()
                                return@subscribeBy
                            }
                            else -> {
                                showLoading(false)
                                showRefresh(true, DEFECT_CODE, it.error?.message)
                            }
                        }
                    }
                }
            }).addTo(compositeDisposable)

        defectCodeAutoCompleteTextView.setOnItemClickListener { parent, _, position, _ ->
            val wheelDefect = parent.getItemAtPosition(position) as? WheelDefect
            if (wheelDefect != null) {
                viewModel.setVisualDefectCode(wheelDefect)
            }
        }

        defectCodeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectCodeTextInputLayout.error = null
            }
            if (text.isNullOrBlank()) {
                viewModel.setVisualDefectCode(WheelDefect())
            }
        }

        defectDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                viewModel.setVisualDefectDescription(text.toString())
            }
        }

        removeDefectCodeButton.setOnClickListener {
            viewModel.setVisualDefectCode(WheelDefect())
            defectCodeAutoCompleteTextView.setText("", false)
        }

        removeDefectDescriptionButton.setOnClickListener {
            viewModel.setVisualDefectDescription("")
            defectDescriptionEditText.setText("")
        }
    }

    private fun loadControlZones() {
        val fullWheel = viewModel.getFullWheel()
        val controlInfo = fullWheel?.wheelControlInfo
        if (controlInfo != null) {
            val wheelControlZonesRequest = WheelControlZonesRequest(
                WheelControlZone.VM_MAGNETIC,
                controlInfo.wheelThickness,
                controlInfo.ringsState,
                WheelDefectInfo.DEFECT_MOMENT_VISUAL,
                controlInfo.repairType,
                fullWheel.type.getWheelType(),
                false,
                true,
                controlInfo.turning == WheelControlInfo.TURNING_YES
            )
            viewModel.loadControlZones(wheelControlZonesRequest, WheelDefectInfo.DEFECT_MOMENT_VISUAL)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
            magneticRadioGroup.isEnabled = false
            defectCodeAutoCompleteTextView.isEnabled = false
            removeDefectCodeButton.isEnabled = false
        } else {
            loadingProgressBar.visibility = View.GONE
            magneticRadioGroup.isEnabled = true
            defectCodeAutoCompleteTextView.isEnabled = true
            removeDefectCodeButton.isEnabled = true
        }
    }

    private fun showRefresh(show: Boolean, errorType: Int = -1, message: String? = null) {
        if (show) {
            visualAndNdtNestedScrollView.visibility = View.INVISIBLE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.visibility = View.VISIBLE
            errorMessageTextView.text = message
            when (errorType) {
                CONTROL_ZONE -> {
                    refreshButton.setOnClickListener {
                        loadControlZones()
                    }
                }
                DEFECT_CODE -> {
                    refreshButton.setOnClickListener {
                        viewModel.loadDefects(WheelDefectInfo.DEFECT_MOMENT_VISUAL)
                    }
                }
            }
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            visualAndNdtNestedScrollView.visibility = View.VISIBLE
        }
    }

    fun checkFields(): Boolean {
        var result = true
        if (defectCodeAutoCompleteTextView.text.isNullOrEmpty()) {
            result = false
            defectCodeTextInputLayout.error = getString(R.string.field_empty_error)
        }
        return result
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}

































