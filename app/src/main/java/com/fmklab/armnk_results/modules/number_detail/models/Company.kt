package com.fmklab.armnk_results.modules.number_detail.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

enum class Company(value: Int) {

    @SerializedName("0")
    @Expose
    NONE(0),
    @SerializedName("1")
    @Expose
    VRK1(1),
    @SerializedName("2")
    @Expose
    VRK2(2),
    @SerializedName("3")
    @Expose
    VRK3(3),
    @SerializedName("4")
    @Expose
    OTHER(4)
}