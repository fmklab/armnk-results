package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.adapters.AxleDefectSpinnerAdapter
import com.fmklab.armnk_results.modules.axle.adapters.DefectBeforeControlTypeSpinnerAdapter
import com.fmklab.armnk_results.modules.axle.adapters.DefectZoneSpinnerAdapter
import com.fmklab.armnk_results.modules.axle.models.AxleDefect
import com.fmklab.armnk_results.modules.axle.models.AxleControlZone
import com.fmklab.armnk_results.modules.axle.models.AxleDefectBeforeControlType
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.DataResponse
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_defect_before_control.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DefectBeforeAxleInputFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel by sharedViewModel<EditAxleViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_defect_before_control, container, false)
    }

    override fun onResume() {
        super.onResume()

        viewModel.loadDefectBeforeControlTypes()

        setupUI()
        setupBindings()
    }

    override fun onPause() {
        super.onPause()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        defectZoneAutoCompleteTextView.keyListener = null
        defectCodeAutoCompleteTextView.keyListener = null

        when (viewModel.editingAxle.repairType) {
            RepairTypeFragment.CURRENT_REPAIR -> currentRepairRadio.isChecked = true
            RepairTypeFragment.MIDDLE_REPAIR -> middleRepairRadio.isChecked = true
            RepairTypeFragment.CAPITAL_REPAIR -> capitalRepairRadio.isChecked = true
        }
    }

    private fun setupBindings() {
        viewModel.axleDefectBeforeControlType
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                        showUpdate(false, null)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            DefectBeforeControlTypeSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                ArrayList(it.data)
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectTypeAutoCompleteTextView.setAdapter(adapter)
                            }
//                            var type =
//                                it.data.find { t -> t.id == viewModel.editingAxle.defectTypeId }
                            var type =
                                it.data.find { t -> t.id == viewModel.editingAxle.defectBeforeControlType?.id }
                            if (it.data.size == 1 && type == null) {
                                type = it.data[0]
                            }
                            if (type != null) {
                                //viewModel.editingAxle.defectTypeId = type.id
                                viewModel.editingAxle.defectBeforeControlType = type
                                defectTypeAutoCompleteTextView.setText(type.toString(), false)
                                viewModel.loadDefectBeforeZones()
                            }
                        }
                        viewModel.editingAxle.controlInfo = getString(R.string.before_control_info)
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showUpdate(true, it.error?.message)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectBeforeZone
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                        showUpdate(false, null)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            DefectZoneSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                ArrayList(it.data)
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectZoneAutoCompleteTextView.setAdapter(adapter)
                            }
//                            var zone =
//                                it.data.find { z -> z.id == viewModel.editingAxle.defectZoneId }
                            var zone =
                                it.data.find { z -> z.id == viewModel.editingAxle.defectControlZone?.id }
                            if (it.data.size == 1 && zone == null) {
                                zone = it.data[0]
                            }
                            if (zone != null) {
                                //viewModel.editingAxle.defectZoneId = zone.id
                                viewModel.editingAxle.defectControlZone = zone
                                defectZoneAutoCompleteTextView.setText(zone.toString(), false)
                                viewModel.loadDefectBeforeCodes()
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showUpdate(true, it.error?.message)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectBeforeCode
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showLoading(true)
                        showUpdate(false, null)
                    }
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data != null) {
                            AxleDefectSpinnerAdapter(
                                requireContext(),
                                R.layout.support_simple_spinner_dropdown_item,
                                ArrayList(it.data)
                            ).also { adapter ->
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                defectCodeAutoCompleteTextView.setAdapter(adapter)
                            }
//                            var defect =
//                                it.data.find { d -> d.id == viewModel.editingAxle.defectCodeId }
                            var defect =
                                it.data.find { d -> d.id == viewModel.editingAxle.defect?.id }
                            if (it.data.size == 1 && defect == null) {
                                defect = it.data[0]
                            }
                            if (defect != null) {
                                //viewModel.editingAxle.defectCodeId = defect.id
                                viewModel.editingAxle.defect = defect
                                defectCodeAutoCompleteTextView.setText(defect.toString(), false)
                            }
                            defectDescriptionEditText.setText(viewModel.editingAxle.defectDescription)
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showUpdate(true, it.error?.message)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.defectBeforeControlInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it != "") {
                    toast(it)
                }
                checkFields()
            }).addTo(compositeDisposable)
        defectTypeAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            defectZoneAutoCompleteTextView.setText("", false)
            defectCodeAutoCompleteTextView.setText("", false)
            val defectType =
                defectTypeAutoCompleteTextView.adapter.getItem(position) as AxleDefectBeforeControlType
           // viewModel.editingAxle.defectTypeId = defectType.id
            viewModel.editingAxle.defectBeforeControlType = defectType
            viewModel.loadDefectBeforeZones()
            createDefectString()
        }
        defectTypeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectTypeTextInputLayout.error = null
            }
            createDefectString()
        }
        defectZoneAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            defectCodeAutoCompleteTextView.setText("", false)
            val zone = defectZoneAutoCompleteTextView.adapter.getItem(position) as AxleControlZone
            //viewModel.editingAxle.defectZoneId = zone.id
            viewModel.editingAxle.defectControlZone = zone
            viewModel.loadDefectBeforeCodes()
            createDefectString()
        }
        defectZoneAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectZoneTextInputLayout.error = null
            }
            createDefectString()
        }
        defectCodeAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val defect = defectCodeAutoCompleteTextView.adapter.getItem(position) as AxleDefect
           // viewModel.editingAxle.defectCodeId = defect.id
            viewModel.editingAxle.defect = defect
            createDefectString()
        }
        defectCodeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                defectCodeTextInputLayout.error = null
            }
        }
        defectDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.editingAxle.defectDescription = text.toString()
            createDefectString()
        }
        removeDefectTypeButton.setOnClickListener {
            defectTypeAutoCompleteTextView.setText("", false)
            defectZoneAutoCompleteTextView.setText("", false)
            val defectZoneAdapter =
                defectZoneAutoCompleteTextView.adapter as? DefectZoneSpinnerAdapter
            defectZoneAdapter?.clear()
            defectCodeAutoCompleteTextView.setText("", false)
            val defectCodeAdapter =
                defectCodeAutoCompleteTextView.adapter as? AxleDefectSpinnerAdapter
            defectCodeAdapter?.clear()
            viewModel.editingAxle.defectBeforeControlType = null
            viewModel.editingAxle.defectControlZone = null
            viewModel.editingAxle.defect = null
//            viewModel.editingAxle.defectTypeId = FullAxle.DEFAULT_DEFECT_TYPE_ID
//            viewModel.editingAxle.defectZoneId = FullAxle.DEFAULT_DEFECT_ZONE_ID
//            viewModel.editingAxle.defectCodeId = FullAxle.DEFAULT_DEFECT_CODE_ID
            createDefectString()
        }
        removeDefectZoneButton.setOnClickListener {
            defectZoneAutoCompleteTextView.setText("", false)
            defectCodeAutoCompleteTextView.setText("", false)
            val adapter = defectCodeAutoCompleteTextView.adapter as? AxleDefectSpinnerAdapter
            adapter?.clear()
            viewModel.editingAxle.defectControlZone = null
            viewModel.editingAxle.defect = null
//            viewModel.editingAxle.defectZoneId = FullAxle.DEFAULT_DEFECT_ZONE_ID
//            viewModel.editingAxle.defectCodeId = FullAxle.DEFAULT_DEFECT_CODE_ID
        }
        removeDefectCodeButton.setOnClickListener {
            defectCodeAutoCompleteTextView.setText("", false)
            viewModel.editingAxle.defect = null
//            viewModel.editingAxle.defectCodeId = FullAxle.DEFAULT_DEFECT_CODE_ID
        }
        refreshButton.setOnClickListener {
            viewModel.loadDefectBeforeControlTypes()
        }
        currentRepairRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.editingAxle.repairType = RepairTypeFragment.CURRENT_REPAIR
            }
        }
        middleRepairRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.editingAxle.repairType = RepairTypeFragment.MIDDLE_REPAIR
            }
        }
        capitalRepairRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.editingAxle.repairType = RepairTypeFragment.CAPITAL_REPAIR
            }
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            defectTypeLoadingProgress.visibility = View.VISIBLE
            defectTypeAutoCompleteTextView.isEnabled = false
            defectZoneAutoCompleteTextView.isEnabled = false
            defectCodeAutoCompleteTextView.isEnabled = false
            removeDefectTypeButton.isEnabled = false
            removeDefectZoneButton.isEnabled = false
            removeDefectCodeButton.isEnabled = false
        } else {
            defectTypeLoadingProgress.visibility = View.GONE
            defectTypeAutoCompleteTextView.isEnabled = true
            defectZoneAutoCompleteTextView.isEnabled = true
            defectCodeAutoCompleteTextView.isEnabled = true
            removeDefectTypeButton.isEnabled = true
            removeDefectZoneButton.isEnabled = true
            removeDefectCodeButton.isEnabled = true
        }
    }

    private fun showUpdate(show: Boolean, message: String?) {
        if (show) {
            defectBeforeControlConstraintLayout.visibility = View.INVISIBLE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.text = message
            errorMessageTextView.visibility = View.VISIBLE
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            defectBeforeControlConstraintLayout.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    fun checkFields(): Boolean {
        var result = true
        if (defectTypeAutoCompleteTextView.text.isNullOrEmpty()) {
            defectTypeTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (defectZoneAutoCompleteTextView.text.isNullOrEmpty()) {
            defectZoneTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (defectCodeAutoCompleteTextView.text.isNullOrEmpty()) {
            defectCodeTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        return result
    }

    private fun createDefectString() {
        var str =
            "обнаружен: \"при дефектации\", тип дефекта: \"${viewModel.editingAxle.defectBeforeControlType?.name}\", код дефекта: \"${viewModel.editingAxle.defect?.title}\", зона обнаружения: \"${viewModel.editingAxle.defectControlZone?.title}\""
        if (viewModel.editingAxle.defectDescription.isNotEmpty()) {
            str += ", описание: \"${viewModel.editingAxle.defectDescription}\""
        }
        viewModel.editingAxle.defectInfo = str
    }

    private fun toast(message: String?) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }
}























