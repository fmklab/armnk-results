package com.fmklab.armnk_results.modules.common.repositories

import android.content.Context
import android.content.SharedPreferences
import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.google.gson.Gson
import kotlin.reflect.typeOf

class LocalRepository<T>(context: Context, name: String) where T: Any {

    private val preferences: SharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    fun dumpDetail(detail: T, key: String) {
        val json = Gson().toJson(detail)
        val editor = preferences.edit()
        editor.putString(key, json)
        editor.apply()
    }

    fun loadDetail(key: String, ofClass: Class<T>): T? {
        val json = preferences.getString(key, null)
        return if (json != null) {
            Gson().fromJson(json, ofClass)
        } else {
            null
        }
    }

    fun dumpObject(obj: Any, key: String) {
        val json = Gson().toJson(obj)
        val editor = preferences.edit()
        editor.putString(key, json)
        editor.apply()
    }

    fun loadObject(key: String, ofClass: Class<*>) : Any? {
        val json = preferences.getString(key, null)
        return if (json != null) {
            Gson().fromJson(json, ofClass)
        } else {
            null
        }
    }
}