package com.fmklab.armnk_results.modules.common.adapters

import android.content.Context
import android.widget.ArrayAdapter

class AutoCompleteTextViewAdapter<T>(context: Context, resource: Int) :
    ArrayAdapter<T>(context, resource) where T: Any {

    private val items: ArrayList<T> = ArrayList()

    fun setItems(items: ArrayList<T>) {
        items.clear()
        items.addAll(items)
        notifyDataSetChanged()
    }


}