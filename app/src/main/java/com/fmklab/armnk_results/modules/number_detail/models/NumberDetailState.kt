package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.common.models.Detector
import com.fmklab.armnk_results.modules.wheels.models.DefectType

abstract class NumberDetailState(
    defectType: DefectType?,
    zone: ControlZone?
) {

    open var defectType: DefectType? = defectType

    open var zone: ControlZone? = zone

    open var defectDescription: String? = null
        get() = null

    open var repairDescription: String? = null
        get() = null

    open var steal: String? = null
        get() = null

    open var owner: String? = null
        get() = null

    open var defectLength: String? = null
        get() = null

    open var defectDepth: String? = null
        get() = null

    open var defectDiameter: String? = null
        get() = null

    open var detector: Detector? = null
        get() = null

    open var defectSize: String? = null
        get() = null

    open var method: String? = null
        get() = null

    abstract fun getDescription(): String
}






































