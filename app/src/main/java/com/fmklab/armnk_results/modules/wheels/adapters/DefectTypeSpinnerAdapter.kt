package com.fmklab.armnk_results.modules.wheels.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fmklab.armnk_results.modules.wheels.models.DefectType

class DefectTypeSpinnerAdapter(context: Context, resource: Int, private val objects: ArrayList<out DefectType>) :
    ArrayAdapter<DefectType>(context, resource, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = super.getView(position, convertView, parent) as TextView
        textView.text = objects[position].name
        return textView
    }
}