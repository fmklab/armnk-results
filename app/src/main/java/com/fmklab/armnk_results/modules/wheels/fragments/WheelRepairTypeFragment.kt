package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.RxBus
import com.fmklab.armnk_results.modules.common.RxEvent
import com.fmklab.armnk_results.modules.wheels.models.WheelControlInfo
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_repair_type.*

class WheelRepairTypeFragment: BottomSheetDialogFragment() {

    private val compositeDisposable = CompositeDisposable()

    companion object {
        private const val WHEEL_REPAIR_INFO_FRAGMENT_TAG = "WheelRepairInfoFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_repair_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBindings()
    }

    private fun setupBindings() {
        RxBus.listen(RxEvent.EventRepairInfoSet::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                dismiss()
            }).addTo(compositeDisposable)
        currentRepairButton.setOnClickListener {
            if (!parentFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_INFO_FRAGMENT_TAG }) {
                val wheelRepairInfoFragment = WheelRepairInfoFragment.newInstance(WheelControlInfo.REPAIR_TYPE_CURRENT)
                wheelRepairInfoFragment.show(parentFragmentManager, WHEEL_REPAIR_INFO_FRAGMENT_TAG)
            }
        }
        middleRepairButton.setOnClickListener {
            if (!parentFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_INFO_FRAGMENT_TAG }) {
                val wheelRepairInfoFragment = WheelRepairInfoFragment.newInstance(WheelControlInfo.REPAIR_TYPE_MIDDLE)
                wheelRepairInfoFragment.show(parentFragmentManager, WHEEL_REPAIR_INFO_FRAGMENT_TAG)
            }
        }
        capitalRepairButton.setOnClickListener {
            if (!parentFragmentManager.fragments.any { frg -> frg.tag == WHEEL_REPAIR_INFO_FRAGMENT_TAG }) {
                val wheelRepairInfoFragment = WheelRepairInfoFragment.newInstance(WheelControlInfo.REPAIR_TYPE_CAPITAL)
                wheelRepairInfoFragment.show(parentFragmentManager, WHEEL_REPAIR_INFO_FRAGMENT_TAG)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}







































