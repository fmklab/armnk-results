package com.fmklab.armnk_results.modules.common.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Detector (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("number")
    @Expose
    val number: String
) {

    companion object {
        const val DEFAULT_ID = -1
    }

    constructor(): this(DEFAULT_ID, "", "")

    override fun toString(): String {
        var result = name
        if (!number.isNullOrEmpty()) {
            result += "(зав. № $number)"
        }
        return result
    }
}

































