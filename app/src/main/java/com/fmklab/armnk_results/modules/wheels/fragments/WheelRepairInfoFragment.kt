package com.fmklab.armnk_results.modules.wheels.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.wheels.models.WheelControlInfo
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_wheel_repair_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WheelRepairInfoFragment: BottomSheetDialogFragment() {

    private val viewModel: EditWheelViewModel by sharedViewModel()
    private var repairType: Int = WheelControlInfo.REPAIR_TYPE_NONE
    private var isStealT: Boolean = false
    private var wheelThickness: Int = WheelControlInfo.WHEEL_THICKNESS_LESS_40
    private var ringsState: Int = WheelControlInfo.RING_STATE_ON
    private var isTurning: Boolean = true

    companion object {
        private const val REPAIR_TYPE_KEY = "RepairType"

        fun newInstance(repairType: Int): WheelRepairInfoFragment {
            val fragment = WheelRepairInfoFragment()
            val bundle = Bundle(1)
            bundle.putInt(REPAIR_TYPE_KEY, repairType)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wheel_repair_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBindings()
        setupUI()
    }

    private fun setupData() {
        val repairType = arguments?.getInt(REPAIR_TYPE_KEY)
        if (repairType != null) {
            this.repairType = repairType
        }
    }

    private fun setupUI() {
        when (repairType) {
            WheelControlInfo.REPAIR_TYPE_CURRENT -> {
                tStealNoRadio.isChecked = true
                tStealNoRadio.isEnabled = false
                tStealYesRadio.isEnabled = false
                wheelThicknessMore50Radio.visibility = View.GONE
                wheelThicknessLess40Radio.isChecked = true
                wheelRingsYesRadio.isEnabled = false
                wheelRingsNoRadio.isChecked = true
                wheelRingsNoRadio.isEnabled = false
            }
            WheelControlInfo.REPAIR_TYPE_MIDDLE -> {
                tStealNoRadio.isChecked = true
                wheelThicknessLess40Radio.isChecked = true
                wheelThicknessMore50Radio.visibility = View.GONE
                wheelRingsNoRadio.isChecked = true
            }
            WheelControlInfo.REPAIR_TYPE_CAPITAL -> {
                tStealNoRadio.isChecked = true
                wheelThicknessLess40Radio.isChecked = true
                wheelThicknessMore50Radio.visibility = View.GONE
                wheelRingsYesRadio.isChecked = true
                wheelRingsYesRadio.isEnabled = false
                wheelRingsNoRadio.isEnabled = false
            }
        }
        isTurningRadio.isChecked = isTurning
    }

    private fun setupBindings() {
        tStealYesRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked && (repairType == WheelControlInfo.REPAIR_TYPE_MIDDLE || repairType == WheelControlInfo.REPAIR_TYPE_CAPITAL)) {
                wheelThicknessMore50Radio.visibility = View.VISIBLE
            } else {
                wheelThicknessMore50Radio.visibility = View.GONE
            }
            isStealT = isChecked
        }

        tStealNoRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked && !wheelThicknessLess40Radio.isChecked && !wheelThicknessMore40Radio.isChecked) {
                wheelThicknessLess40Radio.isChecked = true
            }
        }

        wheelThicknessLess40Radio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                wheelThickness = WheelControlInfo.WHEEL_THICKNESS_LESS_40
            }
        }

        wheelThicknessMore40Radio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                wheelThickness = WheelControlInfo.WHEEL_THICKNESS_MORE_40
            }
        }

        wheelThicknessMore50Radio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                wheelThickness = WheelControlInfo.WHEEL_THICKNESS_MORE_50
            }
        }

        wheelRingsYesRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                ringsState = WheelControlInfo.RING_STATE_OFF
            }
        }

        wheelRingsNoRadio.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                ringsState = WheelControlInfo.RING_STATE_ON
            }
        }

        isTurningRadio.setOnCheckedChangeListener { _, isChecked ->
            isTurning = isChecked
        }

        goNextButton.setOnClickListener {
            viewModel.setRepairInfo(repairType, wheelThickness, ringsState, isTurning, isStealT)
            dismiss()
        }
    }
}
































