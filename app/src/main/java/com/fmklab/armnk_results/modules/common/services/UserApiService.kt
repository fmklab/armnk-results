package com.fmklab.armnk_results.modules.common.services

import com.fmklab.armnk_results.modules.auth.models.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface UserApiService {

    @GET("user/users")
    fun getUsers(
        @Query("companyId") companyId: Int,
        @Header("Authorization") token: String
    ): Call<ArrayList<User>>
}