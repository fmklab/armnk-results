package com.fmklab.armnk_results.modules.wheels.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.models.RecyclerItem
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.activities.DetailItemsActivity
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.modules.common.listeners.PageRecyclerViewOnScrollListener
import com.fmklab.armnk_results.modules.common.models.FatalError
import com.fmklab.armnk_results.modules.common.models.InternetConnectionError
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import com.fmklab.armnk_results.modules.wheels.adapters.WheelItemAdapter
import com.fmklab.armnk_results.modules.wheels.fragments.WheelDetailsFragment
import com.fmklab.armnk_results.modules.wheels.models.FullWheel
import com.fmklab.armnk_results.modules.wheels.models.Wheel
import com.fmklab.armnk_results.modules.wheels.repositories.WheelRepository
import com.fmklab.armnk_results.modules.wheels.viewmodels.WheelViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_wheel.*
import kotlinx.android.synthetic.main.activity_wheel.loadingProgressBar
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel

class WheelActivity : DetailItemsActivity<Wheel>() {

    override val viewModel: DetailItemsViewModel<Wheel> by lifecycleScope.viewModel<WheelViewModel>(
        this
    )
    override val detailsAdapter: DetailItemAdapter<Wheel> by lifecycleScope.inject<WheelItemAdapter>()
    override val recyclerViewLayoutManager: LinearLayoutManager = LinearLayoutManager(this)
    override var detailsFragment: BottomSheetDialogFragment? = null

    companion object {
        const val WHEEL_DETAILS_FRAGMENT_TAG = "WheelDetailsFragmentTag"
        const val WHEEL_ID_TAG = "WheelId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wheel)

        setupUI()
        setupListeners()
        setupData()
        subscribeObservers()
        viewModel.loadDetails(0)
    }

    override fun setupUI() {
        recyclerView = findViewById(R.id.wheelsRecycleView)
        swipeRefreshLayout = findViewById(R.id.wheelsSwipeRefreshLayout)
        addNewDetailFab = findViewById(R.id.addNewWheelFab)
        toolBar = findViewById(R.id.wheelToolbar)
        emptyDataTextView = findViewById(R.id.emptyDataTextView)
        title = getString(R.string.wheels)

        super.setupUI()
    }

    override fun showDetails(position: Int) {
        val wheel = detailsAdapter.getItem(position)
        if (wheel != null) {
            detailsFragment = WheelDetailsFragment.newInstance(wheel)
            if (!supportFragmentManager.fragments.any { frg -> frg.tag == WHEEL_DETAILS_FRAGMENT_TAG }) {
                detailsFragment?.show(supportFragmentManager, WHEEL_DETAILS_FRAGMENT_TAG)
            }
        }
    }

    override fun addNewDetail() {
        val intent = Intent(this, EditWheelActivity::class.java).apply {
            putExtra(WHEEL_ID_TAG, FullWheel.DEFAULT_ID)
        }
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()

        val editedWheel = WheelRepository(this).getEditedWheel()
        if (editedWheel != null) {
            //viewModel.addOrUpdateDetail(editedWheel)
            detailsAdapter.addOrUpdateItem(editedWheel)
        }
    }

//    private val compositeDisposable = CompositeDisposable()
//    private val viewModel: WheelViewModel by lifecycleScope.viewModel(this)
//    private val wheelItemAdapter: WheelItemAdapter by lifecycleScope.inject()
//    private var firstLoading = true
//    private var itemsRefreshing = false
//    private var anotherPageLoading = false
//    private val recyclerViewLayoutManager = LinearLayoutManager(this)
//    private var wheelDetailsFragment: WheelDetailsFragment? = null
//
//    private val recyclerViewOnScrollListener = object : PageRecyclerViewOnScrollListener(recyclerViewLayoutManager) {
//        override fun onLoadMore(currentPage: Int) {
//            if (!firstLoading && !itemsRefreshing) {
//                viewModel.loadWheelList(currentPage)
//                anotherPageLoading = true
//            }
//        }
//    }
//
//    companion object {
//        const val WHEEL_DETAILS_FRAGMENT_TAG = "WheelDetailsFragmentTag"
//        const val WHEEL_ID_TAG = "WheelId"
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_wheel)
//
//        setupUI()
//        setupData()
//        setupBindings()
//    }
//
//    private fun setupUI() {
//        setSupportActionBar(wheelToolbar)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        wheelsRecycleView.layoutManager = recyclerViewLayoutManager
//        wheelsRecycleView.setHasFixedSize(true)
//    }
//
//    private fun setupData() {
//        wheelsRecycleView.adapter = wheelItemAdapter
//    }
//
//    private fun setupBindings() {
//        viewModel.wheelList
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeBy(onNext = {
//                when(it.status) {
//                    DataResponse.Status.LOADING -> {
//                        if (firstLoading) {
//                            wheelItemAdapter.clearItems()
//                            recyclerViewOnScrollListener.reset()
//                            showRefresh(false)
//                            showLoading(true)
//                        }
//                        if (anotherPageLoading) {
//                            wheelItemAdapter.addStatusItem(RecyclerItem.loading())
//                        }
//                    }
//                    DataResponse.Status.SUCCESS -> {
//                        supportActionBar?.title = getString(R.string.wheels)
//                        if (firstLoading) {
//                            showLoading(false)
//                            firstLoading = false
//                            if (it.data == null) {
//                                showRefresh(true, getString(R.string.wheel_list_empty))
//                                return@subscribeBy
//                            } else {
//                                wheelItemAdapter.addItems(it.data)
//                                return@subscribeBy
//                            }
//                        }
//                        if (anotherPageLoading) {
//                            anotherPageLoading = false
//                            if (it.data == null) {
//                                toastShort(getString(R.string.no_more_wheels))
//                                return@subscribeBy
//                            } else {
//                                wheelItemAdapter.addItems(it.data)
//                                return@subscribeBy
//                            }
//                        }
//                        if (itemsRefreshing) {
//                            itemsRefreshing = false
//                            wheelsSwipeRefreshLayout.isRefreshing = false
//                            recyclerViewOnScrollListener.reset()
//                            wheelItemAdapter.clearItemsWithoutNotify()
//                            if (it.data == null) {
//                                showRefresh(true, getString(R.string.wheel_list_empty))
//                                return@subscribeBy
//                            } else {
//                                wheelItemAdapter.addItems(it.data)
//                                return@subscribeBy
//                            }
//                        }
//                    }
//                    DataResponse.Status.ERROR -> {
//                        if (it.error is FatalError) {
//                            finish()
//                            return@subscribeBy
//                        }
//                        if (firstLoading || itemsRefreshing) {
//                            when (it.error) {
//                                is InternetConnectionError -> {
//                                    supportActionBar?.title = getString(R.string.wait_for_connection)
//                                    itemsRefreshing = true
//                                    viewModel.loadWheelList(0)
//                                }
//                                else -> {
//                                    wheelsSwipeRefreshLayout.isRefreshing = false
//                                    showLoading(false)
//                                    showRefresh(true, it.error?.message)
//                                    firstLoading = true
//                                }
//                            }
//                        }
//                        if (anotherPageLoading) {
//                            wheelItemAdapter.addStatusItem(RecyclerItem.error(it.error))
//                        }
//                    }
//                }
//            }).addTo(compositeDisposable)
//
//        refreshButton.setOnClickListener {
//            itemsRefreshing = true
//            viewModel.loadWheelList(0)
//        }
//
//        wheelItemAdapter.onItemAddListener = object : WheelItemAdapter.OnItemAddListener {
//            override fun onItemAdded() {
//                val firstItemPosition = recyclerViewLayoutManager.findFirstCompletelyVisibleItemPosition()
//                if (firstItemPosition == 0) {
//                    wheelsRecycleView.smoothScrollToPosition(0)
//                }
//            }
//        }
//
//        wheelItemAdapter.onMoreInfoClickListener = object : WheelItemAdapter.OnMoreInfoClickListener {
//            override fun onMoreInfoClick(position: Int) {
//                val wheel = wheelItemAdapter.getItem(position)
//                if (wheel != null) {
//                    wheelDetailsFragment = WheelDetailsFragment.newInstance(wheel)
//                    wheelDetailsFragment?.show(supportFragmentManager, WHEEL_DETAILS_FRAGMENT_TAG)
//                }
//            }
//        }
//
//        wheelItemAdapter.onRefreshItemClickListener = object : WheelItemAdapter.OnRefreshItemClickListener {
//            override fun onRefreshItemClick() {
//                anotherPageLoading = true
//                viewModel.loadLastPage()
//            }
//        }
//
//        wheelsRecycleView.setOnScrollListener(recyclerViewOnScrollListener)
//
//        wheelsSwipeRefreshLayout.setOnRefreshListener {
//            itemsRefreshing = true
//            anotherPageLoading = false
//            viewModel.loadWheelList(0)
//        }
//
//        addNewWheelFab.setOnClickListener {
//            val intent = Intent(this, EditWheelActivity::class.java).apply {
//                putExtra(WHEEL_ID_TAG, FullWheel.DEFAULT_ID)
//            }
//            startActivity(intent)
//        }
//    }
//
//    private fun showLoading(show: Boolean) {
//        if (show) {
//            addNewWheelFab.hide()
//            loadingProgressBar.visibility = View.VISIBLE
//            wheelsSwipeRefreshLayout.isEnabled = false
//        } else {
//            loadingProgressBar.visibility = View.GONE
//            addNewWheelFab.show()
//            wheelsSwipeRefreshLayout.isEnabled = true
//        }
//    }
//
//    private fun showRefresh(show: Boolean, message: String? = null) {
//        if (show) {
//            addNewWheelFab.hide()
//            wheelsSwipeRefreshLayout.visibility = View.GONE
//            refreshConstraintLayout.visibility = View.VISIBLE
//            refreshMessageTextView.text = message
//        } else {
//            refreshConstraintLayout.visibility = View.GONE
//            wheelsSwipeRefreshLayout.visibility = View.VISIBLE
//        }
//    }
//
//    private fun toastShort(message: String?) {
//        if (message != null) {
//            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
//        }
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item.itemId) {
//            android.R.id.home -> {
//                finish()
//                true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//
//        wheelDetailsFragment?.dismiss()
//        val editedWheel = WheelRepository(this).getEditedWheel()
//        if (editedWheel != null) {
//            wheelItemAdapter.addOrUpdateItem(editedWheel)
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//
//        compositeDisposable.clear()
//    }
}

































