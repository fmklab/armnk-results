package com.fmklab.armnk_results.modules.common.models

class RecyclerItem<T>(val item: T?, var status: Status, val error: Error?) {

    enum class Status {
        ITEM,
        LOADING,
        ERROR
    }

    companion object {
        fun <T> item(item: T): RecyclerItem<T> {
            return RecyclerItem(
                item,
                Status.ITEM,
                null
            )
        }
        fun <T> loading(): RecyclerItem<T> {
            return RecyclerItem(
                null,
                Status.LOADING,
                null
            )
        }
        fun <T> error(error: Error? = null): RecyclerItem<T> {
            return RecyclerItem(
                null,
                Status.ERROR,
                error
            )
        }
    }
}