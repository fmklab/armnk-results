package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.*
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class NumberDetailDto (
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("partId")
    @Expose
    var partId: Int,
    @SerializedName("controlDate")
    @Expose
    var controlDate: Date = Date(),
    @SerializedName("factory")
    @Expose
    var factory: Factory = Factory(),
    @SerializedName("factoryYear")
    @Expose
    var factoryYear: Year = Year(),
    @SerializedName("number")
    @Expose
    var number: String = "",
    @SerializedName("isNewDetail")
    @Expose
    var isNewDetail: Boolean = false,
    @SerializedName("result")
    @Expose
    var controlResult: ControlResult = ControlResult(),
    @SerializedName("isFromServicePoint")
    @Expose
    var isFromServicePoint: Boolean = false,
    @SerializedName("noRejectionCriteria")
    @Expose
    var noRejectionCriteria: Boolean = false,
    @SerializedName("spec")
    @Expose
    var spec: String = "",
    @SerializedName("defectInfo")
    @Expose
    var defectInfo: String = "",
    @SerializedName("servicePoint")
    @Expose
    var servicePoint: ServicePoint = ServicePoint(Company.NONE, Fault.NONE),
    @SerializedName("defectMoment")
    @Expose
    var defectMoment: DefectMoment = DefectMoment.NONE,
    @SerializedName("defectType")
    @Expose
    var defectType: DefectType? = null,
    @SerializedName("zone")
    @Expose
    var zone: ControlZone? = null,
    @SerializedName("defectDescription")
    @Expose
    var defectDescription: String? = null,
    @SerializedName("repairDescription")
    @Expose
    var repairDescription: String? = null,
    @SerializedName("defectLength")
    @Expose
    var defectLength: String? = null,
    @SerializedName("defectDepth")
    @Expose
    var defectDepth: String? = null,
    @SerializedName("defectDiameter")
    @Expose
    var defectDiameter: String? = null,
    @SerializedName("steal")
    @Expose
    var steal: String? = null,
    @SerializedName("owner")
    @Expose
    var owner: String? = null,
    @SerializedName("defectSize")
    @Expose
    var defectSize: String? = null,
    @SerializedName("detector")
    @Expose
    var detector: String? = null,
    @SerializedName("method")
    @Expose
    var method: String? = null
) {

    fun toNumberDetail(): NumberDetail {
        val numberDetail = NumberDetail(
            id,
            partId,
            controlDate,
            factory,
            factoryYear,
            number,
            isNewDetail,
            controlResult,
            isFromServicePoint,
            noRejectionCriteria,
            spec,
            servicePoint,
            defectMoment
        )
        if (controlResult.name == "брак") {
            numberDetail.state = when (defectMoment) {
                DefectMoment.DEFECATION -> DefecationState(defectType, zone, steal, owner, defectDescription)
                DefectMoment.VISUAL -> VisualState(defectType, zone, steal, owner, defectLength, defectDepth, defectDiameter)
                DefectMoment.NDT -> NdtState(defectType, zone, steal, owner, method, Detector(0, detector ?: "", ""), defectSize)
                else -> DefectState(defectType, zone, steal, owner)
            }
        } else if (controlResult.name == "ремонт") {
            numberDetail.state = RepairState(defectType, zone, repairDescription)
        } else {
            numberDetail.state = GoodDetailState()
        }

        return numberDetail
    }
}




































