package com.fmklab.armnk_results.modules.number_detail.models

import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.wheels.models.DefectType

class VisualState(
    defectType: DefectType?,
    zone: ControlZone?,
    steal: String?,
    owner: String?,
    defectLength: String?,
    defectDepth: String?,
    defectDiameter: String?
) :
    DefectState(defectType, zone, steal, owner) {

    constructor(state: NumberDetailState?) : this(
        state?.defectType,
        state?.zone,
        state?.steal,
        state?.owner,
        state?.defectLength,
        state?.defectDepth,
        state?.defectDiameter
    )

    override var defectLength: String? = defectLength
        get() = field

    override var defectDepth: String? = defectDepth
        get() = field

    override var defectDiameter: String? = defectDiameter
        get() = field

    override fun getDescription(): String {
        return "обнаружен: \"визуально\"; тип дефекта: \"${defectType?.name}\"; зона выявления: \"${zone?.name}\"; " +
                "L=$defectLength мм; H=$defectDepth мм; D=$defectDiameter мм; сталь: \"$steal\"; собственник: \"$owner\";"
    }
}




































