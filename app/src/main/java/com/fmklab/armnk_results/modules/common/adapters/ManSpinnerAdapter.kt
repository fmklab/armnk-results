package com.fmklab.armnk_results.modules.common.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fmklab.armnk_results.modules.auth.models.User

class ManSpinnerAdapter(context: Context, resource: Int, private val objects: Array<out User>) :
    ArrayAdapter<User>(context, resource, objects) {

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = super.getView(position, convertView, parent) as TextView
        textView.text = objects[position].toString()
        return textView
    }
}