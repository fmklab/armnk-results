package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelDefectVisualInfo (
    @SerializedName("wheelDefect")
    @Expose
    var wheelDefect: WheelDefect = WheelDefect(),
    @SerializedName("description")
    @Expose
    var description: String = "",
    @SerializedName("defectZone")
    @Expose
    var defectZone: WheelControlZone = WheelControlZone()
) {

    fun getDefectInfo(): String {
        var result = ", метод (зона): \"${defectZone.title}\", код дефекта: \"${wheelDefect.title}\""
        if (description.isNotBlank()) {
            result += ", описание: \"$description\""
        }

        return result
    }
}