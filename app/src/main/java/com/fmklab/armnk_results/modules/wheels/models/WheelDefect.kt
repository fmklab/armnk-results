package com.fmklab.armnk_results.modules.wheels.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelDefect (
    @SerializedName("id")
    @Expose
    val id: Int = DEFAULT_ID,
    @SerializedName("title")
    @Expose
    val title: String = ""
) {

    companion object {
        const val DEFAULT_ID = -1
    }

    override fun toString(): String {
        return title
    }
}