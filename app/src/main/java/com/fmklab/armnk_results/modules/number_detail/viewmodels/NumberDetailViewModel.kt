package com.fmklab.armnk_results.modules.number_detail.viewmodels

import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.modules.common.services.ArmApiService
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetailListItem
import com.fmklab.armnk_results.modules.number_detail.services.NumberDetailApiService
import io.reactivex.rxjava3.core.Observable

class NumberDetailViewModel(
    private val currentUser: CurrentUser,
    private val numberDetailApi: NumberDetailApiService,
    private val armApi: ArmApiService,
    private val partId: Int
): DetailItemsViewModel<NumberDetailListItem>() {

    override fun getLoadDetailsObservable(page: Int): Observable<ArrayList<NumberDetailListItem>> {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        return numberDetailApi.getNumberDetails(companyId, partId, page, token)
    }

    override fun getSearchDetailObservable(searchString: String): Observable<ArrayList<NumberDetailListItem>> {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        return numberDetailApi.searchDetails(companyId, partId, searchString, token)
    }
}