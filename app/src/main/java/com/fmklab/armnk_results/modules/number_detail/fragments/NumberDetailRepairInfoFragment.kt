package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_number_detail_repair_info.*
import kotlinx.android.synthetic.main.fragment_number_detail_repair_info.defectTypeAutoCompleteTextView
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NumberDetailRepairInfoFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditNumberDetailViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_repair_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupListeners()
        setupBindings()
    }

    private fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        zoneAutoCompleteTextView.keyListener = null
    }

    private fun setupBindings() {
        viewModel.repairDataLoadingState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showRepairDataLoading(it)
            }
            .addTo(compositeDisposable)

        viewModel.repairDataErrorState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Snackbar.make(repairInfoNestedScrollView, it.message as CharSequence, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.refresh) {
                        viewModel.loadRepairDataClickConsumer.accept(Unit)
                    }
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.repairDefectTypesState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.repairZonesState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                zoneAutoCompleteTextView.initArrayAdapter(requireContext(), it)
            }
            .addTo(compositeDisposable)

        viewModel.defectType
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.setText(it.toString(), false)
            }
            .addTo(compositeDisposable)

        viewModel.zone
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                zoneAutoCompleteTextView.setText(it.toString(), false)
            }
            .addTo(compositeDisposable)

        viewModel.repairDescription
            .filter { defectDescriptionEditText.text.toString() != it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectDescriptionEditText.setText(it)
            }
            .addTo(compositeDisposable)

        viewModel.emptyDefectTypeErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeTextInputLayout.error = getString(R.string.field_empty_error)
            }
            .addTo(compositeDisposable)

        viewModel.emptyZoneErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                zoneTextInputLayout.error = getString(R.string.field_empty_error)
            }
            .addTo(compositeDisposable)

        viewModel.repairDescriptionErrorState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectDescriptionTextInputLayout.error = getString(R.string.repair_info_error)
            }
            .addTo(compositeDisposable)

        defectTypeAutoCompleteTextView
            .itemSelected<DefectType>()
            .subscribeOn(Schedulers.io())
            .subscribe(viewModel.defectType)
            .addTo(compositeDisposable)

        zoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .subscribeOn(Schedulers.io())
            .subscribe(viewModel.zone)
            .addTo(compositeDisposable)

        defectDescriptionEditText
            .textChanges()
            .skipInitialValue()
            .subscribeOn(Schedulers.io())
            .subscribe { viewModel.repairDescription.accept(it.toString()) }
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        defectTypeAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                defectTypeTextInputLayout.error = null
            }
        }

        zoneAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                zoneTextInputLayout.error = null
            }
        }

        defectDescriptionEditText.doOnTextChanged { text, _, _, count ->
            if (text != null && count >= 10) {
                defectDescriptionTextInputLayout.error = null
            }
        }
    }

    private fun showRepairDataLoading(show: Boolean) {
//        if (show) {
//            defectTypeLoadingProgress.visibility = View.VISIBLE
//            defectTypeLoadingProgress.isIndeterminate = true
//            zoneLoadingProgress.visibility = View.VISIBLE
//            zoneLoadingProgress.isIndeterminate = true
//        } else {
//            defectTypeLoadingProgress.visibility = View.GONE
//            zoneLoadingProgress.visibility = View.GONE
//        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}









































