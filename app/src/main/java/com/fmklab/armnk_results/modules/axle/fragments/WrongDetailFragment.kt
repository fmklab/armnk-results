package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_wrong_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WrongDetailFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel by sharedViewModel<EditAxleViewModel>()
    private val visualAndNtdDefectFragment = VisualAndNtdDefectFragment()
    private val ntdDefectFragment = NtdDefectFragment()

    companion object {
        const val VISUAL_CONTROL = 2
        const val NDT_CONTROL = 3
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wrong_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBindings()
        setupData()
    }

    private fun setupBindings() {
        viewModel.wrongDetailInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                checkFields()
            }).addTo(compositeDisposable)
        visualAndNtdRadio.setOnCheckedChangeListener { _, isChecked ->
            val transaction = childFragmentManager.beginTransaction()
            val fragment = childFragmentManager.findFragmentByTag("VisualAndNtdFragmentTag")
            if (isChecked) {
                viewModel.editingAxle.controlType = VISUAL_CONTROL
                if (fragment != null) {
                    transaction.remove(fragment)
                }
                transaction.add(
                    R.id.detectTypeFrameLayout,
                    visualAndNtdDefectFragment,
                    "VisualAndNtdFragmentTag"
                ).commit()
            } else {
                if (fragment != null) {
                    transaction.remove(fragment).commit()
                }
            }
        }
        ntdRadio.setOnCheckedChangeListener { _, isChecked ->
            val transaction = childFragmentManager.beginTransaction()
            val fragment = childFragmentManager.findFragmentByTag("NtdDefectFragmentTag")
            if (isChecked) {
                viewModel.editingAxle.controlType = NDT_CONTROL
                if (fragment != null) {
                    transaction.remove(fragment)
                }
                transaction.add(
                    R.id.detectTypeFrameLayout,
                    ntdDefectFragment,
                    "NtdDefectFragmentTag"
                ).commit()

            } else {
                if (fragment != null) {
                    transaction.remove(fragment).commit()
                }
            }
        }
    }

    private fun setupData() {
        when (viewModel.editingAxle.controlType) {
            VISUAL_CONTROL -> visualAndNtdRadio.isChecked = true
            NDT_CONTROL -> ntdRadio.isChecked = true
            else -> visualAndNtdRadio.isChecked = true
        }
    }

    fun checkFields(): Boolean {
        return when (viewModel.editingAxle.controlType) {
            VISUAL_CONTROL -> visualAndNtdDefectFragment.checkFields()
            NDT_CONTROL -> ntdDefectFragment.checkFields()
            else -> false
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}