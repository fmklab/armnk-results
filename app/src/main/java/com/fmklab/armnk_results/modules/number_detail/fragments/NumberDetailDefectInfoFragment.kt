package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.number_detail.models.DefectMoment
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.jakewharton.rxbinding4.widget.checkedChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_defect_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NumberDetailDefectInfoFragment: Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel: EditNumberDetailViewModel by sharedViewModel()
    private val numberDetailDefecationFragment = NumberDetailDefecationFragment()
    private val numberDetailVisualFragment = NumberDetailVisualFragment()
    private val numberDetailNdtFragment = NumberDetailNdtFragment()

    companion object {
        private const val DEFECATION_FRAGMENT_TAG = "DefecationFragmentTag"
        private const val VISUAL_FRAGMENT_TAG = "VisualFragmentTag"
        private const val NDT_FRAGMENT_TAG = "NdtFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_defect_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        setupListeners()
//        setupBindings()
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)

        if (menuVisible) {
            setupBindings()
        }
    }

    private fun setupBindings() {
        viewModel.defectMoment
            .distinct()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    DefectMoment.DEFECATION -> defecationRadio.isChecked = true
                    DefectMoment.VISUAL -> visualRadio.isChecked = true
                    DefectMoment.NDT -> ndtRadio.isChecked = true
                    else -> visualRadio.isChecked = true
                }
            }
            .addTo(compositeDisposable)

        defecationRadio
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                if (it) {
                    viewModel.defectMoment.accept(DefectMoment.DEFECATION)
                    viewModel.loadDefectData.accept(Unit)
                    loadFragment(DefectMoment.DEFECATION, numberDetailDefecationFragment, DEFECATION_FRAGMENT_TAG)
                } else {
                    unloadFragment(DEFECATION_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

        visualRadio
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                if (it) {
                    viewModel.defectMoment.accept(DefectMoment.VISUAL)
                    viewModel.loadDefectData.accept(Unit)
                    loadFragment(DefectMoment.VISUAL, numberDetailVisualFragment, VISUAL_FRAGMENT_TAG)
                } else {
                    unloadFragment(VISUAL_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

        ndtRadio
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                if (it) {
                    viewModel.defectMoment.accept(DefectMoment.NDT)
                    viewModel.loadDefectData.accept(Unit)
                    viewModel.loadMethods.accept(Unit)
                    viewModel.loadDetectors.accept(Unit)
                    loadFragment(DefectMoment.NDT, numberDetailNdtFragment, NDT_FRAGMENT_TAG)
                } else {
                    unloadFragment(NDT_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

    }

    private fun loadFragment(defectMoment: DefectMoment, fragment: Fragment, fragmentTag: String) {
        val transaction = parentFragmentManager.beginTransaction()
        val currentFragment = parentFragmentManager.findFragmentByTag(fragmentTag)
        if (currentFragment != null) {
            transaction.remove(currentFragment)
        }
        transaction.add(R.id.defectInfoFrameLayout, fragment, fragmentTag).commit()
    }

    private fun unloadFragment(fragmentTag: String) {
        val transaction = parentFragmentManager.beginTransaction()
        val fragment = parentFragmentManager.findFragmentByTag(fragmentTag)
        if (fragment != null) {
            transaction.remove(fragment).commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}

































