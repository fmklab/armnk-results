package com.fmklab.armnk_results.modules.number_detail.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ServicePoint (
    @SerializedName("company")
    @Expose
    var company: Company,
    @SerializedName("fault")
    @Expose
    var fault: Fault
)