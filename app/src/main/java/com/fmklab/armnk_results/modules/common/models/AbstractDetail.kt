package com.fmklab.armnk_results.modules.common.models

abstract class AbstractDetail {

    abstract var id: Int
    protected set
}