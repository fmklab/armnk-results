package com.fmklab.armnk_results.modules.common.extensions

import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import com.fmklab.armnk_results.modules.common.ApiErrorHandler
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.google.android.material.datepicker.MaterialDatePicker
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.Relay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.schedulers.Schedulers
import java.net.ConnectException
import java.util.*


fun Observable<Throwable>.catchConnectException(): Observable<Boolean> {
    return this.flatMap { throwable ->
        if (throwable is ConnectException) {
            Observable.just(true)
        } else {
            Observable.error<Boolean>(throwable)
        }
    }
}

fun <T> Observable<T>.apiQuery(): Observable<DataResponse<T>> where T: Any {
    return this
        .subscribeOn(Schedulers.io())
        .map { data -> DataResponse.success(data) }
        .onErrorResumeNext { t ->
            Observable.just(DataResponse.error(ApiErrorHandler.getError(t)))
        }
}

fun <T> Observable<T>.apiQuery(noInternetConnection: BehaviorRelay<Boolean>): Observable<DataResponse<T>> where T: Any{
    return this
        .subscribeOn(Schedulers.io())
        .map { data -> DataResponse.success(data) }
        .retryWhen { observable ->
            observable.catchConnectException().map { isConnectException ->
                noInternetConnection.accept(isConnectException)
            }
        }
        .onErrorResumeNext { t ->
            Observable.just(DataResponse.error(ApiErrorHandler.getError(t)))
        }
        .doOnNext { noInternetConnection.accept(false) }
}

fun <T> Observable<DataResponse<T>>.subscribeDataResponse(relaySuccess: Relay<T>, relayError: Relay<Error>): Disposable where T: Any {
    return this.subscribe { data ->
        if (data.status == DataResponse.Status.SUCCESS) {
            relaySuccess.accept(data.data)
        } else if (data.status == DataResponse.Status.ERROR) {
            relayError.accept(data.error)
        }
    }
}


inline fun <reified T: Any> AutoCompleteTextView.itemSelected(): Observable<T> {
    return Observable.create { emitter ->
        setOnItemClickListener { parent, _, position, _ ->
            val item = parent.getItemAtPosition(position) as T
            emitter.onNext(item)
        }
    }
}

fun MaterialDatePicker<Long>.dateChanged(): Observable<Date> {
    return Observable.create { emitter ->
        addOnPositiveButtonClickListener {
            val date = it.toDate()
            emitter.onNext(date)
        }
    }
}


































