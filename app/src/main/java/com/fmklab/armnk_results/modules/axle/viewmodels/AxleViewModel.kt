package com.fmklab.armnk_results.modules.axle.viewmodels

import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.modules.axle.services.AxleApiService
import com.fmklab.armnk_results.modules.axle.models.Axle
import com.fmklab.armnk_results.modules.common.viewmodels.DetailItemsViewModel
import io.reactivex.rxjava3.core.Observable

class AxleViewModel(private val currentUser: CurrentUser, private val axleApiService: AxleApiService) :
    DetailItemsViewModel<Axle>() {

    init {

    }

    override fun getLoadDetailsObservable(page: Int): Observable<ArrayList<Axle>> {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        return axleApiService.getAxleList(companyId, page, token)
    }

    override fun getSearchDetailObservable(searchString: String): Observable<ArrayList<Axle>> {
        val token = currentUser.getAccessToken()
        val companyId = currentUser.getUser().company.id
        return axleApiService.searchAxleInList(companyId, searchString, token)
    }
}

























