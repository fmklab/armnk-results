package com.fmklab.armnk_results.modules.wheels.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.models.AxleModel
import com.fmklab.armnk_results.modules.axle.models.AxleModelSelectable
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.axle_search_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AxleSearchItemAdapter: RecyclerView.Adapter<AxleSearchItemViewHolder>(), AxleSearchItemViewHolder.OnAxleSelectedListener {

    private val items: ArrayList<AxleModelSelectable> = ArrayList()
    private var lastSelectedPosition: Int = -1
    var onAxleSelectedListener: AxleSearchItemViewHolder.OnAxleSelectedListener? = null


    fun setItems(items: ArrayList<AxleModel>, lastSelectedAxle: AxleModel? = null) {
        this.items.clear()
        for (item in items) {
            val selectedAxle = AxleModelSelectable(item)
            if (lastSelectedAxle?.id == item.id) {
                selectedAxle.isSelected = true
                selectedAxle.axleModel.isNewWheel1 = lastSelectedAxle.isNewWheel1
                selectedAxle.axleModel.oldWheelId = lastSelectedAxle.oldWheelId
                selectedAxle.axleModel.oldWheelNumber = lastSelectedAxle.oldWheelNumber
                if (lastSelectedAxle.isNewWheel1 == true) {
                    selectedAxle.axleModel.wheel1Id = lastSelectedAxle.wheel1Id
                    selectedAxle.axleModel.wheel1Number = lastSelectedAxle.wheel1Number
                } else if (lastSelectedAxle.isNewWheel1 == false) {
                    selectedAxle.axleModel.wheel2Id = lastSelectedAxle.wheel2Id
                    selectedAxle.axleModel.wheel2Number = lastSelectedAxle.wheel2Number
                }
            }
            this.items.add(selectedAxle)
        }
        notifyDataSetChanged()
    }

    fun getItem(position: Int): AxleModel? {
        if (position > items.size - 1) {
            return null
        }
        return items[position].axleModel
    }

    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AxleSearchItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.axle_search_item, parent, false)
        return AxleSearchItemViewHolder(view, this)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AxleSearchItemViewHolder, position: Int) {
        val item = items[position]
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        holder.item = item
        holder.itemView.controlDateValueTextView.text = simpleDateFormat.format(item.axleModel.controlDate)
        holder.itemView.axleNumberValueTextView.text = item.axleModel.axleNumber
        holder.itemView.factoryNameValueTextView.text = item.axleModel.factory?.code ?: "-"
        holder.itemView.factoryYearValueTextView.text = item.axleModel.factoryYear?.toString() ?: "-"
        holder.itemView.wheelNumberValueTextView.text = "${item.axleModel.wheel1Number} / ${item.axleModel.wheel2Number}"
        val cardView = holder.itemView as? MaterialCardView
        if (cardView?.isChecked == false && item.isSelected || cardView?.isChecked == true && !item.isSelected) {
            cardView.toggle()
        }
    }

    override fun onAxleSelected(axle: AxleModelSelectable, position: Int) {
        if (axle.isSelected) {
            for (item in items) {
                if (item != axle && item.isSelected) {
                    item.isSelected = false
                    notifyItemChanged(items.indexOf(item))
                    onAxleDeselected(item, items.indexOf(item))
                }
            }
        }
        onAxleSelectedListener?.onAxleSelected(axle, position)
    }

    override fun onAxleDeselected(axle: AxleModelSelectable, position: Int) {
        val item = items.find { itm -> itm == axle }
        if (item != null) {
            if (item.axleModel.isNewWheel1 == true) {
                item.axleModel.wheel1Id = item.axleModel.oldWheelId
                item.axleModel.wheel1Number = item.axleModel.oldWheelNumber ?: ""
                item.axleModel.isNewWheel1 = null
            } else if (item.axleModel.isNewWheel1 == false) {
                item.axleModel.wheel2Id = item.axleModel.oldWheelId
                item.axleModel.wheel2Number = item.axleModel.oldWheelNumber ?: ""
                item.axleModel.isNewWheel1 = null
            }
            item.isSelected = false
            notifyItemChanged(items.indexOf(item))
        }
        onAxleSelectedListener?.onAxleDeselected(axle, position)
    }
}





































