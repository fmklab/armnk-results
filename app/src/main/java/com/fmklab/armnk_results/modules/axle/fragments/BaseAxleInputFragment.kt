package com.fmklab.armnk_results.modules.axle.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.activities.EditAxleActivity
import com.fmklab.armnk_results.modules.common.adapters.FactoryNameSpinnerAdapter
import com.fmklab.armnk_results.modules.common.models.AxleError
import com.fmklab.armnk_results.modules.common.models.BaseInfoError
import com.fmklab.armnk_results.modules.axle.models.FullAxle
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.adapters.AxleAndWheelTypeSpinnerAdapter
import com.fmklab.armnk_results.modules.common.models.AxleAndWheelType
import com.fmklab.armnk_results.modules.common.models.Factory
import com.google.android.material.datepicker.MaterialDatePicker
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_base_axle_input.*
import kotlinx.android.synthetic.main.fragment_base_axle_input.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BaseAxleInputFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    private val viewModel by sharedViewModel<EditAxleViewModel>()
    private lateinit var materialDatePicker: MaterialDatePicker<Long>

    companion object {
        private const val MATERIAL_DATE_PICKER_TAG = "MaterialDatePickerTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (view != null) {
            view
        } else {
            val view = inflater.inflate(R.layout.fragment_base_axle_input, container, false)
            view.baseInfoView.visibility = View.GONE
            view
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBindings()
    }

    private fun setupUI() {
        controlDateEditText.keyListener = null
        factoryNameAutoCompleteTextView.keyListener = null
        factoryYearAutoCompleteTextView.keyListener = null
        axleTypeAutocompleteTextView.keyListener = null

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText(getString(R.string.select_date))
        materialDatePicker = builder.build()
    }

    private fun setupBindings() {
        viewModel.axleFullInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false, null)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (viewModel.editingAxle.controlZones.isEmpty() && it.data != null && it.data.repairType != FullAxle.DEFAULT_REPAIR_TYPE) {
                            viewModel.loadDefectAfterInfo()
                        } else {
                            viewModel.loadBaseInputInfo()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectAfterInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        showRefresh(false, null)
                        showLoading(true)
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (it.data != null && viewModel.editingAxle.controlZones.size == 0) {
                            viewModel.editingAxle.controlZones = ArrayList(it.data.controlZones)
                            if (viewModel.editingAxle.selectedControlZonesId.size != 0) {
                                for (controlZone in viewModel.editingAxle.controlZones) {
                                    controlZone.checked =
                                        viewModel.editingAxle.selectedControlZonesId.any { z -> z == controlZone.fieldName }
                                }
                                viewModel.editingAxle.selectedControlZonesId.clear()
                            }
                        }
                        viewModel.loadBaseInputInfo()
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleBaseInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.SUCCESS -> {
                        showLoading(false)
                        if (it.data == null) {
                            return@subscribeBy
                        }
                        FactoryNameSpinnerAdapter(
                            requireActivity(),
                            R.layout.support_simple_spinner_dropdown_item,
                            it.data.factories.toTypedArray()
                        ).also { adapter ->
                            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                            factoryNameAutoCompleteTextView.setAdapter(adapter)
                        }
                        ArrayAdapter(
                            requireActivity(),
                            R.layout.support_simple_spinner_dropdown_item,
                            it.data.years
                        ).also { adapter ->
                            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                            factoryYearAutoCompleteTextView.setAdapter(adapter)
                        }
                        AxleAndWheelTypeSpinnerAdapter(
                            requireActivity(),
                            R.layout.support_simple_spinner_dropdown_item,
                            it.data.types.toTypedArray()
                        ).also { adapter ->
                            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                            axleTypeAutocompleteTextView.setAdapter(adapter)
                        }
                        viewModel.setSpec()
                        setupData()
                    }
                    DataResponse.Status.ERROR -> {
                        showLoading(false)
                        showRefresh(true, it.error)
                    }
                    DataResponse.Status.LOADING -> {
                        showRefresh(false, null)
                        showLoading(true)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.baseInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                checkBaseAxleInputFields()
            }).addTo(compositeDisposable)

        controlDateEditText.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (!parentFragmentManager.fragments.any { frg -> frg.tag == MATERIAL_DATE_PICKER_TAG }) {
                    materialDatePicker.show(parentFragmentManager, MATERIAL_DATE_PICKER_TAG)
                }
                return@setOnTouchListener true
            }
            false
        }

        materialDatePicker.addOnPositiveButtonClickListener {
            val timeZone = TimeZone.getDefault()
            val offsetFromUTC = timeZone.getOffset(Date().time) * -1
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            val date = Date(it + offsetFromUTC)
            controlDateEditText.setText(simpleDateFormat.format(date))
            viewModel.editingAxle.controlDate = date
        }

        controlDateEditText.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
//                val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.US)
//                viewModel.editingAxle.controlDate = simpleDateFormat.parse(text.toString())!!
                controlDateTextInputLayout.error = null
            }
        }
        factoryNameAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                factoryNameTextInputLayout.error = null
            }
        }
        factoryNameAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val factory = factoryNameAutoCompleteTextView.adapter.getItem(position) as Factory
            Log.d("BaseInfo", "Old ID ${viewModel.editingAxle.factory.id}")
            viewModel.editingAxle.factory = factory
            Log.d("BaseInfo", "New ID ${factory.id}")
        }
        factoryYearAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                factoryYearTextInputLayout.error = null
            }
        }
        factoryYearAutoCompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val year = factoryYearAutoCompleteTextView.adapter.getItem(position) as String
            viewModel.editingAxle.factoryYear = year
        }
        axleTypeAutocompleteTextView.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                axleTypeTextInputLayout.error = null
            }
        }
        axleTypeAutocompleteTextView.setOnItemClickListener { _, _, position, _ ->
            val type = axleTypeAutocompleteTextView.adapter.getItem(position) as AxleAndWheelType
            viewModel.editingAxle.type = type
            viewModel.editingAxle.controlZones.clear()
        }
        axleNumberEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.editingAxle.axleNumber = text.toString()
            if (!text.isNullOrBlank()) {
                axleNumberTextInputLayout.error = null
            }
        }

        refreshButton.setOnClickListener {
            viewModel.loadBaseInputInfo()
        }
        controlInfoCheckBox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.editingAxle.defectBeforeNdt = FullAxle.DEFECT_BEFORE_CONTROL
                viewModel.editingAxle.result = getString(R.string.bad_detail)
                viewModel.editingAxle.controlInfo = "ось забракована до НК"
            } else {
                viewModel.editingAxle.defectBeforeNdt = FullAxle.DEFECT_AFTER_CONTROL
                viewModel.editingAxle.result = ""
            }
            val activity = requireActivity() as? EditAxleActivity
            activity?.changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
        }
        isNewDetailCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.editingAxle.isNewDetail = isChecked
        }
    }

    private fun setupData() {
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.US)
        controlDateEditText.setText(simpleDateFormat.format(viewModel.editingAxle.controlDate))
        isNewDetailCheckBox.isChecked = viewModel.editingAxle.isNewDetail
        if (viewModel.editingAxle.id != FullAxle.DEFAULT_ID) {
            factoryNameAutoCompleteTextView.setText(viewModel.editingAxle.factory.toString(), false)
            factoryYearAutoCompleteTextView.setText(
                viewModel.editingAxle.factoryYear,
                false
            )
            axleTypeAutocompleteTextView.setText(viewModel.editingAxle.type.name, false)
            axleNumberEditText.setText(viewModel.editingAxle.axleNumber)
            when (viewModel.editingAxle.defectBeforeNdt) {
                FullAxle.DEFECT_AFTER_CONTROL -> controlInfoCheckBox.isChecked = false
                FullAxle.DEFECT_BEFORE_CONTROL -> controlInfoCheckBox.isChecked = true
            }
        }
    }

    fun checkBaseAxleInputFields(): Boolean {
        var result = true
        if (controlDateEditText.text.isNullOrEmpty()) {
            controlDateTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (factoryNameAutoCompleteTextView.text.isNullOrEmpty()) {
            factoryNameTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (factoryYearAutoCompleteTextView.text.isNullOrEmpty()) {
            factoryYearTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (axleTypeAutocompleteTextView.text.isNullOrEmpty()) {
            axleTypeTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }
        if (axleNumberEditText.text.isNullOrBlank()) {
            axleNumberTextInputLayout.error = getString(R.string.field_empty_error)
            result = false
        }

        return result
    }

    private fun toast(message: String?) {
        if (message != null) {
            Toast.makeText(requireActivity(), message, Toast.LENGTH_LONG).show()
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            loadingProgressBar.visibility = View.VISIBLE
            baseInfoView.visibility = View.GONE
        } else {
            loadingProgressBar.visibility = View.GONE
            baseInfoView.visibility = View.VISIBLE
        }
    }

    private fun showRefresh(show: Boolean, error: Error?) {
        if (show) {
            if (error is AxleError) {
                refreshButton.setOnClickListener {
                    viewModel.loadFullAxle(error.axleId)
                }
            }
            if (error is BaseInfoError) {
                refreshButton.setOnClickListener {
                    viewModel.loadBaseInputInfo()
                }
            }
            baseInfoView.visibility = View.GONE
            refreshButton.visibility = View.VISIBLE
            errorMessageTextView.visibility = View.VISIBLE
            errorMessageTextView.text = error?.message
        } else {
            refreshButton.visibility = View.GONE
            errorMessageTextView.visibility = View.GONE
            baseInfoView.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }
}





























