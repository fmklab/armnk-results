package com.fmklab.armnk_results.modules.axle.activities

import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.controls.MenuFloatingActionButton
import com.fmklab.armnk_results.framework.controls.SavingDialog
import com.fmklab.armnk_results.modules.axle.adapters.AxleEditPagerAdapter
import com.fmklab.armnk_results.modules.axle.fragments.MiddleRepairFragment
import com.fmklab.armnk_results.modules.axle.fragments.RepairTypeFragment
import com.fmklab.armnk_results.modules.axle.models.FullAxle
import com.fmklab.armnk_results.modules.axle.repositories.AxleRepository
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.CheckAxleBaseInfoResponse
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.modules.common.models.EditViewModelState
import com.fmklab.armnk_results.modules.common.models.SaveRepeatDetailError
import com.google.android.material.chip.Chip
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.gordonwong.materialsheetfab.MaterialSheetFab
import com.gordonwong.materialsheetfab.MaterialSheetFabEventListener
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_axle.*
import kotlinx.android.synthetic.main.fragment_base_axle_input.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.properties.Delegates


class EditAxleActivity : AppCompatActivity() {

    private var compositeDisposable = CompositeDisposable()
    private val axleEditPagerAdapter: AxleEditPagerAdapter by lifecycleScope.inject {
        parametersOf(supportFragmentManager)
    }
    private val viewModel: EditAxleViewModel by lifecycleScope.viewModel(this)
    private var repairTypeFragment: RepairTypeFragment by Delegates.notNull()
    private lateinit var menuFloatingActionButton: MaterialSheetFab<MenuFloatingActionButton>
    private var loadingOrError = true
    private val savingDialog = SavingDialog(this)

    companion object {
        const val BASE_INPUTS = 0
        const val DEFECT_BEFORE_CONTROL_INPUTS = 1
        const val DEFECT_AFTER_CONTROL_INPUTS = 2
        const val WRONG_DETAIL_INPUTS = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_axle)
        viewModel

        if (savedInstanceState != null) {
            val json = savedInstanceState.getString("EditingAxle")
            val axle = Gson().fromJson(json, FullAxle::class.java)
            val viewModelJson = savedInstanceState.getString("ViewModelState")
            val viewModelState = Gson().fromJson(viewModelJson, EditViewModelState::class.java)
            viewModel.editingAxle = axle
            viewModel.viewModelState = viewModelState
            viewModel.axleFullInfo.onNext(DataResponse.success(axle))
        } else {
            val axleId = intent.extras?.getInt("axleId")
            viewModel.loadFullAxle(axleId)
        }
        setupUI()
        setupBindings()
    }

    private fun setupUI() {
        setSupportActionBar(editAxleToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        repairTypeFragment = RepairTypeFragment()
        editAxleViewPager.adapter = axleEditPagerAdapter
        editAxleViewPager.setOnTouchListener { _, _ ->
            return@setOnTouchListener true
        }
        menuFloatingActionButton = MaterialSheetFab(
            goodBadDetailFab,
            fabSheet,
            overlay,
            Color.TRANSPARENT,
            R.color.colorPrimary
        )
    }

    private fun setupBindings() {
        repairTypeFragment.selectedRepairType
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                repairTypeFragment.dismiss()
                viewModel.editingAxle.controlZones.clear()
                when (it) {
                    RepairTypeFragment.CURRENT_REPAIR -> {
                        viewModel.editingAxle.repairType = it
                        viewModel.editingAxle.rings = MiddleRepairFragment.NONE_RINGS
                        viewModel.loadDefectAfterInfo()
                        editAxleViewPager.setCurrentItem(
                            DEFECT_AFTER_CONTROL_INPUTS,
                            true
                        )
                    }
                    RepairTypeFragment.MIDDLE_REPAIR_WITH_RINGS -> {
                        viewModel.editingAxle.repairType = it % 3
                        viewModel.editingAxle.rings = MiddleRepairFragment.WITH_RINGS
                        viewModel.loadDefectAfterInfo()
                        editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
                    }
                    RepairTypeFragment.MIDDLE_REPAIR_WITHOUT_RINGS -> {
                        viewModel.editingAxle.repairType = it % 3
                        viewModel.editingAxle.rings = MiddleRepairFragment.WITHOUT_RINGS
                        viewModel.loadDefectAfterInfo()
                        editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
                    }
                    RepairTypeFragment.CAPITAL_REPAIR -> {
                        viewModel.editingAxle.repairType = it
                        viewModel.editingAxle.rings = MiddleRepairFragment.NONE_RINGS
                        viewModel.loadDefectAfterInfo()
                        editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleBaseInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
                        nextAxleInputsFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == BASE_INPUTS) {
                            loadingOrError = false
                            nextAxleInputsFab.show()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                            changeAxleResult()
                            checkRepairType()
                            if (it.data!!.isNewDetailEnabled) {
                                isNewDetailCheckBox.visibility = View.VISIBLE
                            } else {
                                isNewDetailCheckBox.visibility = View.GONE
                            }
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        nextAxleInputsFab.hide()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectBeforeControlType
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
                        saveAxleFab.hide()
                        prevAxleInputsFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == DEFECT_BEFORE_CONTROL_INPUTS) {
                            loadingOrError = false
                            saveAxleFab.show()
                            prevAxleInputsFab.show()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        saveAxleFab.hide()
                        prevAxleInputsFab.hide()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectBeforeZone
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
                        saveAxleFab.hide()
                        prevAxleInputsFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == DEFECT_BEFORE_CONTROL_INPUTS) {
                            loadingOrError = false
                            saveAxleFab.show()
                            prevAxleInputsFab.show()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        saveAxleFab.hide()
                        prevAxleInputsFab.hide()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectBeforeCode
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
                        saveAxleFab.hide()
                        prevAxleInputsFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == DEFECT_BEFORE_CONTROL_INPUTS) {
                            loadingOrError = false
                            saveAxleFab.show()
                            prevAxleInputsFab.show()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        saveAxleFab.hide()
                        prevAxleInputsFab.hide()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectAfterInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
                        prevAxleInputsFab.hide()
                        editControlInfoFab.hide()
                        menuFloatingActionButton.hideSheetThenFab()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == DEFECT_AFTER_CONTROL_INPUTS) {
                            loadingOrError = false
                            prevAxleInputsFab.show()
                            editControlInfoFab.show()
                            menuFloatingActionButton.showFab()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                            changeAxleResult()
                            checkRepairType()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        prevAxleInputsFab.hide()
                        editControlInfoFab.hide()
                        menuFloatingActionButton.hideSheetThenFab()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectVisualInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
//                        prevAxleInputsFab.hide()
//                        saveAxleFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == WRONG_DETAIL_INPUTS) {
                            loadingOrError = false
                            prevAxleInputsFab.show()
                            saveAxleFab.show()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                            changeAxleResult()
                            checkRepairType()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        prevAxleInputsFab.hide()
                        saveAxleFab.hide()
                        menuFloatingActionButton.hideSheetThenFab()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.axleDefectNdtInfo
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        loadingOrError = true
//                        prevAxleInputsFab.hide()
//                        saveAxleFab.hide()
                    }
                    DataResponse.Status.SUCCESS -> {
                        if (editAxleViewPager.currentItem == WRONG_DETAIL_INPUTS) {
                            loadingOrError = false
                            prevAxleInputsFab.show()
                            saveAxleFab.show()
                            changeDefectBeforeNdt(viewModel.editingAxle.defectBeforeNdt)
                            changeAxleResult()
                            checkRepairType()
                        }
                    }
                    DataResponse.Status.ERROR -> {
                        loadingOrError = true
                        prevAxleInputsFab.hide()
                        saveAxleFab.hide()
                        menuFloatingActionButton.hideSheetThenFab()
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.baseInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    CheckAxleBaseInfoResponse.Status.REPAIR_TYPE -> {
                        repairTypeFragment.show(supportFragmentManager, "RepairTypeFragmentTag")
                    }
                    else -> {
                        editAxleViewPager.setCurrentItem(BASE_INPUTS, true)
                    }
                }
            }).addTo(compositeDisposable)
        viewModel.defectBeforeControlInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                editAxleViewPager.setCurrentItem(DEFECT_BEFORE_CONTROL_INPUTS, true)
            }).addTo(compositeDisposable)
        viewModel.defectAfterControlInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
            }).addTo(compositeDisposable)
        viewModel.wrongDetailInfoError
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it != "") {
                    showToast(it)
                }
                editAxleViewPager.setCurrentItem(WRONG_DETAIL_INPUTS, true)
            }).addTo(compositeDisposable)
        viewModel.saveGoodDetailMessage
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                MaterialAlertDialogBuilder(this)
                    .setTitle(getString(R.string.save))
                    .setMessage(it)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        viewModel.editingAxle.result = "годен"
                        viewModel.editingAxle.defectInfo = ""
                        viewModel.save()
                    }
                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                        if (menuFloatingActionButton.isSheetVisible) {
                            menuFloatingActionButton.hideSheet()
                        }
                    }.show()
            }).addTo(compositeDisposable)
        viewModel.saveBadDetailMessage
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                MaterialAlertDialogBuilder(this)
                    .setTitle(getString(R.string.save))
                    .setMessage(it)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        viewModel.save()
                    }
                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                        if (menuFloatingActionButton.isSheetVisible) {
                            menuFloatingActionButton.hideSheet()
                        }
                    }.show()
            }).addTo(compositeDisposable)
        viewModel.axleSaved
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                when (it.status) {
                    DataResponse.Status.LOADING -> {
                        savingDialog.startSavingDialog()
                    }
                    DataResponse.Status.SUCCESS -> {
                        savingDialog.dismissDialog()
                        showToast(getString(R.string.success))
                        AxleRepository(this).saveEditedAxle(viewModel.editingAxle)
                        finish()
                    }
                    DataResponse.Status.ERROR -> {
                        when (it.error) {
                            is SaveRepeatDetailError -> MaterialAlertDialogBuilder(this)
                                .setTitle(getString(R.string.attention))
                                .setMessage(it.error.message)
                                .setPositiveButton("Продолжить") { _, _ ->
                                    viewModel.saveAnyway(it.error.date)
                                }
                                .setNegativeButton("Отмена", null)
                                .show()
                            else -> {
                                showToast("${getString(R.string.saving_error)}. ${it.error?.message}")
                            }
                        }
                        savingDialog.dismissDialog()
                    }
                }
            }).addTo(compositeDisposable)

        nextAxleInputsFab.setOnClickListener {
            when (editAxleViewPager.currentItem) {
                0 -> {
                    if (axleEditPagerAdapter.checkBaseInputFields()) {
                        if ((viewModel.editingAxle.repairType == FullAxle.DEFAULT_REPAIR_TYPE ||
                            (viewModel.editingAxle.repairType == RepairTypeFragment.MIDDLE_REPAIR && viewModel.editingAxle.rings == FullAxle.DEFAULT_RINGS))
                            && viewModel.editingAxle.defectBeforeNdt == FullAxle.DEFECT_AFTER_CONTROL) {
                            repairTypeFragment.show(supportFragmentManager, "RepairTypeFragmentTag")
                        } else if (viewModel.editingAxle.repairType != FullAxle.DEFAULT_REPAIR_TYPE && viewModel.editingAxle.defectBeforeNdt == FullAxle.DEFECT_AFTER_CONTROL) {
                            editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
                        } else if (viewModel.editingAxle.defectBeforeNdt == FullAxle.DEFECT_BEFORE_CONTROL) {
                            editAxleViewPager.setCurrentItem(DEFECT_BEFORE_CONTROL_INPUTS, true)
                        }
                    }
                }
            }
        }
        prevAxleInputsFab.setOnClickListener {
            when (editAxleViewPager.currentItem) {
                DEFECT_BEFORE_CONTROL_INPUTS -> {
                    editAxleViewPager.setCurrentItem(BASE_INPUTS, true)
                }
                DEFECT_AFTER_CONTROL_INPUTS -> {
                    editAxleViewPager.setCurrentItem(BASE_INPUTS, true)
                }
                WRONG_DETAIL_INPUTS -> {
                    editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
                }
            }
        }
        goodDetailButton.setOnClickListener {
            viewModel.editingAxle.result = "годен"
            viewModel.attemptSave()
//            if (axleEditPagerAdapter.checkDefectAfterControlInputFields()) {
//                viewModel.editingAxle.result = "годен"
//                changeAxleResult()
//                MaterialAlertDialogBuilder(this)
//                    .setTitle(getString(R.string.save))
//                    .setMessage("${getString(R.string.save_good_axle_dialog)}\n\n${getString(R.string.control_info)}\n\n${viewModel.editingAxle.controlInfo}")
//                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
//
//                    }
//                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
//                        if (menuFloatingActionButton.isSheetVisible) {
//                            menuFloatingActionButton.hideSheet()
//                        }
//                    }.show()
//            }
        }
        badDetailButton.setOnClickListener {
            if (axleEditPagerAdapter.checkDefectAfterControlInputFields()) {
                viewModel.editingAxle.result = "брак"
                changeAxleResult()
                editAxleViewPager.setCurrentItem(WRONG_DETAIL_INPUTS, true)
                editControlInfoFab.hide()
//                viewModel.loadAxleDefectResult()
            }
        }
        menuFloatingActionButton.setEventListener(object : MaterialSheetFabEventListener() {
            override fun onShowSheet() {
                editControlInfoFab.hide()
                prevAxleInputsFab.isEnabled = false
            }

            override fun onHideSheet() {
                prevAxleInputsFab.isEnabled = true
                if (editAxleViewPager.currentItem == DEFECT_AFTER_CONTROL_INPUTS) {
                    editControlInfoFab.show()
                }
            }
        })
        editControlInfoFab.setOnClickListener {
            repairTypeFragment.show(supportFragmentManager, "RepairTypeFragmentTag")
        }
        editAxleViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                if (state != 0) {
                    prevAxleInputsFab.hide()
                    editControlInfoFab.hide()
                    nextAxleInputsFab.hide()
                    menuFloatingActionButton.hideSheetThenFab()
                    saveAxleFab.hide()
                }
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (positionOffset == 0f) {
                    when (position) {
                        BASE_INPUTS -> {
                            supportActionBar?.title = getString(R.string.base_axle_input_title)
                            if (!loadingOrError) {
                                nextAxleInputsFab.show()
                            }
                        }
                        DEFECT_BEFORE_CONTROL_INPUTS -> {
                            supportActionBar?.title =
                                getString(R.string.defect_before_control_title)
                            if (!loadingOrError) {
                                saveAxleFab.show()
                                prevAxleInputsFab.show()
                            }
                        }
                        DEFECT_AFTER_CONTROL_INPUTS -> {
                            supportActionBar?.title = getString(R.string.defect_after_control_title)
                            if (!loadingOrError) {
                                prevAxleInputsFab.show()
                                editControlInfoFab.show()
                                menuFloatingActionButton.showFab()
                            }
                        }
                        WRONG_DETAIL_INPUTS -> {
                            supportActionBar?.title = getString(R.string.wrong_detail_title)
                            saveAxleFab.show()
                            prevAxleInputsFab.show()
                            menuFloatingActionButton.hideSheetThenFab()
                        }
                    }
                }
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    BASE_INPUTS -> baseChip.isChecked = true
                    DEFECT_BEFORE_CONTROL_INPUTS -> defectBeforeControlChip.isChecked = true
                    DEFECT_AFTER_CONTROL_INPUTS -> defectAfterControlChip.isChecked = true
                    WRONG_DETAIL_INPUTS -> wrongDetailChip.isChecked = true
                }
            }
        })
        saveAxleFab.setOnClickListener {
            viewModel.attemptSave()
        }
        inputsChipGroup.setOnCheckedChangeListener { _, checkedId ->
            val chip = inputsChipGroup.findViewById<Chip>(checkedId) ?: return@setOnCheckedChangeListener
            val scrollBounds = Rect()
            inputsChipsHorizontalScrollView.getHitRect(scrollBounds)
            if (!chip.getLocalVisibleRect(scrollBounds) || scrollBounds.width() < chip.width) {
                inputsChipsHorizontalScrollView.smoothScrollTo(chip.left - chip.paddingLeft, chip.top)
            }
        }
        baseChip.setOnClickListener {
            if (baseChip.isChecked) {
                editAxleViewPager.setCurrentItem(BASE_INPUTS, true)
            } else {
                baseChip.isChecked = true
            }
        }
        defectBeforeControlChip.setOnClickListener {
            if (defectBeforeControlChip.isChecked) {
                editAxleViewPager.setCurrentItem(DEFECT_BEFORE_CONTROL_INPUTS, true)
            } else {
                defectBeforeControlChip.isChecked = true
            }
        }
        defectAfterControlChip.setOnClickListener {
            if (defectAfterControlChip.isChecked) {
                editAxleViewPager.setCurrentItem(DEFECT_AFTER_CONTROL_INPUTS, true)
            } else {
                defectAfterControlChip.isChecked = true
            }
        }
        wrongDetailChip.setOnClickListener {
            if (wrongDetailChip.isChecked) {
                editAxleViewPager.setCurrentItem(WRONG_DETAIL_INPUTS, true)
            } else {
                wrongDetailChip.isChecked = true
            }
        }
        saveAxleButton.setOnClickListener {
            viewModel.attemptSave()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val json = Gson().toJson(viewModel.editingAxle)
        val viewModelJson = Gson().toJson(viewModel.viewModelState)
        outState.putString("EditingAxle", json)
        outState.putString("ViewModelState", viewModelJson)
    }

    override fun onStop() {
        if (repairTypeFragment.isVisible) {
            repairTypeFragment.dismiss()
        }

        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel.dispose()
        compositeDisposable.clear()
    }

    override fun onBackPressed() {
        showExitDialog()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                showExitDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showExitDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.exit))
            .setMessage(getString(R.string.exit_without_saving_dialog))
            .setPositiveButton(R.string.yes) { _, _ ->
                finish()
            }
            .setNegativeButton(R.string.no, null)
            .show()
    }

    fun changeDefectBeforeNdt(defectBeforeNdt: Int) {
        when (defectBeforeNdt) {
            FullAxle.DEFECT_BEFORE_CONTROL -> {
                defectBeforeControlChip?.isEnabled = true
                defectAfterControlChip?.isEnabled = false
                wrongDetailChip?.isEnabled = false
            }
            FullAxle.DEFECT_AFTER_CONTROL -> {
                defectBeforeControlChip?.isEnabled = false
                changeAxleResult()
                checkRepairType()
            }
        }
    }

    private fun changeAxleResult() {
        if (viewModel.editingAxle.result == FullAxle.DEFAULT_RESULT) {
            wrongDetailChip?.isEnabled = false
            return
        }
        when (viewModel.editingAxle.getResult()) {
            true -> {
                wrongDetailChip?.isEnabled = false
            }
            false -> {
                wrongDetailChip.isEnabled = true
            }
        }
    }

    private fun checkRepairType() {
        if (viewModel.editingAxle.repairType == FullAxle.DEFAULT_REPAIR_TYPE) {
            defectAfterControlChip.isEnabled = false
            return
        }
        if (viewModel.editingAxle.repairType == RepairTypeFragment.MIDDLE_REPAIR && viewModel.editingAxle.rings == 0) {
            defectAfterControlChip.isEnabled = false
            return
        }
        defectAfterControlChip.isEnabled = true
    }

    private fun showToast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}





















