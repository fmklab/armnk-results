package com.fmklab.armnk_results.modules.wheels.models

import com.fmklab.armnk_results.modules.common.models.Detector
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WheelDefectNdtInfo (
    @SerializedName("wheelDefect")
    @Expose
    var wheelDefect: WheelDefect = WheelDefect(),
    @SerializedName("description")
    @Expose
    var description: String = "",
    @SerializedName("defectZone")
    @Expose
    var defectZone: WheelControlZone = WheelControlZone(),
    @SerializedName("detector")
    @Expose
    var detector: Detector = Detector(),
    @SerializedName("defectSize")
    @Expose
    var defectSize: String = "",
    @SerializedName("sizeType")
    @Expose
    var sizeType: Int = SIZE_TYPE_NONE
) {

    companion object {
        const val SIZE_TYPE_NONE = 0
        const val SIZE_TYPE_LENGTH = 2
        const val SIZE_TYPE_AMPLITUDE = 1
    }

    fun getDefectInfo(): String {
        var result = "обнаружен: "
        when (defectZone.vmInDb) {
            WheelControlZone.VM_ULTRASONIC -> result += "\"средствами УЗК\""
            WheelControlZone.VM_MAGNETIC -> result += "\"средставами ВТК/МПК\""
        }
        result += ", метод (зона): \"${defectZone.title}\", код дефекта: \"${wheelDefect.title}\", дефектоскоп: \"$detector\", "
        when (sizeType) {
            SIZE_TYPE_LENGTH -> result += "размер: \"$defectSize, мм\""
            SIZE_TYPE_AMPLITUDE -> result += "амплитуда: \"$defectSize, дБ\""
        }
        if (description.isNotBlank()) {
            result += ", описание: \"$description\""
        }

        return result
    }
}






































