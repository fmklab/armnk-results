package com.fmklab.armnk_results.modules.common

class Constants {

    companion object {
        //const val BASE_URL = "http://192.168.0.160:5566/"
        //const val BASE_URL = "http://armnk.1gb.ru/api/"
        const val BASE_URL = "http://80.237.93.18:80/"
        const val DETAIL_AXLE = 1
        const val SIDE_FRAME = 11
        const val SUPERSTRUCTURE_BEAM = 12
    }
}