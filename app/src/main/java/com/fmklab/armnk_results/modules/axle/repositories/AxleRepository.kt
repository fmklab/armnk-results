package com.fmklab.armnk_results.modules.axle.repositories

import android.content.Context
import android.content.SharedPreferences
import com.fmklab.armnk_result.domain.UserAxleModuleSettings
import com.fmklab.armnk_results.modules.axle.models.Axle
import com.fmklab.armnk_results.modules.axle.models.FullAxle
import com.google.gson.Gson

class AxleRepository(context: Context) {

    private val preferences: SharedPreferences =
        context.getSharedPreferences("Axle", Context.MODE_PRIVATE)

    companion object {
        const val MAGNETIC_CONTROL_USER = "MagneticControlUser"
        const val ULTRASONIC_CONTROL_USER = "UltrasonicControlUser"
        const val EDITED_AXLE = "EditedAxle"
        const val SETTINGS = "Settings"
    }

    fun setMagneticControlUser(user: String) {
        val editor = preferences.edit()
        editor.putString(MAGNETIC_CONTROL_USER, user)
        editor.apply()
    }

    fun setUltrasonicControlUser(user: String) {
        val editor = preferences.edit()
        editor.putString(ULTRASONIC_CONTROL_USER, user)
        editor.apply()
    }

    fun getMagneticControlUser(): String? {
        return preferences.getString(MAGNETIC_CONTROL_USER, null)
    }

    fun getUltrasonicControlUser(): String? {
        return preferences.getString(ULTRASONIC_CONTROL_USER, null)
    }

    fun saveEditedAxle(axle: FullAxle?) {
        val json = Gson().toJson(axle)
        val editor = preferences.edit()
        editor.putString(EDITED_AXLE, json)
        editor.apply()
    }


    fun getEditedAxle(): Axle? {
        val json = preferences.getString(EDITED_AXLE, null) ?: return null
        if (json == "null") return null
        val fullAxle = Gson().fromJson(json, FullAxle::class.java)
        val editor = preferences.edit()
        editor.remove(EDITED_AXLE)
        editor.apply()
        return Axle(
            fullAxle.id,
            fullAxle.controlDate,
            fullAxle.factory,
            fullAxle.factoryYear,
            fullAxle.axleNumber,
            fullAxle.type,
            fullAxle.wheel1,
            fullAxle.wheel2,
            fullAxle.wheel1Id,
            fullAxle.wheel2Id,
            fullAxle.controlInfo,
            fullAxle.defectInfo,
            fullAxle.specialistCredentials,
            fullAxle.result,
            fullAxle.createDate,
            fullAxle.isMobileApp
        )
    }

//    fun saveUserSettings(userAxleModuleSettings: UserAxleModuleSettings) {
//        val json = Gson().toJson(userAxleModuleSettings)
//        val editor = preferences.edit()
//        editor.putString(SETTINGS, json)
//        editor.apply()
//    }

    fun saveControlZoneState(id: Int, isChecked: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(id.toString(), isChecked)
        editor.apply()
    }

    fun getControlZoneState(id: Int): Boolean {
        return preferences.getBoolean(id.toString(), false)
    }

    fun saveNewAxleControlZonesState(isChecked: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean("NewAxleZonesState", isChecked)
        editor.apply()
    }

    fun getNewAxleControlZonesState(): Boolean {
        return preferences.getBoolean("NewAxleZonesState", false)
    }

//    fun getUserSettings(): UserAxleModuleSettings? {
//        val json = preferences.getString(SETTINGS, null)
//        return if (json != null) {
//            Gson().fromJson(json, UserAxleModuleSettings::class.java)
//        } else {
//            null
//        }
//    }
}






























