package com.fmklab.armnk_results.modules.number_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.models.ControlZone
import com.fmklab.armnk_results.modules.number_detail.models.NumberDetail
import com.fmklab.armnk_results.modules.number_detail.viewmodels.EditNumberDetailViewModel
import com.fmklab.armnk_results.modules.wheels.models.DefectType
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_number_detail_defecation.*
import kotlinx.android.synthetic.main.fragment_number_detail_defecation.defectDescriptionEditText
import kotlinx.android.synthetic.main.fragment_number_detail_defecation.defectTypeAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_number_detail_repair_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NumberDetailDefecationFragment : NumberDetailAbstractDefectInfoFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_defecation, container, false)
    }

    override fun setupUI() {
        defectTypeAutoCompleteTextView =
            requireView().findViewById(R.id.defectTypeAutoCompleteTextView)
        defectTypeTextInputLayout = requireView().findViewById(R.id.defectTypeTextInputLayout)
        defectZoneAutoCompleteTextView =
            requireView().findViewById(R.id.defectZoneAutoCompleteTextView)
        defectZoneTextInputLayout = requireView().findViewById(R.id.defectZoneTextInputLayout)
        stealAutoCompleteTextView = requireView().findViewById(R.id.stealAutoCompleteTextView)
        stealTextInputLayout = requireView().findViewById(R.id.stealTextInputLayout)
        ownerEditText = requireView().findViewById(R.id.ownerEditText)
        ownerTextInputLayout = requireView().findViewById(R.id.ownerTextInputLayout)
        defectTypeLoadingProgress = requireView().findViewById(R.id.defectTypeLoadingProgress)
//        zoneLoadingProgress = requireView().findViewById(R.id.zoneLoadingProgress)
//        stealLoadingProgress = requireView().findViewById(R.id.stealLoadingProgress)
        viewConstraintLayout = requireView().findViewById(R.id.viewConstraintLayout)

        super.setupUI()
    }

    override fun setupBindings() {
        super.setupBindings()

        viewModel.defectDescription
            .filter { defectDescriptionEditText.text.toString() != it }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectDescriptionEditText.setText(it)
            }
            .addTo(compositeDisposable)

        defectDescriptionEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.defectDescription)
            .addTo(compositeDisposable)

        viewModel.zoneEnabledState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectZoneTextInputLayout.isEnabled = it
                defectZoneAutoCompleteTextView.isEnabled = it
            }
            .addTo(compositeDisposable)
    }
}








































