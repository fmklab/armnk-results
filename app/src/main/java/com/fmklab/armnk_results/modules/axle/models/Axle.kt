package com.fmklab.armnk_results.modules.axle.models

import com.fmklab.armnk_results.modules.common.models.AbstractDetail
import com.fmklab.armnk_results.modules.common.models.AxleAndWheelType
import com.fmklab.armnk_results.modules.common.models.Factory
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class Axle() : AbstractDetail() {

    @SerializedName("id")
    @Expose
    override var id: Int = 0

    @SerializedName("controlDate")
    @Expose
    var controlDate: Date = Date()

    @SerializedName("factory")
    @Expose
    var factory: Factory = Factory()

    @SerializedName("factoryYear")
    @Expose
    var factoryYear: String = "Нет"

    @SerializedName("axleNumber")
    @Expose
    var axleNumber: String = ""

    @SerializedName("type")
    @Expose
    var type: AxleAndWheelType = AxleAndWheelType()

    @SerializedName("wheel1")
    @Expose
    var wheel1: String = ""
    private set

    @SerializedName("wheel2")
    @Expose
    var wheel2: String = ""
    private set

    @SerializedName("wheel1Id")
    @Expose
    var wheel1Id: Int = 0
    private set

    @SerializedName("wheel2Id")
    @Expose
    var wheel2Id: Int = 0
    private set

    @SerializedName("controlInfo")
    @Expose
    var controlInfo: String = ""
    private set

    @SerializedName("defectInfo")
    @Expose
    var defectInfo: String = ""
    private set

    @SerializedName("specialistCredentials")
    @Expose
    var specialistCredentials: String = ""
    private set

    @SerializedName("result")
    @Expose
    var result: String = ""

    @SerializedName("createDate")
    @Expose
    var createDate: Date = Date()
        private set

    @SerializedName("isMobileApp")
    @Expose
    var isMobileApp: Boolean = true
        private set

    companion object {
        const val DEFAULT_ID = 0
        const val DEFAULT_FACTORY_YEAR = "Нет"
        const val DEFAULT_AXLE_NUMBER = ""
    }

    constructor(
        id: Int,
        controlDate: Date,
        factory: Factory,
        factoryYear: String,
        axleNumber: String,
        type: AxleAndWheelType,
        wheel1: String,
        wheel2: String,
        wheel1Id: Int,
        wheel2Id: Int,
        controlInfo: String,
        defectInfo: String,
        specialistCredentials: String,
        result: String,
        createDate: Date,
        isMobileApp:Boolean
    ): this() {
        this.id = id
        this.controlDate = controlDate
        this.factory = factory
        this.factoryYear = factoryYear
        this.axleNumber = axleNumber
        this.type = type
        this.wheel1 = wheel1
        this.wheel2 = wheel2
        this.wheel1Id = wheel1Id
        this.wheel2Id = wheel2Id
        this.controlInfo = controlInfo
        this.defectInfo = defectInfo
        this.specialistCredentials = specialistCredentials
        this.result = result
        this.createDate = createDate
        this.isMobileApp = isMobileApp
    }

    fun getResult(): Boolean {
        return result.toLowerCase(Locale.ROOT) == "годен"
    }
}
























