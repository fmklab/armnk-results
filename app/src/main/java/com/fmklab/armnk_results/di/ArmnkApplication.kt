package com.fmklab.armnk_results.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ArmnkApplication: Application() {

    companion object {
        fun getContextInstance() = this
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@ArmnkApplication)
            modules(appModule
                    + loginModule
                    + splashScreenModule
                    + controlDetailsModule
                    + axleModule
                    + axleEditModule
                    + wheelModule
                    + editWheelModule
                    + noneNumberDetailModule
                    + numberDetailModule
                    + outerRingModule)
        }
    }
}