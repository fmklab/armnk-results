package com.fmklab.armnk_results.di

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.fmklab.armnk_result.data.*
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_result.interactors.GetControlDetails
import com.fmklab.armnk_result.interactors.detail.*
import com.fmklab.armnk_result.interactors.nonenumberdetail.*
import com.fmklab.armnk_result.interactors.nonenumberdetail.CacheDetail
import com.fmklab.armnk_result.interactors.nonenumberdetail.ClearPreferencesCache
import com.fmklab.armnk_result.interactors.nonenumberdetail.GetAllPaged
import com.fmklab.armnk_result.interactors.nonenumberdetail.GetById
import com.fmklab.armnk_result.interactors.numberdetail.*
import com.fmklab.armnk_result.interactors.outerring.AddOuterRing
import com.fmklab.armnk_result.interactors.outerring.GetAllOuterRings
import com.fmklab.armnk_result.interactors.outerring.GetOuterRingById
import com.fmklab.armnk_result.interactors.outerring.SearchOuterRing
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.ErrorHandlerImpl
import com.fmklab.armnk_results.framework.data.*
import com.fmklab.armnk_results.framework.preferences.PreferencesDatabase
import com.fmklab.armnk_results.framework.interactors.*
import com.fmklab.armnk_results.framework.retrofit.*
import com.fmklab.armnk_results.framework.room.ArmnkDatabase
import com.fmklab.armnk_results.modules.axle.activities.AxleActivity
import com.fmklab.armnk_results.modules.axle.services.AxleApiService
import com.fmklab.armnk_results.modules.axle.adapters.AxleItemAdapter
import com.fmklab.armnk_results.modules.axle.viewmodels.AxleViewModel
import com.fmklab.armnk_results.modules.axle.activities.EditAxleActivity
import com.fmklab.armnk_results.modules.axle.adapters.AxleEditPagerAdapter
import com.fmklab.armnk_results.modules.axle.viewmodels.EditAxleViewModel
import com.fmklab.armnk_results.modules.common.Constants
import com.fmklab.armnk_results.modules.common.TokenAuthenticator
import com.fmklab.armnk_results.modules.common.services.ArmApiService
import com.fmklab.armnk_results.modules.common.services.AxleAndWheelApiService
import com.fmklab.armnk_results.modules.common.services.UserApiService
import com.fmklab.armnk_results.modules.number_detail.services.NumberDetailApiService
import com.fmklab.armnk_results.modules.wheels.activities.EditWheelActivity
import com.fmklab.armnk_results.modules.wheels.activities.WheelActivity
import com.fmklab.armnk_results.modules.wheels.adapters.WheelEditPagerAdapter
import com.fmklab.armnk_results.modules.wheels.adapters.WheelItemAdapter
import com.fmklab.armnk_results.modules.wheels.services.WheelApiService
import com.fmklab.armnk_results.modules.wheels.viewmodels.EditWheelViewModel
import com.fmklab.armnk_results.modules.wheels.viewmodels.WheelViewModel
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailAdapter
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailViewModel
import com.fmklab.armnk_results.presentation.login.LoginActivity
import com.fmklab.armnk_results.presentation.login.LoginViewModel
import com.fmklab.armnk_results.presentation.nonenumberdetail.EditNoneNumberDetailViewModel
import com.fmklab.armnk_results.presentation.nonenumberdetail.NoneNumberDetailAdapter
import com.fmklab.armnk_results.presentation.nonenumberdetail.NoneNumberDetailViewModel
import com.fmklab.armnk_results.presentation.numberdetail.NumberDetailPagerAdapter
import com.fmklab.armnk_results.presentation.numberdetail.bogie.*
import com.fmklab.armnk_results.presentation.outerring.*
import com.fmklab.armnk_results.presentation.splashscreen.SplashScreenActivity
import com.fmklab.armnk_results.presentation.splashscreen.SplashScreenViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import tech.nicesky.retrofit2_adapter_rxjava3.RxJava3CallAdapterFactory

val appModule = module {

    fun provideDetailRepository(dataSource: DetailDataSource) = DetailRepository(dataSource)

    fun provideDetailDataSource(api: DetailApi, database: ArmnkDatabase, currentUser: CurrentUser) =
        DetailDataSourceImpl(api, database, currentUser)

    fun provideDetailApi(retrofit: Retrofit) = retrofit.create(DetailApi::class.java)

    single { provideRetrofit(get()) }
    single { provideArmApi(get()) }
    single<ErrorHandler> { ErrorHandlerImpl() }
    single { CurrentUser(androidContext(), createGsonConverter()) }
    single {
        PreferencesDatabase(
            androidContext(),
            createGsonConverter()
        )
    }
    single { provideArmnkDatabase(androidContext()) }
    single { provideDetailApi(get()) }
    single<DetailDataSource> { provideDetailDataSource(get(), get(), get()) }
    single { provideDetailRepository(get()) }
}

val loginModule = module {

    scope<LoginActivity> {

        scoped {
            LoginInteractors(
                Login(
                    provideLoginApi(
                        get()
                    )
                )
            )
        }
        viewModel { LoginViewModel.ViewModel(get(), get(), get()) }
    }
}

val splashScreenModule = module {

    scope<SplashScreenActivity> {

        scoped {
            LoginInteractors(
                Login(
                    provideLoginApi(
                        get()
                    )
                )
            )
        }
        viewModel {
            SplashScreenViewModel.ViewModel(
                get(),
                get(),
                get()
            )
        }
    }
}

val controlDetailsModule = module {

    scope<ControlDetailActivity> {

        scoped<ControlDetailDataSource> { ControlDetailDataSourceImpl(get()) }
        scoped { ControlDetailRepository(get()) }
        scoped {
            ControlDetailInteractors(
                GetControlDetails(get())
            )
        }
        scoped { ControlDetailAdapter(androidContext()) }
        viewModel { ControlDetailViewModel.ViewModel(get(), get(), get()) }
    }
}

val axleModule = module {

    scope<AxleActivity> {

        scoped { AxleItemAdapter() }
        scoped { provideAxleApi(get()) }
        viewModel {
            AxleViewModel(
                get(),
                get()
            )
        }
    }

}

val axleEditModule = module {

    scope<EditAxleActivity> {

        scoped { (fm: FragmentManager) ->
            AxleEditPagerAdapter(
                fm,
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
            )
        }
        scoped { provideAxleAndWheelApi(get()) }
        scoped { provideAxleApi(get()) }
        viewModel { EditAxleViewModel(get(), get(), get(), get()) }
    }
}

val wheelModule = module {

    scope<WheelActivity> {

        scoped { provideWheelApi(get()) }
        scoped { WheelItemAdapter() }
        viewModel { WheelViewModel(get(), get()) }
    }
}

val editWheelModule = module {

    scope<EditWheelActivity> {

        scoped { (fm: FragmentManager) ->
            WheelEditPagerAdapter(
                fm,
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
            )
        }
        scoped { provideAxleAndWheelApi(get()) }
        scoped { provideWheelApi(get()) }
        scoped { provideAxleApi(get()) }
        scoped { provideUserApi(get()) }
        viewModel { EditWheelViewModel(get(), get(), get(), get(), get(), get()) }
    }
}

val noneNumberDetailModule = module {

    factory { NoneNumberDetailScope() }
    scope<NoneNumberDetailScope> {

        scoped { NoneNumberDetailAdapter() }
        scoped<NoneNumberDetailDataSource> {
            NoneNumberDetailDataSourceImpl(
                provideNoneNumberDetailApi(get()), get(), get(), get()
            )
        }
        scoped { NoneNumberDetailRepository(get()) }
        scoped {
            NoneNumberDetailInteractors(
                GetAllPaged(get()),
                GetById(get()),
                GetMethods(get()),
                GetControlZonesByMethod(get()),
                GetDetectors(get()),
                CacheDetail(get()),
                ClearPreferencesCache(get()),
                ClearRequestCache(get()),
                Insert(get())
            )
        }
        viewModel {
            NoneNumberDetailViewModel.ViewModel(
                get(),
                get()
            )
        }
        viewModel {
            EditNoneNumberDetailViewModel.ViewModel(
                get(),
                get(),
                get()
            )
        }
    }
}

val numberDetailModule = module {

    factory { NumberDetailScope() }
    scope<NumberDetailScope> {
        fun provideInteractors(
            repository: NumberDetailRepository,
            detailRepository: DetailRepository
        ) = NumberDetailInteractors(
            com.fmklab.armnk_result.interactors.numberdetail.GetAllPaged(repository),
            com.fmklab.armnk_result.interactors.numberdetail.GetById(repository),
            GetFactories(detailRepository),
            GetYears(detailRepository),
            GetControlResults(detailRepository),
            com.fmklab.armnk_result.interactors.numberdetail.CacheDetail(repository),
            com.fmklab.armnk_result.interactors.numberdetail.ClearPreferencesCache(repository),
            GetRepairDefectTypes(repository),
            GetRepairControlZones(repository),
            GetDefectTypes(detailRepository),
            GetControlZones(detailRepository),
            GetSteals(repository),
            GetMethods(detailRepository),
            GetDetectors(detailRepository),
            CheckAlreadyAddedDetail(repository),
            AddNumberDetail(repository),
            SearchNumberDetail(repository),
            GetAllBogiePaged(repository)
        )

        fun provideBogieInteractors(repository: NumberDetailRepository, detailRepository: DetailRepository) =
            BogieNumberDetailInteractors(
                GetAllBogiePaged(repository),
                SearchBogieNumberDetail(repository),
                GetBogieById(repository),
                GetBogieModels(detailRepository),
                CacheBogieDetail(repository),
                ClearBogiePreferenceCache(repository),
                GetBogieModelFactories(detailRepository),
                GetYears(detailRepository),
                GetControlResults(detailRepository),
                GetDefectTypes(detailRepository),
                GetBogieControlZones(repository),
                GetControlZones(detailRepository),
                GetSteals(repository),
                GetRepairDefectTypes(repository),
                GetRepairControlZones(repository),
                GetMethods(detailRepository),
                GetDetectors(detailRepository),
                CheckAlreadyAddedDetail(repository),
                AddBogieNumberDetail(repository)
            )

        fun provideRepository(dataSource: NumberDetailDataSource) =
            NumberDetailRepository(dataSource)

        fun provideDataSource(
            api: NumberDetailApi,
            currentUser: CurrentUser,
            preferences: PreferencesDatabase,
            db: ArmnkDatabase
        ) =
            NumberDetailDataSourceImpl(api, currentUser, preferences, db)

        fun provideApi(retrofit: Retrofit) = retrofit.create(NumberDetailApi::class.java)

        fun provideRecyclerViewAdapter() =
            com.fmklab.armnk_results.presentation.numberdetail.NumberDetailAdapter()

        fun provideBogieRecyclerViewAdapter() =
            BogieNumberDetailAdapter()

        scoped { provideApi(get()) }
        scoped<NumberDetailDataSource> { provideDataSource(get(), get(), get(), get()) }
        scoped { provideRepository(get()) }
        scoped { provideInteractors(get(), get()) }
        scoped { provideBogieInteractors(get(), get()) }
        scoped { provideRecyclerViewAdapter() }
        scoped { provideBogieRecyclerViewAdapter() }
        viewModel {
            com.fmklab.armnk_results.presentation.numberdetail.NumberDetailViewModel.ViewModel(
                get(),
                get()
            )
        }
        viewModel {
            com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailViewModel.ViewModel(
                get(),
                get(),
                get()
            )
        }
        viewModel {
            BogieNumberDetailViewModel.ViewModel(get(), get())
        }
        viewModel {
            EditBogieNumberDetailViewModel.ViewModel(get(), get(), get())
        }
    }

    scope<com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailActivity> {
        fun provideViewPagerAdapter(fm: FragmentManager) = NumberDetailPagerAdapter(
            fm,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )

        scoped { (fm: FragmentManager) -> provideViewPagerAdapter(fm) }
    }
}

val outerRingModule = module {

    factory { OuterRingScope() }
    scope<OuterRingScope> {

        fun provideInteractors(
            repository: OuterRingRepository,
            detailRepository: DetailRepository
        ) =
            OuterRingInteractors(
                GetAllOuterRings(repository),
                GetOuterRingById(repository),
                GetMethods(detailRepository),
                GetDetectors(detailRepository),
                GetFactories(detailRepository),
                GetYears(detailRepository),
                GetControlResults(detailRepository),
                com.fmklab.armnk_result.interactors.outerring.CacheDetail(repository),
                com.fmklab.armnk_result.interactors.outerring.ClearPreferencesCache(repository),
                GetDefectTypes(detailRepository),
                GetControlZones(detailRepository),
                AddOuterRing(repository),
                SearchOuterRing(repository)
            )

        fun provideRepository(dataSource: OuterRingDataSource) =
            OuterRingRepository(dataSource)

        fun provideDataSource(
            currentUser: CurrentUser,
            api: OuterRingApi,
            preferencesDatabase: PreferencesDatabase
        ) =
            OuterRingDataSourceImpl(currentUser, api, preferencesDatabase)

        fun provideApi(retrofit: Retrofit) = retrofit.create(OuterRingApi::class.java)

        fun provideRecyclerViewAdapter() = OuterRingAdapter()

        scoped { provideApi(get()) }
        scoped<OuterRingDataSource> { provideDataSource(get(), get(), get()) }
        scoped { provideRepository(get()) }
        scoped { provideInteractors(get(), get()) }
        scoped { provideRecyclerViewAdapter() }
        viewModel { OuterRingViewModel.ViewModel(get(), get()) }
        viewModel { EditOuterViewModel.ViewModel(get(), get(), get()) }
    }

    scope<EditOuterRingActivity> {
        fun provideViewPagerAdapter(fm: FragmentManager) = OuterRingPagerAdapter(
            fm,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        scoped { (fm: FragmentManager) -> provideViewPagerAdapter(fm) }
    }
}

fun provideRetrofit(currentUser: CurrentUser): Retrofit {
    val tokenRetrofit = Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(
        GsonConverterFactory.create(
            createGsonConverter()
        )
    ).build()
    val loginApi = tokenRetrofit.create(LoginApi::class.java)
    val client =
        OkHttpClient.Builder().authenticator(TokenAuthenticator(currentUser, loginApi))
            .build()
    return Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(
        GsonConverterFactory.create(
            createGsonConverter()
        )
    ).addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(client).build()
}

fun createGsonConverter(): Gson {
    return GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
}

fun provideAxleApi(retrofit: Retrofit): AxleApiService {
    return retrofit.create(AxleApiService::class.java)
}

fun provideAxleAndWheelApi(retrofit: Retrofit): AxleAndWheelApiService {
    return retrofit.create(AxleAndWheelApiService::class.java)
}

fun provideWheelApi(retrofit: Retrofit): WheelApiService {
    return retrofit.create(WheelApiService::class.java)
}

fun provideUserApi(retrofit: Retrofit): UserApiService {
    return retrofit.create(UserApiService::class.java)
}

fun provideArmApi(retrofit: Retrofit): ArmApiService {
    return retrofit.create(ArmApiService::class.java)
}


fun provideLoginApi(retrofit: Retrofit): LoginApi {
    return retrofit.create(LoginApi::class.java)
}

fun provideArmnkDatabase(context: Context): ArmnkDatabase {
    return ArmnkDatabase.getInstance(context)
}

fun provideNoneNumberDetailApi(retrofit: Retrofit): NoneNumberDetailApi {
    return retrofit.create(NoneNumberDetailApi::class.java)
}









