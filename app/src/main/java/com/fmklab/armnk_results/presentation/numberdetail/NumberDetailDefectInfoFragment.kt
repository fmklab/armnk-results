package com.fmklab.armnk_results.presentation.numberdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.DefectMoment
import com.fmklab.armnk_result.domain.NumberDetail
import com.fmklab.armnk_result.domain.NumberDetailDefectMoment
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.setCheckedDistinct
import com.jakewharton.rxbinding4.widget.checkedChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_number_detail_defect_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NumberDetailDefectInfoFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditNumberDetailViewModel.ViewModel by sharedViewModel()
    private val defecationFragment = NumberDetailDefecationFragment()
    private val visualFragment = NumberDetailVisualFragment()
    private val ndtFragment = NumberDetailNdtInfoFragment()

    private var isFragmentVisible: Boolean = false

    companion object {
        private const val DEFECATION_FRAGMENT_TAG = "DefecationFragmentTag"
        private const val VISUAL_FRAGMENT_TAG = "VisualFragmentTag"
        private const val NDT_FRAGMENT_TAG = "NdtFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_defect_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBinding()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)

        isFragmentVisible = menuVisible
        if (menuVisible) {
            viewModel.inputs.reloadDefectInfo()
        }
    }

    private fun setupBinding() {
        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.defectMoment }
            .subscribe(this::setDefectMoment)
            .addTo(compositeDisposable)

        Observable.zip(
            defecationRadio
                .checkedChanges()
                .skipInitialValue(),
            viewModel.numberDetail(),
            BiFunction<Boolean, NumberDetail, Boolean> { isChecked, _ ->
                isChecked
            }
        )
            .subscribe {
                if (it) {
                    viewModel.inputs.defectMoment(NumberDetailDefectMoment.DEFECATION)
                    if (isFragmentVisible) {
                        viewModel.inputs.loadDefecationInfo()
                    }
                    loadFragment(defecationFragment, DEFECATION_FRAGMENT_TAG)
                } else {
                    unloadFragment(DEFECATION_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

        Observable.zip(
            visualRadio
                .checkedChanges()
                .skipInitialValue(),
            viewModel.numberDetail(),
            BiFunction<Boolean, NumberDetail, Boolean> { isChecked, _ ->
                isChecked
            }
        )
            .subscribe {
                if (it) {
                    viewModel.inputs.defectMoment(NumberDetailDefectMoment.VISUAL)
                    if (isFragmentVisible) {
                        viewModel.inputs.loadVisualInfo()
                    }
                    loadFragment(visualFragment, VISUAL_FRAGMENT_TAG)
                } else {
                    unloadFragment(VISUAL_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

        Observable.zip(
            ndtRadio
                .checkedChanges()
                .skipInitialValue(),
            viewModel.numberDetail(),
            BiFunction<Boolean, NumberDetail, Boolean> { isChecked, _ ->
                isChecked
            }
        )
            .subscribe {
                if (it) {
                    viewModel.inputs.defectMoment(NumberDetailDefectMoment.NDT)
                    if (isFragmentVisible) {
                        viewModel.inputs.loadNdtInfo()
                    }
                    loadFragment(ndtFragment, NDT_FRAGMENT_TAG)
                } else {
                    unloadFragment(NDT_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                for (formItem in it) {
                    when (formItem) {
                        is NumberDetail.InvalidFieldImpl.DefectMoment ->
                            Toast.makeText(requireContext(), formItem.reason, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            .addTo(compositeDisposable)
    }

    private fun setDefectMoment(defectMoment: NumberDetailDefectMoment) {
        when(defectMoment) {
            NumberDetailDefectMoment.DEFECATION -> defecationRadio.setCheckedDistinct(true)
            NumberDetailDefectMoment.VISUAL -> visualRadio.setCheckedDistinct(true)
            NumberDetailDefectMoment.NDT -> ndtRadio.setCheckedDistinct(true)
            else -> {
                defecationRadio.setCheckedDistinct(false)
                visualRadio.setCheckedDistinct(false)
                ndtRadio.setCheckedDistinct(false)
            }
        }
    }

    private fun loadFragment(fragment: Fragment, fragmentTag: String) {
        val transaction = parentFragmentManager.beginTransaction()
        //val currentFragment = parentFragmentManager.findFragmentByTag(fragmentTag)
        if (!parentFragmentManager.fragments.any { frg -> frg.tag == fragmentTag }) {
            transaction.add(R.id.defectInfoFrameLayout, fragment, fragmentTag).commit()
        }
//        if (currentFragment != null) {
//            transaction.remove(currentFragment)
//        }
    }

    private fun unloadFragment(fragmentTag: String) {
        val transaction = parentFragmentManager.beginTransaction()
        val fragment = parentFragmentManager.findFragmentByTag(fragmentTag)
        if (fragment != null) {
            transaction.remove(fragment).commit()
        }
    }
}