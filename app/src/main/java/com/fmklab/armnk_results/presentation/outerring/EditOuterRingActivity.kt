package com.fmklab.armnk_results.presentation.outerring

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import com.fmklab.armnk_result.domain.ControlResult
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.OuterRing
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.di.OuterRingScope
import com.fmklab.armnk_results.framework.controls.SavingDialog
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.viewpager.pageSelections
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.Action
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.activity_edit_number_detail.*
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.*
import org.koin.android.ext.android.getKoin
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.ext.getOrCreateScope
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.reflect.KFunction

class EditOuterRingActivity : BaseActivity<EditOuterViewModel.ViewModel>() {

    private val compositeDisposable = CompositeDisposable()

    private val outerRingScope = getKoin().get<OuterRingScope>().getOrCreateScope()

    override val viewModel: EditOuterViewModel.ViewModel by outerRingScope.viewModel(this)
    private val viewPagerAdapter: OuterRingPagerAdapter by lifecycleScope.inject {
        parametersOf(supportFragmentManager)
    }
    private lateinit var savingDialog: SavingDialog

    companion object {
        private const val BASE_INFO_PAGE = 0
        private const val DEFECT_INFO_PAGE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_number_detail)

        setupUI()
        setupBinding()

        if (savedInstanceState == null) {
            viewModel.inputs.clearCache()
        }

        viewModel.inputs.loadOuterRing()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    override fun onBackPressed() {
        showExitWithoutSavingDialog()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                showExitWithoutSavingDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupUI() {
        setSupportActionBar(editDetailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        editDetailViewPager.adapter = viewPagerAdapter
        savingDialog = SavingDialog(this)
    }

    private fun setupBinding() {
        viewModel.outputs.title()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                supportActionBar?.title = it
                title = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.noInternetConnection()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showNoInternetConnection)
            .addTo(compositeDisposable)

        viewModel.outputs.outerRing()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.controlResult != null }
            .map { it.controlResult?.name }
            .subscribe(this::initChips)
            .addTo(compositeDisposable)

        viewModel.outputs.controlResultChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.name }
            .subscribe(this::navDefectInfoInputs)
            .addTo(compositeDisposable)

        viewModel.outputs.loadOuterRingError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::loadOuterRing)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadMethodDependedDataError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showError(it, viewModel.inputs::loadMethodDependedData) }
            .addTo(compositeDisposable)

        viewModel.outputs.loadFactoryDependedDataError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showError(it, viewModel.inputs::loadFactoryDependedData) }
            .addTo(compositeDisposable)

        viewModel.outputs.showSavingDialog()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showSavingDialog)
            .addTo(compositeDisposable)

        viewModel.outputs.outerRingSaving()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    savingDialog.startSavingDialog()
                } else {
                    savingDialog.dismissDialog()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailSavingError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::acceptSaving)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailSavingSuccess()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setResult(Activity.RESULT_OK, it)
                finish()
            }
            .addTo(compositeDisposable)

        Observable.zip(
            nextPageFab.clicks(),
            viewModel.outputs.outerRing(),
            BiFunction<Unit, OuterRing, String> { _, or ->
                or.controlResult?.name
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::navDefectInfoInputs)
            .addTo(compositeDisposable)

        Observable.zip(
            defectInfoChip.clicks(),
            viewModel.outputs.outerRing(),
            BiFunction<Unit, OuterRing, String> { _, or ->
                or.controlResult?.name
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (!defectInfoChip.isChecked) {
                    defectInfoChip.isChecked = true
                }
                navDefectInfoInputs(it)
            }
            .addTo(compositeDisposable)

        Observable.combineLatest(
            editDetailViewPager.pageSelections(),
            viewModel.outputs.outerRing(),
            BiFunction<Int, OuterRing, Pair<Int, ControlResult?>> { p, or ->
                Pair(p, or.controlResult)
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::initSelectedPage)
            .addTo(compositeDisposable)

        prevPageFab
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe { navBaseInfoPage() }
            .addTo(compositeDisposable)

        baseInputsChip
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe {
                if (!baseInputsChip.isChecked) {
                    baseInputsChip.isChecked = true
                }
                navBaseInfoPage()
            }
            .addTo(compositeDisposable)

        saveDetailButton
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe { viewModel.inputs.attemptSave() }
            .addTo(compositeDisposable)
    }

    private fun showNoInternetConnection(show: Boolean) {
        if (show) {
            supportActionBar?.title = getString(R.string.wait_for_connection)
        } else {
            supportActionBar?.title = title
        }
    }

    private fun initChips(controlResultName: String?) {
        when (controlResultName) {
            "годен" -> {
                nextPageFab.hide()
                defectInfoChip.disable()
            }
            "брак" -> defectInfoChip.enable()
        }
    }

    private fun navDefectInfoInputs(controlResultName: String?) {
        if (controlResultName == "брак") {
            editDetailViewPager.setCurrentItem(DEFECT_INFO_PAGE, true)
        }
    }

    private fun initSelectedPage(info: Pair<Int, ControlResult?>) {
        if (info.first == BASE_INFO_PAGE && info.second != null) {
            when (info.second!!.name) {
                "годен" -> {
                    nextPageFab.hide()
                    defectInfoChip.disable()
                }
                "брак" -> {
                    nextPageFab.show()
                    defectInfoChip.enable()
                }
            }
            prevPageFab.hide()
            baseInputsChip.isChecked = true
        } else if (info.first == DEFECT_INFO_PAGE) {
            nextPageFab.hide()
            prevPageFab.show()
            defectInfoChip.isChecked = true
            viewModel.inputs.defectInfoChanged()
        }
    }

    private fun navBaseInfoPage() {
        editDetailViewPager.setCurrentItem(BASE_INFO_PAGE, true)
    }

    private fun showError(error: Error, refreshAction: KFunction<Unit>) {
        when (error) {
            Error.NotFoundRemote -> showRefreshSnackbar("Данные не найдены", refreshAction)
            Error.SocketTimeout -> showRefreshSnackbar(
                "Время ожидания ответа от сервера истекло",
                refreshAction
            )
            Error.BadRequest -> showRefreshSnackbar("Внутренняя ошибка сервера", refreshAction)
            Error.NoInternetConnection -> toast("Ошибка сети. Проверьте соединение с интернетом")
            is Error.Unknown -> showRefreshSnackbar(
                "Произошла непредвиденная ошибка ${error.message}",
                refreshAction
            )
        }
    }

    private fun showRefreshSnackbar(message: String, action: KFunction<Unit>) {
        Snackbar.make(baseInputsNestedScrollView, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(R.string.refresh)) {
                action.call()
            }
            .show()
    }

    private fun showSavingDialog(message: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.save))
            .setMessage("Сохранть данные\n\n${message}")
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                viewModel.inputs.acceptSaving()
            }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun showExitWithoutSavingDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.exit)
            .setMessage(R.string.exit_without_saving_dialog)
            .setPositiveButton(R.string.yes) { _, _ ->
                finish()
            }
            .setNegativeButton(R.string.no, null)
            .show()
    }
}




































