package com.fmklab.armnk_results.presentation.login

import android.content.Intent
import android.os.Bundle
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setGone
import com.fmklab.armnk_results.framework.extensions.setVisible
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.view.focusChanges
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.activity_auth.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import java.util.concurrent.TimeUnit

class LoginActivity : BaseActivity<LoginViewModel.ViewModel>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: LoginViewModel.ViewModel by lifecycleScope.viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        setupBindings()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    private fun setupBindings() {
        viewModel.outputs.fetchingUser()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    loadingProgressBar.setVisible()
                    loginTextInputEditText.disable()
                    passwordTextInputField.disable()
                } else {
                    loadingProgressBar.setGone()
                    loginTextInputEditText.enable()
                    passwordTextInputField.enable()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidUsernameError()
            .subscribe {
                if (it) {
                    loginTextInputLayout.error = getString(R.string.no_login_error)
                } else {
                    loginTextInputLayout.error = null
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidPasswordError()
            .subscribe {
                if (it) {
                    passwordTextInputLayout.error = getString(R.string.no_password_error)
                } else {
                    passwordTextInputLayout.error = null
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.isLoginButtonEnable()
            .subscribe {
                signInButton.isEnabled = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loginError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    Error.NotFoundRemote -> toast("Неверный логин или пароль")
                    Error.NoInternetConnection -> toast("Ошибка соединения. Проверьте подключение к сети")
                    Error.BadRequest -> toast("Внутренняя ошибка сервера")
                    Error.SocketTimeout -> toast("Время ожидания ответа от сервера истекло")
                    else -> toast("Непредвиденная ошибка:(")
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loginSuccess()
            .subscribe { showControlDetailsScreen() }
            .addTo(compositeDisposable)

        loginTextInputEditText
            .textChanges()
            .map(CharSequence::toString)
            .subscribe { viewModel.inputs.username(it) }
            .addTo(compositeDisposable)

        loginTextInputEditText
            .focusChanges()
            .skipInitialValue()
            .subscribe {
                viewModel.inputs.usernameFocus(it)
            }
            .addTo(compositeDisposable)

        passwordTextInputField
            .focusChanges()
            .skipInitialValue()
            .subscribe { viewModel.inputs.passwordFocus(it) }
            .addTo(compositeDisposable)

        passwordTextInputField
            .textChanges()
            .map(CharSequence::toString)
            .subscribe { viewModel.inputs.password(it) }
            .addTo(compositeDisposable)

        signInButton
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe { viewModel.inputs.loginClick() }
            .addTo(compositeDisposable)

        rememberUserCheckBox
            .checkedChanges()
            .subscribe { viewModel.inputs.rememberMeCheck(it) }
            .addTo(compositeDisposable)
    }

    private fun showControlDetailsScreen() {
        val intent = Intent(this, ControlDetailActivity::class.java)
        startActivity(intent)
        finish()
    }
}


































