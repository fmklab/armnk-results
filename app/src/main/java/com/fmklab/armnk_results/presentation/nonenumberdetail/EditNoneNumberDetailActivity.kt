package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.room.rxjava3.RxRoom
import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.Detector
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.di.NoneNumberDetailScope
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.framework.controls.SavingDialog
import com.fmklab.armnk_results.framework.extensions.*
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.extensions.toDate
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.view.focusChanges
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.*
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.allCheckedEditText
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.controlDateEditText
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.controlZoneAutoCompleteTextView
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.defecationCheckedEditText
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.detectorAutoCompleteTextView
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.editSmallDetailToolbar
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.methodAutoCompleteTextView
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.ndtCheckedEditText
import kotlinx.android.synthetic.main.activity_edit_none_number_detail.visualCheckedEditText
import org.koin.android.ext.android.getKoin
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.ext.getOrCreateScope
import org.koin.ext.getScopeName
import org.koin.ext.scope
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.reflect.KFunction

class EditNoneNumberDetailActivity : BaseActivity<EditNoneNumberDetailViewModel.ViewModel>() {

    private val noneNumberDetailScope = getKoin().get<NoneNumberDetailScope>().getOrCreateScope()
    private val compositeDisposable = CompositeDisposable()

    private var saveDetailMenuItem: MenuItem? = null

    override val viewModel: EditNoneNumberDetailViewModel.ViewModel by noneNumberDetailScope.viewModel(this)
    private val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
    private lateinit var materialDatePicker: MaterialDatePicker<Long>
    private lateinit var savingDialog: SavingDialog
    private var title: String = ""

    companion object {
        private const val DATE_PICKER_TAG = "DatePickerTag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_none_number_detail)

//        setupUI()
//        setupBinding()
//        setupListeners()

        if (savedInstanceState == null) {
            viewModel.inputs.clearCache()
        }

        viewModel.inputs.loadDetail()
    }

    override fun onResume() {
        super.onResume()

        setupUI()
        setupBinding()
        setupListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_edit_detail, menu)
        saveDetailMenuItem = menu!!.findItem(R.id.save_detail_button)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                showExitWithoutSavingDialog()
                true
            }
            R.id.save_detail_button -> {
                viewModel.inputs.save()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        showExitWithoutSavingDialog()
    }

    private fun setupUI() {
        setSupportActionBar(editSmallDetailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        controlDateEditText.keyListener = null
        methodAutoCompleteTextView.keyListener = null
        controlZoneAutoCompleteTextView.keyListener = null
        detectorAutoCompleteTextView.keyListener = null

        controlZoneShimmerContainer.hideShimmer()
        detectorShimmerContainer.hideShimmer()

        savingDialog = SavingDialog(this)

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText(getString(R.string.select_date))
        materialDatePicker = builder.build()
    }

    private fun setupBinding() {
        viewModel.outputs.title()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                supportActionBar?.title = it
                title = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingMethodDependData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showMethodDependDataLoading()
                } else {
                    hideMethodDependDataLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.noneNumberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlDateEditText.setText(simpleDateFormat.format(it.controlDate))
                methodAutoCompleteTextView.setText(it.method, false)
                controlZoneAutoCompleteTextView.setText(it.controlZone?.toString(), false)
                detectorAutoCompleteTextView.setText(it.detector?.toString(), false)
                allCheckedEditText.setTextDistinct(it.allChecked.toString())
                allCheckedEditText.setSelection(allCheckedEditText.text?.length ?: 0)
                defecationCheckedEditText.setTextDistinct(it.defecationChecked.toString())
                defecationCheckedEditText.setSelection(defecationCheckedEditText.text?.length ?: 0)
                visualCheckedEditText.setTextDistinct(it.visualChecked.toString())
                visualCheckedEditText.setSelection(visualCheckedEditText.text?.length ?: 0)
                ndtCheckedEditText.setTextDistinct(it.ndtChecked.toString())
                ndtCheckedEditText.setSelection(ndtCheckedEditText.text?.length ?: 0)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.methods()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodAutoCompleteTextView.initArrayAdapter(this, ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlZones()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlZoneAutoCompleteTextView.initArrayAdapter(this, ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detectors()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detectorAutoCompleteTextView.initArrayAdapter(this, ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.isEmpty()) {
                    methodTextInputLayout.error = null
                    controlZoneTextInputLayout.error = null
                    detectorTextInputLayout.error = null
                    allCheckedTextInputLayout.error = null
                }

                for (formItem in it) {
                    when (formItem) {
                        is NoneNumberDetail.InvalidField.Method -> methodTextInputLayout.error =
                            formItem.reason
                        is NoneNumberDetail.InvalidField.ControlZone -> controlZoneTextInputLayout.error =
                            formItem.reason
                        is NoneNumberDetail.InvalidField.Detector -> detectorTextInputLayout.error =
                            formItem.reason
                        is NoneNumberDetail.InvalidField.AllChecked -> allCheckedTextInputLayout.error =
                            formItem.reason
                    }
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.noInternetConnectionError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    supportActionBar?.title = getString(R.string.wait_for_connection)
                } else {
                    supportActionBar?.title = title
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.error()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showError(it, viewModel.inputs::loadDetail) }
            .addTo(compositeDisposable)

        viewModel.outputs.savingDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    savingDialog.startSavingDialog()
                } else {
                    savingDialog.dismissDialog()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.saveDetailError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::save)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailSaved()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setResult(Activity.RESULT_OK, it)
                finish()
            }
            .addTo(compositeDisposable)

        viewModel.outputs.showSavingDialog()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                MaterialAlertDialogBuilder(this)
                    .setTitle(getString(R.string.save))
                    .setMessage(getString(R.string.save_detail_message))
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        viewModel.inputs.acceptSaving()
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
            }
            .addTo(compositeDisposable)

        allCheckedEditText
            .textChanges()
            .skipInitialValue()
            .subscribeOn(Schedulers.io())
            .map(CharSequence::toString)
            .map {
                if (it.isBlank()) {
                    0
                } else {
                    it.toInt()
                }
            }
            .subscribe(viewModel.inputs::allChecked)
            .addTo(compositeDisposable)

        defecationCheckedEditText
            .textChanges()
            .skipInitialValue()
            .subscribeOn(Schedulers.io())
            .map(CharSequence::toString)
            .map {
                if (it.isBlank()) {
                    0
                } else {
                    it.toInt()
                }
            }
            .subscribe(viewModel.inputs::defecationChecked)
            .addTo(compositeDisposable)

        visualCheckedEditText
            .textChanges()
            .skipInitialValue()
            .subscribeOn(Schedulers.io())
            .map(CharSequence::toString)
            .map {
                if (it.isBlank()) {
                    0
                } else {
                    it.toInt()
                }
            }
            .subscribe(viewModel.inputs::visualChecked)
            .addTo(compositeDisposable)

        ndtCheckedEditText
            .textChanges()
            .skipInitialValue()
            .subscribeOn(Schedulers.io())
            .map(CharSequence::toString)
            .map {
                if (it.isBlank()) {
                    0
                } else {
                    it.toInt()
                }
            }
            .subscribe(viewModel.inputs::ndtChecked)
            .addTo(compositeDisposable)

        methodAutoCompleteTextView
            .itemSelected<String>()
            .subscribeOn(Schedulers.io())
            .subscribe(viewModel.inputs::method)
            .addTo(compositeDisposable)

        controlZoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .subscribeOn(Schedulers.io())
            .subscribe(viewModel.inputs::controlZone)
            .addTo(compositeDisposable)

        detectorAutoCompleteTextView
            .itemSelected<Detector>()
            .subscribeOn(Schedulers.io())
            .subscribe(viewModel.inputs::detector)
            .addTo(compositeDisposable)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupListeners() {
        controlDateEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (!supportFragmentManager.fragments.any { frg -> frg.tag == DATE_PICKER_TAG }) {
                    materialDatePicker.show(
                        supportFragmentManager,
                        DATE_PICKER_TAG
                    )
                }
                return@setOnTouchListener true
            }
            false
        }

        materialDatePicker.addOnPositiveButtonClickListener {
            val date = it.toDate()
            controlDateEditText.setText(simpleDateFormat.format(date))
            viewModel.inputs.controlDate(simpleDateFormat.format(date))
        }

        methodAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            methodTextInputLayout.error = null
        }

        controlZoneAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            controlZoneTextInputLayout.error = null
        }

        detectorAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            detectorTextInputLayout.error = null
        }

        allCheckedEditText.doOnTextChanged { _, _, _, _ ->
            allCheckedTextInputLayout.error = null
        }
    }

    private fun showExitWithoutSavingDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.exit))
            .setMessage(getString(R.string.exit_without_saving_dialog))
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                finish()
            }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun showError(error: Error, refreshAction: KFunction<Unit>) {
        when (error) {
            Error.NotFoundRemote -> showRefreshSnackbar("Данные не найдены", refreshAction)
            Error.SocketTimeout -> showRefreshSnackbar("Время ожидания ответа от сервера истекло", refreshAction)
            Error.BadRequest -> showRefreshSnackbar("Внутренняя ошибка сервера", refreshAction)
            Error.NoInternetConnection -> toast("Ошибка сети. Проверьте соединение с интернетом")
            is Error.Unknown -> showRefreshSnackbar("Произошла непредвиденная ошибка ${error.message}", refreshAction)
        }
    }

    private fun showRefreshSnackbar(message: String, action: KFunction<Unit>) {
        Snackbar.make(editSmallDetailScrollView, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(R.string.refresh)) {
                action.call()
            }
            .show()
    }

    private fun showAllViewLoading() {
        controlDateEditText.disable()
        methodTextInputLayout.disable()
        methodAutoCompleteTextView.disable()
        controlZoneTextInputLayout.disable()
        controlZoneAutoCompleteTextView.disable()
        detectorTextInputLayout.disable()
        detectorAutoCompleteTextView.disable()
        allCheckedEditText.disable()
        defecationCheckedEditText.disable()
        visualCheckedEditText.disable()
        ndtCheckedEditText.disable()
        shimmerViewContainer.showShimmer(true)
    }

    private fun hideAllViewLoading() {
        controlDateEditText.enable()
        methodTextInputLayout.enable()
        methodAutoCompleteTextView.enable()
        controlZoneTextInputLayout.enable()
        controlZoneAutoCompleteTextView.enable()
        detectorTextInputLayout.enable()
        detectorAutoCompleteTextView.enable()
        allCheckedEditText.enable()
        defecationCheckedEditText.enable()
        visualCheckedEditText.enable()
        ndtCheckedEditText.enable()
        shimmerViewContainer.hideShimmer()
        shimmerViewContainer.stopShimmer()
    }

    private fun showMethodDependDataLoading() {
        controlZoneTextInputLayout.disable()
        controlZoneAutoCompleteTextView.disable()
        detectorTextInputLayout.disable()
        detectorAutoCompleteTextView.disable()
        controlZoneShimmerContainer.showShimmer(true)
        detectorShimmerContainer.showShimmer(true)
    }

    private fun hideMethodDependDataLoading() {
        controlZoneTextInputLayout.enable()
        controlZoneAutoCompleteTextView.enable()
        detectorTextInputLayout.enable()
        detectorAutoCompleteTextView.enable()
        controlZoneShimmerContainer.hideShimmer()
        detectorShimmerContainer.hideShimmer()
    }
}









































