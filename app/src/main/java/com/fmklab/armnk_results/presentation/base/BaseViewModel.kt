package com.fmklab.armnk_results.presentation.base

import android.app.Instrumentation
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.fmklab.armnk_results.framework.ActivityResult
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable

abstract class BaseViewModel : ViewModel() {

    private val intent = BehaviorRelay.create<Intent>()
    private val activityResult = BehaviorRelay.create<ActivityResult>()

    fun intent(intent: Intent) = this.intent.accept(intent)

    fun activityResult(activityResult: ActivityResult) = this.activityResult.accept(activityResult)

    protected fun intent(): Observable<Intent> = intent.hide()

    protected fun activityResult(): Observable<ActivityResult> = activityResult.hide()
}