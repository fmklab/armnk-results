package com.fmklab.armnk_results.presentation.outerring

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.forEach
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectMoment
import com.fmklab.armnk_result.domain.DefectType
import com.fmklab.armnk_result.domain.OuterRing
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setCheckedDistinct
import com.fmklab.armnk_results.framework.extensions.setTextDistinct
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_outer_ring_defect_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class OuterRingDefectInfoFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditOuterViewModel.ViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_outer_ring_defect_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBinding()
        setupListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        controlZoneAutoCompleteTextView.keyListener = null
    }

    private fun setupBinding() {
        viewModel.outputs.fetchingDefectInfo()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadDefectInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showAllViewLoading() }
            .addTo(compositeDisposable)

        viewModel.outputs.outerRing()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it.defectMoment) {
                    DefectMoment.DEFECATION -> defecationRadio.setCheckedDistinct(true)
                    DefectMoment.VISUAL -> visualRadio.setCheckedDistinct(true)
                    DefectMoment.NDT -> ndtRadio.setCheckedDistinct(true)
                    DefectMoment.NONE -> visualRadio.setCheckedDistinct(true)
                }
                defectTypeAutoCompleteTextView.setText(it.defectType?.toString() ?: "", false)
                controlZoneAutoCompleteTextView.setText(it.controlZone?.toString() ?: "", false)
                defectDescriptionEditText.setTextDistinct(it.defectDescription)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.defectTypes()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.initArrayAdapter(
                    requireContext(),
                    ArrayList(it)
                )
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlZones()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlZoneAutoCompleteTextView.initArrayAdapter(
                    requireContext(),
                    ArrayList(it)
                )
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showInvalidFields)
            .addTo(compositeDisposable)

        Observable.zip(
            defecationRadio.checkedChanges().skipInitialValue(),
            viewModel.outputs.outerRing(),
            BiFunction<Boolean, OuterRing, Pair<Boolean, DefectMoment>> { isChecked, _ ->
                Pair(isChecked, DefectMoment.DEFECATION)
            }
        )
            .filter { it.first }
            .map { it.second }
            .subscribe(this::changeDefectMoment)
            .addTo(compositeDisposable)

        Observable.zip(
            visualRadio.checkedChanges().skipInitialValue(),
            viewModel.outputs.outerRing(),
            BiFunction<Boolean, OuterRing, Pair<Boolean, DefectMoment>> { isChecked, _ ->
                Pair(isChecked, DefectMoment.VISUAL)
            }
        )
            .filter { it.first }
            .map { it.second }
            .subscribe(this::changeDefectMoment)
            .addTo(compositeDisposable)

        Observable.zip(
            ndtRadio.checkedChanges().skipInitialValue(),
            viewModel.outerRing(),
            BiFunction<Boolean, OuterRing, Pair<Boolean, DefectMoment>> { isChecked, _ ->
                Pair(isChecked, DefectMoment.NDT)
            }
        )
            .filter { it.first }
            .map { it.second }
            .subscribe(this::changeDefectMoment)
            .addTo(compositeDisposable)

        defectTypeAutoCompleteTextView
            .itemSelected<DefectType>()
            .subscribe(viewModel.inputs::defectType)
            .addTo(compositeDisposable)

        controlZoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .subscribe(viewModel.inputs::controlZone)
            .addTo(compositeDisposable)

        defectDescriptionEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::defectDescription)
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        defectTypeAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            defectTypeTextInputLayout.error = null
        }

        controlZoneAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            controlZoneTextInputLayout.error = null
        }
    }

    private fun showAllViewLoading() {
        defectInfoRadioGroup.forEach { it.disable() }
        defectTypeTextInputLayout.disable()
        defectTypeAutoCompleteTextView.disable()
        controlZoneTextInputLayout.disable()
        controlZoneAutoCompleteTextView.disable()
        defectDescriptionTextInputLayout.disable()
        defectDescriptionEditText.disable()
        shimmerViewContainer.showShimmer(true)
    }

    private fun hideAllViewLoading() {
        defectInfoRadioGroup.forEach { it.enable() }
        defectTypeTextInputLayout.enable()
        defectTypeAutoCompleteTextView.enable()
        controlZoneTextInputLayout.enable()
        controlZoneAutoCompleteTextView.enable()
        defectDescriptionTextInputLayout.enable()
        defectDescriptionEditText.enable()
        shimmerViewContainer.hideShimmer()
    }

    private fun showInvalidFields(fields: List<OuterRing.InvalidField>) {
        for (field in fields) {
            when (field) {
                is OuterRing.InvalidField.DefectMoment -> Toast.makeText(
                    requireContext(),
                    field.reason,
                    Toast.LENGTH_SHORT
                ).show()
                is OuterRing.InvalidField.DefectType -> defectTypeTextInputLayout.error = field.reason
                is OuterRing.InvalidField.ControlZone -> controlZoneTextInputLayout.error = field.reason
            }
        }
    }

    private fun changeDefectMoment(defectMoment: DefectMoment) {
        viewModel.inputs.defectMoment(defectMoment)
        viewModel.inputs.loadDefectInfo()
    }
}










































