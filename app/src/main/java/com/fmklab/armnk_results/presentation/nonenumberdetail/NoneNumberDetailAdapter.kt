package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.common.adapters.DetailItemAdapter
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.common.DetailPagingViewHolder
import com.fmklab.armnk_results.presentation.common.DetailsPagingAdapter
import com.fmklab.armnk_results.presentation.common.ErrorPagingViewHolder
import com.fmklab.armnk_results.presentation.common.PagingItem

class NoneNumberDetailAdapter : DetailsPagingAdapter<NoneNumberDetail>() {

    override fun inflateView(parent: ViewGroup, viewType: Int): View {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_ITEM -> inflater.inflate(R.layout.small_detail_item, parent, false)
            VIEW_LOADING -> inflater.inflate(R.layout.progress_item, parent, false)
            else -> inflater.inflate(R.layout.refresh_item, parent, false)
        }
    }

    override fun createDetailPagingViewHolder(view: View): BaseViewHolder<PagingItem<NoneNumberDetail>> {
        return NoneNumberDetailViewHolder(view)
    }
}













































