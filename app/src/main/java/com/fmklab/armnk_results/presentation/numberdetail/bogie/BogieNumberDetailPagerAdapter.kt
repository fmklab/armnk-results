package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.fmklab.armnk_results.presentation.numberdetail.NumberDetailDefectInfoFragment
import com.fmklab.armnk_results.presentation.numberdetail.NumberDetailRepairInfoFragment

class BogieNumberDetailPagerAdapter(fm: FragmentManager, behaviour: Int) : FragmentStatePagerAdapter(fm, behaviour) {

    private val fragments = arrayOf(
        BogieNumberDetailBaseInfoFragment(),
        BogieNumberDetailDefectInfoFragment(),
        BogieNumberDetailRepairInfoFragment()
    )

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragments[position] = fragment
        return fragment
    }
}