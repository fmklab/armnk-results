package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.CompanyEnum
import com.fmklab.armnk_result.domain.FaultEnum
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.setCheckedDistinct
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jakewharton.rxbinding4.widget.checkedChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_service_point.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class BogieServicePointFragment : BottomSheetDialogFragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditBogieNumberDetailViewModel.ViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_service_point, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBinding()
        setupListeners()
    }

    private fun setupBinding() {
        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when(it.servicePoint.companyEnum) {
                    CompanyEnum.NONE -> vrk1Radio.setCheckedDistinct(true)
                    CompanyEnum.VRK1 -> vrk1Radio.setCheckedDistinct(true)
                    CompanyEnum.VRK2 -> vrk2Radio.setCheckedDistinct(true)
                    CompanyEnum.VRK3 -> vrk3Radio.setCheckedDistinct(true)
                    CompanyEnum.OTHER -> otherRadio.setCheckedDistinct(true)
                }
                when(it.servicePoint.faultEnum) {
                    FaultEnum.NONE -> {
                        depotCheckBox.setCheckedDistinct(false)
                        factoryCheckBox.setCheckedDistinct(false)
                        exploitationCheckBox.setCheckedDistinct(false)
                    }
                    FaultEnum.DEPOT -> depotCheckBox.setCheckedDistinct(true)
                    FaultEnum.FACTORY -> factoryCheckBox.setCheckedDistinct(true)
                    FaultEnum.EXPLOITATION -> exploitationCheckBox.setCheckedDistinct(true)
                }
            }
            .addTo(compositeDisposable)

        vrk1Radio
            .checkedChanges()
            .skipInitialValue()
            .filter { it }
            .subscribe {
                viewModel.inputs.servicePointCompany(CompanyEnum.VRK1)
            }
            .addTo(compositeDisposable)

        vrk2Radio
            .checkedChanges()
            .skipInitialValue()
            .filter { it }
            .subscribe {
                viewModel.inputs.servicePointCompany(CompanyEnum.VRK2)
            }
            .addTo(compositeDisposable)

        vrk3Radio
            .checkedChanges()
            .skipInitialValue()
            .filter { it }
            .subscribe {
                viewModel.inputs.servicePointCompany(CompanyEnum.VRK3)
            }
            .addTo(compositeDisposable)

        otherRadio
            .checkedChanges()
            .skipInitialValue()
            .filter { it }
            .subscribe {
                viewModel.inputs.servicePointCompany(CompanyEnum.OTHER)
            }
            .addTo(compositeDisposable)

        depotCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                if (it) {
                    factoryCheckBox.setCheckedDistinct(false)
                    exploitationCheckBox.setCheckedDistinct(false)
                    viewModel.inputs.servicePointFault(FaultEnum.DEPOT)
                } else {
                    setFaultToNone()
                }
            }
            .addTo(compositeDisposable)

        factoryCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                if (it) {
                    depotCheckBox.setCheckedDistinct(false)
                    exploitationCheckBox.setCheckedDistinct(false)
                    viewModel.inputs.servicePointFault(FaultEnum.FACTORY)
                } else {
                    setFaultToNone()
                }
            }
            .addTo(compositeDisposable)

        exploitationCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                if (it) {
                    depotCheckBox.setCheckedDistinct(false)
                    factoryCheckBox.setCheckedDistinct(false)
                    viewModel.inputs.servicePointFault(FaultEnum.EXPLOITATION)
                } else {
                    setFaultToNone()
                }
            }
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        acceptServicePointFab.setOnClickListener {
            dismiss()
        }
    }

    private fun setFaultToNone() {
        if (!depotCheckBox.isChecked && !factoryCheckBox.isChecked && !exploitationCheckBox.isChecked) {
            viewModel.inputs.servicePointFault(FaultEnum.NONE)
        }
    }
}