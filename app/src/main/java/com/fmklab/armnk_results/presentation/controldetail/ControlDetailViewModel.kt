package com.fmklab.armnk_results.presentation.controldetail

import androidx.lifecycle.ViewModel
import com.fmklab.armnk_result.domain.ControlDetail
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_result.domain.User
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.interactors.ControlDetailInteractors
import com.fmklab.armnk_results.framework.room.ControlDetailEntity
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import java.util.concurrent.TimeUnit

interface ControlDetailViewModel {

    interface Inputs {

        fun loadControlDetails()

        fun selectControlDetail(controlDetail: ControlDetail)

        fun logoutClick()
    }

    interface Outputs {

        fun activitySubtitle(): Observable<String>

        fun fetchingControlDetails(): Observable<Boolean>

        fun controlDetails(): Observable<List<ControlDetail>>

        fun showLoginScreen(): Observable<Unit>

        fun showWheelScreen(): Observable<Unit>

        fun showAxleScreen(): Observable<Unit>

        fun showNoneNumberDetailScreen(): Observable<ControlDetail>

        fun showNumberDetailScreen(): Observable<ControlDetail>

        fun showBogieNumberDetailScreen(): Observable<ControlDetail>

        fun showOuterRingScreen(): Observable<ControlDetail>

        fun error(): Observable<Error>
    }

    class ViewModel(
        private val interactors: ControlDetailInteractors,
        private val currentUser: CurrentUser,
        private val errorHandler: ErrorHandler
    ) : BaseViewModel(), Inputs, Outputs {

        private val compositeDisposable = CompositeDisposable()

        private val loadControlDetails = PublishRelay.create<Unit>()
        private val selectedControlDetail = PublishRelay.create<ControlDetail>()

        private val activitySubtitle = BehaviorRelay.create<String>()
        private val fetchingControlDetails = BehaviorRelay.createDefault(false)
        private val controlDetails = BehaviorRelay.create<List<ControlDetail>>()
        private val showLoginScreen = PublishRelay.create<Unit>()
        private val controlDetailsError = PublishRelay.create<Error>()
        private val showWheelScreen = PublishRelay.create<Unit>()
        private val showAxleScreen = PublishRelay.create<Unit>()
        private val showNoneNumberDetailScreen = PublishRelay.create<ControlDetail>()
        private val showNumberDetailScreen = PublishRelay.create<ControlDetail>()
        private val showBogieNumberDetailScreen = PublishRelay.create<ControlDetail>()
        private val showOuterRingScreen = PublishRelay.create<ControlDetail>()

        private val noneNumberDetails = arrayOf(3, 4, 5, 8, 9, 10, 13, 14, 15, 16, 17)
        private val numberDetails = arrayOf(6, 7)
        private val bogieNumberDetails = arrayOf(11, 12)
        private val outerRing = 18

        val inputs: Inputs = this
        val outputs: Outputs = this

        init {
            val controlDetailsNotification = loadControlDetails
                .filter { !fetchingControlDetails.value }
                .switchMap {
                    fetchControlDetails()
                }
                .share()

            controlDetailsNotification
                .filter { it.isOnNext }
                .map { it.value }
                .subscribe(controlDetails)
                .addTo(compositeDisposable)

            controlDetailsNotification
                .filter { it.isOnError }
                .map { errorHandler.getError(it.error) }
                .subscribe(controlDetailsError)
                .addTo(compositeDisposable)

            selectedControlDetail
                .filter { it.id == 1 }
                .map { Unit }
                .subscribe(showWheelScreen)
                .addTo(compositeDisposable)

            selectedControlDetail
                .filter { it.id == 2 }
                .map { Unit }
                .subscribe(showAxleScreen)
                .addTo(compositeDisposable)

            selectedControlDetail
                .filter { noneNumberDetails.contains(it.id) }
                .subscribe(showNoneNumberDetailScreen)
                .addTo(compositeDisposable)

            selectedControlDetail
                .filter { numberDetails.contains(it.id) }
                .subscribe(showNumberDetailScreen)
                .addTo(compositeDisposable)

            selectedControlDetail
                .filter { bogieNumberDetails.contains(it.id) }
                .subscribe(showBogieNumberDetailScreen)
                .addTo(compositeDisposable)

            selectedControlDetail
                .filter { it.id == outerRing }
                .subscribe(showOuterRingScreen)
                .addTo(compositeDisposable)

            currentUser.observeCurrentUser()
                .filter { it == User.DEFAULT }
                .map { Unit }
                .subscribe(showLoginScreen)
                .addTo(compositeDisposable)

            currentUser.observeCurrentUser()
                .filter { it != User.DEFAULT }
                .subscribe { activitySubtitle.accept(it.company.name) }
                .addTo(compositeDisposable)
        }

        private fun fetchControlDetails() =
            interactors.getControlDetails()
                .doOnSubscribe { fetchingControlDetails.accept(true) }
                .doAfterTerminate { fetchingControlDetails.accept(false) }
                .materialize()

        override fun loadControlDetails() = loadControlDetails.accept(Unit)

        override fun selectControlDetail(controlDetail: ControlDetail) =
            selectedControlDetail.accept(controlDetail)

        override fun logoutClick() = currentUser.logout()

        override fun activitySubtitle(): Observable<String> = activitySubtitle.hide()

        override fun fetchingControlDetails(): Observable<Boolean> = fetchingControlDetails.hide()

        override fun controlDetails(): Observable<List<ControlDetail>> = controlDetails.hide()

        override fun showLoginScreen(): Observable<Unit> = showLoginScreen.hide()

        override fun showWheelScreen(): Observable<Unit> = showWheelScreen.hide()

        override fun showAxleScreen(): Observable<Unit> = showAxleScreen.hide()

        override fun showNoneNumberDetailScreen(): Observable<ControlDetail> =
            showNoneNumberDetailScreen.hide()

        override fun showNumberDetailScreen(): Observable<ControlDetail> =
            showNumberDetailScreen.hide()

        override fun showBogieNumberDetailScreen(): Observable<ControlDetail> =
            showBogieNumberDetailScreen.hide()

        override fun showOuterRingScreen(): Observable<ControlDetail> =
            showOuterRingScreen.hide()

        override fun error(): Observable<Error> = controlDetailsError.hide()
    }
}