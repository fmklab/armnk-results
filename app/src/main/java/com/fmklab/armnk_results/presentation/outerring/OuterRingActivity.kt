package com.fmklab.armnk_results.presentation.outerring

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.fmklab.armnk_result.domain.OuterRingListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.di.OuterRingScope
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.DetailsPagingAdapter
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_small_detail.*
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.ext.getOrCreateScope

class OuterRingActivity : PaginationActivity<OuterRingListItem>() {

    private val compositeDisposable = CompositeDisposable()

    private val outerRingScope = getKoin().get<OuterRingScope>().getOrCreateScope()

    override val viewModel: PaginationViewModel.ViewModel<OuterRingListItem> by outerRingScope.viewModel<OuterRingViewModel.ViewModel>(this)

    override val recyclerViewAdapter: DetailsPagingAdapter<OuterRingListItem> by outerRingScope.inject<OuterRingAdapter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_small_detail)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
        outerRingScope.close()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setupUI() {
        setSupportActionBar(smallDetailToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar = findViewById(R.id.smallDetailToolBar)
        recyclerView = findViewById(R.id.smallDetailRecyclerView)
        addNewDetailFab = findViewById(R.id.addNewSmallDetailFab)
        swipeRefreshLayout = findViewById(R.id.smallDetailSwipeRefreshLayout)
        emptyDataTextView = findViewById(R.id.emptyDataTextView)

        super.setupUI()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showNewDetailEditor()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.setClass(this, EditOuterRingActivity::class.java)
                startActivityForResult(it, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun createDetailsFragment(): DetailsBottomSheetDialogFragment<OuterRingListItem> {
        return OuterRingDetailsFragment()
    }
}







































