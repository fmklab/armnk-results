package com.fmklab.armnk_results.presentation.numberdetail.base

import com.fmklab.armnk_result.domain.BaseNumberDetailListItem
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.google.gson.Gson

interface BaseNumberDetailViewModel : PaginationViewModel {

    interface Inputs<T : BaseNumberDetailListItem> : PaginationViewModel.Inputs<T>

    interface Outputs<T : BaseNumberDetailListItem> : PaginationViewModel.Outputs<T>

    abstract class ViewModel<T : BaseNumberDetailListItem>(private val errorHandler: ErrorHandler) :
        PaginationViewModel.ViewModel<T>(errorHandler), Inputs<T>, Outputs<T>
}