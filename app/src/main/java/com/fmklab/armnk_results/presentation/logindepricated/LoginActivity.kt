package com.fmklab.armnk_results.presentation.logindepricated

import android.os.Bundle
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setGone
import com.fmklab.armnk_results.framework.extensions.setVisible
import com.fmklab.armnk_results.presentation.basedepricated.BaseActivity
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.activity_auth.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import java.util.concurrent.TimeUnit

class LoginActivity :
    BaseActivity<LoginViewState, LoginViewEvent, LoginViewModelEvent, LoginViewModel>() {

    override val viewModel: LoginViewModel by lifecycleScope.viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_auth)
        super.onCreate(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        setupBindings()
    }

    override fun renderViewState(viewState: LoginViewState) {
        renderLoading(viewState.loadingStatus)
        renderFormValidation(viewState.formValidationStatuses)
    }

    private fun renderLoading(loadingStatus: LoadingStatus) {
        when (loadingStatus) {
            LoadingStatus.FetchingUser -> {
                loadingProgressBar.setVisible()
                signInButton.disable()
                loginTextInputEditText.disable()
                passwordTextInputField.disable()
            }
            LoadingStatus.NoLoading -> {
                loadingProgressBar.setGone()
                signInButton.enable()
                loginTextInputEditText.enable()
                passwordTextInputField.enable()
            }
        }
    }

    private fun renderFormValidation(formValidationStatuses: List<FormValidationStatus>) {
        loginTextInputLayout.error = null
        passwordTextInputLayout.error = null
        loginTextInputLayout.helperText = getString(R.string.login_helper_text)
        for (vs in formValidationStatuses) {
            when (vs) {
                FormValidationStatus.EmptyUsername -> {
                    loginTextInputLayout.error =
                        getString(R.string.no_login_error)
                }
                FormValidationStatus.EmptyPassword -> passwordTextInputLayout.error =
                    getString(R.string.no_password_error)
            }
        }
    }

    override fun renderViewEvent(viewEvent: LoginViewEvent) {
        TODO("Not yet implemented")
    }

    private fun setupBindings() {
        loginTextInputEditText
            .textChanges()
            .map(CharSequence::toString)
            .subscribe(viewModel.usernameChangedConsumer)
            .addTo(compositeDisposable)

        passwordTextInputField
            .textChanges()
            .map(CharSequence::toString)
            .subscribe(viewModel.passwordChangedConsumer)
            .addTo(compositeDisposable)

        signInButton
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel.viewModelEventsConsumer.accept(LoginViewModelEvent.Login)
            }
            .addTo(compositeDisposable)
    }
}




































