package com.fmklab.armnk_results.presentation.common

import android.view.View
import android.widget.Button
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import kotlinx.android.synthetic.main.refresh_item.view.*

class ErrorPagingViewHolder<T>(itemView: View) : BaseViewHolder<PagingItem<T>>(itemView) {

    val refreshButton: Button = itemView.findViewById(R.id.refreshItemButton)

    override fun bind(item: PagingItem<T>) {
        itemView.refreshMessageTextView.text = item.error
    }

    interface OnRefreshClickListener {

        fun onRefreshClick()
    }
}