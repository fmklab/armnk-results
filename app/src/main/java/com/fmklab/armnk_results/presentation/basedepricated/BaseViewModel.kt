package com.fmklab.armnk_results.presentation.basedepricated

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.Consumer

abstract class BaseViewModel<VS : BaseViewState, VE : BaseViewEvent, VME : BaseViewModelEvent> :
    ViewModel() {

    protected val compositeDisposable = CompositeDisposable()

    private var _viewState: VS? = null

    protected var viewState: VS
        get() = _viewState
            ?: throw UninitializedPropertyAccessException("View state not initialized")
        set(value) {
            _viewState = value
            viewStates.accept(value)
        }

    private val viewStates: BehaviorRelay<VS> = BehaviorRelay.create()
    val viewStatesObservable: Observable<VS> = viewStates.hide()

    protected val viewEvents: PublishRelay<VE> = PublishRelay.create()
    val viewEventsObservable: Observable<VE> = viewEvents.hide()

    protected val viewModelEvents: PublishRelay<VME> = PublishRelay.create()
    val viewModelEventsConsumer: Consumer<VME> = viewModelEvents
}


































