package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.view.clicks
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_small_detail_details.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class NoneNumberDetailsFragment : DetailsBottomSheetDialogFragment<NoneNumberDetail>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: PaginationViewModel.ViewModel<NoneNumberDetail> by sharedViewModel<NoneNumberDetailViewModel.ViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_small_detail_details, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showDetailEditor()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.setClass(requireContext(), EditNoneNumberDetailActivity::class.java)
                startActivityForResult(it, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun bind(detail: NoneNumberDetail) {
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        controlDateValueTextView.text = simpleDateFormat.format(detail.controlDate)
        methodValueTextView.text = detail.method
        controlZoneValueTextView.text = detail.controlZone?.toString() ?: "-"
        detectorValueTextView.text = detail.detector?.toString() ?: "-"
        allCheckedValueTextView.text = detail.allChecked.toString()
        defecationCheckedValueTextView.text = detail.defecationChecked.toString()
        visualCheckedValueTextView.text = detail.visualChecked.toString()
        ndtCheckedValueTextView.text = detail.ndtChecked.toString()
        specValueTextView.text = detail.spec

        editSmallDetailFab.setOnClickListener {
            if (detail.editStatus) {
                viewModel.inputs.editDetail(detail)
            } else {
                Snackbar.make(editMessageCoordinatorLayout, getString(R.string.edit_none_number_detail_error), Snackbar.LENGTH_SHORT).show()
            }
        }
        val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
        createDateValueTextView.text = createDateFormat.format(detail.createDate)
    }
}












































