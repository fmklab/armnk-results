package com.fmklab.armnk_results.presentation.numberdetail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.NumberDetailListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.modules.number_detail.activities.NumberDetailActivity

import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_details.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class NumberDetailsFragment : DetailsBottomSheetDialogFragment<NumberDetailListItem>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: PaginationViewModel.ViewModel<NumberDetailListItem> by sharedViewModel<NumberDetailViewModel.ViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_details, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showDetailEditor()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.setClass(requireContext(), EditNumberDetailActivity::class.java)
                startActivityForResult(it, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun bind(detail: NumberDetailListItem) {
        numberDetailDetailsToolbar.title =
            "${getString(R.string.detail_number)} ${detail.number}"
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        controlDateValueTextView.text = simpleDateFormat.format(detail.controlDate)
        factoryNameValueTextView.text = detail.factoryName
        factoryYearValueTextView.text = detail.factoryYear.toString()
        resultValueTextView.text = detail.controlResult
        if (detail.isFromServicePoint) {
            servicePointImageView.visibility = View.VISIBLE
        } else {
            servicePointImageView.visibility = View.GONE
        }

        if (detail.noRejectionCriteria) {
            noRejectionCriteriaImageView.visibility = View.VISIBLE
        } else {
            noRejectionCriteriaImageView.visibility = View.GONE
        }
        defectInfoValueTextView.text = detail.defectDescription
        specValueTextView.text = detail.spec

        editNumberDetailFab.setOnClickListener {
            if (detail.editStatus) {
                viewModel.inputs.editDetail(detail)
            } else {
                Snackbar.make(
                    editMessageCoordinatorLayout,
                    getString(R.string.edit_none_number_detail_error),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
        val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
        createDateValueTextView.text = createDateFormat.format(detail.createDate)
    }
}






































