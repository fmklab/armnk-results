package com.fmklab.armnk_results.presentation.numberdetail.bogie

import com.fmklab.armnk_result.domain.BogieNumberDetailListItem
import com.fmklab.armnk_result.domain.NumberDetailListItem
import com.fmklab.armnk_results.di.NumberDetailScope
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.DetailsPagingAdapter
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailActivity
import com.fmklab.armnk_results.presentation.numberdetail.NumberDetailAdapter
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailActivity
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.ext.getOrCreateScope

class BogieNumberDetailActivity : BaseNumberDetailActivity<BogieNumberDetailListItem>() {

    private val compositeDisposable = CompositeDisposable()
    private val numberDetailScope = getKoin().get<NumberDetailScope>().getOrCreateScope()

    override val viewModel: PaginationViewModel.ViewModel<BogieNumberDetailListItem> by numberDetailScope.viewModel<BogieNumberDetailViewModel.ViewModel>(
        this
    )
    override val recyclerViewAdapter: DetailsPagingAdapter<BogieNumberDetailListItem> by numberDetailScope.inject<BogieNumberDetailAdapter>()


    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
        numberDetailScope.close()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showNewDetailEditor()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                intent.setClass(this, EditBogieNumberDetailActivity::class.java)
                startActivityForResult(intent, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun createDetailsFragment(): DetailsBottomSheetDialogFragment<BogieNumberDetailListItem> {
        return BogieNumberDetailsFragment()
    }
}
