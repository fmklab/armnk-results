package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.FragmentStatePagerAdapter
import com.fmklab.armnk_result.domain.BogieNumberDetail
import com.fmklab.armnk_result.domain.ControlResult
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.NumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.di.NumberDetailScope
import com.fmklab.armnk_results.framework.controls.SavingDialog
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.viewpager.pageSelections
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.activity_edit_number_detail.*
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.ext.getOrCreateScope
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.reflect.KFunction

class EditBogieNumberDetailActivity : BaseActivity<EditBogieNumberDetailViewModel.ViewModel>() {

    private val compositeDisposable = CompositeDisposable()
    private val numberDetailScope = getKoin().get<NumberDetailScope>().getOrCreateScope()

    override val viewModel: EditBogieNumberDetailViewModel.ViewModel by numberDetailScope.viewModel(
        this
    )
    private lateinit var viewPagerAdapter: BogieNumberDetailPagerAdapter
    private lateinit var savingDialog: SavingDialog
    private val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)

    companion object {
        private const val BASE_INFO_PAGE = 0
        private const val DEFECT_INFO_PAGE = 1
        private const val REPAIR_INFO_PAGE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_number_detail)

        setupUI()
        setupBinding()

        if (savedInstanceState == null) {
            viewModel.inputs.clearCache()
        }

        viewModel.inputs.loadDetail()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                showExitWithoutSavingDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        showExitWithoutSavingDialog()
    }

    private fun setupUI() {
        setSupportActionBar(editDetailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewPagerAdapter = BogieNumberDetailPagerAdapter(
            supportFragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        editDetailViewPager.adapter = viewPagerAdapter
        savingDialog = SavingDialog(this)
    }

    private fun setupBinding() {
        viewModel.outputs.title()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                supportActionBar?.title = it
                title = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.noInternetConnection()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    supportActionBar?.title = getString(R.string.wait_for_connection)
                } else {
                    supportActionBar?.title = title
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadDetailError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::loadDetail)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadBogieModelDependsError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::refreshBogieModelDependData)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadFactoryDependsError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::refreshFactoryDependData)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it.controlResult?.name) {
                    "годен" -> {
                        nextPageFab.hide()
                        defectInfoChip.disable()
                    }
                    "ремонт", "брак" -> {
                        defectInfoChip.enable()
                    }
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlResultChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::navDetailInputs)
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isNotEmpty() }
            .subscribe {
                MaterialAlertDialogBuilder(this)
                    .setMessage("Заполните все поля")
                    .setTitle("Ошибка сохранения")
                    .setPositiveButton("Ок", null)
                    .show()
            }
            .addTo(compositeDisposable)

        Observable.zip(
            nextPageFab.clicks(),
            viewModel.outputs.numberDetail(),
            BiFunction<Unit, BogieNumberDetail, ControlResult> { _, nd: BogieNumberDetail ->
                nd.controlResult
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::navDetailInputs)
            .addTo(compositeDisposable)

        Observable.zip(
            defectInfoChip.clicks(),
            viewModel.outputs.numberDetail(),
            BiFunction<Unit, BogieNumberDetail, ControlResult> { _, nd: BogieNumberDetail ->
                nd.controlResult
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (!defectInfoChip.isChecked) {
                    defectInfoChip.isChecked = true
                }
                navDetailInputs(it)
            }
            .addTo(compositeDisposable)

        Observable.combineLatest(
            editDetailViewPager.pageSelections(),
            viewModel.outputs.numberDetail(),
            BiFunction<Int, BogieNumberDetail, Pair<Int, ControlResult?>> { p: Int, nd: BogieNumberDetail ->
                Pair(p, nd.controlResult)
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.first == BASE_INFO_PAGE && it.second != null) {
                    when (it.second!!.name) {
                        "годен" -> {
                            nextPageFab.hide()
                            defectInfoChip.disable()
                        }
                        "брак", "ремонт" -> {
                            nextPageFab.show()
                            defectInfoChip.enable()
                        }
                    }
                    prevPageFab.hide()
                    baseInputsChip.isChecked = true
                } else if (it.first == REPAIR_INFO_PAGE || it.first == DEFECT_INFO_PAGE) {
                    nextPageFab.hide()
                    prevPageFab.show()
                    defectInfoChip.isChecked = true
                    viewModel.defectInfoChanged()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.repairInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::loadRepairInfo)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.defecationInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::loadDefecationInfo)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.visualInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::loadVisualInfo)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.ndtInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::loadNdtInfo)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.showSavingDialog()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                MaterialAlertDialogBuilder(this)
                    .setTitle(R.string.save)
                    .setMessage("Сохранить данные\n\n${it}")
                    .setPositiveButton(R.string.yes) { _, _ ->
                        viewModel.inputs.acceptSaving()
                    }
                    .setNegativeButton(R.string.no, null)
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailSaving()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    savingDialog.startSavingDialog()
                } else {
                    savingDialog.dismissDialog()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailSavingError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showError(it, viewModel.inputs::acceptSaving)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailAlreadyAdded()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                MaterialAlertDialogBuilder(this)
                    .setTitle(R.string.attention)
                    .setMessage(
                        "Результаты контроля детали № ${it.number} были добавлены в базу менее одного месяца назад. Дата добавления: ${
                            simpleDateFormat.format(
                                it.date!!
                            )
                        }"
                    )
                    .setPositiveButton("Продолжить") { _, _ ->
                        viewModel.inputs.acceptSavingAddedDetail()
                    }
                    .setNegativeButton("Отмена", null)
                    .show()
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detailSaveSuccess()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setResult(Activity.RESULT_OK, it)
                finish()
            }
            .addTo(compositeDisposable)

        prevPageFab
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe {
                editDetailViewPager.setCurrentItem(BASE_INFO_PAGE, true)
            }
            .addTo(compositeDisposable)

        baseInputsChip
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe {
                if (!baseInputsChip.isChecked) {
                    baseInputsChip.isChecked = true
                }
                editDetailViewPager.setCurrentItem(BASE_INFO_PAGE, true)
            }
            .addTo(compositeDisposable)

        saveDetailButton
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe { viewModel.inputs.attemptSave() }
            .addTo(compositeDisposable)
    }

    private fun showRefreshSnackbar(message: String, action: KFunction<Unit>) {
        Snackbar.make(editDetailConstraintLayout, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(R.string.refresh)) {
                action.call()
            }
            .show()
    }

    private fun showError(error: Error, refreshAction: KFunction<Unit>) {
        when (error) {
            Error.NotFoundRemote -> showRefreshSnackbar("Данные не найдены", refreshAction)
            Error.SocketTimeout -> showRefreshSnackbar(
                "Время ожидания ответа от сервера истекло",
                refreshAction
            )
            Error.BadRequest -> showRefreshSnackbar("Внутренняя ошибка сервера", refreshAction)
            Error.NoInternetConnection -> toast("Ошибка сети. Проверьте соединение с интернетом")
            is Error.Unknown -> showRefreshSnackbar(
                "Произошла непредвиденная ошибка ${error.message}",
                refreshAction
            )
        }
    }

    private fun navDetailInputs(controlResult: ControlResult) {
        when (controlResult.name) {
            "ремонт" -> {
                viewModel.inputs.loadRepairInfo()
                editDetailViewPager.setCurrentItem(REPAIR_INFO_PAGE, true)
            }
            "брак" -> {
                editDetailViewPager.setCurrentItem(DEFECT_INFO_PAGE, true)
            }
        }
    }

    private fun showExitWithoutSavingDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.exit)
            .setMessage(R.string.exit_without_saving_dialog)
            .setPositiveButton(R.string.yes) { _, _ ->
                finish()
            }
            .setNegativeButton(R.string.no, null)
            .show()
    }
}

































