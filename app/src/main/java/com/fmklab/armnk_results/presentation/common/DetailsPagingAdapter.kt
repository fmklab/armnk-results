package com.fmklab.armnk_results.presentation.common

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fmklab.armnk_result.domain.Detail
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.nonenumberdetail.NoneNumberDetailViewHolder

abstract class DetailsPagingAdapter<T> : RecyclerView.Adapter<BaseViewHolder<PagingItem<T>>>() where T: Detail {

    companion object {
        const val VIEW_ITEM = 0
        const val VIEW_LOADING = 1
        const val VIEW_ERROR = 2
    }

    var onRefreshClickListener: ErrorPagingViewHolder.OnRefreshClickListener? = null

    var onMoreInfoClickListener: DetailPagingViewHolder.OnMoreInfoClickListener? = null

    private val items = ArrayList<PagingItem<T>>()

    fun add(items: List<T>) {
        this.items.clear()
        this.items.addAll(items.map { PagingItem.value(it) })
        notifyDataSetChanged()
    }

    fun addOrUpdate(newItem: T) {
        val oldItem = items.find { i -> i.value?.id == newItem.id }
        if (oldItem != null) {
            val index = items.indexOf(oldItem)
            items[index] = PagingItem.value(newItem)
        } else {
            items.add(0, PagingItem.value(newItem))
        }
        notifyDataSetChanged()
    }

    fun getItem(position: Int): T? {
        return items[position].value
    }

    fun showLoading() {
        if (items.isEmpty()) {
            items.add(PagingItem.loading())
        } else {
            val lastIndex = getLastIndex()
            items[lastIndex] = items[lastIndex].copy(status = Status.Loading)
        }
        notifyDataSetChanged()
    }

    fun hideLoading() {
        //clearStatusItems()
        if (items.isNotEmpty()) {
            val lastIndex = getLastIndex()
            items[lastIndex] = items[lastIndex].copy(status = Status.Value)
            notifyDataSetChanged()
        }
    }

    fun hideError() {
        if (items.isNotEmpty()) {
            val lastIndex = getLastIndex()
            items[lastIndex] = items[lastIndex].copy(Status.Value)
            notifyDataSetChanged()
        }
    }

    fun showError(error: String) {
        if (items.isEmpty()) {
            items.add(PagingItem.error(error))
        } else {
            val lastIndex = getLastIndex()
            items[lastIndex] = items[lastIndex].copy(status = Status.Error, error = error)
        }
        notifyDataSetChanged()
    }

    fun valueItemCount(): Int {
        return items.count { i -> i.status == Status.Value }
    }

    private fun getLastIndex(): Int = items.size -1

    private fun clearStatusItems() {
        if (items.isNotEmpty()) {
            items.removeAll { i -> i.status != Status.Value }
            notifyDataSetChanged()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].status) {
            Status.Loading -> VIEW_LOADING
            Status.Value -> VIEW_ITEM
            Status.Error -> VIEW_ERROR
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<PagingItem<T>> {
        val view = inflateView(parent, viewType)
        return when (viewType) {
            VIEW_ITEM -> createDetailPagingViewHolder(view)
            VIEW_LOADING -> LoadingPagingViewHolder(view)
            VIEW_ERROR -> ErrorPagingViewHolder(view)
            else -> LoadingPagingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<PagingItem<T>>, position: Int) {
        val item = items[position]
        holder.bind(item)
        when (holder) {
            is ErrorPagingViewHolder -> {
                holder.refreshButton.setOnClickListener {
                    onRefreshClickListener?.onRefreshClick()
                }
            }
            is DetailPagingViewHolder -> {
                holder.moreInfoButton.setOnClickListener {
                    onMoreInfoClickListener?.onMoreInfoClick(position)
                }
            }
        }
    }

    protected abstract fun inflateView(parent: ViewGroup, viewType: Int): View

    protected abstract fun createDetailPagingViewHolder(view: View): BaseViewHolder<PagingItem<T>>
}





































