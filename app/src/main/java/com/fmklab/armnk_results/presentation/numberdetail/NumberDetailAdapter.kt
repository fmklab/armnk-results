package com.fmklab.armnk_results.presentation.numberdetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.NumberDetailListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.common.DetailsPagingAdapter
import com.fmklab.armnk_results.presentation.common.PagingItem
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailAdapter

class NumberDetailAdapter : BaseNumberDetailAdapter<NumberDetailListItem>() {

    override fun createDetailPagingViewHolder(view: View): BaseViewHolder<PagingItem<NumberDetailListItem>> {
        return NumberDetailViewHolder(view)
    }
}