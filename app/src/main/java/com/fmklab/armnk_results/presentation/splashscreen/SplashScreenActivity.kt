package com.fmklab.armnk_results.presentation.splashscreen

import android.content.Intent
import android.os.Bundle
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.setGone
import com.fmklab.armnk_results.framework.extensions.setInvisible
import com.fmklab.armnk_results.framework.extensions.setVisible
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.fmklab.armnk_results.presentation.login.LoginActivity
import com.jakewharton.rxbinding4.view.clicks
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.activity_loading_screen.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel
import java.util.concurrent.TimeUnit

class SplashScreenActivity : BaseActivity<SplashScreenViewModel.ViewModel>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: SplashScreenViewModel.ViewModel by lifecycleScope.viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading_screen)

        setupBindings()
    }

    override fun onResume() {
        super.onResume()

        viewModel.loadSavedLoginParams()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    private fun setupBindings() {
        viewModel.outputs.fetchingSavedLoginParams()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    loadingProgressBar.setVisible()
                    errorMessageTextView.setGone()
                    refreshButton.setGone()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.serverError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    Error.NoInternetConnection -> showRefresh("Ошибка соединения. Проверьте подключение к сети")
                    Error.BadRequest -> showRefresh("Внутренняя ошибка сервера")
                    Error.SocketTimeout -> showRefresh("Время ожидания ответа от сервера истекло")
                    Error.NotFoundRemote -> showLoginScreen()
                    //else -> showLoginScreen()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadingSuccess()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showControlDetailsScreen()
            }
            .addTo(compositeDisposable)

        viewModel.outputs.savedLoginParamsNotFound()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showLoginScreen()
            }
            .addTo(compositeDisposable)

        refreshButton
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe { viewModel.inputs.loadSavedLoginParams() }
            .addTo(compositeDisposable)
    }

    private fun showLoginScreen() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showControlDetailsScreen() {
        val intent = Intent(this, ControlDetailActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showRefresh(message: String) {
        loadingProgressBar.setInvisible()
        refreshButton.setVisible()
        errorMessageTextView.setVisible()
        errorMessageTextView.text = message
    }
}







































