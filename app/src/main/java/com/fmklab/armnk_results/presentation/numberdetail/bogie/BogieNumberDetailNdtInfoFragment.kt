package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setTextDistinct
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailViewModel
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_ndt.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class BogieNumberDetailNdtInfoFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditBogieNumberDetailViewModel.ViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_ndt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBinding()
        setupListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        methodAutoCompleteTextView.keyListener = null
        defectTypeAutoCompleteTextView.keyListener = null
        detectorAutoCompleteTextView.keyListener = null
        defectZoneAutoCompleteTextView.keyListener = null
        stealAutoCompleteTextView.keyListener = null
    }

    private fun setupBinding() {
        viewModel.outputs.fetchingNdtInfo()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.ndtInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showAllViewLoading() }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingMethodDependData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showMethodDependedDataLoading()
                } else {
                    hideMethodDependedDataLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodAutoCompleteTextView.setText(it.method, false)
                defectTypeAutoCompleteTextView.setText(it.defectType?.toString() ?: "", false)
                detectorAutoCompleteTextView.setText(it.detector?.toString() ?: "", false)
                defectSizeEditText.setTextDistinct(it.defectSize)
                defectZoneAutoCompleteTextView.setText(it.controlZone?.toString() ?: "", false)
                stealAutoCompleteTextView.setText(it.steal, false)
                ownerEditText.setTextDistinct(it.owner)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.methods()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.defectTypes()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.detectors()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detectorAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlZones()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectZoneAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.steals()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                stealAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                for (formItem in it) {
                    when (formItem) {
                        is BogieNumberDetail.InvalidFieldImpl.Method ->
                            methodTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.DefectType ->
                            defectTypeTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Detector ->
                            detectorTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.ControlZone ->
                            defectZoneTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Steal ->
                            stealTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Owner ->
                            ownerTextInputLayout.error = formItem.reason
                    }
                }
            }
            .addTo(compositeDisposable)

        methodAutoCompleteTextView
            .itemSelected<String>()
            .subscribe(viewModel.inputs::method)
            .addTo(compositeDisposable)

        defectTypeAutoCompleteTextView
            .itemSelected<DefectType>()
            .subscribe(viewModel.inputs::defectType)
            .addTo(compositeDisposable)

        detectorAutoCompleteTextView
            .itemSelected<Detector>()
            .subscribe(viewModel.inputs::detector)
            .addTo(compositeDisposable)

        defectSizeEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::defectSize)
            .addTo(compositeDisposable)

        defectZoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .subscribe(viewModel.inputs::controlZone)
            .addTo(compositeDisposable)

        stealAutoCompleteTextView
            .itemSelected<String>()
            .subscribe(viewModel.inputs::steal)
            .addTo(compositeDisposable)

        ownerEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::owner)
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        methodAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            methodTextInputLayout.error = null
        }

        defectTypeAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            defectTypeTextInputLayout.error = null
        }

        detectorAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            detectorTextInputLayout.error = null
        }

        defectZoneAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            defectZoneTextInputLayout.error = null
        }

        stealAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            stealTextInputLayout.error = null
        }

        ownerEditText.doOnTextChanged { _, _, _, _ ->
            ownerTextInputLayout.error = null
        }
    }

    private fun showAllViewLoading() {
        methodAutoCompleteTextView.disable()
        methodTextInputLayout.disable()
        defectTypeAutoCompleteTextView.disable()
        defectTypeTextInputLayout.disable()
        detectorAutoCompleteTextView.disable()
        detectorTextInputLayout.disable()
        defectSizeEditText.disable()
        defectZoneAutoCompleteTextView.disable()
        defectZoneTextInputLayout.disable()
        stealAutoCompleteTextView.disable()
        stealTextInputLayout.disable()
        ownerEditText.disable()
        shimmerViewContainer.showShimmer(true)
        detectorShimmerContainer.hideShimmer()
    }

    private fun hideAllViewLoading() {
        methodAutoCompleteTextView.enable()
        methodTextInputLayout.enable()
        defectTypeAutoCompleteTextView.enable()
        defectTypeTextInputLayout.enable()
        detectorAutoCompleteTextView.enable()
        detectorTextInputLayout.enable()
        defectSizeEditText.enable()
        defectZoneAutoCompleteTextView.enable()
        defectZoneTextInputLayout.enable()
        stealAutoCompleteTextView.enable()
        stealTextInputLayout.enable()
        ownerEditText.enable()
        shimmerViewContainer.hideShimmer()
        detectorShimmerContainer.hideShimmer()
    }

    private fun showMethodDependedDataLoading() {
        detectorAutoCompleteTextView.disable()
        detectorTextInputLayout.disable()
        detectorShimmerContainer.showShimmer(true)
    }

    private fun hideMethodDependedDataLoading() {
        detectorAutoCompleteTextView.enable()
        detectorTextInputLayout.enable()
        detectorShimmerContainer.hideShimmer()
    }
}