package com.fmklab.armnk_results.presentation.logindepricated

import com.fmklab.armnk_results.presentation.basedepricated.BaseViewModelEvent

sealed class LoginViewModelEvent :
    BaseViewModelEvent {

    object Login : LoginViewModelEvent()
}