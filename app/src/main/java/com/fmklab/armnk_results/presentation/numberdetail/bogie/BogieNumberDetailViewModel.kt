package com.fmklab.armnk_results.presentation.numberdetail.bogie

import com.fmklab.armnk_result.domain.BogieNumberDetailListItem
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.framework.extensions.toResult
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.BogieNumberDetailInteractors
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailViewModel
import com.google.gson.Gson
import io.reactivex.rxjava3.core.Observable

interface BogieNumberDetailViewModel : BaseNumberDetailViewModel {

    interface Inputs : BaseNumberDetailViewModel.Inputs<BogieNumberDetailListItem>

    interface Outputs : BaseNumberDetailViewModel.Outputs<BogieNumberDetailListItem>

    class ViewModel(
        private val errorHandler: ErrorHandler,
        private val interactors: BogieNumberDetailInteractors
    ) : BaseNumberDetailViewModel.ViewModel<BogieNumberDetailListItem>(errorHandler), Inputs, Outputs {

        override val inputs: PaginationViewModel.Inputs<BogieNumberDetailListItem> = this
        override val outputs: PaginationViewModel.Outputs<BogieNumberDetailListItem> = this

        override fun fetchDetails(
            page: Int,
            partId: Int
        ): Observable<Result<List<BogieNumberDetailListItem>>> =
            interactors.getAllBogiePaged(partId, page)
                .map { data -> Result.success(data) }
                .onErrorResumeNext { t -> Observable.just(Result.error(errorHandler.getError(t))) }

        override fun getEditedDetail(json: String): BogieNumberDetailListItem {
            return Gson().fromJson(json, BogieNumberDetailListItem::class.java)
        }

        override fun searchDetail(
            partId: Int,
            number: String
        ): Observable<Result<List<BogieNumberDetailListItem>>> {
            return interactors.searchBogieNumberDetail(partId, number)
                .toResult(errorHandler)
        }
    }
}