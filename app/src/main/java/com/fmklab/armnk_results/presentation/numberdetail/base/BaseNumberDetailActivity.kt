package com.fmklab.armnk_results.presentation.numberdetail.base

import android.os.Bundle
import android.view.MenuItem
import com.fmklab.armnk_result.domain.BaseNumberDetailListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailActivity
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_small_detail.*

abstract class BaseNumberDetailActivity<T : BaseNumberDetailListItem> : PaginationActivity<T>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_small_detail)
        super.onCreate(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setupUI() {
        setSupportActionBar(smallDetailToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar = findViewById(R.id.smallDetailToolBar)
        recyclerView = findViewById(R.id.smallDetailRecyclerView)
        addNewDetailFab = findViewById(R.id.addNewSmallDetailFab)
        swipeRefreshLayout = findViewById(R.id.smallDetailSwipeRefreshLayout)
        emptyDataTextView = findViewById(R.id.emptyDataTextView)

        super.setupUI()
    }
}