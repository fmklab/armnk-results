package com.fmklab.armnk_results.presentation.numberdetail

import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_result.domain.NumberDetailListItem
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.NumberDetailInteractors
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailViewModel
import com.google.gson.Gson
import io.reactivex.rxjava3.core.Observable

interface NumberDetailViewModel : BaseNumberDetailViewModel {

    interface Inputs : BaseNumberDetailViewModel.Inputs<NumberDetailListItem>

    interface Outputs : BaseNumberDetailViewModel.Outputs<NumberDetailListItem>

    class ViewModel(
        private val interactors: NumberDetailInteractors,
        private var errorHandler: ErrorHandler
    ) : PaginationViewModel.ViewModel<NumberDetailListItem>(errorHandler), Inputs, Outputs {

        override val inputs: PaginationViewModel.Inputs<NumberDetailListItem> = this

        override val outputs: PaginationViewModel.Outputs<NumberDetailListItem> = this

        override fun fetchDetails(
            page: Int,
            partId: Int
        ): Observable<Result<List<NumberDetailListItem>>> = interactors.getAllPaged(partId, page)
            .map { data -> Result.success(data) }
            .onErrorResumeNext { t -> Observable.just(Result.error(errorHandler.getError(t))) }

        override fun getEditedDetail(json: String): NumberDetailListItem {
            return Gson().fromJson(json, NumberDetailListItem::class.java)
        }

        override fun searchDetail(partId: Int, number: String): Observable<Result<List<NumberDetailListItem>>> {
            return interactors.searchNumberDetail(partId, number).toResultApi(errorHandler)
        }
    }
}