package com.fmklab.armnk_results.presentation.common

import android.content.Intent
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.fmklab.armnk_results.presentation.nonenumberdetail.NoneNumberDetailActivity
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo

interface PaginationViewModel {

    interface Inputs<T : Detail> {

        fun loadFirstPage()

        fun selectDetail(detail: T)

        fun editDetail(detail: T)

        fun newDetail()

        fun nextPage()

        fun refresh()

        fun search(number: String)
    }

    interface Outputs<T : Detail> {

        fun fetchingDetails(): Observable<Boolean>

        fun details(): Observable<List<T>>

        fun error(): Observable<Error>

        fun showMoreInfo(): Observable<T>

        fun showNewDetailEditor(): Observable<Intent>

        fun showDetailEditor(): Observable<Intent>

        fun lastPageLoaded(): Observable<Unit>

        fun title(): Observable<String>

        fun resultItem(): Observable<T>

        fun itemsSearching(): Observable<Boolean>

        fun searchedDetails(): Observable<List<T>>
    }

    abstract class ViewModel<T : Detail>(private val errorHandler: ErrorHandler) : BaseViewModel(),
        Inputs<T>, Outputs<T> {

        private val compositeDisposable = CompositeDisposable()

        private val loadFirstPage = BehaviorRelay.create<Unit>()
        private val selectDetail = BehaviorRelay.create<T>()
        private val editDetail = PublishRelay.create<T>()
        private val newDetail = PublishRelay.create<Unit>()
        private val nextPage = PublishRelay.create<Boolean>()
        private val search = PublishRelay.create<String>()

        private val fetchingDetails = BehaviorRelay.createDefault(false)
        private val details = PublishRelay.create<List<T>>()
        private val error = PublishRelay.create<Error>()
        private val showNewDetailEditor = PublishRelay.create<Intent>()
        private val showDetailEditor = PublishRelay.create<Intent>()
        private val lastPageLoaded = PublishRelay.create<Unit>()
        private val title = BehaviorRelay.create<String>()
        private val resultItem = PublishRelay.create<T>()
        private val itemsSearching = BehaviorRelay.createDefault(false)
        private val searchedDetails = PublishRelay.create<List<T>>()

        private val loadingPage = loadFirstPage.switchMap {
            nextPage.scan(0, { accum: Int, isRefresh: Boolean ->
                if (isRefresh) {
                    accum
                } else {
                    accum + 1
                }
            })
        }

        private val pagedData = loadFirstPage.switchMap {
            details.scan(
                ArrayList(),
                { accum: List<T>, conc: List<T> ->
                    ArrayList(accum + conc).toList()
                })
                .skip(1)
        }

        abstract val inputs: Inputs<T>
        abstract val outputs: Outputs<T>

        init {

            val controlDetail  = intent()
                .map {
                    val json = it.extras?.getString(ControlDetailActivity.SELECTED_CONTROL_DETAIL_TAG)!!
                    Gson().fromJson(json, ControlDetail::class.java)
                }

            val selectedDetail = controlDetail
                .map { it.id }

            newDetail
                .withLatestFrom(
                    controlDetail,
                    BiFunction<Unit, ControlDetail, Intent> { _, cd: ControlDetail ->
                        val json = Gson().toJson(cd)
                        Intent().apply {
                            putExtra(PaginationActivity.SELECTED_CONTROL_DETAIL_TAG, json)
                            putExtra(PaginationActivity.SELECTED_DETAIL_ID_TAG, 0)
                        }
                    }
                )
                .subscribe(showNewDetailEditor)
                .addTo(compositeDisposable)

            editDetail
                .withLatestFrom(
                    controlDetail,
                    BiFunction<T, ControlDetail, Intent> { d: T, cd: ControlDetail ->
                        val json = Gson().toJson(cd)
                        Intent().apply {
                            putExtra(PaginationActivity.SELECTED_CONTROL_DETAIL_TAG, json)
                            putExtra(PaginationActivity.SELECTED_DETAIL_ID_TAG, d.id)
                        }
                    }
                )
                .subscribe(showDetailEditor)
                .addTo(compositeDisposable)

            controlDetail
                .map { it.name }
                .subscribe(title)
                .addTo(compositeDisposable)

            loadingPage
                .withLatestFrom(
                    selectedDetail,
                    BiFunction<Int, Int, Pair<Int, Int>> { page: Int, partId: Int ->
                        Pair(page, partId)
                    }
                )
                .filter { !fetchingDetails.value }
                .concatMap {
                    fetchDetails(it.first, it.second)
                        .doOnSubscribe { fetchingDetails.accept(true) }
                        .doAfterTerminate { fetchingDetails.accept(false) }
                }
                .subscribe {
                    if (it.isError) {
                        error.accept(it.error)
                    } else if (it.isSuccess) {
                        if (it.data!!.isEmpty()) {
                            lastPageLoaded.accept(Unit)
                        }
                        details.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)

            activityResult()
                .filter { it.isOk() }
                .map {
                    getEditedDetail(it.intent.extras?.getString(PaginationActivity.RESULT_ITEM_KEY)!!)
                }
                .subscribe(resultItem)
                .addTo(compositeDisposable)

            search
                .withLatestFrom(
                    controlDetail,
                    BiFunction<String, ControlDetail, Pair<String, Int>> { n, cd ->
                        Pair(n, cd.id)
                    }
                )
                .filter { !itemsSearching.value }
                .switchMap {
                    searchDetail(it.second, it.first)
                        .doOnSubscribe { itemsSearching.accept(true) }
                        .doAfterTerminate { itemsSearching.accept(false) }
                }
                .subscribe {
                    if (it.isError) {
                        error.accept(it.error)
                    } else if (it.isSuccess) {
                        searchedDetails.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)
        }

        abstract fun fetchDetails(page: Int, partId: Int): Observable<Result<List<T>>>

        abstract fun getEditedDetail(json: String): T

        abstract fun searchDetail(partId: Int, number: String): Observable<Result<List<T>>>

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.dispose()
        }

        override fun loadFirstPage() = loadFirstPage.accept(Unit)

        override fun selectDetail(detail: T) = selectDetail.accept(detail)

        override fun editDetail(detail: T) = editDetail.accept(detail)

        override fun newDetail() = newDetail.accept(Unit)

        override fun nextPage() = nextPage.accept(false)

        override fun refresh() = nextPage.accept(true)

        override fun search(number: String) = search.accept(number)

        override fun fetchingDetails(): Observable<Boolean> = fetchingDetails.hide()

        override fun details(): Observable<List<T>> = pagedData.hide()

        override fun error(): Observable<Error> = error.hide()

        override fun showMoreInfo(): Observable<T> = selectDetail.hide()

        override fun showNewDetailEditor(): Observable<Intent> = showNewDetailEditor.hide()

        override fun lastPageLoaded(): Observable<Unit> = lastPageLoaded.hide()

        override fun title(): Observable<String> = title.hide()

        override fun showDetailEditor(): Observable<Intent> = showDetailEditor.hide()

        override fun resultItem(): Observable<T> = resultItem.hide()

        override fun itemsSearching(): Observable<Boolean> = itemsSearching.hide()

        override fun searchedDetails(): Observable<List<T>> = searchedDetails.hide()
    }
}









































