package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.BogieNumberDetail
import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectType
import com.fmklab.armnk_result.domain.NumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setTextDistinct
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailViewModel
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_defecation.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class BogieNumberDetailDefecationFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditBogieNumberDetailViewModel.ViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_defecation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBinding()
        setupListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        defectZoneAutoCompleteTextView.keyListener = null
        stealAutoCompleteTextView.keyListener = null
    }

    private fun setupBinding() {
        viewModel.outputs.fetchingDefecationInfo()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.defecationInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showAllViewLoading() }
            .addTo(compositeDisposable)

        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.setText(it.defectType?.name ?: "", false)
                defectZoneAutoCompleteTextView.setText(it.controlZone?.name ?: "", false)
                stealAutoCompleteTextView.setText(it.steal, false)
                ownerEditText.setTextDistinct(it.owner)
                defectDescriptionEditText.setTextDistinct(it.defectDescription)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.defectTypes()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlZones()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectZoneAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.steals()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                stealAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlZoneEnabled()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    defectZoneAutoCompleteTextView.enable()
                    defectZoneTextInputLayout.enable()
                } else {
                    defectZoneAutoCompleteTextView.disable()
                    defectZoneTextInputLayout.disable()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                for (formItem in it) {
                    when (formItem) {
                        is BogieNumberDetail.InvalidFieldImpl.DefectType ->
                            defectTypeTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.ControlZone ->
                            defectZoneTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Steal ->
                            stealTextInputLayout.error = formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Owner ->
                            ownerTextInputLayout.error = formItem.reason
                    }
                }
            }
            .addTo(compositeDisposable)

        defectTypeAutoCompleteTextView
            .itemSelected<DefectType>()
            .subscribe(viewModel.inputs::defectType)
            .addTo(compositeDisposable)

        defectZoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .subscribe(viewModel.inputs::controlZone)
            .addTo(compositeDisposable)

        stealAutoCompleteTextView
            .itemSelected<String>()
            .subscribe(viewModel.inputs::steal)
            .addTo(compositeDisposable)

        ownerEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::owner)
            .addTo(compositeDisposable)

        defectDescriptionEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::defectDescription)
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        defectTypeAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            defectTypeTextInputLayout.error = null
        }

        defectZoneAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            defectZoneTextInputLayout.error = null
        }

        stealAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            stealTextInputLayout.error = null
        }

        ownerEditText.doOnTextChanged { _, _, _, _ ->
            ownerTextInputLayout.error = null
        }
    }

    private fun showAllViewLoading() {
        defectTypeTextInputLayout.disable()
        defectTypeAutoCompleteTextView.disable()
        defectZoneTextInputLayout.disable()
        defectTypeAutoCompleteTextView.disable()
        stealTextInputLayout.disable()
        stealAutoCompleteTextView.disable()
        ownerEditText.disable()
        defectDescriptionEditText.disable()
        shimmerViewContainer.showShimmer(true)
    }

    private fun hideAllViewLoading() {
        defectTypeTextInputLayout.enable()
        defectTypeAutoCompleteTextView.enable()
        defectZoneTextInputLayout.enable()
        defectTypeAutoCompleteTextView.enable()
        stealTextInputLayout.enable()
        stealAutoCompleteTextView.enable()
        ownerEditText.enable()
        defectDescriptionEditText.enable()
        shimmerViewContainer.hideShimmer()
    }
}