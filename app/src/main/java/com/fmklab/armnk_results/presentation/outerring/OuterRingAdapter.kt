package com.fmklab.armnk_results.presentation.outerring

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.OuterRingListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.common.DetailsPagingAdapter
import com.fmklab.armnk_results.presentation.common.PagingItem

class OuterRingAdapter : DetailsPagingAdapter<OuterRingListItem>() {

    override fun inflateView(parent: ViewGroup, viewType: Int): View {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType) {
            VIEW_ITEM -> inflater.inflate(R.layout.number_detail_item, parent,false)
            VIEW_LOADING -> inflater.inflate(R.layout.progress_item, parent, false)
            else -> inflater.inflate(R.layout.refresh_item, parent, false)
        }
    }

    override fun createDetailPagingViewHolder(view: View): BaseViewHolder<PagingItem<OuterRingListItem>> {
        return OuterRingViewHolder(view)
    }
}