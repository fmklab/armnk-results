package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.view.View
import com.fmklab.armnk_result.domain.BogieNumberDetailListItem
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailViewHolder

class BogieNumberDetailViewHolder(itemView: View) :
    BaseNumberDetailViewHolder<BogieNumberDetailListItem>(itemView)