package com.fmklab.armnk_results.presentation.controldetail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.ControlDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.base.BaseRecyclerViewAdapter
import com.fmklab.armnk_results.presentation.base.BaseViewHolder

class ControlDetailAdapter(private val context: Context) : BaseRecyclerViewAdapter<ControlDetail>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ControlDetail> {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.control_detail_item, parent, false)

        return ControlDetailViewHolder(view, context)
    }
}