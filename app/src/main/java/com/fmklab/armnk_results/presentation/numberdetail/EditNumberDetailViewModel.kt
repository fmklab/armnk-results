package com.fmklab.armnk_results.presentation.numberdetail

import android.content.Intent
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.*
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.NumberDetailInteractors
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.functions.Function5
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

interface EditNumberDetailViewModel {

    interface Inputs {

        fun loadDetail()

        fun controlDate(value: Date)

        fun factory(value: Factory)

        fun factoryYear(value: FactoryYear?)

        fun isNewDetail(value: Boolean)

        fun number(value: String)

        fun isFromServicePoint(value: Boolean)

        fun servicePointCompany(value: CompanyEnum)

        fun servicePointFault(value: FaultEnum)

        fun noRejectionCriteria(value: Boolean)

        fun controlResult(value: ControlResult)

        fun refreshFactoryDependData()

        fun clearCache()

        fun loadRepairInfo()

        fun repairDefectType(value: DefectType)

        fun repairControlZone(value: ControlZone)

        fun repairDescription(value: String)

        fun defectMoment(value: NumberDetailDefectMoment)

        fun loadDefecationInfo()

        fun defectType(value: DefectType)

        fun controlZone(value: ControlZone?)

        fun steal(value: String)

        fun owner(value: String)

        fun defectDescription(value: String)

        fun loadVisualInfo()

        fun defectLength(value: String)

        fun defectDepth(value: String)

        fun defectDiameter(value: String)

        fun loadNdtInfo()

        fun method(value: String)

        fun defectSize(value: String)

        fun detector(value: Detector)

        fun attemptSave()

        fun reloadDefectInfo()

        fun defectInfoChanged()

        fun acceptSaving()

        fun acceptSavingAddedDetail()
    }

    interface Outputs {

        fun title(): Observable<String>

        fun numberDetail(): Observable<NumberDetail>

        fun fetchingDetail(): Observable<Boolean>

        fun fetchingFactoryDependData(): Observable<Boolean>

        fun loadDetailError(): Observable<Error>

        fun loadFactoryDependsError(): Observable<Error>

        fun factories(): Observable<List<Factory>>

        fun factoryYears(): Observable<List<FactoryYear>>

        fun controlResults(): Observable<List<ControlResult>>

        fun noInternetConnection(): Observable<Boolean>

        fun fetchingRepairInfo(): Observable<Boolean>

        fun repairDefectTypes(): Observable<List<DefectType>>

        fun repairControlZones(): Observable<List<ControlZone>>

        fun controlResultChanged(): Observable<ControlResult>

        fun repairInfoError(): Observable<Error>

        fun fetchingDefecationInfo(): Observable<Boolean>

        fun defectTypes(): Observable<List<DefectType>>

        fun controlZones(): Observable<List<ControlZone>>

        fun steals(): Observable<List<String>>

        fun defecationInfoError(): Observable<Error>

        fun fetchingVisualInfo(): Observable<Boolean>

        fun visualInfoError(): Observable<Error>

        fun fetchingNdtInfo(): Observable<Boolean>

        fun methods(): Observable<List<String>>

        fun detectors(): Observable<List<Detector>>

        fun fetchingMethodDependData(): Observable<Boolean>

        fun ndtInfoError(): Observable<Error>

        fun controlZoneEnabled(): Observable<Boolean>

        fun invalidForm(): Observable<List<InvalidField>>

        fun showSavingDialog(): Observable<String>

        fun detailSaving(): Observable<Boolean>

        fun detailAlreadyAdded(): Observable<AddedDetail>

        fun detailSavingError(): Observable<Error>

        fun detailSaveSuccess(): Observable<Intent>
    }

    @Suppress("UNCHECKED_CAST")
    class ViewModel(
        private val interactors: NumberDetailInteractors,
        private val errorHandler: ErrorHandler,
        private val currentUser: CurrentUser
    ) : BaseViewModel(), Inputs, Outputs {

        private val compositeDisposable = CompositeDisposable()

        private val numberDetailInput = PublishRelay.create<NumberDetail>()
        private val loadDetail = PublishRelay.create<Unit>()
        private val factoryChanged = PublishRelay.create<Factory>()
        private val loadRepairInfo = PublishRelay.create<Unit>()
        private val loadDefecationInfo = PublishRelay.create<Unit>()
        private val loadVisualInfo = PublishRelay.create<Unit>()
        private val loadNdtInfo = PublishRelay.create<Unit>()
        private val methodChanged = PublishRelay.create<String>()
        private val defectTypeChanged = PublishRelay.create<DefectType>()
        private val attemptSave = PublishRelay.create<Unit>()
        private val reloadDefectInfo = PublishRelay.create<Unit>()
        private val detailSaving = BehaviorRelay.createDefault(false)
        private val detailAlreadyAdded = PublishRelay.create<AddedDetail>()
        private val detailSavingError = PublishRelay.create<Error>()

        private val title = BehaviorRelay.create<String>()
        private val numberDetailOutput = BehaviorRelay.create<NumberDetail>()
        private val fetchingDetail = BehaviorRelay.createDefault(false)
        private val fetchingFactoryDependData = BehaviorRelay.createDefault(false)
        private val loadDetailError = PublishRelay.create<Error>()
        private val loadFactoryDependsError = PublishRelay.create<Error>()
        private val noInternetConnection = BehaviorRelay.createDefault(false)
        private val factories = BehaviorRelay.create<List<Factory>>()
        private val factoryYears = BehaviorRelay.create<List<FactoryYear>>()
        private val controlResults = BehaviorRelay.create<List<ControlResult>>()
        private val fetchingRepairInfo = BehaviorRelay.createDefault(false)
        private val repairDefectTypes = BehaviorRelay.create<List<DefectType>>()
        private val repairControlZones = BehaviorRelay.create<List<ControlZone>>()
        private val controlResultChanged = PublishRelay.create<ControlResult>()
        private val defectInfoChanged = BehaviorRelay.createDefault(false)
        private val repairInfoError = PublishRelay.create<Error>()
        private val fetchingDefecationInfo = BehaviorRelay.createDefault(false)
        private val defectTypes = BehaviorRelay.create<List<DefectType>>()
        private val controlZones = BehaviorRelay.create<List<ControlZone>>()
        private val steals = BehaviorRelay.create<List<String>>()
        private val defecationInfoError = PublishRelay.create<Error>()
        private val fetchingVisualInfo = BehaviorRelay.createDefault(false)
        private val visualInfoError = PublishRelay.create<Error>()
        private val fetchingNdtInfo = BehaviorRelay.createDefault(false)
        private val methods = BehaviorRelay.create<List<String>>()
        private val detectors = BehaviorRelay.create<List<Detector>>()
        private val fetchingMethodDependData = BehaviorRelay.createDefault(false)
        private val ndtInfoError = PublishRelay.create<Error>()
        private val controlZoneEnabled = BehaviorRelay.createDefault(true)
        private val invalidForm = PublishRelay.create<List<InvalidField>>()
        private val showSavingDialog = PublishRelay.create<String>()
        private val acceptSaving = PublishRelay.create<Unit>()
        private val acceptSavingAddedDetail = PublishRelay.create<Unit>()
        private val detailSaveSuccess = PublishRelay.create<Intent>()

        private val saveDetail = PublishRelay.create<Unit>()

        val inputs: Inputs = this
        val outputs: Outputs = this

        init {
            val selectedDetailId = intent()
                .subscribeOn(Schedulers.io())
                .map { it.extras?.getInt(PaginationActivity.SELECTED_DETAIL_ID_TAG)!! }

            val selectedControlDetail = intent()
                .subscribeOn(Schedulers.io())
                .map {
                    val json =
                        it.extras?.getString(ControlDetailActivity.SELECTED_CONTROL_DETAIL_TAG)!!
                    Gson().fromJson(json, ControlDetail::class.java)
                }

            selectedControlDetail
                .map { it.name }
                .subscribe(title)
                .addTo(compositeDisposable)

            loadDetail
                .withLatestFrom(
                    selectedControlDetail,
                    selectedDetailId,
                    Function3<Unit, ControlDetail, Int, Pair<Int, Int>> { _, cd: ControlDetail, id: Int ->
                        Pair(cd.id, id)
                    }
                )
                .filter { !fetchingDetail.value }
                .doOnNext { fetchingDetail.accept(true) }
                .switchMap {
                    fetchDetail(it.second).toResultApi(noInternetConnection, errorHandler)
                        .switchMap { nd ->
                            if (nd.isError && nd.error != Error.NotFoundRemote) {
                                Observable.just(
                                    NumberDetailInputs(
                                        nd,
                                        Result.error(nd.error!!),
                                        Result.error(nd.error),
                                        Result.error(nd.error)
                                    )
                                )
                            } else {
                                Observable.zip(
                                    fetchFactories(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchFactoryYears(nd.data?.factory?.id ?: 0).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchControlResults(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    Function3 { factories, factoryYears, controlResults ->
                                        if (nd.isError && nd.error == Error.NotFoundRemote) {
                                            NumberDetailInputs(
                                                Result.success(
                                                    NumberDetail.default(it.first)
                                                        .spec(currentUser.getUser().toString())
                                                ) as Result<NumberDetail>,
                                                factories,
                                                factoryYears,
                                                controlResults
                                            )
                                        } else {
                                            NumberDetailInputs(
                                                nd,
                                                factories,
                                                factoryYears,
                                                controlResults
                                            )
                                        }
                                    }
                                )
                            }
                        }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach { fetchingDetail.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = it.detail.data!!
                        val factories = it.factories.data!!
                        val factoryYears = it.factoryYears.data!!
                        val controlResults = it.controlResults.data!!
                        detail.factory(factories.firstOrNull { f -> f.id == detail.factory?.id })
                        detail.controlResult(controlResults.firstOrNull { c -> c.id == detail.controlResult?.id })
                        this.factories.accept(factories)
                        this.factoryYears.accept(factoryYears)
                        this.controlResults.accept(controlResults)
                        numberDetailInput.accept(detail)
                    } else {
                        loadDetailError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            numberDetailInput
                .flatMap {
                    interactors.cacheDetail(it)
                    Observable.just(it)
                }
                .subscribe(numberDetailOutput)
                .addTo(compositeDisposable)

            factoryChanged
                .filter { !fetchingFactoryDependData.value }
                .doOnNext { fetchingFactoryDependData.accept(true) }
                .map { it.id }
                .switchMap {
                    fetchFactoryYears(it).toResultApi(noInternetConnection, errorHandler)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach { fetchingFactoryDependData.accept(false) }
                .subscribe {
                    if (it.isError) {
                        loadFactoryDependsError.accept(it.error)
                    } else if (it.isSuccess) {
                        factoryYears.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)

            factoryYears
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<List<FactoryYear>, NumberDetail, Boolean> { factoryYears, nd ->
                        factoryYears.contains(nd.factoryYear)
                    }
                )
                .subscribe {
                    if (!it) {
                        factoryYear(null)
                    }
                }
                .addTo(compositeDisposable)

            loadRepairInfo
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<Unit, ControlDetail, Int> { _, cd: ControlDetail ->
                        cd.id
                    }
                )
                .filter { !fetchingRepairInfo.value }
                .doOnNext { fetchingRepairInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchRepairDefectTypes(it).toResultApi(noInternetConnection, errorHandler),
                        fetchRepairControlZones(it).toResultApi(noInternetConnection, errorHandler),
                        BiFunction<Result<List<DefectType>>, Result<List<ControlZone>>, NumberDetailRepairInputs> { dt: Result<List<DefectType>>, cz: Result<List<ControlZone>> ->
                            NumberDetailRepairInputs(dt, cz)
                        }
                    )
                }
                .doOnEach { fetchingRepairInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        repairDefectTypes.accept(defectTypes)
                        repairControlZones.accept(controlZones)
                        numberDetailInput.accept(detail)
                    } else {
                        repairInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            repairDefectTypes
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.defectType == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<DefectType, NumberDetail, DefectType> { dt: DefectType, _ ->
                        dt
                    }
                )
                .subscribe(this::defectType)
                .addTo(compositeDisposable)

            repairControlZones
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.controlZone == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<ControlZone, NumberDetail, ControlZone> { cz, _ ->
                        cz
                    }
                )
                .subscribe(this::controlZone)
                .addTo(compositeDisposable)

            defectTypes
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.defectType == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<DefectType, NumberDetail, DefectType> { dt, _ ->
                        dt
                    }
                )
                .subscribe(this::defectType)
                .addTo(compositeDisposable)

            loadDefecationInfo
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<Unit, ControlDetail, Int> { _, cd: ControlDetail ->
                        cd.id
                    }
                )
                .filter { !fetchingDefecationInfo.value }
                .doOnNext { fetchingDefecationInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchDefectTypes(it, DefectMoment.DEFECATION).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchControlZones(it, DefectMoment.DEFECATION).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchSteals().toResultApi(noInternetConnection, errorHandler),
                        Function3<Result<List<DefectType>>, Result<List<ControlZone>>, Result<List<String>>, NumberDetailDefecationInputs> { dt: Result<List<DefectType>>, cz: Result<List<ControlZone>>, s: Result<List<String>> ->
                            NumberDetailDefecationInputs(dt, cz, s)
                        }
                    )
                }
                .doOnEach { fetchingDefecationInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        val steals = it.steals.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.defectTypes.accept(defectTypes)
                        this.controlZones.accept(controlZones)
                        this.steals.accept(steals)
                        numberDetailInput.accept(detail)
                        if (detail.defectType != null) {
                            defectTypeChanged.accept(detail.defectType)
                        }
                    } else {
                        defecationInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            loadVisualInfo
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<Unit, ControlDetail, Int> { _, cd: ControlDetail ->
                        cd.id
                    }
                )
                .filter { !fetchingVisualInfo.value }
                .doOnNext { fetchingVisualInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchDefectTypes(it, DefectMoment.VISUAL).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchControlZones(it, DefectMoment.VISUAL).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchSteals().toResultApi(noInternetConnection, errorHandler),
                        Function3<Result<List<DefectType>>, Result<List<ControlZone>>, Result<List<String>>, NumberDetailVisualInputs> { dt, cz, s ->
                            NumberDetailVisualInputs(dt, cz, s)
                        }
                    )
                }
                .doOnEach { fetchingVisualInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        val steals = it.steals.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.defectTypes.accept(defectTypes)
                        this.controlZones.accept(controlZones)
                        this.steals.accept(steals)
                        numberDetailInput.accept(detail)
                    } else {
                        visualInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            loadNdtInfo
                .withLatestFrom(
                    selectedControlDetail,
                    numberDetailOutput,
                    Function3<Unit, ControlDetail, NumberDetail, Pair<Int, NumberDetail>> { _, cd, nd ->
                        Pair(cd.id, nd)
                    }
                )
                .filter { !fetchingNdtInfo.value }
                .doOnNext { fetchingNdtInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchMethods(it.first).toResultApi(noInternetConnection, errorHandler),
                        fetchDefectTypes(it.first, DefectMoment.NDT).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchDetectors(if (it.second.method == "") "base" else it.second.method).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchControlZones(it.first, DefectMoment.NDT).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchSteals().toResultApi(noInternetConnection, errorHandler),
                        Function5<Result<List<String>>, Result<List<DefectType>>, Result<List<Detector>>, Result<List<ControlZone>>, Result<List<String>>, NumberDetailNdtInputs> {
                                m, dt, d, cz, s ->
                            NumberDetailNdtInputs(m, dt, d, cz, s)
                        }
                    )
                }
                .doOnEach { fetchingNdtInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val methods = it.methods.data!!
                        val defectTypes = it.defectTypes.data!!
                        val detectors = it.detectors.data!!
                        val controlZones = it.controlZones.data!!
                        val steals = it.steals.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.methods.accept(methods)
                        this.defectTypes.accept(defectTypes)
                        this.detectors.accept(detectors)
                        this.controlZones.accept(controlZones)
                        this.steals.accept(steals)
                        numberDetailInput.accept(detail)
                    } else {
                        ndtInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            methodChanged
                .filter { !fetchingMethodDependData.value }
                .doOnNext { fetchingMethodDependData.accept(true) }
                .switchMap {
                    fetchDetectors(it).toResultApi(noInternetConnection, errorHandler)
                }
                .doOnEach { fetchingMethodDependData.accept(false) }
                .subscribe {
                    if (it.isSuccess) {
                        detectors.accept(it.data)
                    } else if (it.isError) {
                        ndtInfoError.accept(it.error)
                        detectors.accept(ArrayList())
                    }
                }
                .addTo(compositeDisposable)

            defectTypeChanged
                .filter { !it.needControlZone }
                .subscribe {
                    controlZone(null)
                }
                .addTo(compositeDisposable)

            defectTypeChanged
                .map { it.needControlZone }
                .subscribe(controlZoneEnabled)
                .addTo(compositeDisposable)

            reloadDefectInfo
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, NumberDetail, NumberDetailDefectMoment> { _, nd ->
                        nd.defectMoment
                    }
                )
                .subscribe {
                    when (it) {
                        NumberDetailDefectMoment.DEFECATION -> loadDefecationInfo()
                        NumberDetailDefectMoment.VISUAL -> loadVisualInfo()
                        NumberDetailDefectMoment.NONE -> loadNdtInfo()
                        else -> {}
                    }
                }
                .addTo(compositeDisposable)

            controlResultChanged
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<ControlResult, NumberDetail, NumberDetail> { _, nd ->
                        nd
                    }
                )
                .subscribe { it.createDefectInfo() }
                .addTo(compositeDisposable)

            val saveDetailValidation = attemptSave
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, NumberDetail, Pair<Boolean, List<InvalidField>>> { _, d: NumberDetail ->
                        d.validate()
                    }
                )
                .subscribeOn(Schedulers.io())

            saveDetailValidation
                .map { it.second }
                .subscribe(invalidForm)
                .addTo(compositeDisposable)

            saveDetailValidation
                .filter { it.first }
                .map { Unit }
                .withLatestFrom(
                    numberDetailOutput,
                    defectInfoChanged,
                    Function3<Unit, NumberDetail, Boolean, String> { _, nd, changed ->
                        if (changed) {
                            nd.createDefectInfo()
                        }
                        nd.defectInfo
                    }
                )
                .subscribe(showSavingDialog)
                .addTo(compositeDisposable)

            acceptSaving
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, NumberDetail, Pair<Int, String>> { _, nd ->
                        Pair(nd.partId, nd.number)
                    }
                )
                .filter { !detailSaving.value }
                .doOnNext { detailSaving.accept(true) }
                .switchMap {
                    fetchAlreadyAddedDetail(it.first, it.second).toResultApi(noInternetConnection, errorHandler)
                }
                .doOnEach { detailSaving.accept(false) }
                .subscribe {
                    if (it.isSuccess) {
                        if (it.data!!.status) {
                            detailAlreadyAdded.accept(it.data)
                        } else {
                            saveDetail.accept(Unit)
                        }
                    } else if (it.isError) {
                        detailSavingError.accept(it.error)
                    }
                }
                .addTo(compositeDisposable)

            acceptSavingAddedDetail
                .subscribe { saveDetail.accept(Unit) }
                .addTo(compositeDisposable)

            val saveDetailNotification = saveDetail
                .filter { !detailSaving.value }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, NumberDetail, NumberDetail> { _, nd ->
                        nd
                    }
                )
                .switchMap {
                    addDetail(it)
                }
                .share()

            saveDetailNotification
                .filter { it.isOnError }
                .map { it.error }
                .subscribe {
                    detailSavingError.accept(errorHandler.getError(it))
                }
                .addTo(compositeDisposable)

            saveDetailNotification
                .filter { it.isOnNext }
                .map { it.value }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Int, NumberDetail, NumberDetailListItem> { id, nd ->
                        nd.toListItem(id)
                    }
                )
                .subscribe {
                    val json = Gson().toJson(it)
                    val intent = Intent()
                    intent.putExtra(PaginationActivity.RESULT_ITEM_KEY, json)
                    detailSaveSuccess.accept(intent)
                }
                .addTo(compositeDisposable)
        }

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.dispose()
        }

        private fun fetchDetail(id: Int) =
            interactors.getById(id)

        private fun fetchFactories(partId: Int) =
            interactors.getFactories(partId)

        private fun fetchFactoryYears(factoryId: Int) =
            interactors.getFactoryYears(factoryId)

        private fun fetchControlResults(partId: Int) =
            interactors.getControlResults(partId)

        private fun fetchRepairDefectTypes(partId: Int) =
            interactors.getRepairDefectTypes(partId)

        private fun fetchRepairControlZones(partId: Int) =
            interactors.getRepairControlZones(partId)

        private fun fetchDefectTypes(partId: Int, defectMoment: DefectMoment) =
            interactors.getDefectTypes(partId, defectMoment)

        private fun fetchControlZones(partId: Int, defectMoment: DefectMoment) =
            interactors.getControlZones(partId, defectMoment)

        private fun fetchSteals() = interactors.getSteals()

        private fun fetchDetectors(method: String) = interactors.getDetectors(method)

        private fun fetchMethods(partId: Int) = interactors.getMethods(partId)

        private fun fetchAlreadyAddedDetail(partId: Int, number: String) =
            interactors.checkAlreadyAddedDetail(partId, number)

        private fun addDetail(numberDetail: NumberDetail) =
            interactors.addNumberDetail(numberDetail)
                .doOnSubscribe { detailSaving.accept(true) }
                .doOnTerminate { detailSaving.accept(false) }
                .materialize()

        override fun loadDetail() = loadDetail.accept(Unit)

        override fun controlDate(value: Date) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.controlDate(value) as NumberDetail)
            }
        }

        override fun factory(value: Factory) {
            factoryChanged.accept(value)
            numberDetailInput.accept(numberDetailOutput.value.factory(value) as NumberDetail)
        }

        override fun factoryYear(value: FactoryYear?) {
            numberDetailInput.accept(numberDetailOutput.value.factoryYear(value) as NumberDetail)
        }

        override fun isNewDetail(value: Boolean) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.isNewDetail(value) as NumberDetail)
            }
        }

        override fun number(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.number(value) as NumberDetail)
            }
        }

        override fun isFromServicePoint(value: Boolean) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.isFromServicePoint(value) as NumberDetail)
            }
        }

        override fun servicePointCompany(value: CompanyEnum) {
            numberDetailInput.accept(numberDetailOutput.value.servicePointCompany(value) as NumberDetail)
        }

        override fun servicePointFault(value: FaultEnum) {
            numberDetailInput.accept(numberDetailOutput.value.servicePointFault(value) as NumberDetail)
        }

        override fun noRejectionCriteria(value: Boolean) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.noRejectionCriteria(value) as NumberDetail)
            }
        }

        override fun controlResult(value: ControlResult) {
            numberDetailInput.accept(numberDetailOutput.value.controlResult(value) as NumberDetail)
            controlResultChanged.accept(value)
            defectInfoChanged.accept(true)
        }

        override fun refreshFactoryDependData() =
            factoryChanged.accept(numberDetailOutput.value.factory)

        override fun clearCache() = interactors.clearPreferencesCache()

        override fun loadRepairInfo() = loadRepairInfo.accept(Unit)

        override fun repairDefectType(value: DefectType) {
            numberDetailInput.accept(numberDetailOutput.value.defectType(value) as NumberDetail)
        }

        override fun repairControlZone(value: ControlZone) {
            numberDetailInput.accept(numberDetailOutput.value.controlZone(value) as NumberDetail)
        }

        override fun repairDescription(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.repairDescription(value) as NumberDetail)
            }
        }

        override fun defectMoment(value: NumberDetailDefectMoment) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectMoment(value) as NumberDetail)
            }
        }

        override fun loadDefecationInfo() = loadDefecationInfo.accept(Unit)

        override fun defectType(value: DefectType) {
            if (numberDetailOutput.hasValue()) {
                defectTypeChanged.accept(value)
                numberDetailInput.accept(numberDetailOutput.value.defectType(value) as NumberDetail)
            }
        }

        override fun controlZone(value: ControlZone?) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.controlZone(value) as NumberDetail)
            }
        }

        override fun steal(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.steal(value) as NumberDetail)
            }
        }

        override fun owner(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.owner(value) as NumberDetail)
            }
        }

        override fun defectDescription(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectDescription(value) as NumberDetail)
            }
        }

        override fun loadVisualInfo() = loadVisualInfo.accept(Unit)

        override fun defectLength(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectLength(value) as NumberDetail)
            }
        }

        override fun defectDepth(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectDepth(value) as NumberDetail)
            }
        }

        override fun defectDiameter(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectDiameter(value) as NumberDetail)
            }
        }

        override fun loadNdtInfo() = loadNdtInfo.accept(Unit)

        override fun method(value: String) {
            if (numberDetailOutput.hasValue()) {
                methodChanged.accept(value)
                numberDetailInput.accept(numberDetailOutput.value.method(value) as NumberDetail)
            }
        }

        override fun defectSize(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectSize(value) as NumberDetail)
            }
        }

        override fun detector(value: Detector) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.detector(value) as NumberDetail)
            }
        }

        override fun attemptSave() = attemptSave.accept(Unit)

        override fun reloadDefectInfo() = reloadDefectInfo.accept(Unit)

        override fun defectInfoChanged() = defectInfoChanged.accept(true)

        override fun acceptSaving() = acceptSaving.accept(Unit)

        override fun acceptSavingAddedDetail() = acceptSavingAddedDetail.accept(Unit)

        override fun title(): Observable<String> = title.hide()

        override fun numberDetail(): Observable<NumberDetail> = numberDetailOutput.hide()

        override fun fetchingDetail(): Observable<Boolean> = fetchingDetail.hide()

        override fun fetchingFactoryDependData(): Observable<Boolean> =
            fetchingFactoryDependData.hide()

        override fun loadDetailError(): Observable<Error> = loadDetailError.hide()

        override fun loadFactoryDependsError(): Observable<Error> = loadFactoryDependsError.hide()

        override fun factories(): Observable<List<Factory>> = factories.hide()

        override fun factoryYears(): Observable<List<FactoryYear>> = factoryYears.hide()

        override fun controlResults(): Observable<List<ControlResult>> = controlResults.hide()

        override fun noInternetConnection(): Observable<Boolean> = noInternetConnection.hide()

        override fun fetchingRepairInfo(): Observable<Boolean> = fetchingRepairInfo.hide()

        override fun repairDefectTypes(): Observable<List<DefectType>> = repairDefectTypes.hide()

        override fun repairControlZones(): Observable<List<ControlZone>> = repairControlZones.hide()

        override fun controlResultChanged(): Observable<ControlResult> = controlResultChanged.hide()

        override fun repairInfoError(): Observable<Error> = repairInfoError.hide()

        override fun fetchingDefecationInfo(): Observable<Boolean> = fetchingDefecationInfo.hide()

        override fun defectTypes(): Observable<List<DefectType>> = defectTypes.hide()

        override fun controlZones(): Observable<List<ControlZone>> = controlZones.hide()

        override fun steals(): Observable<List<String>> = steals.hide()

        override fun defecationInfoError(): Observable<Error> = defecationInfoError.hide()

        override fun fetchingVisualInfo(): Observable<Boolean> = fetchingVisualInfo.hide()

        override fun visualInfoError(): Observable<Error> = visualInfoError.hide()

        override fun fetchingNdtInfo(): Observable<Boolean> = fetchingNdtInfo.hide()

        override fun methods(): Observable<List<String>> = methods.hide()

        override fun detectors(): Observable<List<Detector>> = detectors.hide()

        override fun fetchingMethodDependData(): Observable<Boolean> = fetchingMethodDependData.hide()

        override fun ndtInfoError(): Observable<Error> = ndtInfoError.hide()

        override fun controlZoneEnabled(): Observable<Boolean> = controlZoneEnabled.hide()

        override fun invalidForm(): Observable<List<InvalidField>> = invalidForm.hide()

        override fun showSavingDialog(): Observable<String> = showSavingDialog.hide()

        override fun detailSaving(): Observable<Boolean> = detailSaving.hide()

        override fun detailAlreadyAdded(): Observable<AddedDetail> = detailAlreadyAdded.hide()

        override fun detailSavingError(): Observable<Error> = detailSavingError.hide()

        override fun detailSaveSuccess(): Observable<Intent> = detailSaveSuccess.hide()
    }
}









































