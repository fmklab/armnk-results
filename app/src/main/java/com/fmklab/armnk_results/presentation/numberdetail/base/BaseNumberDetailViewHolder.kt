package com.fmklab.armnk_results.presentation.numberdetail.base

import android.graphics.Color
import android.view.View
import com.bumptech.glide.Glide
import com.fmklab.armnk_result.domain.BaseNumberDetailListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.common.DetailPagingViewHolder
import com.fmklab.armnk_results.presentation.common.PagingItem
import kotlinx.android.synthetic.main.number_detail_item.view.*
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseNumberDetailViewHolder<T : BaseNumberDetailListItem>(itemView: View) :
    DetailPagingViewHolder<T>(itemView) {

    override fun bind(item: PagingItem<T>) {
        super.bind(item)

        if (item.value == null) {
            return
        }

        with(item.value) {
            if (editStatus) {
                Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.status_unlock)
                    .into(itemView.statusImageView)
            } else {
                Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.status_lock)
                    .into(itemView.statusImageView)
            }
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            itemView.controlDateTextView.text = simpleDateFormat.format(controlDate)
            itemView.detailNumberValueTextView.text = number
            itemView.factoryNameValueTextView.text = factoryName
            itemView.factoryYearValueTextView.text = factoryYear.toString()
            when (controlResult) {
                "годен" -> {
                    itemView.resultTextView.setBackgroundColor(Color.parseColor("#2b9432"))
                }
                "брак" -> {
                    itemView.resultTextView.setBackgroundColor(Color.parseColor("#b03636"))
                }
                "ремонт" -> {
                    itemView.resultTextView.setBackgroundColor(Color.parseColor("#B6AF0B"))
                }
            }
            itemView.resultTextView.text = controlResult
            when (isMobileApp) {
                true -> Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_smartphone_24)
                    .into(itemView.sourceImageView)
                false -> Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_computer_24)
                    .into(itemView.sourceImageView)
            }
            val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
            itemView.createDateTextView.text = createDateFormat.format(createDate)
        }
    }
}