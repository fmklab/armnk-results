package com.fmklab.armnk_results.presentation.common

import com.fmklab.armnk_result.domain.Error

data class PagingItem<T>(val status: Status, val value: T?, val error: String?) {

    companion object {
        fun <T> value(value: T): PagingItem<T> {
            return PagingItem(Status.Value, value, null)
        }

        fun <T> loading(): PagingItem<T> {
            return PagingItem(Status.Loading, null, null)
        }

        fun <T> error(error: String): PagingItem<T> {
            return PagingItem(Status.Error, null, error)
        }
    }
}

sealed class Status {

    object Value : Status()
    object Loading : Status()
    object Error : Status()
}