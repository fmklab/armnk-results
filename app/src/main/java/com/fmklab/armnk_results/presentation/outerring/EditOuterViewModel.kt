package com.fmklab.armnk_results.presentation.outerring

import android.content.Intent
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.OuterRingDefectInputs
import com.fmklab.armnk_results.framework.OuterRingInputs
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.OuterRingInteractors
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.functions.Function5
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*

interface EditOuterViewModel {

    interface Inputs {

        fun loadOuterRing()

        fun clearCache()

        fun controlDate(value: Date)

        fun method(value: String)

        fun detector(value: Detector)

        fun factory(value: Factory)

        fun factoryYear(value: FactoryYear?)

        fun number(value: String)

        fun controlResult(value: ControlResult)

        fun loadMethodDependedData()

        fun loadFactoryDependedData()

        fun loadDefectInfo()

        fun defectMoment(value: DefectMoment)

        fun defectType(value: DefectType?)

        fun controlZone(value: ControlZone?)

        fun defectDescription(value: String)

        fun attemptSave()

        fun defectInfoChanged()

        fun acceptSaving()
    }

    interface Outputs {

        fun title(): Observable<String>

        fun noInternetConnection(): Observable<Boolean>

        fun outerRing(): Observable<OuterRing>

        fun controlResultChanged(): Observable<ControlResult>

        fun fetchingOuterRing(): Observable<Boolean>

        fun methods(): Observable<List<String>>

        fun detectors(): Observable<List<Detector>>

        fun factories(): Observable<List<Factory>>

        fun factoryYears(): Observable<List<FactoryYear>>

        fun controlResults(): Observable<List<ControlResult>>

        fun loadOuterRingError(): Observable<Error>

        fun fetchingMethodDependedData(): Observable<Boolean>

        fun loadMethodDependedDataError(): Observable<Error>

        fun fetchingFactoryDependedData(): Observable<Boolean>

        fun loadFactoryDependedDataError(): Observable<Error>

        fun fetchingDefectInfo(): Observable<Boolean>

        fun defectTypes(): Observable<List<DefectType>>

        fun controlZones(): Observable<List<ControlZone>>

        fun loadDefectInfoError(): Observable<Error>

        fun invalidForm(): Observable<List<OuterRing.InvalidField>>

        fun showSavingDialog(): Observable<String>

        fun outerRingSaving(): Observable<Boolean>

        fun detailSavingError(): Observable<Error>

        fun detailSavingSuccess(): Observable<Intent>
    }

    class ViewModel(
        private val interactors: OuterRingInteractors,
        private val errorHandler: ErrorHandler,
        private val currentUser: CurrentUser
    ) : BaseViewModel(), Inputs, Outputs {

        private val compositeDisposable = CompositeDisposable()

        val inputs: Inputs = this
        val outputs: Outputs = this

        private val outerRingInput = PublishRelay.create<OuterRing>()
        private val loadOuterRing = PublishRelay.create<Unit>()
        private val methodChanged = PublishRelay.create<String>()
        private val factoryChanged = PublishRelay.create<Factory>()
        private val loadDefectInfo = PublishRelay.create<Unit>()
        private val attemptSave = PublishRelay.create<Unit>()
        private val defectInfoChanged = BehaviorRelay.createDefault(false)
        private val acceptSaving = PublishRelay.create<Unit>()

        private val title = BehaviorRelay.create<String>()
        private val noInternetConnection = BehaviorRelay.createDefault(false)
        private val outerRingOutput = BehaviorRelay.create<OuterRing>()
        private val controlResultChanged = PublishRelay.create<ControlResult>()
        private val fetchingOuterRing = BehaviorRelay.createDefault(false)
        private val methods = BehaviorRelay.create<List<String>>()
        private val detectors = BehaviorRelay.create<List<Detector>>()
        private val factories = BehaviorRelay.create<List<Factory>>()
        private val factoryYears = BehaviorRelay.create<List<FactoryYear>>()
        private val controlResults = BehaviorRelay.create<List<ControlResult>>()
        private val loadOuterRingError = PublishRelay.create<Error>()
        private val fetchingMethodDependedData = BehaviorRelay.createDefault(false)
        private val loadMethodDependedDataError = PublishRelay.create<Error>()
        private val fetchingFactoryDependedData = BehaviorRelay.createDefault(false)
        private val loadFactoryDependedDataError = PublishRelay.create<Error>()
        private val fetchingDefectInfo = BehaviorRelay.createDefault(false)
        private val defectTypes = BehaviorRelay.create<List<DefectType>>()
        private val controlZones = BehaviorRelay.create<List<ControlZone>>()
        private val loadDefectInfoError = PublishRelay.create<Error>()
        private val invalidForm = PublishRelay.create<List<OuterRing.InvalidField>>()
        private val showSavingDialog = PublishRelay.create<String>()
        private val outerRingSaving = BehaviorRelay.createDefault(false)
        private val detailSavingError = PublishRelay.create<Error>()
        private val detailSavingSuccess = PublishRelay.create<Intent>()

        init {
            val selectedDetailId = intent()
                .subscribeOn(Schedulers.io())
                .map { it.extras?.getInt(PaginationActivity.SELECTED_DETAIL_ID_TAG)!! }

            val selectedControlDetail = intent()
                .subscribeOn(Schedulers.io())
                .map {
                    val json =
                        it.extras?.getString(ControlDetailActivity.SELECTED_CONTROL_DETAIL_TAG)!!
                    Gson().fromJson(json, ControlDetail::class.java)
                }

            selectedControlDetail
                .map { it.name }
                .subscribe(title)
                .addTo(compositeDisposable)

            loadOuterRing
                .withLatestFrom(
                    selectedControlDetail,
                    selectedDetailId,
                    Function3<Unit, ControlDetail, Int, Pair<Int, Int>> { _, cd, id ->
                        Pair(cd.id, id)
                    }
                )
                .filter { !fetchingOuterRing.value }
                .doOnNext { fetchingOuterRing.accept(true) }
                .switchMap {
                    fetchDetail(it.second).toResultApi(noInternetConnection, errorHandler)
                        .switchMap { or ->
                            if (or.isError && or.error != Error.NotFoundRemote) {
                                Observable.just(
                                    OuterRingInputs(
                                        or,
                                        Result.error(or.error!!),
                                        Result.error(or.error),
                                        Result.error(or.error),
                                        Result.error(or.error),
                                        Result.error(or.error)
                                    )
                                )
                            } else {
                                Observable.zip(
                                    fetchMethods(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchDetectors(or.data?.method ?: "base").toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchFactories(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchFactoryYears(or.data?.factory?.id ?: 0).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchControlResults(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    Function5 { m, d, f, y, r ->
                                        if (or.isError && or.error == Error.NotFoundRemote) {
                                            OuterRingInputs(
                                                Result.success(
                                                    OuterRing.default(it.first)
                                                        .spec(currentUser.getUser().toString())
                                                ),
                                                m, d, f, y, r
                                            )
                                        } else {
                                            OuterRingInputs(or, m, d, f, y, r)
                                        }
                                    }
                                )
                            }
                        }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach { fetchingOuterRing.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val outerRing = it.outerRing.data!!
                        val methods = it.methods.data!!
                        val detectors = it.detectors.data!!
                        val factories = it.factories.data!!
                        val factoryYears = it.factoryYears.data!!
                        val controlResults = it.controlResults.data!!
                        outerRing.factory(factories.firstOrNull { f -> f.id == outerRing.factory?.id })
                        outerRing.controlResult(controlResults.firstOrNull { c -> c.id == outerRing.controlResult?.id })
                        this.methods.accept(methods)
                        this.detectors.accept(detectors)
                        this.factories.accept(factories)
                        this.factoryYears.accept(factoryYears)
                        this.controlResults.accept(controlResults)
                        outerRingInput.accept(outerRing)
                    } else {
                        loadOuterRingError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            outerRingInput
                .flatMap {
                    interactors.cacheDetail(it)
                    Observable.just(it)
                }
                .subscribe(outerRingOutput)
                .addTo(compositeDisposable)

            methodChanged
                .filter { !fetchingMethodDependedData.value }
                .doOnNext { fetchingMethodDependedData.accept(true) }
                .switchMap {
                    fetchDetectors(it).toResultApi(noInternetConnection, errorHandler)
                }
                .doOnEach { fetchingMethodDependedData.accept(false) }
                .subscribe {
                    if (it.isSuccess) {
                        detectors.accept(it.data)
                    } else if (it.isError) {
                        loadMethodDependedDataError.accept(it.error)
                    }
                }
                .addTo(compositeDisposable)

            factoryChanged
                .filter { !fetchingMethodDependedData.value }
                .doOnNext { fetchingFactoryDependedData.accept(true) }
                .switchMap {
                    fetchFactoryYears(it.id).toResultApi(
                        noInternetConnection,
                        errorHandler
                    )
                }
                .doOnEach { fetchingFactoryDependedData.accept(false) }
                .subscribe {
                    if (it.isSuccess) {
                        factoryYears.accept(it.data)
                    } else if (it.isError) {
                        loadFactoryDependedDataError.accept(it.error)
                    }
                }
                .addTo(compositeDisposable)

            factoryYears
                .withLatestFrom(
                    outerRingOutput,
                    BiFunction<List<FactoryYear>, OuterRing, Boolean> { fy, or ->
                        fy.contains(or.factoryYear)
                    }
                )
                .subscribe {
                    if (!it) {
                        factoryYear(null)
                    }
                }
                .addTo(compositeDisposable)

            loadDefectInfo
                .withLatestFrom(
                    outerRingOutput,
                    BiFunction<Unit, OuterRing, Pair<Int, DefectMoment>> { _, or ->
                        Pair(or.partId, or.defectMoment)
                    }
                )
                .filter { !fetchingDefectInfo.value }
                .doOnNext { fetchingDefectInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchDefectTypes(it.first, it.second).toResultApi(noInternetConnection, errorHandler),
                        fetchControlZones(it.first, it.second).toResultApi(noInternetConnection, errorHandler),
                        BiFunction<Result<List<DefectType>>, Result<List<ControlZone>>, OuterRingDefectInputs> { dt, cz ->
                            OuterRingDefectInputs(dt, cz)
                        }
                    )
                }
                .doOnEach { fetchingDefectInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = outerRingOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.defectTypes.accept(defectTypes)
                        this.controlZones.accept(controlZones)
                        outerRingInput.accept(detail)
                    } else {
                        loadDefectInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            defectTypes
                .filter { outerRingOutput.hasValue() && outerRingOutput.value.defectType == null && it.size == 1 }
                .map { it.first() }
                .subscribe { defectType(it) }
                .addTo(compositeDisposable)

            val saveDetailValidation = attemptSave
                .withLatestFrom(
                    outerRingOutput,
                     BiFunction<Unit, OuterRing, Pair<Boolean, List<OuterRing.InvalidField>>> { _, or ->
                         or.validate()
                     }
                )
                .subscribeOn(Schedulers.io())

            saveDetailValidation
                .map { it.second }
                .subscribe(invalidForm)
                .addTo(compositeDisposable)

            saveDetailValidation
                .filter { it.first }
                .map { Unit }
                .withLatestFrom(
                    outerRingOutput,
                    defectInfoChanged,
                    Function3<Unit, OuterRing, Boolean, String> { _, or, changed ->
                        if (changed) {
                            or.createDefectInfo()
                        }
                        or.defectInfo
                    }
                )
                .subscribe(showSavingDialog)
                .addTo(compositeDisposable)

            val saveDetailNotification = acceptSaving
                .filter { !outerRingSaving.value }
                .withLatestFrom(
                    outerRingOutput,
                    BiFunction<Unit, OuterRing, OuterRing> { _, or ->
                        or
                    }
                )
                .switchMap { addDetail(it) }
                .share()

            saveDetailNotification
                .filter { it.isOnError }
                .map { it.error }
                .subscribe {
                    detailSavingError.accept(errorHandler.getError(it))
                }
                .addTo(compositeDisposable)

            saveDetailNotification
                .filter { it.isOnNext }
                .map { it.value }
                .withLatestFrom(
                    outerRingOutput,
                    BiFunction<Int, OuterRing, OuterRingListItem> { id, or ->
                        or.toListItem(id)
                    }
                )
                .subscribe {
                    val json = Gson().toJson(it)
                    val intent = Intent()
                    intent.putExtra(PaginationActivity.RESULT_ITEM_KEY, json)
                    detailSavingSuccess.accept(intent)
                }
                .addTo(compositeDisposable)
        }

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.clear()
        }

        private fun fetchDetail(id: Int) = interactors.getOuterRingById(id)

        private fun fetchFactories(partId: Int) = interactors.getFactories(partId)

        private fun fetchMethods(partId: Int) = interactors.getMethods(partId)

        private fun fetchDetectors(method: String) = interactors.getDetectors(method)

        private fun fetchFactoryYears(factoryId: Int) = interactors.getFactoryYear(factoryId)

        private fun fetchControlResults(partId: Int) = interactors.getControlResults(partId)

        private fun fetchDefectTypes(partId: Int, defectMoment: DefectMoment) =
            interactors.getDefectTypes(partId, defectMoment)

        private fun fetchControlZones(partId: Int, defectMoment: DefectMoment) =
            interactors.getControlZones(partId, defectMoment)

        private fun addDetail(outerRing: OuterRing) =
            interactors.addOuterRing(outerRing)
                .doOnSubscribe { outerRingSaving.accept(true) }
                .doAfterTerminate { outerRingSaving.accept(false) }
                .materialize()

        override fun title(): Observable<String> = title.hide()

        override fun noInternetConnection(): Observable<Boolean> = noInternetConnection.hide()

        override fun outerRing(): Observable<OuterRing> = outerRingOutput.hide()

        override fun controlResultChanged(): Observable<ControlResult> = controlResultChanged.hide()

        override fun fetchingOuterRing(): Observable<Boolean> = fetchingOuterRing.hide()

        override fun methods(): Observable<List<String>> = methods.hide()

        override fun detectors(): Observable<List<Detector>> = detectors.hide()

        override fun factories(): Observable<List<Factory>> = factories.hide()

        override fun factoryYears(): Observable<List<FactoryYear>> = factoryYears.hide()

        override fun controlResults(): Observable<List<ControlResult>> = controlResults.hide()

        override fun loadOuterRingError(): Observable<Error> = loadOuterRingError.hide()

        override fun fetchingMethodDependedData(): Observable<Boolean> =
            fetchingMethodDependedData.hide()

        override fun loadMethodDependedDataError(): Observable<Error> =
            loadMethodDependedDataError.hide()

        override fun fetchingFactoryDependedData(): Observable<Boolean> =
            fetchingFactoryDependedData.hide()

        override fun loadFactoryDependedDataError(): Observable<Error> =
            loadFactoryDependedDataError.hide()

        override fun fetchingDefectInfo(): Observable<Boolean> = fetchingDefectInfo.hide()

        override fun defectTypes(): Observable<List<DefectType>> = defectTypes.hide()

        override fun controlZones(): Observable<List<ControlZone>> = controlZones.hide()

        override fun loadDefectInfoError(): Observable<Error> = loadDefectInfoError.hide()

        override fun invalidForm(): Observable<List<OuterRing.InvalidField>> = invalidForm.hide()

        override fun showSavingDialog(): Observable<String> = showSavingDialog.hide()

        override fun outerRingSaving(): Observable<Boolean> = outerRingSaving.hide()

        override fun detailSavingError(): Observable<Error> = detailSavingError.hide()

        override fun detailSavingSuccess(): Observable<Intent> = detailSavingSuccess.hide()

        override fun acceptSaving() = acceptSaving.accept(Unit)

        override fun loadOuterRing() = loadOuterRing.accept(Unit)

        override fun clearCache() = interactors.clearPreferencesCache()

        override fun controlDate(value: Date) {
            if (outerRingOutput.hasValue()) {
                outerRingInput.accept(outerRingOutput.value.controlDate(value))
            }
        }

        override fun method(value: String) {
            methodChanged.accept(value)
            outerRingInput.accept(outerRingOutput.value.method(value))
        }

        override fun detector(value: Detector) {
            outerRingInput.accept(outerRingOutput.value.detector(value))
        }

        override fun factory(value: Factory) {
            factoryChanged.accept(value)
            outerRingInput.accept(outerRingOutput.value.factory(value))
        }

        override fun factoryYear(value: FactoryYear?) {
            outerRingInput.accept(outerRingOutput.value.factoryYear(value))
        }

        override fun number(value: String) {
            if (outerRingOutput.hasValue()) {
                outerRingInput.accept(outerRingOutput.value.number(value))
            }
        }

        override fun controlResult(value: ControlResult) {
            if (outerRingOutput.hasValue()) {
                controlResultChanged.accept(value)
                outerRingInput.accept(outerRingOutput.value.controlResult(value))
                defectInfoChanged.accept(true)
            }
        }

        override fun loadMethodDependedData() {
            if (outerRingOutput.hasValue()) {
                methodChanged.accept(outerRingOutput.value.method)
            }
        }

        override fun loadFactoryDependedData() {
            if (outerRingOutput.hasValue()) {
                factoryChanged.accept(outerRingOutput.value.factory)
            }
        }

        override fun loadDefectInfo() = loadDefectInfo.accept(Unit)

        override fun defectMoment(value: DefectMoment) {
            if (outerRingOutput.hasValue()) {
                outerRingInput.accept(outerRingOutput.value.defectMoment(value))
            }
        }

        override fun defectType(value: DefectType?) {
            outerRingInput.accept(outerRingOutput.value.defectType(value))
        }

        override fun controlZone(value: ControlZone?) {
            if (outerRingOutput.hasValue()) {
                outerRingInput.accept(outerRingOutput.value.controlZone(value))
            }
        }

        override fun defectDescription(value: String) {
            if (outerRingOutput.hasValue()) {
                outerRingInput.accept(outerRingOutput.value.defectDescription(value))
            }
        }

        override fun attemptSave() = attemptSave.accept(Unit)

        override fun defectInfoChanged() = defectInfoChanged.accept(true)
    }
}









































