package com.fmklab.armnk_results.presentation.numberdetail

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class NumberDetailPagerAdapter(fm: FragmentManager, behaviour: Int) :
    FragmentStatePagerAdapter(fm, behaviour) {

    private var fragments = arrayOf(
        NumberDetailBaseInfoFragment(),
        NumberDetailDefectInfoFragment(),
        NumberDetailRepairInfoFragment()
    )

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragments[position] = fragment
        return fragment
    }
}