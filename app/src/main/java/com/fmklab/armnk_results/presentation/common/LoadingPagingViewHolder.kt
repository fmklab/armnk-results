package com.fmklab.armnk_results.presentation.common

import android.view.View
import com.fmklab.armnk_results.presentation.base.BaseViewHolder

class LoadingPagingViewHolder<T>(itemView: View) : BaseViewHolder<PagingItem<T>>(itemView) {

    override fun bind(item: PagingItem<T>) {}
}