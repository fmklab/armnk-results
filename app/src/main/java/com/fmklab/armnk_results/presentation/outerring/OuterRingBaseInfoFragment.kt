package com.fmklab.armnk_results.presentation.outerring

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.forEach
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setCheckedDistinct
import com.fmklab.armnk_results.framework.extensions.setTextDistinct
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.extensions.toDate
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.radiobutton.MaterialRadioButton
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_outer_ring_base_input.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class OuterRingBaseInfoFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditOuterViewModel.ViewModel by sharedViewModel()
    private lateinit var materialDatePicker: MaterialDatePicker<Long>
    private val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)

    companion object {
        private const val DATE_PICKER_TAG = "DatePickerTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_outer_ring_base_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBinding()
        setupListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        controlDateEditText.keyListener = null
        methodAutoComleteTextView.keyListener = null
        detectorAutoCompleteTextView.keyListener = null
        factoryNameAutoCompleteTextView.keyListener = null
        factoryYearAutoCompleteTextView.keyListener = null

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText(getString(R.string.select_date))
        materialDatePicker = builder.build()
    }

    private fun setupBinding() {
        viewModel.outputs.fetchingOuterRing()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.outerRing()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlDateEditText.setText(simpleDateFormat.format(it.controlDate))
                methodAutoComleteTextView.setText(it.method, false)
                detectorAutoCompleteTextView.setText(it.detector?.toString() ?: "", false)
                factoryNameAutoCompleteTextView.setText(it.factory?.toString() ?: "", false)
                factoryYearAutoCompleteTextView.setText(it.factoryYear?.toString() ?: "", false)
                numberDetailEditText.setTextDistinct(it.number)

            }
            .addTo(compositeDisposable)

        viewModel.outputs.methods()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                methodAutoComleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.detectors()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                detectorAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.factories()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                factoryNameAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.factoryYears()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                factoryYearAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingMethodDependedData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showMethodDependedDataLoading()
                } else {
                    hideMethodDependedDataLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadMethodDependedDataError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showMethodDependedDataLoading() }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingFactoryDependedData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showFactoryDependedDataLoading()
                } else {
                    hideFactoryDependedDataLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadFactoryDependedDataError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showFactoryDependedDataLoading() }
            .addTo(compositeDisposable)

        Observable.zip(
            viewModel.outputs.outerRing(),
            viewModel.outputs.controlResults(),
            BiFunction<OuterRing, List<ControlResult>, Pair<List<ControlResult>, ControlResult?>> { or, cr ->
                Pair(cr, or.controlResult)
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::initControlResultCheckBoxes)
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showInvalidFields)
            .addTo(compositeDisposable)

        methodAutoComleteTextView
            .itemSelected<String>()
            .subscribe(viewModel.inputs::method)
            .addTo(compositeDisposable)

        detectorAutoCompleteTextView
            .itemSelected<Detector>()
            .subscribe(viewModel.inputs::detector)
            .addTo(compositeDisposable)

        factoryNameAutoCompleteTextView
            .itemSelected<Factory>()
            .subscribe(viewModel.inputs::factory)
            .addTo(compositeDisposable)

        factoryYearAutoCompleteTextView
            .itemSelected<FactoryYear>()
            .subscribe(viewModel.inputs::factoryYear)
            .addTo(compositeDisposable)

        numberDetailEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::number)
            .addTo(compositeDisposable)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupListeners() {
        controlDateEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (!parentFragmentManager.fragments.any { frg -> frg.tag == DATE_PICKER_TAG }) {
                    materialDatePicker.show(parentFragmentManager, DATE_PICKER_TAG)
                }
                return@setOnTouchListener true
            }
            false
        }

        materialDatePicker.addOnPositiveButtonClickListener {
            val date = it.toDate()
            viewModel.inputs.controlDate(date)
        }

        methodAutoComleteTextView.doOnTextChanged { _, _, _, _ ->
            methodTextInputLayout.error = null
        }

        detectorAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            detectorTextInputLayout.error = null
        }

        factoryNameAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            factoryNameTextInputLayout.error = null
        }

        factoryYearAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            factoryYearTextInputLayout.error = null
        }

        numberDetailEditText.doOnTextChanged { _, _, _, _ ->
            numberDetailTextInputLayout.error = null
        }
    }

    private fun showAllViewLoading() {
        controlDateTextInputLayout.disable()
        controlDateEditText.disable()
        methodTextInputLayout.disable()
        methodAutoComleteTextView.disable()
        detectorTextInputLayout.disable()
        detectorAutoCompleteTextView.disable()
        factoryNameTextInputLayout.disable()
        factoryNameAutoCompleteTextView.disable()
        factoryYearTextInputLayout.disable()
        factoryYearAutoCompleteTextView.disable()
        numberDetailTextInputLayout.disable()
        numberDetailEditText.disable()
        resultRadioGroup.forEach { it.disable() }
        shimmerViewContainer.showShimmer(true)
    }

    private fun hideAllViewLoading() {
        controlDateTextInputLayout.enable()
        controlDateEditText.enable()
        methodTextInputLayout.enable()
        methodAutoComleteTextView.enable()
        detectorTextInputLayout.enable()
        detectorAutoCompleteTextView.enable()
        factoryNameTextInputLayout.enable()
        factoryNameAutoCompleteTextView.enable()
        factoryYearTextInputLayout.enable()
        factoryYearAutoCompleteTextView.enable()
        numberDetailTextInputLayout.enable()
        numberDetailEditText.enable()
        resultRadioGroup.forEach { it.enable() }
        shimmerViewContainer.hideShimmer()
    }

    private fun showMethodDependedDataLoading() {
        detectorTextInputLayout.disable()
        detectorAutoCompleteTextView.disable()
        detectorShimmerContainer.showShimmer(true)
    }

    private fun hideMethodDependedDataLoading() {
        detectorTextInputLayout.enable()
        detectorAutoCompleteTextView.enable()
        detectorShimmerContainer.hideShimmer()
    }

    private fun showFactoryDependedDataLoading() {
        factoryYearTextInputLayout.disable()
        factoryYearAutoCompleteTextView.disable()
        factoryYearShimmerLayout.showShimmer(true)
    }

    private fun hideFactoryDependedDataLoading() {
        factoryYearTextInputLayout.enable()
        factoryYearAutoCompleteTextView.enable()
        factoryYearShimmerLayout.hideShimmer()
    }

    private fun initControlResultCheckBoxes(data: Pair<List<ControlResult>, ControlResult?>) {
        resultRadioGroup.removeAllViews()
        for (result in data.first) {
            val radioButton = MaterialRadioButton(requireContext())
            resultRadioGroup.addView(radioButton)
            radioButton.tag = result
            radioButton.text = result.name
            if (result == data.second) {
                radioButton.setCheckedDistinct(true)
            }
            radioButton
                .checkedChanges()
                .skipInitialValue()
                .filter { isChecked -> isChecked }
                .map { radioButton.tag }
                .ofType(ControlResult::class.java)
                .subscribe(viewModel.inputs::controlResult)
                .addTo(compositeDisposable)
        }
    }

    private fun showInvalidFields(fields: List<OuterRing.InvalidField>) {
        for (field in fields) {
            when (field) {
                is OuterRing.InvalidField.Method -> methodTextInputLayout.error = field.reason
                is OuterRing.InvalidField.Detector -> detectorTextInputLayout.error = field.reason
                is OuterRing.InvalidField.Factory -> factoryNameTextInputLayout.error = field.reason
                is OuterRing.InvalidField.FactoryYear -> factoryYearTextInputLayout.error =
                    field.reason
                is OuterRing.InvalidField.Number -> numberDetailTextInputLayout.error = field.reason
                is OuterRing.InvalidField.ControlResult -> Toast.makeText(
                    requireContext(),
                    field.reason,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}









































