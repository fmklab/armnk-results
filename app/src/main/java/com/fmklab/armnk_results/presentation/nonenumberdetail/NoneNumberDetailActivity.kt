package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.di.NoneNumberDetailScope
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.framework.ActivityResult
import com.fmklab.armnk_results.presentation.common.*
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_small_detail.*
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.ext.getOrCreateScope

class NoneNumberDetailActivity : PaginationActivity<NoneNumberDetail>() {

    private val compositeDisposable = CompositeDisposable()

    private val noneNumberDetailScope = getKoin().get<NoneNumberDetailScope>().getOrCreateScope()

    override val viewModel: PaginationViewModel.ViewModel<NoneNumberDetail> by noneNumberDetailScope.viewModel<NoneNumberDetailViewModel.ViewModel>(this)
    override val recyclerViewAdapter: DetailsPagingAdapter<NoneNumberDetail> by noneNumberDetailScope.inject<NoneNumberDetailAdapter>()



    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_small_detail)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
        noneNumberDetailScope.close()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return true
    }

    override fun setupUI() {
        setSupportActionBar(smallDetailToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar = findViewById(R.id.smallDetailToolBar)
        recyclerView = findViewById(R.id.smallDetailRecyclerView)
        addNewDetailFab = findViewById(R.id.addNewSmallDetailFab)
        swipeRefreshLayout = findViewById(R.id.smallDetailSwipeRefreshLayout)
        emptyDataTextView = findViewById(R.id.emptyDataTextView)

        super.setupUI()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showNewDetailEditor()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                intent.setClass(this, EditNoneNumberDetailActivity::class.java)
                startActivityForResult(intent, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun createDetailsFragment(): NoneNumberDetailsFragment {
        return NoneNumberDetailsFragment()
    }
}




































