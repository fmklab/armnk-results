package com.fmklab.armnk_results.presentation.common

import android.view.View
import android.widget.Button
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.base.BaseViewHolder

open class DetailPagingViewHolder<T>(itemView: View) : BaseViewHolder<PagingItem<T>>(itemView) {

    val moreInfoButton: Button = itemView.findViewById(R.id.moreInfoButton)

    override fun bind(item: PagingItem<T>) {}

    interface OnMoreInfoClickListener {

        fun onMoreInfoClick(position: Int)
    }
}