package com.fmklab.armnk_results.presentation.controldetail

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.fmklab.armnk_result.domain.ControlDetail
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import kotlinx.android.synthetic.main.control_detail_item.view.*
import org.koin.experimental.property.inject

class ControlDetailViewHolder(itemView: View, private val context: Context) : BaseViewHolder<ControlDetail>(itemView) {

    override fun bind(item: ControlDetail) {
        val resources = context.resources
        val imageId = resources.getIdentifier(item.image, "drawable", context.packageName)
        Glide.with(itemView)
            .asDrawable()
            .load(imageId)
            .into(itemView.controlDetailImageView)
        itemView.controlDetailNameTextView.text = item.name
    }
}