package com.fmklab.armnk_results.presentation.logindepricated

import com.fmklab.armnk_results.presentation.basedepricated.BaseViewEvent

sealed class LoginViewEvent :
    BaseViewEvent {

    object ShowMenuScreen : LoginViewEvent()
}