package com.fmklab.armnk_results.presentation.logindepricated

import com.fmklab.armnk_results.framework.LoginParams
import com.fmklab.armnk_results.presentation.basedepricated.BaseViewState

data class LoginViewState(
    val loadingStatus: LoadingStatus,
    val exceptionStatus: ExceptionStatus?,
    val formValidationStatuses: List<FormValidationStatus>,
    val loginParams: LoginParams
) : BaseViewState {

    companion object {
        val DEFAULT =
            LoginViewState(
                LoadingStatus.NoLoading,
                ExceptionStatus.UnknownException,
                ArrayList(),
                LoginParams("", "")
            )
    }
}

sealed class LoadingStatus {

    object NoLoading : LoadingStatus()
    object FetchingUser : LoadingStatus()
}

sealed class ExceptionStatus {

    object UserNotFoundException : ExceptionStatus()
    object NoInternetConnectionException : ExceptionStatus()
    object SocketTimeoutException : ExceptionStatus()
    object ServerBadRequestException : ExceptionStatus()
    object UnknownException : ExceptionStatus()
}

sealed class FormValidationStatus {

    object EmptyUsername : FormValidationStatus()
    object EmptyPassword : FormValidationStatus()
}