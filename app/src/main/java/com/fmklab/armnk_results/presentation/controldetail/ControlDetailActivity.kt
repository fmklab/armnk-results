package com.fmklab.armnk_results.presentation.controldetail

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.fmklab.armnk_result.domain.ControlDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.modules.axle.activities.AxleActivity
import com.fmklab.armnk_results.modules.wheels.activities.WheelActivity
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.login.LoginActivity
import com.fmklab.armnk_results.presentation.nonenumberdetail.NoneNumberDetailActivity
import com.fmklab.armnk_results.presentation.numberdetail.NumberDetailActivity
import com.fmklab.armnk_results.presentation.numberdetail.bogie.BogieNumberDetailActivity
import com.fmklab.armnk_results.presentation.outerring.OuterRingActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.jakewharton.rxbinding4.swiperefreshlayout.refreshes
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.activity_control_details.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel

class ControlDetailActivity : BaseActivity<ControlDetailViewModel.ViewModel>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: ControlDetailViewModel.ViewModel by lifecycleScope.viewModel(this)
    private val controlDetailAdapter: ControlDetailAdapter by lifecycleScope.inject()

    companion object {
        const val SELECTED_CONTROL_DETAIL_TAG = "SelectedControlDetailTag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_control_details)

        setupUI()
        setupData()
        setupBinding()
        setupListeners()
        viewModel.inputs.loadControlDetails()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_control_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logoutMenuItem -> {
                viewModel.inputs.logoutClick()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        showExitDialog()
    }

    private fun setupUI() {
        setSupportActionBar(controlDetailsToolBar)
        controlDetailsRecyclerView.layoutManager = LinearLayoutManager(this)
        controlDetailsRecyclerView.setHasFixedSize(true)
        controlDetailsRecyclerView.isMotionEventSplittingEnabled = false
    }

    private fun setupData() {
        controlDetailsRecyclerView.adapter = controlDetailAdapter
    }

    private fun setupBinding() {
        viewModel.outputs.fetchingControlDetails()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlDetailsSwipeRefreshLayout.isRefreshing = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.error()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                toast("Произошла непредвиденная ошибка")
            }
            .addTo(compositeDisposable)

        viewModel.outputs.controlDetails()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlDetailAdapter.addAll(it)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.showWheelScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showWheelScreen() }
            .addTo(compositeDisposable)

        viewModel.outputs.showAxleScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showAxleScreen() }
            .addTo(compositeDisposable)

        viewModel.outputs.showNoneNumberDetailScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showNoneNumberDetailScreen)
            .addTo(compositeDisposable)

        viewModel.outputs.showNumberDetailScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showNumberDetailScreen)
            .addTo(compositeDisposable)

        viewModel.outputs.showBogieNumberDetailScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showBogieNumberDetailScreen)
            .addTo(compositeDisposable)

        viewModel.outputs.showLoginScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showLoginScreen() }
            .addTo(compositeDisposable)

        viewModel.outputs.activitySubtitle()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { supportActionBar?.subtitle = it }
            .addTo(compositeDisposable)

        viewModel.outputs.showOuterRingScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showOuterRingScreen)
            .addTo(compositeDisposable)

        controlDetailsSwipeRefreshLayout
            .refreshes()
            .subscribe { viewModel.inputs.loadControlDetails() }
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        controlDetailAdapter.onItemClickListener = object : BaseViewHolder.OnItemClickListener {
            override fun onItemClick(position: Int) {
                val controlDetail = controlDetailAdapter.getItem(position)
                viewModel.inputs.selectControlDetail(controlDetail)
            }
        }
    }

    private fun showAxleScreen() {
        val intent = Intent(this, AxleActivity::class.java)
        startActivity(intent)
    }

    private fun showWheelScreen() {
        val intent = Intent(this, WheelActivity::class.java)
        startActivity(intent)
    }

    private fun showNoneNumberDetailScreen(controlDetail: ControlDetail) {
        val intent = Intent(this, NoneNumberDetailActivity::class.java).apply {
            val json = Gson().toJson(controlDetail)
            putExtra(SELECTED_CONTROL_DETAIL_TAG, json)
        }
        startActivity(intent)
    }

    private fun showNumberDetailScreen(controlDetail: ControlDetail) {
        val intent = Intent(this, NumberDetailActivity::class.java).apply {
            val json = Gson().toJson(controlDetail)
            putExtra(SELECTED_CONTROL_DETAIL_TAG, json)
        }
        startActivity(intent)
    }

    private fun showBogieNumberDetailScreen(controlDetail: ControlDetail) {
        val intent = Intent(this, BogieNumberDetailActivity::class.java).apply {
            val json = Gson().toJson(controlDetail)
            putExtra(SELECTED_CONTROL_DETAIL_TAG, json)
        }
        startActivity(intent)
    }

    private fun showOuterRingScreen(controlDetail: ControlDetail) {
        val intent = Intent(this, OuterRingActivity::class.java).apply {
            val json = Gson().toJson(controlDetail)
            putExtra(SELECTED_CONTROL_DETAIL_TAG, json)
        }
        startActivity(intent)
    }

    private fun showLoginScreen() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showExitDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.exit))
            .setMessage(getString(R.string.exit_from_app))
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                finish()
            }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }
}






































