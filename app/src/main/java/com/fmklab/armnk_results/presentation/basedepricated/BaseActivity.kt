package com.fmklab.armnk_results.presentation.basedepricated

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo

abstract class BaseActivity<VS : BaseViewState, VE : BaseViewEvent, VME : BaseViewModelEvent, VM : BaseViewModel<VS, VE, VME>> :
    AppCompatActivity() {

    protected val compositeDisposable = CompositeDisposable()

    abstract val viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun setupView() {
        viewModel.viewStatesObservable
            .subscribe(this::renderViewState)
            .addTo(compositeDisposable)

        viewModel.viewEventsObservable
            .subscribe(this::renderViewEvent)
            .addTo(compositeDisposable)
    }

    abstract fun renderViewState(viewState: VS)

    abstract fun renderViewEvent(viewEvent: VE)
}