package com.fmklab.armnk_results.presentation.logindepricated

import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_results.framework.interactors.LoginInteractors
import com.fmklab.armnk_results.presentation.basedepricated.BaseViewModel
import com.jakewharton.rxrelay3.BehaviorRelay
import io.reactivex.rxjava3.functions.Consumer

class LoginViewModel(
    private val interactors: LoginInteractors,
    private val errorHandler: ErrorHandler
) :
    BaseViewModel<LoginViewState, LoginViewEvent, LoginViewModelEvent>() {

    private val usernameChanged = BehaviorRelay.createDefault("")
    val usernameChangedConsumer: Consumer<String> = usernameChanged

    private val passwordChanged = BehaviorRelay.createDefault("")
    val passwordChangedConsumer: Consumer<String> = passwordChanged

    init {
        viewState =
            LoginViewState.DEFAULT
        setupLoginEvent()
    }

    private fun setupLoginEvent() {
//        val loginParams =
//            Observable.combineLatest(usernameChanged, passwordChanged, BiFunction(::LoginParams))
//        val loginParamsValidation = viewModelEvents.filterTo(LoginViewModelEvent.Login::class.java)
//            .withLatestFrom(
//                loginParams,
//                BiFunction<LoginViewModelEvent.Login, LoginParams, Pair<LoginParams, Boolean>> { _, p: LoginParams ->
//                    val validationResult = ArrayList<FormValidationStatus>()
//                    if (p.login.isBlank()) {
//                        validationResult.add(FormValidationStatus.EmptyUsername)
//                    }
//                    if (p.password.isBlank()) {
//                        validationResult.add(FormValidationStatus.EmptyPassword)
//                    }
//                    if (validationResult.isNotEmpty()) {
//                        viewState = viewState.copy(formValidationStatuses = validationResult)
//                        Pair(p, false)
//                    } else {
//                        Pair(p, true)
//                    }
//                })
//        loginParamsValidation
//            .filter { it.second && viewState.loadingStatus == LoadingStatus.NoLoading }
//            .doOnNext {
//                viewState = viewState.copy(
//                    loadingStatus = LoadingStatus.FetchingUser,
//                    exceptionStatus = null,
//                    formValidationStatuses = ArrayList()
//                )
//            }
//            .flatMap {
//                interactors.login(it.first).toResult(errorHandler)
//            }
//            .observeOn(AndroidSchedulers.mainThread())
//            .doOnEach { viewState = viewState.copy(loadingStatus = LoadingStatus.NoLoading) }
//            .subscribe {
//
//            }
//            .addTo(compositeDisposable)
    }
}








































