package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.forEach
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setCheckedDistinct
import com.fmklab.armnk_results.framework.extensions.setTextDistinct
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.fmklab.armnk_results.modules.common.extensions.toDate
import com.fmklab.armnk_results.presentation.numberdetail.NumberDetailBaseInfoFragment
import com.fmklab.armnk_results.presentation.numberdetail.ServicePointFragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.radiobutton.MaterialRadioButton
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.controlDateEditText
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryNameAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryNameTextInputLayout
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryYearAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.factoryYearTextInputLayout
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.isNewDetailCheckBox
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.numberDetailEditText
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.numberDetailTextInputLayout
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.rejectionCriteriaCheckBox
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.resultRadioGroup
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.servicePointCheckBox
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.servicePointDescriptionTextView
import kotlinx.android.synthetic.main.fragment_number_detail_base_input.shimmerViewContainer
import kotlinx.android.synthetic.main.fragment_number_detail_bogie_base_input.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BogieNumberDetailBaseInfoFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditBogieNumberDetailViewModel.ViewModel by sharedViewModel()
    private val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
    private lateinit var materialDatePicker: MaterialDatePicker<Long>

    companion object {
        private const val DATE_PICKER_TAG = "DatePickerTag"
        private const val SERVICE_POINT_FRAGMENT_TAG = "ServicePointFragmentTag"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_bogie_base_input, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBinding()
        setupListeners()
    }

    private fun setupUI() {
        controlDateEditText.keyListener = null
        factoryNameAutoCompleteTextView.keyListener = null
        factoryYearAutoCompleteTextView.keyListener = null

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText(getString(R.string.select_date))
        materialDatePicker = builder.build()
    }

    private fun setupBinding() {
        viewModel.outputs.fetchingDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlDateEditText.setText(simpleDateFormat.format(it.controlDate))
                bogieModelAutoCompleteTextView.setText(it.bogieModel?.toString() ?: "", false)
                factoryNameAutoCompleteTextView.setText(it.factory?.toString() ?: "", false)
                factoryYearAutoCompleteTextView.setText(it.factoryYear?.toString() ?: "", false)
                isNewDetailCheckBox.setCheckedDistinct(it.isNewDetail)
                numberDetailEditText.setTextDistinct(it.number)
                servicePointCheckBox.setCheckedDistinct(it.isFromServicePoint)
                rejectionCriteriaCheckBox.setCheckedDistinct(it.noRejectionCriteria)
                servicePointDescriptionTextView.text = it.servicePoint.toString()
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingBogieModels()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {

            }
            .addTo(compositeDisposable)

        viewModel.outputs.loadDetailError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showAllViewLoading() }
            .addTo(compositeDisposable)

        viewModel.outputs.bogieModels()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                bogieModelAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingBogieModelDependedData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showBogieModelDependDataLoading()
                } else {
                    hideBogieModelDependDataLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.factories()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                factoryNameAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingFactoryDependData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showFactoryDependDataLoading()
                } else {
                    hideFactoryDependDataLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.factoryYears()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                factoryYearAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        bogieModelAutoCompleteTextView
            .itemSelected<BogieModel>()
            .subscribe {
                viewModel.inputs.bogieModel(it)
            }
            .addTo(compositeDisposable)

        factoryNameAutoCompleteTextView
            .itemSelected<Factory>()
            .subscribe {
                viewModel.inputs.factory(it)
            }
            .addTo(compositeDisposable)

        factoryYearAutoCompleteTextView
            .itemSelected<FactoryYear>()
            .subscribe {
                viewModel.inputs.factoryYear(it)
            }
            .addTo(compositeDisposable)

        Observable.zip(
            viewModel.outputs.controlResults(),
            viewModel.outputs.numberDetail(),
            BiFunction<List<ControlResult>, BogieNumberDetail, Pair<List<ControlResult>, ControlResult?>> { controlResults, nd ->
                Pair(controlResults, nd.controlResult)
            }
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                resultRadioGroup.removeAllViews()
                for (result in it.first) {
                    val radioButton = MaterialRadioButton(requireContext())
                    resultRadioGroup.addView(radioButton)
                    radioButton.tag = result
                    radioButton.text = result.name
                    if (result == it.second) {
                        radioButton.setCheckedDistinct(true)
                    }
                    radioButton
                        .checkedChanges()
                        .skipInitialValue()
                        .filter { isChecked -> isChecked }
                        .map { radioButton.tag }
                        .ofType(ControlResult::class.java)
                        .subscribe(viewModel.inputs::controlResult)
                        .addTo(compositeDisposable)
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                for (formItem in it) {
                    when (formItem) {
                        is BogieNumberDetail.InvalidFieldImpl.BogieModel -> bogieModelTextInputLayout.error =
                            formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Factory -> factoryNameTextInputLayout.error =
                            formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.FactoryYear -> factoryYearTextInputLayout.error =
                            formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Number -> numberDetailTextInputLayout.error =
                            formItem.reason
                        is BogieNumberDetail.InvalidFieldImpl.Result ->
                            Toast.makeText(requireContext(), formItem.reason, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            .addTo(compositeDisposable)

        isNewDetailCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe(viewModel.inputs::isNewDetail)
            .addTo(compositeDisposable)

        numberDetailEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::number)
            .addTo(compositeDisposable)

        servicePointCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe {
                viewModel.inputs.isFromServicePoint(it)
                if (it) {
                    rejectionCriteriaCheckBox.enable()
                } else {
                    rejectionCriteriaCheckBox.disable()
                }
            }
            .addTo(compositeDisposable)

        servicePointCheckBox
            .clicks()
            .filter { servicePointCheckBox.isChecked }
            .subscribe { showServicePointFragment() }
            .addTo(compositeDisposable)

        rejectionCriteriaCheckBox
            .checkedChanges()
            .skipInitialValue()
            .subscribe(viewModel::noRejectionCriteria)
            .addTo(compositeDisposable)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupListeners() {
        controlDateEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (!parentFragmentManager.fragments.any { frg -> frg.tag == DATE_PICKER_TAG }) {
                    materialDatePicker.show(parentFragmentManager,
                        DATE_PICKER_TAG
                    )
                }
                return@setOnTouchListener true
            }
            false
        }

        materialDatePicker.addOnPositiveButtonClickListener {
            val date = it.toDate()
            viewModel.inputs.controlDate(date)
        }

        bogieModelAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            bogieModelTextInputLayout.error = null
        }

        factoryNameAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            factoryNameTextInputLayout.error = null
        }

        factoryYearAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            factoryYearTextInputLayout.error = null
        }

        numberDetailEditText.doOnTextChanged { _, _, _, _ ->
            numberDetailTextInputLayout.error = null
        }
    }

    private fun showAllViewLoading() {
        controlDateEditText.disable()
        bogieModelTextInputLayout.disable()
        bogieModelAutoCompleteTextView.disable()
        factoryNameTextInputLayout.disable()
        factoryNameAutoCompleteTextView.disable()
        factoryYearTextInputLayout.disable()
        factoryYearAutoCompleteTextView.disable()
        isNewDetailCheckBox.disable()
        numberDetailTextInputLayout.disable()
        numberDetailEditText.disable()
        servicePointCheckBox.disable()
        resultRadioGroup.forEach { it.disable() }
        shimmerViewContainer.showShimmer(true)
    }

    private fun hideAllViewLoading() {
        controlDateEditText.enable()
        bogieModelTextInputLayout.enable()
        bogieModelAutoCompleteTextView.enable()
        factoryNameTextInputLayout.enable()
        factoryNameAutoCompleteTextView.enable()
        factoryYearTextInputLayout.enable()
        factoryYearAutoCompleteTextView.enable()
        isNewDetailCheckBox.enable()
        numberDetailTextInputLayout.enable()
        numberDetailEditText.enable()
        servicePointCheckBox.enable()
        resultRadioGroup.forEach { it.enable() }
        shimmerViewContainer.hideShimmer()
    }

    private fun showBogieModelDependDataLoading() {
        factoryNameTextInputLayout.disable()
        factoryNameAutoCompleteTextView.disable()
        factoryShimmerLayout.showShimmer(true)
    }

    private fun hideBogieModelDependDataLoading() {
        factoryNameTextInputLayout.enable()
        factoryNameAutoCompleteTextView.enable()
        factoryShimmerLayout.hideShimmer()
    }

    private fun showFactoryDependDataLoading() {
        factoryYearTextInputLayout.disable()
        factoryYearAutoCompleteTextView.disable()
        factoryYearShimmerLayout.showShimmer(true)
    }

    private fun hideFactoryDependDataLoading() {
        factoryYearTextInputLayout.enable()
        factoryYearAutoCompleteTextView.enable()
        factoryYearShimmerLayout.hideShimmer()
    }

    private fun showServicePointFragment() {
        if (!parentFragmentManager.fragments.any { frg -> frg.tag == SERVICE_POINT_FRAGMENT_TAG }) {
            BogieServicePointFragment().show(parentFragmentManager,
                SERVICE_POINT_FRAGMENT_TAG
            )
        }
    }
}






























