package com.fmklab.armnk_results.presentation.outerring

import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_result.domain.OuterRingListItem
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.OuterRingInteractors
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.google.gson.Gson
import io.reactivex.rxjava3.core.Observable

interface OuterRingViewModel : PaginationViewModel {

    interface Inputs : PaginationViewModel.Inputs<OuterRingListItem>

    interface Outputs : PaginationViewModel.Outputs<OuterRingListItem>

    class ViewModel(
        private val interactors: OuterRingInteractors,
        private val errorHandler: ErrorHandler
    ) : PaginationViewModel.ViewModel<OuterRingListItem>(errorHandler), Inputs, Outputs {

        override val inputs: PaginationViewModel.Inputs<OuterRingListItem> = this

        override val outputs: PaginationViewModel.Outputs<OuterRingListItem> = this

        override fun fetchDetails(
            page: Int,
            partId: Int
        ): Observable<Result<List<OuterRingListItem>>> = interactors.getAllOuterRings(partId, page)
            .map { data -> Result.success(data) }
            .onErrorResumeNext { t -> Observable.just(Result.error(errorHandler.getError(t))) }

        override fun getEditedDetail(json: String): OuterRingListItem {
            return Gson().fromJson(json, OuterRingListItem::class.java)
        }

        override fun searchDetail(
            partId: Int,
            number: String
        ): Observable<Result<List<OuterRingListItem>>> {
            return interactors.searchOuterRing(partId, number).toResultApi(errorHandler)
        }
    }
}