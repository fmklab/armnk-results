package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.util.Log
import androidx.lifecycle.ViewModel
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.ControlDetailInteractors
import com.fmklab.armnk_results.framework.interactors.NoneNumberDetailInteractors
import com.fmklab.armnk_results.modules.common.DataResponse
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.kotlin.addTo
import java.net.ConnectException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

interface NoneNumberDetailViewModel : PaginationViewModel {

    interface Inputs : PaginationViewModel.Inputs<NoneNumberDetail>

    interface Outputs : PaginationViewModel.Outputs<NoneNumberDetail>

    class ViewModel(
        private val interactors: NoneNumberDetailInteractors,
        private val errorHandler: ErrorHandler
    ) : PaginationViewModel.ViewModel<NoneNumberDetail>(errorHandler), Inputs, Outputs {


        override val inputs: PaginationViewModel.Inputs<NoneNumberDetail> = this
        override val outputs: PaginationViewModel.Outputs<NoneNumberDetail> = this

        override fun fetchDetails(page: Int, partId: Int): Observable<Result<List<NoneNumberDetail>>> =
            interactors.getAllPaged(partId, page)
                .map { data -> Result.success(data) }
                .onErrorResumeNext { t ->
                    Observable.just(Result.error(errorHandler.getError(t)))
                }

        override fun getEditedDetail(json: String): NoneNumberDetail {
            return Gson().fromJson(json, NoneNumberDetail::class.java)
        }

        override fun searchDetail(
            partId: Int,
            number: String
        ): Observable<Result<List<NoneNumberDetail>>> {
            return Observable.empty()
        }
    }
}








































