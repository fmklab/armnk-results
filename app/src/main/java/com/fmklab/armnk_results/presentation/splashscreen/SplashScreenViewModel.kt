package com.fmklab.armnk_results.presentation.splashscreen

import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.interactors.LoginInteractors
import com.fmklab.armnk_results.framework.LoginParams
import com.fmklab.armnk_results.framework.retrofit.Session
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

interface SplashScreenViewModel {

    interface Inputs {

        fun loadSavedLoginParams()

        fun retryLoginClick()
    }

    interface Outputs {

        fun serverError(): Observable<Error>

        fun fetchingSavedLoginParams(): Observable<Boolean>

        fun loadingSuccess(): Observable<Unit>

        fun savedLoginParamsNotFound(): Observable<Unit>
    }

    class ViewModel(
        private val interactors: LoginInteractors,
        private val errorHandler: ErrorHandler,
        private val currentUser: CurrentUser
    ) : BaseViewModel(), Inputs, Outputs {

        private val compositeDisposable = CompositeDisposable()

        private val retryLoginClicked = PublishRelay.create<Unit>()
        private val loadSavedLoginParams = PublishRelay.create<Unit>()

        private val serverError = PublishRelay.create<Error>()
        private val fetchingSavedLoginParams = BehaviorRelay.createDefault(false)
        private val loadingSuccess = PublishRelay.create<Unit>()
        private val savedLoginNotFound = PublishRelay.create<Unit>()

        private val savedLoginParams = BehaviorRelay.create<LoginParams>()

        val inputs: Inputs = this
        val outputs: Outputs = this

        init {
            val savedLoginParamsNotification = loadSavedLoginParams
                .flatMap {
                    currentUser.observeLoginParams().subscribeOn(Schedulers.io()).materialize()
                }
                .share()

            savedLoginParamsNotification
                .subscribeOn(Schedulers.io())
                .filter { it.isOnNext }
                .map { it.value }
                .subscribe(savedLoginParams)
                .addTo(compositeDisposable)

            savedLoginParams
                .subscribeOn(Schedulers.io())
                .filter { !it.isValid() }
                .map { Unit }
                .subscribe(savedLoginNotFound)
                .addTo(compositeDisposable)

            val fetchingSavedLoginParamsNotification = savedLoginParams
                .filter { it.isValid() }
                .switchMap {
                    submit(it)
                }
                .share()

            fetchingSavedLoginParamsNotification
                .subscribeOn(Schedulers.io())
                .filter { it.isOnNext }
                .map { it.value }
                .subscribe(this::success)
                .addTo(compositeDisposable)

            fetchingSavedLoginParamsNotification
                .subscribeOn(Schedulers.io())
                .filter { it.isOnError }
                .delay(2, TimeUnit.SECONDS)
                .map { errorHandler.getError(it.error) }
                .subscribe(serverError)
                .addTo(compositeDisposable)
        }

        private fun submit(loginParams: LoginParams) =
            interactors.login(loginParams)
                .doOnSubscribe { fetchingSavedLoginParams.accept(true) }
                .doAfterTerminate { fetchingSavedLoginParams.accept(false) }
                .materialize()

        private fun success(session: Session) {
            currentUser.login(session)
            loadingSuccess.accept(Unit)
        }

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.dispose()
        }

        override fun loadSavedLoginParams() = loadSavedLoginParams.accept(Unit)

        override fun retryLoginClick() = retryLoginClicked.accept(Unit)

        override fun serverError(): Observable<Error> = serverError.hide()

        override fun fetchingSavedLoginParams(): Observable<Boolean> = fetchingSavedLoginParams.hide()

        override fun loadingSuccess(): Observable<Unit> = loadingSuccess.hide()

        override fun savedLoginParamsNotFound(): Observable<Unit> = savedLoginNotFound
    }
}








































