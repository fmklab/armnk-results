package com.fmklab.armnk_results.presentation.numberdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.DefectType
import com.fmklab.armnk_result.domain.NumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.extensions.disable
import com.fmklab.armnk_results.framework.extensions.enable
import com.fmklab.armnk_results.framework.extensions.setTextDistinct
import com.fmklab.armnk_results.modules.common.extensions.initArrayAdapter
import com.fmklab.armnk_results.modules.common.extensions.itemSelected
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_repair_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NumberDetailRepairInfoFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()

    private val viewModel: EditNumberDetailViewModel.ViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_repair_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupBinding()
        setupListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    private fun setupUI() {
        defectTypeAutoCompleteTextView.keyListener = null
        zoneAutoCompleteTextView.keyListener = null
    }

    private fun setupBinding() {
        viewModel.outputs.numberDetail()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.setText(it.defectType?.toString() ?: "", false)
                zoneAutoCompleteTextView.setText(it.controlZone?.toString() ?: "", false)
                defectDescriptionEditText.setTextDistinct(it.repairDescription)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.fetchingRepairInfo()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    showAllViewLoading()
                } else {
                    hideAllViewLoading()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.repairInfoError()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { showAllViewLoading() }
            .addTo(compositeDisposable)

        viewModel.outputs.repairDefectTypes()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                defectTypeAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.repairControlZones()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                zoneAutoCompleteTextView.initArrayAdapter(requireContext(), ArrayList(it))
            }
            .addTo(compositeDisposable)

        viewModel.outputs.invalidForm()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                for (formItem in it) {
                    when (formItem) {
                        is NumberDetail.InvalidFieldImpl.DefectType ->
                            defectTypeTextInputLayout.error = formItem.reason
                        is NumberDetail.InvalidFieldImpl.ControlZone ->
                            zoneTextInputLayout.error = formItem.reason
                        is NumberDetail.InvalidFieldImpl.RepairDescription ->
                            defectDescriptionTextInputLayout.error = formItem.reason
                    }
                }
            }
            .addTo(compositeDisposable)

        defectTypeAutoCompleteTextView
            .itemSelected<DefectType>()
            .subscribe(viewModel.inputs::repairDefectType)
            .addTo(compositeDisposable)

        zoneAutoCompleteTextView
            .itemSelected<ControlZone>()
            .subscribe(viewModel.inputs::repairControlZone)
            .addTo(compositeDisposable)

        defectDescriptionEditText
            .textChanges()
            .skipInitialValue()
            .map(CharSequence::toString)
            .subscribe(viewModel.inputs::repairDescription)
            .addTo(compositeDisposable)
    }

    private fun setupListeners() {
        defectTypeAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            defectTypeTextInputLayout.error = null
        }

        zoneAutoCompleteTextView.doOnTextChanged { _, _, _, _ ->
            zoneTextInputLayout.error = null
        }

        defectDescriptionEditText.doOnTextChanged { _, _, _, _ ->
            defectDescriptionTextInputLayout.error = null
        }
    }

    private fun showAllViewLoading() {
        defectTypeTextInputLayout.disable()
        defectTypeAutoCompleteTextView.disable()
        zoneTextInputLayout.disable()
        zoneAutoCompleteTextView.disable()
        defectDescriptionEditText.disable()
        shimmerViewContainer.showShimmer(true)
    }

    private fun hideAllViewLoading() {
        defectTypeTextInputLayout.enable()
        defectTypeAutoCompleteTextView.enable()
        zoneTextInputLayout.enable()
        zoneAutoCompleteTextView.enable()
        defectDescriptionEditText.enable()
        shimmerViewContainer.hideShimmer()
    }
}





































