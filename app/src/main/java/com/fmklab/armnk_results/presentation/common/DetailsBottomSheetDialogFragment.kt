package com.fmklab.armnk_results.presentation.common

import android.os.Bundle
import android.view.View
import com.fmklab.armnk_result.domain.Detail
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo

abstract class DetailsBottomSheetDialogFragment<T : Detail> : BottomSheetDialogFragment() {

    private val compositeDisposable = CompositeDisposable()

    protected abstract val viewModel: PaginationViewModel.ViewModel<T>

    protected var title: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBinding()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    protected open fun setupBinding() {
        viewModel.outputs.showMoreInfo()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                bind(it)
            }
            .addTo(compositeDisposable)

        viewModel.outputs.title()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                title = it
            }
            .addTo(compositeDisposable)
    }

    protected abstract fun bind(detail: T)
}








































