package com.fmklab.armnk_results.presentation.outerring

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.OuterRingListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_outer_ring_details.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class OuterRingDetailsFragment : DetailsBottomSheetDialogFragment<OuterRingListItem>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: PaginationViewModel.ViewModel<OuterRingListItem> by sharedViewModel<OuterRingViewModel.ViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_outer_ring_details, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    override fun bind(detail: OuterRingListItem) {
        outerRingDetailsToolbar.title = "${getString(R.string.detail_number)} ${detail.number}"
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        controlDateValueTextView.text = simpleDateFormat.format(detail.controlDate)
        methodValueTextView.text = detail.method
        detectorValueTextView.text = detail.detector
        factoryNameValueTextView.text = detail.factoryName
        factoryYearValueTextView.text = detail.factoryYear.toString()
        resultValueTextView.text = detail.controlResult
        defectDescriptionValueTextView.text = detail.defectInfo
        specValueTextView.text = detail.spec

        editNumberDetailFab.setOnClickListener {
            if (detail.editStatus) {
                viewModel.inputs.editDetail(detail)
            } else {
                Snackbar.make(
                    editMessageCoordinatorLayout,
                    getString(R.string.edit_none_number_detail_error),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
        val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
        createDateValueTextView.text = createDateFormat.format(detail.createDate)
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showDetailEditor()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.setClass(requireContext(), EditOuterRingActivity::class.java)
                startActivityForResult(it, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }
}







































