package com.fmklab.armnk_results.presentation.login

import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_result.domain.ErrorHandler
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.interactors.LoginInteractors
import com.fmklab.armnk_results.framework.LoginParams
import com.fmklab.armnk_results.framework.retrofit.Session
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.kotlin.addTo

interface LoginViewModel {

    interface Inputs {

        fun username(username: String)

        fun password(password: String)

        fun rememberMeCheck(isChecked: Boolean)

        fun loginClick()

        fun usernameFocus(hasFocus: Boolean)

        fun passwordFocus(hasFocus: Boolean)
    }

    interface Outputs {

        fun invalidUsernameError(): Observable<Boolean>

        fun invalidPasswordError(): Observable<Boolean>

        fun loginSuccess(): Observable<Unit>

        fun loginError(): Observable<Error>

        fun fetchingUser(): Observable<Boolean>

        fun isLoginButtonEnable(): Observable<Boolean>
    }

    class ViewModel(
        private val interactors: LoginInteractors,
        private val errorHandler: ErrorHandler,
        private val currentUser: CurrentUser
    ) :
        BaseViewModel(), Inputs, Outputs {

        private val compositeDisposable = CompositeDisposable()

        private val usernameChanged = PublishRelay.create<String>()
        private val passwordChanged = PublishRelay.create<String>()
        private val rememberMeChecked = PublishRelay.create<Boolean>()
        private val loginClicked = PublishRelay.create<Unit>()
        private val usernameFocused = PublishRelay.create<Boolean>()
        private val passwordFocused = PublishRelay.create<Boolean>()

        private val loginSuccess = PublishRelay.create<Unit>()
        private val loginError = PublishRelay.create<Error>()
        private val fetchingUser = BehaviorRelay.createDefault(false)
        private val invalidUsername = PublishRelay.create<Boolean>()
        private val invalidPassword = PublishRelay.create<Boolean>()
        private val isLoginButtonEnable = PublishRelay.create<Boolean>()


        val inputs: Inputs = this
        val outputs: Outputs = this

        init {
            val loginParams = Observable.combineLatest(
                usernameChanged,
                passwordChanged,
                rememberMeChecked,
                Function3(::LoginParams)
            )

            val loginNotification = loginClicked
                .withLatestFrom(
                    loginParams,
                    BiFunction<Unit, LoginParams, LoginParams> { _, p: LoginParams ->
                        p
                    })
                .filter { !fetchingUser.value && it.isValid() }
                .switchMap {
                    if (it.rememberMe) {
                        rememberLoginParams(it)
                    }
                    submit(it)
                }
                .share()

            loginNotification
                .filter { it.isOnNext }
                .map { it.value }
                .subscribe(this::success)
                .addTo(compositeDisposable)

            loginNotification
                .filter { it.isOnError }
                .map { errorHandler.getError(it.error) }
                .subscribe(loginError)
                .addTo(compositeDisposable)

            usernameFocused
                .withLatestFrom(
                    usernameChanged,
                    BiFunction<Boolean, String, Pair<Boolean, String>> { hf: Boolean, un: String ->
                        Pair(hf, un)
                    })
                .map { !it.first && it.second.isBlank() }
                .distinctUntilChanged()
                .subscribe(invalidUsername)
                .addTo(compositeDisposable)

            passwordFocused
                .withLatestFrom(
                    passwordChanged,
                    BiFunction<Boolean, String, Pair<Boolean, String>> { hf: Boolean, un: String ->
                        Pair(hf, un)
                    })
                .map { !it.first && it.second.isBlank() }
                .distinctUntilChanged()
                .subscribe(invalidPassword)
                .addTo(compositeDisposable)

            loginParams
                .map { it.isValid() }
                .subscribe(isLoginButtonEnable)
                .addTo(compositeDisposable)
        }

        private fun rememberLoginParams(loginParams: LoginParams) =
            currentUser.rememberLoginParams(loginParams)

        private fun submit(loginParams: LoginParams) =
            interactors.login(loginParams)
                .doOnSubscribe {
                    fetchingUser.accept(true)
                }
                .doAfterTerminate {
                    fetchingUser.accept(false)
                }
                .materialize()

        private fun success(session: Session) {
            currentUser.login(session)
            loginSuccess.accept(Unit)
        }

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.dispose()
        }

        override fun username(username: String) = usernameChanged.accept(username)

        override fun password(password: String) = passwordChanged.accept(password)

        override fun rememberMeCheck(isChecked: Boolean) = rememberMeChecked.accept(isChecked)

        override fun loginClick() = loginClicked.accept(Unit)

        override fun usernameFocus(hasFocus: Boolean) = usernameFocused.accept(hasFocus)

        override fun passwordFocus(hasFocus: Boolean) = passwordFocused.accept(hasFocus)

        override fun invalidUsernameError(): Observable<Boolean> = invalidUsername.hide()

        override fun invalidPasswordError(): Observable<Boolean> = invalidPassword.hide()

        override fun loginSuccess(): Observable<Unit> = loginSuccess.hide()

        override fun loginError(): Observable<Error> = loginError.hide()

        override fun fetchingUser(): Observable<Boolean> = fetchingUser.hide()

        override fun isLoginButtonEnable(): Observable<Boolean> = isLoginButtonEnable.hide()
    }
}









































