package com.fmklab.armnk_results.presentation.numberdetail

import android.os.Bundle
import android.view.MenuItem
import com.fmklab.armnk_result.domain.NumberDetailListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.di.NumberDetailScope
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.DetailsPagingAdapter
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailActivity
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_small_detail.*
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.ext.getOrCreateScope

class NumberDetailActivity : BaseNumberDetailActivity<NumberDetailListItem>() {

    private val compositeDisposable = CompositeDisposable()

    private val numberDetailScope = getKoin().get<NumberDetailScope>().getOrCreateScope()

    override val viewModel: PaginationViewModel.ViewModel<NumberDetailListItem> by numberDetailScope.viewModel<NumberDetailViewModel.ViewModel>(
        this
    )

    override val recyclerViewAdapter: DetailsPagingAdapter<NumberDetailListItem> by numberDetailScope.inject<NumberDetailAdapter>()

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
        numberDetailScope.close()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showNewDetailEditor()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                intent.setClass(this, EditNumberDetailActivity::class.java)
                startActivityForResult(intent, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun createDetailsFragment(): NumberDetailsFragment {
        return NumberDetailsFragment()
    }
}






























