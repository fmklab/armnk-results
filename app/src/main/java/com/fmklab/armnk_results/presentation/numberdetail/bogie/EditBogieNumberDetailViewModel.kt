package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.content.Intent
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.*
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.BogieNumberDetailInteractors
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.functions.Function4
import io.reactivex.rxjava3.functions.Function5
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

interface EditBogieNumberDetailViewModel {

    interface Inputs {

        fun loadDetail()

        fun controlDate(value: Date)

        fun bogieModel(value: BogieModel?)

        fun factory(value: Factory?)

        fun factoryYear(value: FactoryYear?)

        fun isNewDetail(value: Boolean)

        fun number(value: String)

        fun isFromServicePoint(value: Boolean)

        fun servicePointCompany(value: CompanyEnum)

        fun servicePointFault(value: FaultEnum)

        fun noRejectionCriteria(value: Boolean)

        fun controlResult(value: ControlResult)

        fun refreshBogieModelDependData()

        fun refreshFactoryDependData()

        fun clearCache()

        fun loadRepairInfo()

        fun repairDefectType(value: DefectType)

        fun repairControlZone(value: ControlZone)

        fun repairDescription(value: String)

        fun defectMoment(value: NumberDetailDefectMoment)

        fun loadDefecationInfo()

        fun defectType(value: DefectType)

        fun controlZone(value: ControlZone?)

        fun steal(value: String)

        fun owner(value: String)

        fun defectDescription(value: String)

        fun loadVisualInfo()

        fun defectLength(value: String)

        fun defectDepth(value: String)

        fun defectDiameter(value: String)

        fun loadNdtInfo()

        fun method(value: String)

        fun defectSize(value: String)

        fun detector(value: Detector)

        fun attemptSave()

        fun reloadDefectInfo()

        fun defectInfoChanged()

        fun acceptSaving()

        fun acceptSavingAddedDetail()
    }

    interface Outputs {

        fun title(): Observable<String>

        fun numberDetail(): Observable<BogieNumberDetail>

        fun fetchingDetail(): Observable<Boolean>

        fun fetchingBogieModels(): Observable<Boolean>

        fun fetchingFactoryDependData(): Observable<Boolean>

        fun fetchingBogieModelDependedData(): Observable<Boolean>

        fun loadDetailError(): Observable<Error>

        fun loadBogieModelDependsError(): Observable<Error>

        fun loadFactoryDependsError(): Observable<Error>

        fun bogieModels(): Observable<List<BogieModel>>

        fun factories(): Observable<List<Factory>>

        fun factoryYears(): Observable<List<FactoryYear>>

        fun controlResults(): Observable<List<ControlResult>>

        fun noInternetConnection(): Observable<Boolean>

        fun fetchingRepairInfo(): Observable<Boolean>

        fun repairDefectTypes(): Observable<List<DefectType>>

        fun repairControlZones(): Observable<List<ControlZone>>

        fun controlResultChanged(): Observable<ControlResult>

        fun repairInfoError(): Observable<Error>

        fun fetchingDefecationInfo(): Observable<Boolean>

        fun defectTypes(): Observable<List<DefectType>>

        fun controlZones(): Observable<List<ControlZone>>

        fun steals(): Observable<List<String>>

        fun defecationInfoError(): Observable<Error>

        fun fetchingVisualInfo(): Observable<Boolean>

        fun visualInfoError(): Observable<Error>

        fun fetchingNdtInfo(): Observable<Boolean>

        fun methods(): Observable<List<String>>

        fun detectors(): Observable<List<Detector>>

        fun fetchingMethodDependData(): Observable<Boolean>

        fun ndtInfoError(): Observable<Error>

        fun controlZoneEnabled(): Observable<Boolean>

        fun invalidForm(): Observable<List<InvalidField>>

        fun showSavingDialog(): Observable<String>

        fun detailSaving(): Observable<Boolean>

        fun detailAlreadyAdded(): Observable<AddedDetail>

        fun detailSavingError(): Observable<Error>

        fun detailSaveSuccess(): Observable<Intent>
    }

    @Suppress("UNCHECKED_CAST")
    class ViewModel(
        private val interactors: BogieNumberDetailInteractors,
        private val errorHandler: ErrorHandler, private val currentUser: CurrentUser
    ) : BaseViewModel(), Inputs, Outputs {

        private val compositeDisposable = CompositeDisposable()

        private val numberDetailInput = PublishRelay.create<BogieNumberDetail>()
        private val loadDetail = PublishRelay.create<Unit>()
        private val bogieModelChanged = PublishRelay.create<BogieModel>()
        private val factoryChanged = PublishRelay.create<Factory>()
        private val loadRepairInfo = PublishRelay.create<Unit>()
        private val loadDefecationInfo = PublishRelay.create<Unit>()
        private val loadVisualInfo = PublishRelay.create<Unit>()
        private val loadNdtInfo = PublishRelay.create<Unit>()
        private val methodChanged = PublishRelay.create<String>()
        private val defectTypeChanged = PublishRelay.create<DefectType>()
        private val attemptSave = PublishRelay.create<Unit>()
        private val reloadDefectInfo = PublishRelay.create<Unit>()
        private val detailSaving = BehaviorRelay.createDefault(false)
        private val detailAlreadyAdded = PublishRelay.create<AddedDetail>()
        private val detailSavingError = PublishRelay.create<Error>()

        private val title = BehaviorRelay.create<String>()
        private val numberDetailOutput = BehaviorRelay.create<BogieNumberDetail>()
        private val fetchingDetail = BehaviorRelay.createDefault(false)
        private val fetchingBogieModels = BehaviorRelay.createDefault(false)
        private val fetchingFactoryDependData = BehaviorRelay.createDefault(false)
        private val fetchingBogieModelDependedData = BehaviorRelay.createDefault(false)
        private val loadDetailError = PublishRelay.create<Error>()
        private val loadBogieModelDependsError = PublishRelay.create<Error>()
        private val loadFactoryDependsError = PublishRelay.create<Error>()
        private val noInternetConnection = BehaviorRelay.createDefault(false)
        private val bogieModels = BehaviorRelay.create<List<BogieModel>>()
        private val factories = BehaviorRelay.create<List<Factory>>()
        private val factoryYears = BehaviorRelay.create<List<FactoryYear>>()
        private val controlResults = BehaviorRelay.create<List<ControlResult>>()
        private val fetchingRepairInfo = BehaviorRelay.createDefault(false)
        private val repairDefectTypes = BehaviorRelay.create<List<DefectType>>()
        private val repairControlZones = BehaviorRelay.create<List<ControlZone>>()
        private val controlResultChanged = PublishRelay.create<ControlResult>()
        private val defectInfoChanged = BehaviorRelay.createDefault(false)
        private val repairInfoError = PublishRelay.create<Error>()
        private val fetchingDefecationInfo = BehaviorRelay.createDefault(false)
        private val defectTypes = BehaviorRelay.create<List<DefectType>>()
        private val controlZones = BehaviorRelay.create<List<ControlZone>>()
        private val steals = BehaviorRelay.create<List<String>>()
        private val defecationInfoError = PublishRelay.create<Error>()
        private val fetchingVisualInfo = BehaviorRelay.createDefault(false)
        private val visualInfoError = PublishRelay.create<Error>()
        private val fetchingNdtInfo = BehaviorRelay.createDefault(false)
        private val methods = BehaviorRelay.create<List<String>>()
        private val detectors = BehaviorRelay.create<List<Detector>>()
        private val fetchingMethodDependData = BehaviorRelay.createDefault(false)
        private val ndtInfoError = PublishRelay.create<Error>()
        private val controlZoneEnabled = BehaviorRelay.createDefault(true)
        private val invalidForm = PublishRelay.create<List<InvalidField>>()
        private val showSavingDialog = PublishRelay.create<String>()
        private val acceptSaving = PublishRelay.create<Unit>()
        private val acceptSavingAddedDetail = PublishRelay.create<Unit>()
        private val detailSaveSuccess = PublishRelay.create<Intent>()

        private val saveDetail = PublishRelay.create<Unit>()

        val inputs: Inputs = this
        val outputs: Outputs = this

        init {
            val selectedDetailId = intent()
                .subscribeOn(Schedulers.io())
                .map { it.extras?.getInt(PaginationActivity.SELECTED_DETAIL_ID_TAG)!! }

            val selectedControlDetail = intent()
                .subscribeOn(Schedulers.io())
                .map {
                    val json =
                        it.extras?.getString(ControlDetailActivity.SELECTED_CONTROL_DETAIL_TAG)!!
                    Gson().fromJson(json, ControlDetail::class.java)
                }

            selectedControlDetail
                .map { it.name }
                .subscribe(title)
                .addTo(compositeDisposable)

            //region Load Detail

            loadDetail
                .withLatestFrom(
                    selectedControlDetail,
                    selectedDetailId,
                    Function3<Unit, ControlDetail, Int, Pair<Int, Int>> { _, cd, id ->
                        Pair(cd.id, id)
                    }
                )
                .filter { !fetchingDetail.value }
                .doOnNext { fetchingDetail.accept(true) }
                .switchMap {
                    fetchDetail(it.second).toResultApi(noInternetConnection, errorHandler)
                        .switchMap { nd ->
                            if (nd.isError && nd.error != Error.NotFoundRemote) {
                                Observable.just(
                                    BogieNumberDetailInputs(
                                        nd,
                                        Result.error(nd.error!!),
                                        Result.error(nd.error),
                                        Result.error(nd.error),
                                        Result.error(nd.error)
                                    )
                                )
                            } else {
                                Observable.zip(
                                    fetchBogieModels(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchFactories(
                                        it.first,
                                        nd.data?.bogieModel?.id ?: 0
                                    ).toResultApi(noInternetConnection, errorHandler),
                                    fetchYears(nd.data?.factory?.id ?: 0).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    fetchControlResults(it.first).toResultApi(
                                        noInternetConnection,
                                        errorHandler
                                    ),
                                    Function4 { bogieModels, factories, years, controlResults ->
                                        if (nd.isError && nd.error == Error.NotFoundRemote) {
                                            BogieNumberDetailInputs(
                                                Result.success(
                                                    BogieNumberDetail.default(it.first)
                                                        .spec(currentUser.getUser().toString())
                                                ) as Result<BogieNumberDetail>,
                                                bogieModels,
                                                factories,
                                                years,
                                                controlResults
                                            )
                                        } else {
                                            BogieNumberDetailInputs(
                                                nd,
                                                bogieModels,
                                                factories,
                                                years,
                                                controlResults
                                            )
                                        }
                                    }
                                )
                            }
                        }
                }
                .subscribeOn(Schedulers.io())
                .doOnEach { fetchingDetail.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = it.detail.data!!
                        val bogieModels = it.bogieModels.data!!
                        val factories = it.factories.data!!
                        val years = it.factoryYears.data!!
                        val controlResults = it.controlResults.data!!
                        detail.bogieModel(bogieModels.firstOrNull { b -> b.id == detail.bogieModel?.id })
                        detail.factory(factories.firstOrNull { f -> f.id == detail.factory?.id })
                        detail.controlResult(controlResults.firstOrNull { c -> c.id == detail.controlResult?.id })
                        this.bogieModels.accept(bogieModels)
                        this.factories.accept(factories)
                        this.factoryYears.accept(years)
                        this.controlResults.accept(controlResults)
                        numberDetailInput.accept(detail)
                    } else {
                        loadDetailError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            //endregion

            numberDetailInput
                .flatMap {
                    interactors.cacheBogieDetail(it)
                    Observable.just(it)
                }
                .subscribe(numberDetailOutput)
                .addTo(compositeDisposable)

            bogieModelChanged
                .filter { !fetchingBogieModelDependedData.value }
                .doOnNext { fetchingBogieModelDependedData.accept(true) }
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<BogieModel, ControlDetail, Pair<Int, Int>> { bogieModel, controlDetail ->
                        Pair(controlDetail.id, bogieModel.id)
                    }
                )
                .switchMap {
                    fetchFactories(it.first, it.second).toResultApi(
                        noInternetConnection,
                        errorHandler
                    )
                }
                .subscribeOn(Schedulers.io())
                .doOnEach { fetchingBogieModelDependedData.accept(false) }
                .subscribe {
                    if (it.isError) {
                        loadBogieModelDependsError.accept(it.error)
                    } else if (it.isSuccess) {
                        factories.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)

            bogieModelChanged
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<BogieModel, BogieNumberDetail, Pair<BogieNumberDetail, Int>> { bogieModel, detail ->
                        Pair(detail, bogieModel.id)
                    }
                )
                .filter { it.first.defectMoment == NumberDetailDefectMoment.VISUAL }
                .switchMap {
                    fetchBogieControlZones(
                        it.first.partId,
                        DefectMoment.VISUAL,
                        it.second
                    ).toResultApi(noInternetConnection, errorHandler)
                }
                .subscribeOn(Schedulers.io())
                .subscribe {
                    if (it.isError) {
                        loadBogieModelDependsError.accept(it.error)
                    } else {
                        controlZones.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)

            bogieModelChanged
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<BogieModel, BogieNumberDetail, Pair<BogieNumberDetail, Int>> { bogieModel, detail ->
                        Pair(detail, bogieModel.id)
                    }
                )
                .filter { it.first.defectMoment == NumberDetailDefectMoment.NDT }
                .switchMap {
                    fetchBogieControlZones(
                        it.first.partId,
                        DefectMoment.NDT,
                        it.second
                    ).toResultApi(noInternetConnection, errorHandler)
                }
                .subscribeOn(Schedulers.io())
                .subscribe {
                    if (it.isError) {
                        loadBogieModelDependsError.accept(it.error)
                    } else {
                        controlZones.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)

            factories
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<List<Factory>, BogieNumberDetail, Boolean> { factories, detail ->
                        factories.contains(detail.factory)
                    }
                )
                .subscribe {
                    if (!it) {
                        factory(null)
                    }
                }
                .addTo(compositeDisposable)

            controlZones
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<List<ControlZone>, BogieNumberDetail, Boolean> { controlZones, detail ->
                        controlZones.contains(detail.controlZone)
                    }
                )
                .subscribe {
                    if (!it) {
                        controlZone(null)
                    }
                }
                .addTo(compositeDisposable)

            factoryChanged
                .filter { !fetchingFactoryDependData.value }
                .doOnNext { fetchingFactoryDependData.accept(true) }
                .map { it.id }
                .switchMap {
                    fetchYears(it).toResultApi(noInternetConnection, errorHandler)
                }
                .subscribeOn(Schedulers.io())
                .doOnEach { fetchingFactoryDependData.accept(false) }
                .subscribe {
                    if (it.isError) {
                        loadFactoryDependsError.accept(it.error)
                    } else if (it.isSuccess) {
                        factoryYears.accept(it.data)
                    }
                }
                .addTo(compositeDisposable)

            factoryYears
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<List<FactoryYear>, BogieNumberDetail, Boolean> { factoryYears, detail ->
                        factoryYears.contains(detail.factoryYear)
                    }
                )
                .subscribe {
                    if (!it) {
                        factoryYear(null)
                    }
                }
                .addTo(compositeDisposable)

            //region Load Repair Info

            loadRepairInfo
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<Unit, ControlDetail, Int> { _, cd: ControlDetail ->
                        cd.id
                    }
                )
                .filter { !fetchingRepairInfo.value }
                .doOnNext { fetchingRepairInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchRepairDefectTypes(it).toResultApi(noInternetConnection, errorHandler),
                        fetchRepairControlZones(it).toResultApi(noInternetConnection, errorHandler),
                        BiFunction<Result<List<DefectType>>, Result<List<ControlZone>>, NumberDetailRepairInputs> { dt: Result<List<DefectType>>, cz: Result<List<ControlZone>> ->
                            NumberDetailRepairInputs(dt, cz)
                        }
                    )
                }
                .doOnEach { fetchingRepairInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        repairDefectTypes.accept(defectTypes)
                        repairControlZones.accept(controlZones)
                        numberDetailInput.accept(detail)
                    } else {
                        repairInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            //endregion

            repairDefectTypes
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.defectType == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<DefectType, BogieNumberDetail, DefectType> { dt: DefectType, _ ->
                        dt
                    }
                )
                .subscribe(this::defectType)
                .addTo(compositeDisposable)

            repairControlZones
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.controlZone == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<ControlZone, BogieNumberDetail, ControlZone> { cz, _ ->
                        cz
                    }
                )
                .subscribe(this::controlZone)
                .addTo(compositeDisposable)

            defectTypes
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.defectType == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<DefectType, BogieNumberDetail, DefectType> { dt, _ ->
                        dt
                    }
                )
                .subscribe(this::defectType)
                .addTo(compositeDisposable)

            //region Load Defecation Info

            loadDefecationInfo
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<Unit, ControlDetail, Int> { _, cd: ControlDetail ->
                        cd.id
                    }
                )
                .filter { !fetchingDefecationInfo.value }
                .doOnNext { fetchingDefecationInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchDefectTypes(it, DefectMoment.DEFECATION).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchControlZones(it, DefectMoment.DEFECATION).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchSteals().toResultApi(noInternetConnection, errorHandler),
                        Function3<Result<List<DefectType>>, Result<List<ControlZone>>, Result<List<String>>, NumberDetailDefecationInputs> { dt: Result<List<DefectType>>, cz: Result<List<ControlZone>>, s: Result<List<String>> ->
                            NumberDetailDefecationInputs(dt, cz, s)
                        }
                    )
                }
                .doOnEach { fetchingDefecationInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        val steals = it.steals.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.defectTypes.accept(defectTypes)
                        this.controlZones.accept(controlZones)
                        this.steals.accept(steals)
                        numberDetailInput.accept(detail)
                        if (detail.defectType != null) {
                            defectTypeChanged.accept(detail.defectType)
                        }
                    } else {
                        defecationInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            //endregion

            //region Load Visual Info

            loadVisualInfo
                .withLatestFrom(
                    selectedControlDetail,
                    numberDetailOutput,
                    Function3<Unit, ControlDetail, BogieNumberDetail, Pair<Int, Int>> { _, cd, detail ->
                        Pair(cd.id, detail.bogieModel?.id ?: 0)
                    }
                )
                .filter { !fetchingVisualInfo.value }
                .doOnNext { fetchingVisualInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchDefectTypes(it.first, DefectMoment.VISUAL).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchBogieControlZones(
                            it.first,
                            DefectMoment.VISUAL,
                            it.second
                        ).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchSteals().toResultApi(noInternetConnection, errorHandler),
                        Function3<Result<List<DefectType>>, Result<List<ControlZone>>, Result<List<String>>, NumberDetailVisualInputs> { dt, cz, s ->
                            NumberDetailVisualInputs(dt, cz, s)
                        }
                    )
                }
                .doOnEach { fetchingVisualInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val defectTypes = it.defectTypes.data!!
                        val controlZones = it.controlZones.data!!
                        val steals = it.steals.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.defectTypes.accept(defectTypes)
                        this.controlZones.accept(controlZones)
                        this.steals.accept(steals)
                        numberDetailInput.accept(detail)
                    } else {
                        visualInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            //endregion

            //region Load NDT Info

            loadNdtInfo
                .withLatestFrom(
                    selectedControlDetail,
                    numberDetailOutput,
                    Function3<Unit, ControlDetail, BogieNumberDetail, Pair<Int, BogieNumberDetail>> { _, cd, nd ->
                        Pair(cd.id, nd)
                    }
                )
                .filter { !fetchingNdtInfo.value }
                .doOnNext { fetchingNdtInfo.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchMethods(it.first).toResultApi(noInternetConnection, errorHandler),
                        fetchDefectTypes(it.first, DefectMoment.NDT).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchDetectors(if (it.second.method == "") "base" else it.second.method).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchBogieControlZones(it.first, DefectMoment.NDT, it.second.bogieModel?.id ?: 0).toResultApi(
                            noInternetConnection,
                            errorHandler
                        ),
                        fetchSteals().toResultApi(noInternetConnection, errorHandler),
                        Function5<Result<List<String>>, Result<List<DefectType>>, Result<List<Detector>>, Result<List<ControlZone>>, Result<List<String>>, NumberDetailNdtInputs> {
                                m, dt, d, cz, s ->
                            NumberDetailNdtInputs(m, dt, d, cz, s)
                        }
                    )
                }
                .doOnEach { fetchingNdtInfo.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        val detail = numberDetailOutput.value
                        val methods = it.methods.data!!
                        val defectTypes = it.defectTypes.data!!
                        val detectors = it.detectors.data!!
                        val controlZones = it.controlZones.data!!
                        val steals = it.steals.data!!
                        detail.defectType(defectTypes.firstOrNull { dt -> dt.id == detail.defectType?.id })
                        detail.controlZone(controlZones.firstOrNull { cz -> cz.id == detail.controlZone?.id })
                        this.methods.accept(methods)
                        this.defectTypes.accept(defectTypes)
                        this.detectors.accept(detectors)
                        this.controlZones.accept(controlZones)
                        this.steals.accept(steals)
                        numberDetailInput.accept(detail)
                    } else {
                        ndtInfoError.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            //endregion

            defectTypes
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.defectType == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<DefectType, BogieNumberDetail, DefectType> { defectType, _ ->
                        defectType
                    }
                )
                .subscribeOn(Schedulers.io())
                .subscribe(this::defectType)
                .addTo(compositeDisposable)

            controlZones
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.controlZone == null }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<ControlZone, BogieNumberDetail, ControlZone> { controlZone, _ ->
                        controlZone
                    }
                )
                .subscribeOn(Schedulers.io())
                .subscribe(this::controlZone)
                .addTo(compositeDisposable)

            methodChanged
                .filter { !fetchingMethodDependData.value }
                .doOnNext { fetchingMethodDependData.accept(true) }
                .switchMap {
                    fetchDetectors(it).toResultApi(noInternetConnection, errorHandler)
                }
                .doOnEach { fetchingMethodDependData.accept(false) }
                .subscribe {
                    if (it.isSuccess) {
                        detectors.accept(it.data)
                    } else if (it.isError) {
                        ndtInfoError.accept(it.error)
                        detectors.accept(ArrayList())
                    }
                }
                .addTo(compositeDisposable)

            methods
                .filter { it.size == 1 && numberDetailOutput.hasValue() && numberDetailOutput.value.method == "" }
                .map { it.first() }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<String, BogieNumberDetail, String> { method, _ ->
                        method
                    }
                )
                .subscribeOn(Schedulers.io())
                .subscribe(this::method)
                .addTo(compositeDisposable)

            defectTypeChanged
                .filter { !it.needControlZone }
                .subscribe {
                    controlZone(null)
                }
                .addTo(compositeDisposable)

            defectTypeChanged
                .map { it.needControlZone }
                .subscribe(controlZoneEnabled)
                .addTo(compositeDisposable)

            reloadDefectInfo
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, BogieNumberDetail, NumberDetailDefectMoment> { _, nd ->
                        nd.defectMoment
                    }
                )
                .subscribe {
                    when (it) {
                        NumberDetailDefectMoment.DEFECATION -> loadDefecationInfo()
                        NumberDetailDefectMoment.VISUAL -> loadVisualInfo()
                        NumberDetailDefectMoment.NDT -> loadNdtInfo()
                        else -> {}
                    }
                }
                .addTo(compositeDisposable)

            controlResultChanged
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<ControlResult, BogieNumberDetail, BogieNumberDetail> { _, nd ->
                        nd
                    }
                )
                .subscribe { it.createDefectInfo() }
                .addTo(compositeDisposable)

            val saveDetailValidation = attemptSave
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, BogieNumberDetail, Pair<Boolean, List<InvalidField>>> { _, d ->
                        d.validate()
                    }
                )
                .subscribeOn(Schedulers.io())

            saveDetailValidation
                .map { it.second }
                .subscribe(invalidForm)
                .addTo(compositeDisposable)

            saveDetailValidation
                .filter { it.first }
                .map { Unit }
                .withLatestFrom(
                    numberDetailOutput,
                    defectInfoChanged,
                    Function3<Unit, BogieNumberDetail, Boolean, String> { _, nd, changed ->
                        if (changed) {
                            nd.createDefectInfo()
                        }
                        nd.defectInfo
                    }
                )
                .subscribe(showSavingDialog)
                .addTo(compositeDisposable)

            acceptSaving
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, BogieNumberDetail, Pair<Int, String>> { _, nd ->
                        Pair(nd.partId, nd.number)
                    }
                )
                .filter { !detailSaving.value }
                .doOnNext { detailSaving.accept(true) }
                .switchMap {
                    fetchAlreadyAddedDetail(it.first, it.second).toResultApi(noInternetConnection, errorHandler)
                }
                .doOnEach { detailSaving.accept(false) }
                .subscribe {
                    if (it.isSuccess) {
                        if (it.data!!.status) {
                            detailAlreadyAdded.accept(it.data)
                        } else {
                            saveDetail.accept(Unit)
                        }
                    } else if (it.isError) {
                        detailSavingError.accept(it.error)
                    }
                }
                .addTo(compositeDisposable)

            acceptSavingAddedDetail
                .subscribe { saveDetail.accept(Unit) }
                .addTo(compositeDisposable)

            val saveDetailNotification = saveDetail
                .filter { !detailSaving.value }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Unit, BogieNumberDetail, BogieNumberDetail> { _, nd ->
                        nd
                    }
                )
                .switchMap {
                    addDetail(it)
                }
                .share()

            saveDetailNotification
                .filter { it.isOnError }
                .map { it.error }
                .subscribe {
                    detailSavingError.accept(errorHandler.getError(it))
                }
                .addTo(compositeDisposable)

            saveDetailNotification
                .filter { it.isOnNext }
                .map { it.value }
                .withLatestFrom(
                    numberDetailOutput,
                    BiFunction<Int, BogieNumberDetail, BogieNumberDetailListItem> { id, nd ->
                        nd.toListItem(id) as BogieNumberDetailListItem
                    }
                )
                .subscribe {
                    val json = Gson().toJson(it)
                    val intent = Intent()
                    intent.putExtra(PaginationActivity.RESULT_ITEM_KEY, json)
                    detailSaveSuccess.accept(intent)
                }
                .addTo(compositeDisposable)
        }

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.clear()
        }

        private fun fetchDetail(id: Int) =
            interactors.getBogieById(id)
                .subscribeOn(Schedulers.io())

        private fun fetchControlResults(partId: Int) =
            interactors.getControlResults(partId)
                .subscribeOn(Schedulers.io())

        private fun fetchBogieModels(partId: Int) =
            interactors.getBogieModels(partId)
                .subscribeOn(Schedulers.io())

        private fun fetchFactories(partId: Int, bogieModelId: Int) =
            interactors.getBogieModelFactories(partId, bogieModelId)
                .subscribeOn(Schedulers.io())

        private fun fetchYears(factoryId: Int) =
            interactors.getYears(factoryId)
                .subscribeOn(Schedulers.io())

        private fun fetchDefectTypes(partId: Int, defectMoment: DefectMoment) =
            interactors.getDefectTypes(partId, defectMoment)
                .subscribeOn(Schedulers.io())

        private fun fetchBogieControlZones(
            partId: Int,
            defectMoment: DefectMoment,
            bogieModelId: Int
        ) =
            interactors.getBogieControlZones(partId, defectMoment, bogieModelId)
                .subscribeOn(Schedulers.io())

        private fun fetchSteals() =
            interactors.getSteals()
                .subscribeOn(Schedulers.io())

        private fun fetchControlZones(partId: Int, defectMoment: DefectMoment) =
            interactors.getControlZones(partId, defectMoment)
                .subscribeOn(Schedulers.io())

        private fun fetchRepairDefectTypes(partId: Int) =
            interactors.getRepairDefectTypes(partId)
                .subscribeOn(Schedulers.io())

        private fun fetchRepairControlZones(partId: Int) =
            interactors.getRepairControlZones(partId)
                .subscribeOn(Schedulers.io())

        private fun fetchMethods(partId: Int) =
            interactors.getMethods(partId)
                .subscribeOn(Schedulers.io())

        private fun fetchDetectors(method: String) =
            interactors.getDetectors(method)
                .subscribeOn(Schedulers.io())

        private fun fetchAlreadyAddedDetail(partId: Int, number: String) =
            interactors.checkAlreadyAddedDetail(partId, number)
                .subscribeOn(Schedulers.io())

        private fun addDetail(bogieNumberDetail: BogieNumberDetail) =
            interactors.addBogieNumberDetail(bogieNumberDetail)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { detailSaving.accept(true) }
                .doOnTerminate { detailSaving.accept(false) }
                .materialize()

        private fun cacheDetail(detail: BogieNumberDetail) =
            interactors.cacheBogieDetail(detail)

        //region Inputs

        override fun loadDetail() = loadDetail.accept(Unit)

        override fun controlDate(value: Date) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.controlDate(value) as BogieNumberDetail)
            }
        }

        override fun bogieModel(value: BogieModel?) {
            if (numberDetailOutput.hasValue()) {
                bogieModelChanged.accept(value)
                numberDetailInput.accept(numberDetailOutput.value.bogieModel(value))
            }
        }

        override fun factory(value: Factory?) {
            if (numberDetailOutput.hasValue()) {
                if (value == null) {
                    factoryChanged.accept(Factory.default())
                } else {
                    factoryChanged.accept(value)
                }
                numberDetailInput.accept(numberDetailOutput.value.factory(value) as BogieNumberDetail)
            }
        }

        override fun factoryYear(value: FactoryYear?) {
            if (value?.value == 0) return
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.factoryYear(value) as BogieNumberDetail)
            }
        }

        override fun isNewDetail(value: Boolean) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.isNewDetail(value) as BogieNumberDetail)
            }
        }

        override fun number(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.number(value) as BogieNumberDetail)
            }
        }

        override fun isFromServicePoint(value: Boolean) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.isFromServicePoint(value) as BogieNumberDetail)
            }
        }

        override fun servicePointCompany(value: CompanyEnum) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.servicePointCompany(value) as BogieNumberDetail)
            }
        }

        override fun servicePointFault(value: FaultEnum) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.servicePointFault(value) as BogieNumberDetail)
            }
        }

        override fun noRejectionCriteria(value: Boolean) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.noRejectionCriteria(value) as BogieNumberDetail)
            }
        }

        override fun controlResult(value: ControlResult) {
            if (numberDetailOutput.hasValue()) {
                controlResultChanged.accept(value)
                defectInfoChanged.accept(true)
                numberDetailInput.accept(numberDetailOutput.value.controlResult(value) as BogieNumberDetail)
            }
        }

        override fun refreshBogieModelDependData() =
            bogieModelChanged.accept(numberDetailOutput.value.bogieModel)

        override fun refreshFactoryDependData() =
            factoryChanged.accept(numberDetailOutput.value.factory)

        override fun clearCache() {
            interactors.clearBogiePreferenceCache()
        }

        override fun loadRepairInfo() = loadRepairInfo.accept(Unit)

        override fun repairDefectType(value: DefectType) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectType(value) as BogieNumberDetail)
            }
        }

        override fun repairControlZone(value: ControlZone) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.controlZone(value) as BogieNumberDetail)
            }
        }

        override fun repairDescription(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.repairDescription(value) as BogieNumberDetail)
            }
        }

        override fun defectMoment(value: NumberDetailDefectMoment) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectMoment(value) as BogieNumberDetail)
            }
        }

        override fun loadDefecationInfo() = loadDefecationInfo.accept(Unit)

        override fun defectType(value: DefectType) {
            if (numberDetailOutput.hasValue()) {
                defectTypeChanged.accept(value)
                numberDetailInput.accept(numberDetailOutput.value.defectType(value) as BogieNumberDetail)
            }
        }

        override fun controlZone(value: ControlZone?) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.controlZone(value) as BogieNumberDetail)
            }
        }

        override fun steal(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.steal(value) as BogieNumberDetail)
            }
        }

        override fun owner(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.owner(value) as BogieNumberDetail)
            }
        }

        override fun defectDescription(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectDescription(value) as BogieNumberDetail)
            }
        }

        override fun loadVisualInfo() = loadVisualInfo.accept(Unit)

        override fun defectLength(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectLength(value) as BogieNumberDetail)
            }
        }

        override fun defectDepth(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectDepth(value) as BogieNumberDetail)
            }
        }

        override fun defectDiameter(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectDiameter(value) as BogieNumberDetail)
            }
        }

        override fun loadNdtInfo() = loadNdtInfo.accept(Unit)

        override fun method(value: String) {
            if (numberDetailOutput.hasValue()) {
                methodChanged.accept(value)
                numberDetailInput.accept(numberDetailOutput.value.method(value) as BogieNumberDetail)
            }
        }

        override fun defectSize(value: String) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.defectSize(value) as BogieNumberDetail)
            }
        }

        override fun detector(value: Detector) {
            if (numberDetailOutput.hasValue()) {
                numberDetailInput.accept(numberDetailOutput.value.detector(value) as BogieNumberDetail)
            }
        }

        override fun attemptSave() = attemptSave.accept(Unit)

        override fun reloadDefectInfo() = reloadDefectInfo.accept(Unit)

        override fun defectInfoChanged() = defectInfoChanged.accept(true)

        override fun acceptSaving() = acceptSaving.accept(Unit)

        override fun acceptSavingAddedDetail() = acceptSavingAddedDetail.accept(Unit)

        //endregion

        //region Outputs

        override fun title(): Observable<String> = title.hide()

        override fun numberDetail(): Observable<BogieNumberDetail> = numberDetailOutput.hide()

        override fun fetchingDetail(): Observable<Boolean> = fetchingDetail.hide()

        override fun fetchingBogieModels(): Observable<Boolean> = fetchingBogieModels.hide()

        override fun fetchingFactoryDependData(): Observable<Boolean> =
            fetchingFactoryDependData.hide()

        override fun fetchingBogieModelDependedData(): Observable<Boolean> =
            fetchingBogieModelDependedData.hide()

        override fun loadDetailError(): Observable<Error> = loadDetailError.hide()

        override fun loadBogieModelDependsError(): Observable<Error> =
            loadBogieModelDependsError.hide()

        override fun loadFactoryDependsError(): Observable<Error> = loadFactoryDependsError.hide()

        override fun bogieModels(): Observable<List<BogieModel>> = bogieModels.hide()

        override fun factories(): Observable<List<Factory>> = factories.hide()

        override fun factoryYears(): Observable<List<FactoryYear>> = factoryYears.hide()

        override fun controlResults(): Observable<List<ControlResult>> = controlResults.hide()

        override fun noInternetConnection(): Observable<Boolean> = noInternetConnection.hide()

        override fun fetchingRepairInfo(): Observable<Boolean> = fetchingRepairInfo.hide()

        override fun repairDefectTypes(): Observable<List<DefectType>> = repairDefectTypes.hide()

        override fun repairControlZones(): Observable<List<ControlZone>> = repairControlZones.hide()

        override fun controlResultChanged(): Observable<ControlResult> = controlResultChanged.hide()

        override fun repairInfoError(): Observable<Error> = repairInfoError.hide()

        override fun fetchingDefecationInfo(): Observable<Boolean> = fetchingDefecationInfo.hide()

        override fun defectTypes(): Observable<List<DefectType>> = defectTypes.hide()

        override fun controlZones(): Observable<List<ControlZone>> = controlZones.hide()

        override fun steals(): Observable<List<String>> = steals.hide()

        override fun defecationInfoError(): Observable<Error> = defecationInfoError.hide()

        override fun fetchingVisualInfo(): Observable<Boolean> = fetchingVisualInfo.hide()

        override fun visualInfoError(): Observable<Error> = visualInfoError.hide()

        override fun fetchingNdtInfo(): Observable<Boolean> = fetchingNdtInfo.hide()

        override fun methods(): Observable<List<String>> = methods.hide()

        override fun detectors(): Observable<List<Detector>> = detectors.hide()

        override fun fetchingMethodDependData(): Observable<Boolean> =
            fetchingMethodDependData.hide()

        override fun ndtInfoError(): Observable<Error> = ndtInfoError.hide()

        override fun controlZoneEnabled(): Observable<Boolean> = controlZoneEnabled.hide()

        override fun invalidForm(): Observable<List<InvalidField>> = invalidForm.hide()

        override fun showSavingDialog(): Observable<String> = showSavingDialog.hide()

        override fun detailSaving(): Observable<Boolean> = detailSaving.hide()

        override fun detailAlreadyAdded(): Observable<AddedDetail> = detailAlreadyAdded.hide()

        override fun detailSavingError(): Observable<Error> = detailSavingError.hide()

        override fun detailSaveSuccess(): Observable<Intent> = detailSaveSuccess.hide()

        //endregion
    }
}






































