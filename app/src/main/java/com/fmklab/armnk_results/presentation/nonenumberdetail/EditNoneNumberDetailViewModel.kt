package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.content.Intent
import com.fmklab.armnk_result.domain.*
import com.fmklab.armnk_results.framework.CurrentUser
import com.fmklab.armnk_results.framework.NoneNumberDetailInputs
import com.fmklab.armnk_results.framework.Result
import com.fmklab.armnk_results.framework.extensions.toResultApi
import com.fmklab.armnk_results.framework.interactors.NoneNumberDetailInteractors
import com.fmklab.armnk_results.presentation.base.BaseViewModel
import com.fmklab.armnk_results.presentation.common.PaginationActivity
import com.fmklab.armnk_results.presentation.controldetail.ControlDetailActivity
import com.google.gson.Gson
import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

interface EditNoneNumberDetailViewModel {

    interface Inputs {

        fun loadDetail()

        fun controlDate(value: String)

        fun method(value: String)

        fun controlZone(value: ControlZone)

        fun detector(value: Detector)

        fun allChecked(value: Int)

        fun defecationChecked(value: Int)

        fun visualChecked(value: Int)

        fun ndtChecked(value: Int)

        fun save()

        fun clearCache()

        fun acceptSaving()
    }

    interface Outputs {

        fun title(): Observable<String>

        fun noneNumberDetail(): Observable<NoneNumberDetail>

        fun fetchingDetail(): Observable<Boolean>

        fun fetchingMethodDependData(): Observable<Boolean>

        fun error(): Observable<Error>

        fun methods(): Observable<List<String>>

        fun controlZones(): Observable<List<ControlZone>>

        fun detectors(): Observable<List<Detector>>

        fun noInternetConnectionError(): Observable<Boolean>

        fun invalidForm(): Observable<List<NoneNumberDetail.InvalidField>>

        fun savingDetail(): Observable<Boolean>

        fun saveDetailError(): Observable<Error>

        fun detailSaved(): Observable<Intent>

        fun showSavingDialog(): Observable<Unit>
    }

    class ViewModel(
        private val interactors: NoneNumberDetailInteractors,
        private val errorHandler: ErrorHandler,
        private val currentUser: CurrentUser
    ) : BaseViewModel(), Inputs,
        Outputs {

        private val compositeDisposable = CompositeDisposable()

        private val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        private val noneNumberDetailInput = PublishRelay.create<NoneNumberDetail>()
        private val loadDetail = PublishRelay.create<Unit>()
        private val save = PublishRelay.create<Unit>()
        private val methodChanged = PublishRelay.create<String>()
        private val acceptSaving = PublishRelay.create<Unit>()

        private val title = BehaviorRelay.create<String>()
        private val fetchingDetail = BehaviorRelay.createDefault(false)
        private val fetchingMethodDependData = BehaviorRelay.createDefault(false)
        private val error = PublishRelay.create<Error>()
        private val noneNumberDetailOutput = BehaviorRelay.create<NoneNumberDetail>()
        private val methods = BehaviorRelay.create<List<String>>()
        private val controlZones = BehaviorRelay.create<List<ControlZone>>()
        private val detectors = BehaviorRelay.create<List<Detector>>()
        private val noInternetConnectionError = BehaviorRelay.createDefault(false)
        private val invalidForm = PublishRelay.create<List<NoneNumberDetail.InvalidField>>()
        private val savingDetail = BehaviorRelay.createDefault(false)
        private val saveDetailError = PublishRelay.create<Error>()
        private val detailSaved = PublishRelay.create<Intent>()
        private val showSavingDialog = PublishRelay.create<Unit>()

        val inputs: Inputs = this
        val outputs: Outputs = this

        init {
            val selectedDetailId = intent()
                .subscribeOn(Schedulers.io())
                .map { it.extras?.getInt(PaginationActivity.SELECTED_DETAIL_ID_TAG)!! }

            val selectedControlDetail = intent()
                .subscribeOn(Schedulers.io())
                .map {
                    val json =
                        it.extras?.getString(ControlDetailActivity.SELECTED_CONTROL_DETAIL_TAG)!!
                    Gson().fromJson(json, ControlDetail::class.java)
                }

            intent()
                .subscribeOn(Schedulers.io())
                .map {
                    val json =
                        it.extras?.getString(ControlDetailActivity.SELECTED_CONTROL_DETAIL_TAG)!!
                    val detail = Gson().fromJson(json, ControlDetail::class.java)
                    detail.name
                }
                .subscribe(title)
                .addTo(compositeDisposable)

            loadDetail
                .withLatestFrom(
                    selectedControlDetail,
                    selectedDetailId,
                    Function3<Unit, ControlDetail, Int, Pair<Int, Int>> { _, cd: ControlDetail, id: Int ->
                        Pair(cd.id, id)
                    }
                )
                .filter { !fetchingDetail.value }
                .doOnNext { fetchingDetail.accept(true) }
                .switchMap {
                    fetchDetail(it.second).toResultApi(noInternetConnectionError, errorHandler)
                        .switchMap { nnd ->
                            if (nnd.isError && nnd.error != Error.NotFoundRemote) {
                                Observable.just(
                                    NoneNumberDetailInputs(
                                        nnd,
                                        Result.error(nnd.error!!),
                                        Result.error(nnd.error),
                                        Result.error(nnd.error)
                                    )
                                )
                            } else {
                                Observable.zip(
                                    fetchMethods(it.first).toResultApi(
                                        noInternetConnectionError,
                                        errorHandler
                                    ),
                                    fetchControlZones(
                                        it.first,
                                        if (nnd.data?.method == "") "base" else nnd.data?.method
                                            ?: "base"
                                    ).toResultApi(noInternetConnectionError, errorHandler),
                                    fetchDetectors(
                                        if (nnd.data?.method == "") "base" else nnd.data?.method
                                            ?: "base"
                                    ).toResultApi(
                                        noInternetConnectionError,
                                        errorHandler
                                    ),
                                    Function3 { methods, controlZones, detectors ->
                                        if (nnd.isError && nnd.error == Error.NotFoundRemote) {
                                            NoneNumberDetailInputs(
                                                Result.success(
                                                    NoneNumberDetail.default(it.first)
                                                        .spec(currentUser.getUser().toString())
                                                ),
                                                methods,
                                                controlZones,
                                                detectors
                                            )
                                        } else {
                                            NoneNumberDetailInputs(
                                                nnd,
                                                methods,
                                                controlZones,
                                                detectors
                                            )
                                        }
                                    }
                                )
                            }
                        }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach { fetchingDetail.accept(false) }
                .subscribe {
                    if (it.isSuccess()) {
                        noneNumberDetailInput.accept(it.detail.data)
                        methods.accept(it.methods.data)
                        controlZones.accept(it.controlZones.data)
                        detectors.accept(it.detectors.data)
                    } else {
                        error.accept(it.getError())
                    }
                }
                .addTo(compositeDisposable)

            methods
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && noneNumberDetailOutput.hasValue() && noneNumberDetailOutput.value.method == "" }
                .map { it.first() }
                .withLatestFrom(
                    noneNumberDetailOutput,
                    BiFunction<String, NoneNumberDetail, String> { method: String, _ ->
                        method
                    }
                )
                .subscribe(this::method)
                .addTo(compositeDisposable)

            controlZones
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && noneNumberDetailOutput.hasValue() && noneNumberDetailOutput.value.controlZone == null }
                .map { it.first() }
                .withLatestFrom(
                    noneNumberDetailOutput,
                    BiFunction<ControlZone, NoneNumberDetail, ControlZone> { cz: ControlZone, _ ->
                        cz
                    }
                )
                .subscribe(this::controlZone)
                .addTo(compositeDisposable)

            detectors
                .subscribeOn(Schedulers.io())
                .filter { it.size == 1 && noneNumberDetailOutput.hasValue() && noneNumberDetailOutput.value.detector == null }
                .map { it.first() }
                .withLatestFrom(
                    noneNumberDetailOutput,
                    BiFunction<Detector, NoneNumberDetail, Detector> { detector: Detector, _ ->
                        detector
                    }
                )
                .subscribe(this::detector)
                .addTo(compositeDisposable)

            noneNumberDetailInput
                .flatMap {
                    interactors.cacheDetail(it)
                    Observable.just(it)
                }
                .subscribe(noneNumberDetailOutput)
                .addTo(compositeDisposable)

            val saveDetailValidation = save
                .filter { !savingDetail.value }
                .withLatestFrom(
                    noneNumberDetailInput,
                    BiFunction<Unit, NoneNumberDetail, Pair<Boolean, List<NoneNumberDetail.InvalidField>>> { _, d: NoneNumberDetail ->
                        d.validate()
                    }
                )
                .subscribeOn(Schedulers.io())

            saveDetailValidation
                .map { it.second }
                .subscribe(invalidForm)
                .addTo(compositeDisposable)

            saveDetailValidation
                .filter { it.first }
                .subscribe { showSavingDialog.accept(Unit) }
                .addTo(compositeDisposable)

            val saveDetailNotification = acceptSaving
                .filter { !savingDetail.value }
                .withLatestFrom(
                    noneNumberDetailOutput,
                    BiFunction<Unit, NoneNumberDetail, NoneNumberDetail> { _, d: NoneNumberDetail ->
                        d
                    }
                )
                .switchMap { saveDetail(it) }
                .share()


            saveDetailNotification
                .filter { it.isOnError }
                .map { it.error }
                .subscribe {
                    saveDetailError.accept(errorHandler.getError(it))
                }
                .addTo(compositeDisposable)

            saveDetailNotification
                .filter { it.isOnNext }
                .map { it.value }
                .subscribe {
                    val json = Gson().toJson(noneNumberDetailOutput.value.copyWithNewId(it))
                    val intent = Intent()
                    intent.putExtra(PaginationActivity.RESULT_ITEM_KEY, json)
                    detailSaved.accept(intent)
                }
                .addTo(compositeDisposable)

            methodChanged
                .filter { !fetchingMethodDependData.value && it.isNotBlank() }
                .withLatestFrom(
                    selectedControlDetail,
                    BiFunction<String, ControlDetail, Pair<String, ControlDetail>> { method: String, controlDetail: ControlDetail ->
                        Pair(method, controlDetail)
                    }
                )
                .doOnNext { fetchingMethodDependData.accept(true) }
                .switchMap {
                    Observable.zip(
                        fetchControlZones(it.second.id, it.first).toResultApi(
                            noInternetConnectionError,
                            errorHandler
                        ),
                        fetchDetectors(it.first).toResultApi(
                            noInternetConnectionError,
                            errorHandler
                        ),
                        BiFunction { controlZones: Result<List<ControlZone>>, detectors: Result<List<Detector>> ->
                            Pair(controlZones, detectors)
                        }
                    )
                }
                //.observeOn(AndroidSchedulers.mainThread())
                .doOnEach { fetchingMethodDependData.accept(false) }
                .subscribe {
                    when {
                        it.first.isError -> {
                            error.accept(it.first.error)
                            controlZones.accept(ArrayList())
                            detectors.accept(ArrayList())
                        }
                        it.second.isError -> {
                            error.accept(it.second.error)
                            controlZones.accept(ArrayList())
                            detectors.accept(ArrayList())
                        }
                        else -> {
                            controlZones.accept(it.first.data)
                            detectors.accept(it.second.data)
                        }
                    }
                }
                .addTo(compositeDisposable)
        }

        override fun onCleared() {
            super.onCleared()

            compositeDisposable.dispose()
            interactors.clearRequestCache()
        }

        private fun fetchDetail(id: Int) =
            interactors.getById(id)

        private fun fetchMethods(partId: Int) =
            interactors.getMethods(partId)

        private fun fetchControlZones(partId: Int, method: String) =
            interactors.getControlZonesByMethod(partId, method)

        private fun fetchDetectors(method: String) =
            interactors.getDetectors(method)

        private fun saveDetail(noneNumberDetail: NoneNumberDetail) =
            interactors.insert(noneNumberDetail)
                .doOnSubscribe { savingDetail.accept(true) }
                .doOnTerminate { savingDetail.accept(false) }
                .materialize()


        override fun loadDetail() = loadDetail.accept(Unit)

        override fun controlDate(value: String) {
            noneNumberDetailInput.accept(
                noneNumberDetailOutput.value.controlDate(
                    simpleDateFormat.parse(
                        value
                    )!!
                )
            )
        }

        override fun method(value: String) {
            methodChanged.accept(value)
            noneNumberDetailInput.accept(noneNumberDetailOutput.value.method(value))
        }

        override fun controlZone(value: ControlZone) {
            noneNumberDetailInput.accept(noneNumberDetailOutput.value.controlZone(value))
        }

        override fun detector(value: Detector) {
            noneNumberDetailInput.accept(noneNumberDetailOutput.value.detector(value))
        }

        override fun allChecked(value: Int) {
            if (noneNumberDetailOutput.hasValue()) {
                noneNumberDetailInput.accept(noneNumberDetailOutput.value.allChecked(value))
            }
        }

        override fun defecationChecked(value: Int) {
            if (noneNumberDetailOutput.hasValue()) {
                noneNumberDetailInput.accept(noneNumberDetailOutput.value.defecationChecked(value))
            }
        }

        override fun visualChecked(value: Int) {
            if (noneNumberDetailOutput.hasValue()) {
                noneNumberDetailInput.accept(noneNumberDetailOutput.value.visualChecked(value))
            }
        }

        override fun ndtChecked(value: Int) {
            if (noneNumberDetailOutput.hasValue()) {
                noneNumberDetailInput.accept(noneNumberDetailOutput.value.ndtChecked(value))
            }
        }

        override fun save() = save.accept(Unit)

        override fun clearCache() {
            interactors.clearPreferencesCache()
        }

        override fun acceptSaving() = acceptSaving.accept(Unit)

        override fun title(): Observable<String> = title.hide()

        override fun noneNumberDetail(): Observable<NoneNumberDetail> =
            noneNumberDetailOutput.hide()

        override fun fetchingDetail(): Observable<Boolean> = fetchingDetail.hide()

        override fun fetchingMethodDependData(): Observable<Boolean> =
            fetchingMethodDependData.hide()

        override fun error(): Observable<Error> = error.hide()

        override fun methods(): Observable<List<String>> = methods.hide()

        override fun controlZones(): Observable<List<ControlZone>> = controlZones.hide()

        override fun detectors(): Observable<List<Detector>> = detectors.hide()

        override fun noInternetConnectionError(): Observable<Boolean> =
            noInternetConnectionError.hide()

        override fun invalidForm(): Observable<List<NoneNumberDetail.InvalidField>> =
            invalidForm.hide()

        override fun savingDetail(): Observable<Boolean> = savingDetail.hide()

        override fun saveDetailError(): Observable<Error> = saveDetailError.hide()

        override fun detailSaved(): Observable<Intent> = detailSaved.hide()

        override fun showSavingDialog(): Observable<Unit> = showSavingDialog.hide()
    }
}









































