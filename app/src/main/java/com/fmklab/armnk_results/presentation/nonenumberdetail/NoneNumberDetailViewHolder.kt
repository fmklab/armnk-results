package com.fmklab.armnk_results.presentation.nonenumberdetail

import android.view.View
import android.widget.Button
import com.bumptech.glide.Glide
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.common.DetailPagingViewHolder
import com.fmklab.armnk_results.presentation.common.PagingItem
import kotlinx.android.synthetic.main.small_detail_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class NoneNumberDetailViewHolder(itemView: View) : DetailPagingViewHolder<NoneNumberDetail>(itemView) {

    override fun bind(item: PagingItem<NoneNumberDetail>) {
        super.bind(item)

        if (item.value == null) {
            return
        }
        with(item.value) {
            if (editStatus) {
                Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.status_unlock)
                    .into(itemView.statusImageView)
            } else {
                Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.status_lock)
                    .into(itemView.statusImageView)
            }
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            itemView.controlDateTextView.text = simpleDateFormat.format(controlDate)
            itemView.allCheckedValueTextView.text = allChecked.toString()
            itemView.defecationCheckedValueTextView.text = defecationChecked.toString()
            itemView.visualCheckedValueTextView.text = visualChecked.toString()
            itemView.ndtCheckedValueTextView.text = ndtChecked.toString()
            when (isMobileApp) {
                true -> Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_smartphone_24)
                    .into(itemView.sourceImageView)
                false -> Glide.with(itemView)
                    .asDrawable()
                    .load(R.drawable.ic_baseline_computer_24)
                    .into(itemView.sourceImageView)
            }
            val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
            itemView.createDateTextView.text = createDateFormat.format(createDate)
        }
    }
}








































