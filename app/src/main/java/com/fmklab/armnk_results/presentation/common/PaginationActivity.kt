package com.fmklab.armnk_results.presentation.common

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.fmklab.armnk_result.domain.Detail
import com.fmklab.armnk_result.domain.Error
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.RecyclerViewPagination
import com.fmklab.armnk_results.framework.extensions.setGone
import com.fmklab.armnk_results.framework.extensions.setInvisible
import com.fmklab.armnk_results.framework.extensions.setVisible
import com.fmklab.armnk_results.presentation.base.BaseActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jakewharton.rxbinding4.appcompat.queryTextChanges
import com.jakewharton.rxbinding4.swiperefreshlayout.refreshes
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.queryTextChanges
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import java.util.concurrent.TimeUnit

abstract class PaginationActivity<T : Detail> : BaseActivity<PaginationViewModel.ViewModel<T>>() {

    private val compositeDisposable = CompositeDisposable()

    protected lateinit var toolbar: Toolbar
    protected lateinit var recyclerView: RecyclerView
    protected lateinit var addNewDetailFab: FloatingActionButton
    protected lateinit var swipeRefreshLayout: SwipeRefreshLayout
    protected lateinit var emptyDataTextView: TextView
    private lateinit var recyclerViewLayoutManager: LinearLayoutManager

    private lateinit var recyclerViewPagination: RecyclerViewPagination
    protected abstract val recyclerViewAdapter: DetailsPagingAdapter<T>


    companion object {
        private const val DETAILS_FRAGMENT_TAG = "DetailsFragmentTag"
        const val RESULT_ITEM_KEY = "ResultItemKey"
        const val SELECTED_DETAIL_ID_TAG = "SelectedDetailIdTag"
        const val SELECTED_CONTROL_DETAIL_TAG = "SelectedControlDetailTag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupUI()
        setupPagination()
        setupData()
        setupBinding()
        setupListeners()

        viewModel.inputs.loadFirstPage()
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
        recyclerViewPagination.stop()
    }

    override fun onPause() {
        (supportFragmentManager.fragments.firstOrNull { frg -> frg.tag == DETAILS_FRAGMENT_TAG } as? BottomSheetDialogFragment)?.dismiss()

        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchMenuItem = menu?.findItem(R.id.search_button)
        val searchView = searchMenuItem?.actionView as androidx.appcompat.widget.SearchView
        searchView.queryHint = getString(R.string.search_by_number)
        searchView.queryTextChanges()
            .skipInitialValue()
            .debounce(300, TimeUnit.MILLISECONDS)
            .map(CharSequence::toString)
            .subscribe {
                if (it.isBlank()) {
                    viewModel.inputs.loadFirstPage()
                } else {
                    viewModel.inputs.search(it)
                }
            }
            .addTo(compositeDisposable)

        searchMenuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                recyclerViewPagination.stop()
                swipeRefreshLayout.isEnabled = false
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                recyclerViewPagination.start()
                swipeRefreshLayout.isEnabled = true
                viewModel.inputs.loadFirstPage()
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    protected open fun setupUI() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerViewLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = recyclerViewLayoutManager
        recyclerView.setHasFixedSize(true)
    }

    protected open fun setupData() {
        recyclerView.adapter = recyclerViewAdapter
    }

    protected fun setupPagination() {
        recyclerViewPagination = RecyclerViewPagination(
            recyclerView,
            viewModel.inputs::nextPage,
            viewModel.outputs.fetchingDetails()
        )
    }

    protected open fun setupListeners() {
        recyclerViewAdapter.onRefreshClickListener =
            object : ErrorPagingViewHolder.OnRefreshClickListener {
                override fun onRefreshClick() {
                    recyclerViewPagination.start()
                    recyclerViewAdapter.hideError()
                    viewModel.inputs.refresh()
                }
            }

        recyclerViewAdapter.onMoreInfoClickListener =
            object : DetailPagingViewHolder.OnMoreInfoClickListener {
                override fun onMoreInfoClick(position: Int) {
                    val item = recyclerViewAdapter.getItem(position)
                    if (item != null) {
                        viewModel.inputs.selectDetail(item)
                    }
                }
            }
    }

    protected open fun setupBinding() {
        viewModel.outputs.fetchingDetails()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) {
                    recyclerViewAdapter.showLoading()
                    addNewDetailFab.hide()
                } else {
                    addNewDetailFab.show()
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.details()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                swipeRefreshLayout.isRefreshing = false
                if (it.isEmpty() && recyclerViewAdapter.valueItemCount() == 0) {
                    showEmptyList()
                } else {
                    hideEmptyList()
                    recyclerViewAdapter.add(it)
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.lastPageLoaded()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                recyclerViewPagination.stop()
            }
            .addTo(compositeDisposable)

        viewModel.outputs.error()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    Error.NoInternetConnection -> toast("Ошибка соединения. Проверьте подключение к сети")
                    Error.BadRequest -> toast("Внутренняя ошибка сервера")
                    Error.SocketTimeout -> toast("Время ожидания ответа от сервера истекло")
                    is Error.Unknown -> toast("Непредвиденная ошибка ${it.message}")
                }
                swipeRefreshLayout.isRefreshing = false
                recyclerViewPagination.stop()
                recyclerViewAdapter.showError("Ошибка загрузки")
            }
            .addTo(compositeDisposable)

        viewModel.outputs.showMoreInfo()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (!supportFragmentManager.fragments.any { frg -> frg.tag == DETAILS_FRAGMENT_TAG }) {
                    val fragment = createDetailsFragment()
                    fragment.show(supportFragmentManager, DETAILS_FRAGMENT_TAG)
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.title()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                supportActionBar?.title = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.resultItem()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                recyclerViewAdapter.addOrUpdate(it)
                val firstItemPosition = recyclerViewLayoutManager.findFirstCompletelyVisibleItemPosition()
                if (firstItemPosition == 0) {
                    recyclerView.smoothScrollToPosition(0)
                }
            }
            .addTo(compositeDisposable)

        viewModel.outputs.itemsSearching()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                swipeRefreshLayout.isRefreshing = it
            }
            .addTo(compositeDisposable)

        viewModel.outputs.searchedDetails()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                swipeRefreshLayout.isRefreshing = false
                if (it.isEmpty()) {
                    showEmptyList()
                } else {
                    hideEmptyList()
                    recyclerViewAdapter.add(it)
                }
            }
            .addTo(compositeDisposable)

        swipeRefreshLayout
            .refreshes()
            .subscribe {
                recyclerViewPagination.start()
                viewModel.inputs.loadFirstPage()
            }
            .addTo(compositeDisposable)

        addNewDetailFab
            .clicks()
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe { viewModel.inputs.newDetail() }
            .addTo(compositeDisposable)
    }

    private fun showEmptyList() {
        recyclerView.setInvisible()
        emptyDataTextView.setVisible()
    }

    private fun hideEmptyList() {
        recyclerView.setVisible()
        emptyDataTextView.setGone()
    }

    protected abstract fun createDetailsFragment(): DetailsBottomSheetDialogFragment<T>
}







































