package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmklab.armnk_result.domain.BogieNumberDetailListItem
import com.fmklab.armnk_results.R
import com.fmklab.armnk_results.framework.ActivityRequestCodes
import com.fmklab.armnk_results.presentation.common.DetailsBottomSheetDialogFragment
import com.fmklab.armnk_results.presentation.common.PaginationViewModel
import com.fmklab.armnk_results.presentation.numberdetail.EditNumberDetailActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import kotlinx.android.synthetic.main.fragment_number_detail_bogie_details.*
import kotlinx.android.synthetic.main.fragment_number_detail_details.*
import kotlinx.android.synthetic.main.fragment_number_detail_details.controlDateValueTextView
import kotlinx.android.synthetic.main.fragment_number_detail_details.createDateValueTextView
import kotlinx.android.synthetic.main.fragment_number_detail_details.defectInfoValueTextView
import kotlinx.android.synthetic.main.fragment_number_detail_details.editMessageCoordinatorLayout
import kotlinx.android.synthetic.main.fragment_number_detail_details.editNumberDetailFab
import kotlinx.android.synthetic.main.fragment_number_detail_details.factoryNameValueTextView
import kotlinx.android.synthetic.main.fragment_number_detail_details.factoryYearValueTextView
import kotlinx.android.synthetic.main.fragment_number_detail_details.noRejectionCriteriaImageView
import kotlinx.android.synthetic.main.fragment_number_detail_details.numberDetailDetailsToolbar
import kotlinx.android.synthetic.main.fragment_number_detail_details.resultValueTextView
import kotlinx.android.synthetic.main.fragment_number_detail_details.servicePointImageView
import kotlinx.android.synthetic.main.fragment_number_detail_details.specValueTextView
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class BogieNumberDetailsFragment : DetailsBottomSheetDialogFragment<BogieNumberDetailListItem>() {

    private val compositeDisposable = CompositeDisposable()

    override val viewModel: PaginationViewModel.ViewModel<BogieNumberDetailListItem> by sharedViewModel<BogieNumberDetailViewModel.ViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number_detail_bogie_details, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.clear()
    }

    override fun setupBinding() {
        super.setupBinding()

        viewModel.outputs.showDetailEditor()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.setClass(requireContext(), EditBogieNumberDetailActivity::class.java)
                startActivityForResult(it, ActivityRequestCodes.DETAIL_EDITOR_FLOW)
            }
            .addTo(compositeDisposable)
    }

    override fun bind(detail: BogieNumberDetailListItem) {
        numberDetailDetailsToolbar.title =
            "${getString(R.string.detail_number)} ${detail.number}"
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        controlDateValueTextView.text = simpleDateFormat.format(detail.controlDate)
        bogieModelValueTextView.text = detail.bogieModel
        factoryNameValueTextView.text = detail.factoryName
        factoryYearValueTextView.text = detail.factoryYear.toString()
        resultValueTextView.text = detail.controlResult
        if (detail.isFromServicePoint) {
            servicePointImageView.visibility = View.VISIBLE
        } else {
            servicePointImageView.visibility = View.GONE
        }

        if (detail.noRejectionCriteria) {
            noRejectionCriteriaImageView.visibility = View.VISIBLE
        } else {
            noRejectionCriteriaImageView.visibility = View.GONE
        }
        defectInfoValueTextView.text = detail.defectDescription
        specValueTextView.text = detail.spec

        editNumberDetailFab.setOnClickListener {
            if (detail.editStatus) {
                viewModel.inputs.editDetail(detail)
            } else {
                Snackbar.make(
                    editMessageCoordinatorLayout,
                    getString(R.string.edit_none_number_detail_error),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
        val createDateFormat = SimpleDateFormat("dd.MM.yy' в 'HH:mm", Locale.ROOT)
        createDateValueTextView.text = createDateFormat.format(detail.createDate)
    }
}