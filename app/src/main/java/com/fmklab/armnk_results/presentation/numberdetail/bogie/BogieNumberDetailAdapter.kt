package com.fmklab.armnk_results.presentation.numberdetail.bogie

import android.view.View
import com.fmklab.armnk_result.domain.BogieNumberDetailListItem
import com.fmklab.armnk_results.presentation.base.BaseViewHolder
import com.fmklab.armnk_results.presentation.common.PagingItem
import com.fmklab.armnk_results.presentation.numberdetail.base.BaseNumberDetailAdapter

class BogieNumberDetailAdapter : BaseNumberDetailAdapter<BogieNumberDetailListItem>() {

    override fun createDetailPagingViewHolder(view: View): BaseViewHolder<PagingItem<BogieNumberDetailListItem>> {
        return BogieNumberDetailViewHolder(view)
    }
}