package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.NoneNumberDetail

class NoneNumberDetailRepository(
    private val noneNumberDetailDataSource: NoneNumberDetailDataSource
) {

    fun getAllPaged(partId: Int, page: Int) =
        noneNumberDetailDataSource.getAllPaged(partId, page)

    fun getById(id: Int) = noneNumberDetailDataSource.getById(id)

    fun getControlZonesByMethod(partId: Int, method: String) =
        noneNumberDetailDataSource.getControlZonesByMethod(partId, method)

    fun insert(noneNumberDetail: NoneNumberDetail) =
        noneNumberDetailDataSource.insert(noneNumberDetail)

    fun cacheDetail(noneNumberDetail: NoneNumberDetail) =
        noneNumberDetailDataSource.cacheDetail(noneNumberDetail)

    fun clearPreferencesCache() = noneNumberDetailDataSource.clearPreferencesCache()

    fun clearRequestCache() = noneNumberDetailDataSource.clearRequestCache()
}