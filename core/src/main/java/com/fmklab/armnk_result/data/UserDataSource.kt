package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.User
import io.reactivex.rxjava3.core.Observable

interface UserDataSource {

    fun getUser(): Observable<User>

    fun setUser(user: User)
}