package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.*
import io.reactivex.rxjava3.core.Observable

interface DetailDataSource {

    fun getDetectors(method: String): Observable<List<Detector>>

    fun getMethods(partId: Int): Observable<List<String>>

    fun getFactories(partId: Int): Observable<List<Factory>>

    fun getBogieModelFactories(partId: Int, bogieModelId: Int): Observable<List<Factory>>

    fun getYears(factoryId: Int): Observable<List<FactoryYear>>

    fun getControlResults(partId: Int): Observable<List<ControlResult>>

    fun getDefectTypes(partId: Int, defectMoment: DefectMoment): Observable<List<DefectType>>

    fun getControlZones(partId: Int, defectMoment: DefectMoment): Observable<List<ControlZone>>

    fun getBogieModels(partId: Int): Observable<List<BogieModel>>
}