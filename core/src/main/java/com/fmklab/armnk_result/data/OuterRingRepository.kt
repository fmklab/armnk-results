package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.OuterRing

class OuterRingRepository(private val outerRingDataSource: OuterRingDataSource) {

    fun getAll(partId: Int, page: Int) = outerRingDataSource.getAll(partId, page)

    fun getById(id: Int) = outerRingDataSource.getById(id)

    fun cacheDetail(outerRing: OuterRing) = outerRingDataSource.cacheDetail(outerRing)

    fun clearPreferencesCache() = outerRingDataSource.clearPreferencesCache()

    fun add(outerRing: OuterRing) = outerRingDataSource.add(outerRing)

    fun search(partId: Int, number: String) = outerRingDataSource.search(partId, number)
}