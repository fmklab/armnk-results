package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.BogieNumberDetail
import com.fmklab.armnk_result.domain.DefectMoment
import com.fmklab.armnk_result.domain.NumberDetail

class NumberDetailRepository(private var numberDetailDataSource: NumberDetailDataSource) {

    fun getAllPaged(partId: Int, page: Int) = numberDetailDataSource.getAllPaged(partId, page)

    fun getById(id: Int) = numberDetailDataSource.getById(id)

    fun cacheDetail(numberDetail: NumberDetail) = numberDetailDataSource.cacheDetail(numberDetail)

    fun clearPreferencesCache() = numberDetailDataSource.clearPreferencesCache()

    fun getRepairDefectTypes(partId: Int) = numberDetailDataSource.getRepairDefectTypes(partId)

    fun getRepairControlZones(partId: Int) = numberDetailDataSource.getRepairControlZones(partId)

    fun getSteals() = numberDetailDataSource.getSteals()

    fun checkAlreadyAddedDetail(partId: Int, number: String) =
        numberDetailDataSource.checkAlreadyAddedDetail(partId, number)

    fun add(numberDetail: NumberDetail) = numberDetailDataSource.add(numberDetail)

    fun search(partId: Int, number: String) = numberDetailDataSource.search(partId, number)

    fun getAllBogiePaged(partId: Int, page: Int) =
        numberDetailDataSource.getAllBogiePaged(partId, page)

    fun searchBogie(partId: Int, number: String) =
        numberDetailDataSource.searchBogie(partId, number)

    fun getBogieById(id: Int) = numberDetailDataSource.getBogieById(id)

    fun cacheBogieDetail(numberDetail: BogieNumberDetail) =
        numberDetailDataSource.cacheBogieDetail(numberDetail)

    fun clearBogiePreferenceCache() = numberDetailDataSource.clearBogiePreferenceCache()

    fun getBogieControlZones(partId: Int, defectMoment: DefectMoment, bogieModelId: Int) =
        numberDetailDataSource.getBogieControlZones(partId, defectMoment, bogieModelId)

    fun addBogie(bogieNumberDetail: BogieNumberDetail) = numberDetailDataSource.addBogie(bogieNumberDetail)
}


































