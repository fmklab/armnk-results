package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.ControlZone
import com.fmklab.armnk_result.domain.Detector
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_result.domain.User
import io.reactivex.rxjava3.core.Observable

interface NoneNumberDetailDataSource {

    fun getAllPaged(partId: Int, page: Int): Observable<List<NoneNumberDetail>>

    fun getById(id: Int): Observable<NoneNumberDetail>

    fun cacheDetail(noneNumberDetail: NoneNumberDetail)

    fun getControlZonesByMethod(partId: Int, method: String): Observable<List<ControlZone>>

    fun insert(noneNumberDetail: NoneNumberDetail): Observable<Int>

    fun clearPreferencesCache()

    fun clearRequestCache()
}