package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.*
import io.reactivex.rxjava3.core.Observable

interface NumberDetailDataSource {

    fun getAllPaged(partId: Int, page: Int): Observable<List<NumberDetailListItem>>

    fun getById(id: Int): Observable<NumberDetail>

    fun cacheDetail(numberDetail: NumberDetail)

    fun clearPreferencesCache()

    fun getRepairDefectTypes(partId: Int): Observable<List<DefectType>>

    fun getRepairControlZones(partId: Int): Observable<List<ControlZone>>

    fun getSteals(): Observable<List<String>>

    fun checkAlreadyAddedDetail(partId: Int, number: String): Observable<AddedDetail>

    fun add(numberDetail: NumberDetail): Observable<Int>

    fun search(partId: Int, number: String): Observable<List<NumberDetailListItem>>

    fun getAllBogiePaged(partId: Int, page: Int): Observable<List<BogieNumberDetailListItem>>

    fun searchBogie(partId: Int, number: String): Observable<List<BogieNumberDetailListItem>>

    fun getBogieById(id: Int): Observable<BogieNumberDetail>

    fun cacheBogieDetail(numberDetail: BogieNumberDetail)

    fun clearBogiePreferenceCache()

    fun getBogieControlZones(
        partId: Int,
        defectMoment: DefectMoment,
        bogieModelId: Int
    ): Observable<List<ControlZone>>

    fun addBogie(bogieNumberDetail: BogieNumberDetail): Observable<Int>
}







































