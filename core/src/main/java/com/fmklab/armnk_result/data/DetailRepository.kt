package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.DefectMoment

class DetailRepository(private val detailDataSource: DetailDataSource) {

    fun getDetectors(method: String) = detailDataSource.getDetectors(method)

    fun getMethods(partId: Int) = detailDataSource.getMethods(partId)

    fun getFactories(partId: Int) = detailDataSource.getFactories(partId)

    fun getBogieModelFactories(partId: Int, bogieModelId: Int) =
        detailDataSource.getBogieModelFactories(partId, bogieModelId)

    fun getYears(factoryId: Int) = detailDataSource.getYears(factoryId)

    fun getControlResults(partId: Int) = detailDataSource.getControlResults(partId)

    fun getDefectTypes(partId: Int, defectMoment: DefectMoment) =
        detailDataSource.getDefectTypes(partId, defectMoment)

    fun getControlZones(partId: Int, defectMoment: DefectMoment) =
        detailDataSource.getControlZones(partId, defectMoment)

    fun getBogieModels(partId: Int) = detailDataSource.getBogieModels(partId)
}