package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.User

class UserRepository(private val userDataSource: UserDataSource) {

    fun getUser() = userDataSource.getUser()

    fun setUser(user: User) = userDataSource.setUser(user)
}