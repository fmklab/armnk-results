package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.OuterRing
import com.fmklab.armnk_result.domain.OuterRingListItem
import io.reactivex.rxjava3.core.Observable

interface OuterRingDataSource {

    fun getAll(partId: Int, page: Int): Observable<List<OuterRingListItem>>

    fun getById(id: Int): Observable<OuterRing>

    fun cacheDetail(outerRing: OuterRing)

    fun clearPreferencesCache()

    fun add(outerRing: OuterRing): Observable<Int>

    fun search(partId: Int, number: String): Observable<List<OuterRingListItem>>
}