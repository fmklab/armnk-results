package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.ControlDetail
import io.reactivex.rxjava3.core.Observable

interface ControlDetailDataSource {

    fun getAll(): Observable<List<ControlDetail>>
}