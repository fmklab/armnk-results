package com.fmklab.armnk_result.data

import com.fmklab.armnk_result.domain.ControlDetail

class ControlDetailRepository(private val controlDetailDataSource: ControlDetailDataSource) {

    fun getAll() = controlDetailDataSource.getAll()
}