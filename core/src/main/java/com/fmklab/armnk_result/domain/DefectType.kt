package com.fmklab.armnk_result.domain

data class DefectType (
    val id: Int,
    val name: String,
    val needControlZone: Boolean
) {

    companion object {
        fun default(id: Int) = DefectType(id, "", true)
    }

    override fun toString(): String {
        return name
    }
}