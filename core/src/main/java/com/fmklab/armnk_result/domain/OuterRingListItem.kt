package com.fmklab.armnk_result.domain
import java.util.*

class OuterRingListItem(
    id: Int,
    partId: Int,
    val controlDate: Date,
    val method: String,
    val factoryName: String,
    val factoryYear: FactoryYear,
    val number: String,
    val controlResult: String,
    val defectInfo: String,
    val spec: String,
    val editStatus: Boolean,
    val detector: String,
    val createDate: Date,
    val isMobileApp: Boolean
) : Detail(id, partId)