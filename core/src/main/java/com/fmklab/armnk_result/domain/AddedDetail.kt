package com.fmklab.armnk_result.domain
import java.util.*

data class AddedDetail (
    val number: String?,
    val date: Date?,
    val status: Boolean
)