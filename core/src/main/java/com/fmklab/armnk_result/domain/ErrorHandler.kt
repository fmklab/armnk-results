package com.fmklab.armnk_result.domain

interface ErrorHandler {

    fun getError(throwable: Throwable): Error
}