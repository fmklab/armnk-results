package com.fmklab.armnk_result.domain

data class User(
    val id: Long = 0,
    val name1: String,
    val name2: String,
    val name3: String,
    val company: Company
) {

    companion object {
        val DEFAULT = User(0, "", "", "", Company(0, ""))
    }

    override fun toString(): String {
        return if (name2.trim().isNotEmpty() && name3.trim().isNotEmpty()) {
            "$name1 ${name2[0]}.${name3[0]}."
        } else if (name2.trim().isNotEmpty() && name3.trim().isEmpty()) {
            "$name1 ${name2[0]}."
        } else if (name2.trim().isEmpty() && name3.trim().isNotEmpty()) {
            "$name1 ${name3[0]}."
        } else {
            name1
        }
    }

    fun getCompanyId() = company.id
}