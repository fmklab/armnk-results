package com.fmklab.armnk_result.domain

enum class CompanyEnum(val value: Int, val desc: String) {

    NONE(0, ""),
    VRK1(1, "Компания: ВРК-1"),
    VRK2(2, "Компания: ВРК-2"),
    VRK3(3, "Компания: ВРК-3"),
    OTHER(4, "Компания: Прочие")
}