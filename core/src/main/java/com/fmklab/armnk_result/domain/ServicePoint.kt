package com.fmklab.armnk_result.domain

data class ServicePoint (
    val companyEnum: CompanyEnum,
    val faultEnum: FaultEnum
) {

    companion object {
        fun default(): ServicePoint = ServicePoint(CompanyEnum.NONE, FaultEnum.NONE)
    }

    override fun toString(): String {
        return "${companyEnum.desc} ${faultEnum.desc}"
    }

    fun company(): Int {
        return companyEnum.value
    }

    fun fault(): Int {
        return faultEnum.value
    }
}