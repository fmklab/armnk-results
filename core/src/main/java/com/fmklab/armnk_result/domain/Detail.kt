package com.fmklab.armnk_result.domain

abstract class Detail (
    open val id: Int = 0,
    open val partId: Int
)