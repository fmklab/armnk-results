package com.fmklab.armnk_result.domain

enum class FaultEnum(val value: Int, val desc: String) {

    NONE(0, ""),
    DEPOT(1, "Вина: Депо"),
    FACTORY(2, "Вина: Завод"),
    EXPLOITATION(3, "Вина: Эксплуатация")
}