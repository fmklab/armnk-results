package com.fmklab.armnk_result.domain

import java.util.*
import kotlin.collections.ArrayList

abstract class BaseNumberDetail(id: Int, partId: Int) : Detail(id, partId) {

    var controlDate: Date = Date()
        private set

    var factory: Factory? = null
        private set

    var factoryYear: FactoryYear? = null
        private set

    var isNewDetail: Boolean = false
        private set

    var number: String = ""
        private set

    var isFromServicePoint: Boolean = false
        private set

    var servicePoint: ServicePoint = ServicePoint.default()
        private set

    var noRejectionCriteria: Boolean = false
        private set

    var controlResult: ControlResult? = null
        private set

    var spec: String = ""
        private set

    var defectType: DefectType? = null
        private set

    var controlZone: ControlZone? = null
        private set

    var repairDescription: String = ""
        private set

    var defectMoment: NumberDetailDefectMoment = NumberDetailDefectMoment.NONE
        private set

    var steal: String = ""
        private set

    var owner: String = ""
        private set

    var defectDescription: String = ""
        private set

    var defectLength: String = ""
        private set

    var defectDepth: String = ""
        private set

    var defectDiameter: String = ""
        private set

    var method: String = ""
        private set

    var detector: Detector? = null
        private set

    var defectSize: String = ""
        private set

    var defectInfo: String = ""
        private set

    fun controlDate(controlDate: Date) = apply { this.controlDate = controlDate }

    fun factory(factory: Factory?) = apply { this.factory = factory }

    fun factoryYear(factoryYear: FactoryYear?) = apply { this.factoryYear = factoryYear }

    fun isNewDetail(isNewDetail: Boolean) = apply { this.isNewDetail = isNewDetail }

    fun number(number: String) = apply { this.number = number }

    fun isFromServicePoint(isFromServicePoint: Boolean) = apply {
        if (!isFromServicePoint) {
            servicePoint = ServicePoint.default()
            noRejectionCriteria = false
        }
        this.isFromServicePoint = isFromServicePoint
    }

    fun servicePointCompany(companyEnum: CompanyEnum) =
        apply { servicePoint = servicePoint.copy(companyEnum = companyEnum) }

    fun servicePointFault(faultEnum: FaultEnum) =
        apply { servicePoint = servicePoint.copy(faultEnum = faultEnum) }

    fun noRejectionCriteria(noRejectionCriteria: Boolean) =
        apply { this.noRejectionCriteria = noRejectionCriteria }

    fun controlResult(controlResult: ControlResult?) = apply { this.controlResult = controlResult }

    fun spec(spec: String) = apply { this.spec = spec }

    fun defectType(defectType: DefectType?) = apply { this.defectType = defectType }

    fun controlZone(controlZone: ControlZone?) = apply { this.controlZone = controlZone }

    fun repairDescription(description: String) = apply { this.repairDescription = description }

    fun defectMoment(defectMoment: NumberDetailDefectMoment) = apply { this.defectMoment = defectMoment }

    fun steal(steal: String) = apply { this.steal = steal }

    fun owner(owner: String) = apply { this.owner = owner }

    fun defectDescription(description: String) = apply { this.defectDescription = description }

    fun defectLength(length: String) = apply { this.defectLength = length }

    fun defectDepth(depth: String) = apply { this.defectDepth = depth }

    fun defectDiameter(diameter: String) = apply { this.defectDiameter = diameter }

    fun method(method: String) = apply {
        this.method = method
        if (detector?.methodStr != null && detector?.methodStr != method) {
            detector = null
        }
    }

    fun detector(detector: Detector) = apply { this.detector = detector }

    fun defectSize(defectSize: String) = apply { this.defectSize = defectSize }

    fun defectInfo(defectInfo: String) = apply { this.defectInfo = defectInfo }

    fun createDefectInfo() {
        var defectInfo: String
        when (controlResult?.name) {
            "ремонт" -> {
                defectInfo = "тип дефекта: \"${defectType?.toString() ?: ""}\"; зона выявления: \"${controlZone?.toString() ?: ""}\";" +
                        "\"описание дефекта: \"${repairDescription}\";"
            }
            "брак" -> {
                defectInfo = "обнаружен: "
                when (defectMoment) {
                    NumberDetailDefectMoment.DEFECATION -> {
                        defectInfo += "\"при дефектации\"; тип дефекта: \"${defectType.toString()}\"; "
                        if (controlZone != null) {
                            defectInfo += "зона выявления: \"${controlZone.toString()}\"; "
                        }
                        if (defectDescription.isNotBlank()) {
                            defectInfo += "описание: \"${defectDescription}\"; "
                        }
                        defectInfo += "сталь: \"${steal}\"; собственник \"${owner}\"; "
                    }
                    NumberDetailDefectMoment.VISUAL -> {
                        defectInfo += "\"визуально\"; тип дефекта: \"${defectType.toString()}\"; зона выявления: " +
                                "\"${controlZone.toString()}\""
                        if (defectLength.isNotBlank()) {
                            defectInfo += "L=${defectLength} мм; "
                        }
                        if (defectDepth.isNotBlank()) {
                            defectInfo += "H=${defectDepth} мм; "
                        }
                        if (defectDiameter.isNotBlank()) {
                            defectInfo += "D=${defectDiameter} мм; "
                        }
                        defectInfo += "сталь: \"${steal}\"; собственник \"${owner}\"; "
                    }
                    NumberDetailDefectMoment.NDT -> {
                        defectInfo += "\"средствами НК\"; вид (метод): \"${method}\"; тип дефекта: \"${defectType.toString()}\"; "
                        if (defectSize.isNotBlank()) {
                            defectInfo += "размер (трещины), мм: \"${defectSize}\"; "
                        }
                        defectInfo += "тип дефектоскопа: \"${detector.toString()}\"; зона выявления: \"${controlZone.toString()}\"; " +
                                "сталь: \"${steal}\"; собственник \"${owner}\"; "
                    }
                    else -> defectInfo = ""
                }
            }
            else -> {
                defectInfo = "Годная деталь"
            }
        }
        this.defectInfo = defectInfo
    }

    abstract fun validate(): Pair<Boolean, List<InvalidField>>

    abstract fun toListItem(id: Int): BaseNumberDetailListItem
}