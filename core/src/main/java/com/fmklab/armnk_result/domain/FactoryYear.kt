package com.fmklab.armnk_result.domain

data class FactoryYear (
    val value: Int
) {

    companion object {
        fun default(): FactoryYear = FactoryYear(-1)
    }

    override fun toString(): String {
        return if (value == -1) {
            "Нет"
        } else {
            value.toString()
        }
    }
}