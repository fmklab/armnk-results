package com.fmklab.armnk_result.domain

import java.util.*

class NumberDetailListItem(
    id: Int = 0,
    partId: Int,
    number: String,
    controlDate: Date,
    factoryName: String,
    factoryYear: FactoryYear,
    controlResult: String,
    defectDescription: String,
    spec: String,
    isFromServicePoint: Boolean,
    noRejectionCriteria: Boolean,
    editStatus: Boolean,
    createDate: Date,
    isMobileApp: Boolean
) : BaseNumberDetailListItem(
    id,
    partId,
    number,
    controlDate,
    factoryName,
    factoryYear,
    controlResult,
    defectDescription,
    spec,
    isFromServicePoint,
    noRejectionCriteria,
    editStatus,
    createDate,
    isMobileApp
)