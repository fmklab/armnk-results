package com.fmklab.armnk_result.domain

sealed class Error {

    object NotFoundRemote : Error()
    object NoInternetConnection : Error()
    object BadRequest : Error()
    object SocketTimeout : Error()
    data class Unknown(val message: String) : Error()
    object NotFoundSharedPreferences : Error()
}