package com.fmklab.armnk_result.domain

data class Detector (
    val id: Int,
    val name: String,
    val code: String,
    val methodStr: String
) {

    companion object {
        fun default(name: String, methodStr: String) = Detector(0, name, "", methodStr)
    }

    override fun toString(): String {
        return if (!code.isBlank()) {
            "$name (зав. № $code)"
        } else {
            name
        }
    }
}