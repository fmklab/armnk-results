package com.fmklab.armnk_result.domain
import java.util.*
import kotlin.collections.ArrayList

class OuterRing(id: Int, partId: Int) : Detail(id, partId) {

    companion object {
        fun default(partId: Int) = OuterRing(0, partId)
    }

    var controlDate: Date = Date()
        private set

    var method: String = ""
        private set

    var detector: Detector? = null
        private set

    var factory: Factory? = null
        private set

    var factoryYear: FactoryYear? = null
        private set

    var number: String = ""
        private set

    var controlResult: ControlResult? = null
        private set

    var defectMoment: DefectMoment = DefectMoment.NONE
        private set

    var defectType: DefectType? = null
        private set

    var controlZone: ControlZone? = null
        private set

    var defectDescription: String = ""
        private set

    var defectInfo: String = ""
        private set

    var spec: String = ""
        private set

    fun controlDate(value: Date) = apply { controlDate = value }

    fun method(value: String) = apply {
        method = value
        if (detector?.methodStr != null && detector?.methodStr != value) {
            detector = null
        }
    }

    fun detector(value: Detector?) = apply { detector = value }

    fun factory(value: Factory?) = apply { factory = value }

    fun factoryYear(value: FactoryYear?) = apply { factoryYear = value }

    fun number(value: String) = apply { number = value }

    fun controlResult(value: ControlResult?) = apply { controlResult = value }

    fun defectMoment(value: DefectMoment) = apply { defectMoment = value }

    fun defectType(value: DefectType?) = apply { defectType = value }

    fun controlZone(value: ControlZone?) = apply { controlZone = value }

    fun defectDescription(value: String) = apply { defectDescription = value }

    fun spec(value: String) = apply { spec = value }

    fun defectInfo(value: String) = apply { defectInfo = value }

    fun createDefectInfo() {
        var defectInfo: String
        when (controlResult?.name) {
            "брак" -> {
                defectInfo = "обнаружен: "
                when (defectMoment) {
                    DefectMoment.DEFECATION -> {
                        defectInfo += "\"при дефектации\"; "
                    }
                    DefectMoment.VISUAL -> {
                        defectInfo += "\"при ВО при НК\"; "
                    }
                    DefectMoment.NDT -> {
                        defectInfo += "\"средствами НК\"; "
                    }
                    else -> defectInfo = ""
                }
                defectInfo += "тип дефекта: \"${defectType.toString()}\"; " +
                        "зона обнаружения: \"${controlZone.toString()}\"; "
                if (defectDescription.isNotBlank()) {
                    defectInfo += "описание: \"${defectDescription}\""
                }
            }
            else -> defectInfo = "Годная деталь"
        }
        this.defectInfo = defectInfo
    }

    fun validate(): Pair<Boolean, List<InvalidField>> {
        val invalidFields = ArrayList<InvalidField>()
        var isValid = true
        if (method.isBlank()) {
            invalidFields.add(InvalidField.Method("Поле не должно быть пустым"))
            isValid = false
        }
        if (detector == null) {
            invalidFields.add(InvalidField.Detector("Поле не должно быть пустым"))
            isValid = false
        }
        if (factory == null) {
            invalidFields.add(InvalidField.Factory("Поле не должно быть пустым"))
            isValid = false
        }
        if (factoryYear == null) {
            invalidFields.add(InvalidField.FactoryYear("Поле не должно быть пустым"))
            isValid = false
        }
        if (number.isBlank()) {
            invalidFields.add(InvalidField.Number("Поле не должно быть пустым"))
            isValid = false
        }
        if (controlResult == null) {
            invalidFields.add(InvalidField.ControlResult("Не выбран результат контроля"))
            isValid = false
        }
        if (controlResult != null && controlResult!!.name == "брак") {
            if (defectMoment == DefectMoment.NONE) {
                invalidFields.add(InvalidField.DefectMoment("Не выбран момент браковки детали"))
                isValid = false
            } else {
                if (defectType == null) {
                    invalidFields.add(InvalidField.DefectType("Поле не должно быть пустым"))
                    isValid = false
                }
                if (controlZone == null) {
                    invalidFields.add(InvalidField.ControlZone("Поле не должно быть пустым"))
                    isValid = false
                }
            }
        }

        return Pair(isValid, invalidFields)
    }

    fun toListItem(id: Int): OuterRingListItem {
        return OuterRingListItem(
            id,
            partId,
            controlDate,
            method,
            factory.toString(),
            factoryYear!!,
            number,
            controlResult!!.name,
            defectInfo,
            spec,
            true,
            detector.toString(),
            Date(),
            true
        )
    }

    sealed class InvalidField {

        data class Method(val reason: String) : InvalidField()
        data class Detector(val reason: String) : InvalidField()
        data class Factory(val reason: String) : InvalidField()
        data class FactoryYear(val reason: String) : InvalidField()
        data class Number(val reason: String) : InvalidField()
        data class ControlResult(val reason: String) : InvalidField()
        data class DefectMoment(val reason: String) : InvalidField()
        data class DefectType(val reason: String) : InvalidField()
        data class ControlZone(val reason: String) : InvalidField()
    }
}








































