package com.fmklab.armnk_result.domain

enum class DefectMoment(val value: Int) {

    NONE(0),
    VISUAL(1),
    NDT(2),
    DEFECATION(3)
}