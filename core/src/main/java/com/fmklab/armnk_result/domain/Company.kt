package com.fmklab.armnk_result.domain

data class Company (
    val id: Int = 0,
    val name: String
)