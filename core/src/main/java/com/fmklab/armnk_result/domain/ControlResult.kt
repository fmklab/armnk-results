package com.fmklab.armnk_result.domain

data class ControlResult (
    val id: Int,
    var name: String
) {

    companion object {
        fun default(id: Int = 0): ControlResult = ControlResult(id, "")
    }
}