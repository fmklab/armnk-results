package com.fmklab.armnk_result.domain

data class UserAxleModuleSettings (
    val isAR4Selected: Boolean
)