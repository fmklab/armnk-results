package com.fmklab.armnk_result.domain

import java.util.*

abstract class BaseNumberDetailListItem(
    id: Int = 0,
    partId: Int,
    val number: String,
    var controlDate: Date,
    var factoryName: String,
    var factoryYear: FactoryYear,
    var controlResult: String,
    var defectDescription: String,
    var spec: String,
    var isFromServicePoint: Boolean,
    var noRejectionCriteria: Boolean,
    var editStatus: Boolean,
    var createDate: Date,
    var isMobileApp: Boolean
) : Detail(id, partId)