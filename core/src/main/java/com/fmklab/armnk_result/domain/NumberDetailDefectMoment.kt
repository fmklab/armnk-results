package com.fmklab.armnk_result.domain

enum class NumberDetailDefectMoment(val value: Int) {

    NONE(1),
    VISUAL(1),
    NDT(2),
    DEFECATION(3)
}