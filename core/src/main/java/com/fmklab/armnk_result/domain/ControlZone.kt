package com.fmklab.armnk_result.domain

data class ControlZone (
    val id: Int = 0,
    val name: String,
    val methodStr: String?
) {

    companion object {
        fun default(id: Int) = ControlZone(id, "", null)
    }

    override fun toString(): String {
        return name
    }
}