package com.fmklab.armnk_result.domain

data class BogieModel(
    val id: Int,
    val name: String
) {

    companion object {
        fun default(id: Int) = BogieModel(id, "")
    }

    override fun toString(): String {
        return name
    }
}