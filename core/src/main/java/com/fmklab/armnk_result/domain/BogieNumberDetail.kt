package com.fmklab.armnk_result.domain

import java.util.*
import kotlin.collections.ArrayList

class BogieNumberDetail(id: Int, partId: Int) : BaseNumberDetail(id, partId) {

    companion object {
        fun default(partId: Int) = BogieNumberDetail(0, partId)
    }

    var bogieModel: BogieModel? = null
        private set

    fun bogieModel(value: BogieModel?) = apply { bogieModel = value }

    override fun validate(): Pair<Boolean, List<InvalidField>> {
        val invalidFields = ArrayList<InvalidFieldImpl>()
        var isValid = true
        if (factory == null) {
            invalidFields.add(InvalidFieldImpl.Factory("Поле не должно быть пустым"))
            isValid = false
        }
        if (factoryYear == null) {
            invalidFields.add(InvalidFieldImpl.FactoryYear("Поле не должно быть пустым"))
            isValid = false
        }
        if (number.isBlank()) {
            invalidFields.add(InvalidFieldImpl.Number("Поле не должно быть пустым"))
            isValid = false
        }
        if (bogieModel == null) {
            invalidFields.add(InvalidFieldImpl.BogieModel("Поле не должно быть пустым"))
            isValid = false
        }
        if (controlResult == null) {
            invalidFields.add(InvalidFieldImpl.Result("Не выбран результат контроля"))
            isValid = false
        }
        if (controlResult != null && controlResult!!.name == "брак") {
            if (defectMoment == NumberDetailDefectMoment.NONE) {
                invalidFields.add(InvalidFieldImpl.DefectMoment("Не выбран момент браковки детали"))
                isValid = false
            } else {
                if (defectType == null) {
                    invalidFields.add(InvalidFieldImpl.DefectType("Поле не должно быть пустым"))
                    isValid = false
                }
                if (defectType?.needControlZone == true && controlZone == null) {
                    invalidFields.add(InvalidFieldImpl.ControlZone("Поле не должно быть пустым"))
                    isValid = false
                }
                if (steal.isBlank()) {
                    invalidFields.add(InvalidFieldImpl.Steal("Поле не должно быть пустым"))
                    isValid = false
                }
                if (owner.isBlank()) {
                    invalidFields.add(InvalidFieldImpl.Owner("Поле не должно быть пустым"))
                    isValid = false
                }
            }
            if (defectMoment == NumberDetailDefectMoment.NDT) {
                if (method.isBlank()) {
                    invalidFields.add(InvalidFieldImpl.Method("Поле не должно быть пустым"))
                    isValid = false
                }
                if (detector == null) {
                    invalidFields.add(InvalidFieldImpl.Detector("Поле не должно быть пустым"))
                    isValid = false
                }
            }
        } else if (controlResult != null && controlResult!!.name == "ремонт") {
            if (defectType == null) {
                invalidFields.add(InvalidFieldImpl.DefectType("Поле не должно быть пустым"))
                isValid = false
            }
            if (controlZone == null) {
                invalidFields.add(InvalidFieldImpl.ControlZone("Поле не должно быть пустым"))
                isValid = false
            }
            if (repairDescription.length < 10) {
                invalidFields.add(InvalidFieldImpl.RepairDescription("Минимум 10 символов"))
                isValid = false
            }
        }

        return Pair(isValid, invalidFields)
    }

    override fun toListItem(id: Int): BaseNumberDetailListItem {
        return BogieNumberDetailListItem(
            id,
            partId,
            number,
            controlDate,
            factory!!.toString(),
            factoryYear!!,
            controlResult!!.name,
            defectInfo,
            spec,
            isFromServicePoint,
            noRejectionCriteria,
            true,
            Date(),
            true,
            bogieModel!!.toString()
        )
    }

    sealed class InvalidFieldImpl : InvalidField {

        data class Factory(val reason: String) : InvalidFieldImpl()
        data class FactoryYear(val reason: String) : InvalidFieldImpl()
        data class Number(val reason: String) : InvalidFieldImpl()
        data class Result(val reason: String) : InvalidFieldImpl()
        data class DefectMoment(val reason: String) : InvalidFieldImpl()
        data class DefectType(val reason: String) : InvalidFieldImpl()
        data class ControlZone(val reason: String) : InvalidFieldImpl()
        data class Steal(val reason: String) : InvalidFieldImpl()
        data class Owner(val reason: String) : InvalidFieldImpl()
        data class Method(val reason: String) : InvalidFieldImpl()
        data class Detector(val reason: String) : InvalidFieldImpl()
        data class RepairDescription(val reason: String) : InvalidFieldImpl()
        data class BogieModel(val reason: String) : InvalidFieldImpl()
    }
}