package com.fmklab.armnk_result.domain

data class ControlDetail(
    val id: Int = 0,
    val name: String,
    val image: String
) {

    companion object {
        val NONE = ControlDetail(0, "", "")
    }
}