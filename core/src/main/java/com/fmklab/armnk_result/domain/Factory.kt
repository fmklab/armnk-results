package com.fmklab.armnk_result.domain

data class Factory (
    val id: Int,
    val code: String
) {

    companion object {
        fun default(id: Int = 0): Factory = Factory(id, "")
    }

    override fun toString(): String {
        return code
    }
}