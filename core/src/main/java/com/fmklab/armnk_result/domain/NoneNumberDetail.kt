package com.fmklab.armnk_result.domain

import java.util.*
import kotlin.collections.ArrayList

class NoneNumberDetail(id: Int, partId: Int) : Detail(id, partId) {

    companion object {
        fun default(partId: Int): NoneNumberDetail {
            return NoneNumberDetail(0, partId)
        }
    }

    var controlDate: Date = Date()

    var method: String = ""
        private set

    var controlZone: ControlZone? = null
        private set

    var detector: Detector? = null
        private set

    var allChecked: Int = 0
        private set

    var defecationChecked: Int = 0
        private set

    var visualChecked: Int = 0
        private set

    var ndtChecked: Int = 0
        private set

    var spec: String = ""
        private set

    var editStatus: Boolean = true
        private set

    var createDate: Date = Date()
        private set

    var isMobileApp: Boolean = true
        private set

    fun controlDate(date: Date) = apply { controlDate = date }

    fun method(method: String) = apply {
        this.method = method
        if (controlZone?.methodStr != null && controlZone?.methodStr != method) {
            controlZone = null
        }
        if (detector?.methodStr != null && detector?.methodStr != method) {
            detector = null
        }
    }

    fun controlZone(controlZone: ControlZone?) = apply {
        if (controlZone?.methodStr != null && controlZone.methodStr != method && method != "") {
            this.controlZone = null
        } else {
            this.controlZone = controlZone
        }
    }

    fun detector(detector: Detector?) = apply {
        if (detector?.methodStr != null && detector.methodStr != method && method != "") {
            this.detector = null
        } else {
            this.detector = detector
        }
    }

    fun allChecked(allChecked: Int) = apply { this.allChecked = allChecked }

    fun defecationChecked(defecationChecked: Int) =
        apply { this.defecationChecked = defecationChecked }

    fun visualChecked(visualChecked: Int) = apply { this.visualChecked = visualChecked }

    fun ndtChecked(ndtChecked: Int) = apply { this.ndtChecked = ndtChecked }

    fun spec(spec: String) = apply { this.spec = spec }

    fun editStatus(editStatus: Boolean) = apply { this.editStatus = editStatus }

    fun createDate(createDate: Date) = apply { this.createDate = createDate }

    fun isMobileApp(isMobileApp: Boolean) = apply { this.isMobileApp = isMobileApp }

    fun validate(): Pair<Boolean, List<InvalidField>> {
        val invalidFields = ArrayList<InvalidField>()
        var isValid = true
        if (method.isBlank()) {
            invalidFields.add(InvalidField.Method("Поле не должно быть пустым"))
            isValid = false
        }
        if (controlZone == null) {
            invalidFields.add(InvalidField.ControlZone("Поле не должно быть пустым"))
            isValid = false
        }
        if (detector == null) {
            invalidFields.add(InvalidField.Detector("Поле не должно быть пустым"))
            isValid = false
        }
        if (allChecked == 0) {
            invalidFields.add(InvalidField.AllChecked("Кол-во деталей не задано"))
            isValid = false
        }
        if (allChecked < defecationChecked + visualChecked + ndtChecked) {
            invalidFields.add(InvalidField.AllChecked("Сумма ВО и НК привышает кол-во проверенных деталей"))
            isValid = false
        }

        return Pair(isValid, invalidFields)
    }

    fun copyWithNewId(id: Int): NoneNumberDetail {
        val newDetail = NoneNumberDetail(id, partId)
        newDetail.controlDate = this.controlDate
        newDetail.method = this.method
        newDetail.controlZone = this.controlZone
        newDetail.detector = this.detector
        newDetail.allChecked = this.allChecked
        newDetail.defecationChecked = this.defecationChecked
        newDetail.visualChecked = this.visualChecked
        newDetail.ndtChecked = this.ndtChecked
        newDetail.spec = this.spec
        newDetail.editStatus = this.editStatus

        return newDetail
    }

    sealed class InvalidField {

        data class Method(val reason: String) : InvalidField()
        data class ControlZone(val reason: String) : InvalidField()
        data class Detector(val reason: String) : InvalidField()
        data class AllChecked(val reason: String) : InvalidField()
    }
}







































