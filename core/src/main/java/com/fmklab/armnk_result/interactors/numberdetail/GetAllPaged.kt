package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class GetAllPaged(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke(partId: Int, page: Int) = numberDetailRepository.getAllPaged(partId, page)
}