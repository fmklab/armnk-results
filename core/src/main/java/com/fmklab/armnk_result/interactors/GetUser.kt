package com.fmklab.armnk_result.interactors

import com.fmklab.armnk_result.data.UserRepository

class GetUser(private val userRepository: UserRepository) {

    operator fun invoke() = userRepository.getUser()
}