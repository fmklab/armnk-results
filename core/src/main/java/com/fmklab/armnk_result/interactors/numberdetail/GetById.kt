package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class GetById(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke(id: Int) = numberDetailRepository.getById(id)
}