package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class GetRepairDefectTypes(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke(partId: Int) = numberDetailRepository.getRepairDefectTypes(partId)
}