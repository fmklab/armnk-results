package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository
import com.fmklab.armnk_result.domain.DefectMoment

class GetBogieControlZones(private val repository: NumberDetailRepository) {

    operator fun invoke(partId: Int, defectMoment: DefectMoment, bogieModelId: Int) =
        repository.getBogieControlZones(partId, defectMoment, bogieModelId)
}