package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository

class GetById(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke(id: Int) = noneNumberDetailRepository.getById(id)
}