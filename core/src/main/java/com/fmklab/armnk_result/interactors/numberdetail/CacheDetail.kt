package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository
import com.fmklab.armnk_result.domain.NumberDetail

class CacheDetail(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke(numberDetail: NumberDetail) = numberDetailRepository.cacheDetail(numberDetail)
}