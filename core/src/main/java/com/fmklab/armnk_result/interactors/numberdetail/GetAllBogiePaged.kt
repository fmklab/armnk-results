package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class GetAllBogiePaged(private val repository: NumberDetailRepository) {

    operator fun invoke(partId: Int, page: Int) = repository.getAllBogiePaged(partId, page)
}