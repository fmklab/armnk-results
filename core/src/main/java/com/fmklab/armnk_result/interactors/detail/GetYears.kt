package com.fmklab.armnk_result.interactors.detail

import com.fmklab.armnk_result.data.DetailRepository

class GetYears(private val detailRepository: DetailRepository) {

    operator fun invoke(factoryId: Int) = detailRepository.getYears(factoryId)
}