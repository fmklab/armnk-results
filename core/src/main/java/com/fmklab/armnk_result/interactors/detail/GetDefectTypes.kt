package com.fmklab.armnk_result.interactors.detail

import com.fmklab.armnk_result.data.DetailRepository
import com.fmklab.armnk_result.domain.DefectMoment

class GetDefectTypes(private val detailRepository: DetailRepository) {

    operator fun invoke(partId: Int, defectMoment: DefectMoment) =
        detailRepository.getDefectTypes(partId, defectMoment)
}