package com.fmklab.armnk_result.interactors.outerring

import com.fmklab.armnk_result.data.OuterRingRepository

class SearchOuterRing(private val outerRingRepository: OuterRingRepository) {

    operator fun invoke(partId: Int, number: String) = outerRingRepository.search(partId, number)
}