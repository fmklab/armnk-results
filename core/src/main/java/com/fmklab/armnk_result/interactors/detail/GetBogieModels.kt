package com.fmklab.armnk_result.interactors.detail

import com.fmklab.armnk_result.data.DetailRepository

class GetBogieModels(private val repository: DetailRepository) {

    operator fun invoke(partId: Int) = repository.getBogieModels(partId)
}