package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository
import com.fmklab.armnk_result.domain.NoneNumberDetail
import com.fmklab.armnk_result.domain.User

class Insert(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke(noneNumberDetail: NoneNumberDetail) =
        noneNumberDetailRepository.insert(noneNumberDetail)
}