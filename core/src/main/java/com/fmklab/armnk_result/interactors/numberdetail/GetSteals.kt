package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class GetSteals(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke() = numberDetailRepository.getSteals()
}