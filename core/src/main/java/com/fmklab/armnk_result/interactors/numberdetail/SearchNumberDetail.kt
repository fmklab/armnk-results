package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class SearchNumberDetail(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke(partId: Int, number: String) = numberDetailRepository.search(partId, number)
}