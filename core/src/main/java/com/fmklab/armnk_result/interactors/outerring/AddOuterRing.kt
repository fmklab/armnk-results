package com.fmklab.armnk_result.interactors.outerring

import com.fmklab.armnk_result.data.OuterRingRepository
import com.fmklab.armnk_result.domain.OuterRing

class AddOuterRing(private val outerRingRepository: OuterRingRepository) {

    operator fun invoke(outerRing: OuterRing) = outerRingRepository.add(outerRing)
}