package com.fmklab.armnk_result.interactors.outerring

import com.fmklab.armnk_result.data.OuterRingRepository

class GetAllOuterRings(private val outerRingRepository: OuterRingRepository) {

    operator fun invoke(partId: Int, page: Int) = outerRingRepository.getAll(partId, page)
}