package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository

class ClearPreferencesCache(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke() = noneNumberDetailRepository.clearPreferencesCache()
}