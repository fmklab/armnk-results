package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository

class GetControlZonesByMethod(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke(partId: Int, method: String) =
        noneNumberDetailRepository.getControlZonesByMethod(partId, method)
}