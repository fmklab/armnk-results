package com.fmklab.armnk_result.interactors.outerring

import com.fmklab.armnk_result.data.OuterRingDataSource
import com.fmklab.armnk_result.data.OuterRingRepository

class ClearPreferencesCache(private val outerRingRepository: OuterRingRepository) {

    operator fun invoke() = outerRingRepository.clearPreferencesCache()
}