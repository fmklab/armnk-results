package com.fmklab.armnk_result.interactors

import com.fmklab.armnk_result.data.UserRepository
import com.fmklab.armnk_result.domain.User

class SetUser(private val userRepository: UserRepository) {

    operator fun invoke(user: User) = userRepository.setUser(user)
}