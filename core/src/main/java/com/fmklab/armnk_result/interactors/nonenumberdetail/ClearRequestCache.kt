package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository

class ClearRequestCache(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke() = noneNumberDetailRepository.clearRequestCache()
}