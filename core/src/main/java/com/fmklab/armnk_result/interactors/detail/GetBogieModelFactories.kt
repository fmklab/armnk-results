package com.fmklab.armnk_result.interactors.detail

import com.fmklab.armnk_result.data.DetailRepository

class GetBogieModelFactories(private val repository: DetailRepository) {

    operator fun invoke(partId: Int, bogieModelId: Int) =
        repository.getBogieModelFactories(partId, bogieModelId)
}