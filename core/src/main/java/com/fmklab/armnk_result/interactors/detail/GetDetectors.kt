package com.fmklab.armnk_result.interactors.detail

import com.fmklab.armnk_result.data.DetailRepository

class GetDetectors(private val detailRepository: DetailRepository) {

    operator fun invoke(method: String) = detailRepository.getDetectors(method)
}