package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class GetBogieById(private val repository: NumberDetailRepository) {

    operator fun invoke(id: Int) = repository.getBogieById(id)
}