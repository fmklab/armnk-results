package com.fmklab.armnk_result.interactors.detail

import com.fmklab.armnk_result.data.DetailRepository

class GetFactories(private val detailRepository: DetailRepository) {

    operator fun invoke(partId: Int) = detailRepository.getFactories(partId)
}