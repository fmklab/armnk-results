package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository

class GetAllPaged(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke(partId: Int, page: Int) =
        noneNumberDetailRepository.getAllPaged(partId, page)
}