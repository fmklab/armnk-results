package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class SearchBogieNumberDetail(private val repository: NumberDetailRepository) {

    operator fun invoke(partId: Int, number: String) = repository.searchBogie(partId, number)
}