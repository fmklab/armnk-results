package com.fmklab.armnk_result.interactors

import com.fmklab.armnk_result.data.ControlDetailRepository

class GetControlDetails(private val controlDetailRepository: ControlDetailRepository) {

    operator fun invoke() = controlDetailRepository.getAll()
}