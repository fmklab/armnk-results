package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository
import com.fmklab.armnk_result.domain.BogieNumberDetail

class CacheBogieDetail(private val repository: NumberDetailRepository) {

    operator fun invoke(numberDetail: BogieNumberDetail) = repository.cacheBogieDetail(numberDetail)
}