package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository

class ClearBogiePreferenceCache(private val repository: NumberDetailRepository) {

    operator fun invoke() = repository.clearBogiePreferenceCache()
}