package com.fmklab.armnk_result.interactors.nonenumberdetail

import com.fmklab.armnk_result.data.NoneNumberDetailRepository
import com.fmklab.armnk_result.domain.NoneNumberDetail

class CacheDetail(private val noneNumberDetailRepository: NoneNumberDetailRepository) {

    operator fun invoke(noneNumberDetail: NoneNumberDetail) =
        noneNumberDetailRepository.cacheDetail(noneNumberDetail)
}