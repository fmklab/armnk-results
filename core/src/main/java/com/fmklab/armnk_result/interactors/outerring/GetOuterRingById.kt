package com.fmklab.armnk_result.interactors.outerring

import com.fmklab.armnk_result.data.OuterRingRepository

class GetOuterRingById(private val outerRingRepository: OuterRingRepository) {

    operator fun invoke(id: Int) = outerRingRepository.getById(id)
}