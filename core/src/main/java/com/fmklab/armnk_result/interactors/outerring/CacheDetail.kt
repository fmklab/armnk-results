package com.fmklab.armnk_result.interactors.outerring

import com.fmklab.armnk_result.data.OuterRingRepository
import com.fmklab.armnk_result.domain.OuterRing

class CacheDetail(private val outerRingRepository: OuterRingRepository) {

    operator fun invoke(outerRing: OuterRing) = outerRingRepository.cacheDetail(outerRing)
}