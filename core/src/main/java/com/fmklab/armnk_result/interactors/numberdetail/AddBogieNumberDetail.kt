package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository
import com.fmklab.armnk_result.domain.BogieNumberDetail

class AddBogieNumberDetail(private val repository: NumberDetailRepository) {

    operator fun invoke(bogieNumberDetail: BogieNumberDetail) = repository.addBogie(bogieNumberDetail)
}