package com.fmklab.armnk_result.interactors.numberdetail

import com.fmklab.armnk_result.data.NumberDetailRepository
import com.fmklab.armnk_result.domain.NumberDetail

class AddNumberDetail(private val numberDetailRepository: NumberDetailRepository) {

    operator fun invoke(numberDetail: NumberDetail) = numberDetailRepository.add(numberDetail)
}